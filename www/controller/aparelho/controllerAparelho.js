var ControllerAparelho = function () {

    var daoAparelho = new DaoAparelho();
    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarAparelho", function( event ) {
            menu.initialize();
            $('#formAdicionarAparelho').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarAparelho", function( event ) {
            menu.initialize();
            $('#formEditarAparelho').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idAparelho = event.delegateTarget.activeElement.attributes['idaparelho'].value;
            editar(idAparelho);
        });

        $( document ).on( "pageinit", "#aparelhos", function( event ) {
            menu.initialize();
            listarAparelhos();
        });
    }

    function listarAparelhos() {
        daoAparelho.buscarAll(listViewAparelho);
    }

    function listViewAparelho(json) {
        $("#listviewAparelho").empty();
        jQuery.each(json, function(i, item) {

            var id = item.id;
            var descricao = item.descricao;
            var codigo = item.codigo;
            var fabricante = item.fabricante;
            var precoAVista = item.precoAVista;
            var precoBase24 = item.precoBase24;

            var innerA = '  <li>' +
                '               <a href="view/aparelho/plano/aparelhosPlano.html" data-ajax="true" idAparelho="'+id+'" data-role="button">'+ descricao+
                '                   <p><small>'+codigo+'</small></p>' +
                '                   <p><small>'+fabricante+'</small></p>' +
                '                   <p><small>Preço a Vista R$ '+precoAVista+'</small></p>' +
                '                   <p><small>Preço Base 24X R$ '+precoBase24+'</small></p>' +
                '               </a>'+
                '               <a href="view/aparelho/editarAparelho.html" data-ajax="true" idAparelho="'+id+'">'+descricao+'</a>' +
                '           </li>';
            $("#listviewAparelho").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoAparelho.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoAparelho.create();
    }

    function dropdb() {
        daoAparelho.drop();
    }

    function save() {
        daoAparelho.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idAparelho) {
        daoAparelho.buscaById(idAparelho, popularDadosTela);
    }

    function buscarDadosTela() {

        var idAparelho = getString('idAparelho');
        var descricao = getString("descricao");
        var tipo = getString('tipo');
        var fabricante = getString('fabricante');
        var codigo = getString('codigo');
        var gama = getString('gama');
        var tecnologia = getString('tecnologia');
        var tipoSimCard = getString('tipoSimCard');
        var precoAVista = getString('precoAVista');
        var descontoClaroFacilNascional = getString('descontoClaroFacilNascional');
        var subsidio100pts = getString('subsidio100pts');
        var subsidio140pts = getString('subsidio140pts');
        var claroTotalAACE = getString('claroTotalAACE');
        var precoBase24 = getString('precoBase24');

        var aparelho = new Aparelho();
        aparelho.setId(idAparelho);
        aparelho.setTipo(tipo);
        aparelho.setFabricante(fabricante);
        aparelho.setCodigo(codigo);
        aparelho.setGama(gama);
        aparelho.setTecnologia(tecnologia);
        aparelho.setTipoSimCard(tipoSimCard);
        aparelho.setPrecoAVista(precoAVista);
        aparelho.setDescontoClaroFacilNascional(descontoClaroFacilNascional);
        aparelho.setSubsidio100pts(subsidio100pts);
        aparelho.setSubsidio140pts(subsidio140pts);
        aparelho.setClaroTotalAACE(claroTotalAACE);
        aparelho.setPrecoBase24(precoBase24);
        aparelho.setDescricao(descricao);

        return aparelho;
    }

    function popularDadosTela(json) {
        var aparelhoJson = json[0];

        var idAparelho = aparelhoJson.id;
        var descricao = aparelhoJson.descricao;
        var tipo = aparelhoJson.tipo;
        var fabricante = aparelhoJson.fabricante;
        var codigo = aparelhoJson.codigo;
        var gama = aparelhoJson.gama;
        var tecnologia = aparelhoJson.tecnologia;
        var tipoSimCard = aparelhoJson.tipoSimCard;
        var precoAVista = aparelhoJson.precoAVista;
        var descontoClaroFacilNascional = aparelhoJson.descontoClaroFacilNascional;
        var subsidio100pts = aparelhoJson.subsidio100pts;
        var subsidio140pts = aparelhoJson.subsidio140pts;
        var claroTotalAACE = aparelhoJson.claroTotalAACE;
        var precoBase24 = aparelhoJson.precoBase24;

        setString('idAparelho', idAparelho);
        setString('descricao', descricao);
        setString('tipo', tipo);
        setCombo('fabricante', fabricante);
        setString('codigo', codigo);
        setCombo('gama', gama);
        setCombo('tecnologia', tecnologia);
        setCombo('tipoSimCard', tipoSimCard);
        setString('precoAVista', precoAVista);
        setString('descontoClaroFacilNascional', descontoClaroFacilNascional);
        setString('subsidio100pts', subsidio100pts);
        setString('subsidio140pts', subsidio140pts);
        setString('claroTotalAACE', claroTotalAACE);
        setString('precoBase24', precoBase24);
    }

    function depoisSalvar(retorno) {
        console.log('save aparelho '+retorno);
        $('#nmAparelhos').click();
    }
    
    function baixar(successCallback) {
        daoAparelho.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoAparelho.enviar(successCallback);
    }
};