var ControllerAparelhoPlano = function () {

    var daoAparelhoPlano = new DaoAparelhoPlano();
    var daoAparelho = new DaoAparelho();
    var daoFranquia = new DaoFranquia();
    var daoPlano = new DaoPlano();

    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarAparelhoPlano", function( event ) {
            menu.initialize();
            preencherComboBox();
            atriburBlur();
            $('#formAdicionarAparelhoPlano').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarAparelhoPlano", function( event ) {
            menu.initialize();
            preencherComboBox();
            atriburBlur();
            $('#formEditarAparelhoPlano').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idAparelhoPlano = event.delegateTarget.activeElement.attributes['idaparelhoplano'].value;
            editar(idAparelhoPlano);
        });

        $( document ).on( "pageinit", "#aparelhosPlano", function( event ) {
            menu.initialize();
            listarAparelhosPlano();
            APARELHO = event.delegateTarget.activeElement.attributes['idaparelho'].value;
        });
    }

    function preencherComboBox() {
        daoPlano.buscarAll(function (json) {
            preencherPlano(json);
        });

        daoAparelho.buscarAll(function (json) {
           preencherComboAtributos('aparelho', json);
            setCombo('aparelho', APARELHO);
        });

    }

    function depoisPreencherPlano() {
        daoFranquia.buscarAllByPlano(getString('plano'), function (json) {
            preencherFranquia(json);
        });
    }

    function atriburBlur() {
        $('#plano').blur(function (event) {
            depoisPreencherPlano();
        });
    }

    function preencherFranquia(json) {

        $("#franquia").empty();
        $('#franquia').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#franquia').append($('<option>', {
                value: item.id,
                text : item.franquia
            }));
        });
    }

    function preencherPlano(json) {
        $("#plano").empty();
        $('#plano').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#plano').append($('<option>', {
                value: item.id,
                text : item.descricao,
                tipo: item.tipo,

            }));
        });
    }

    function listarAparelhosPlano() {
        daoAparelhoPlano.buscarAll(listViewAparelhosPlano);
    }

    function listViewAparelhosPlano(json) {
        $("#listviewAparelhoPlano").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/aparelho/plano/editarAparelhoPlano.html" data-ajax="true" idAparelhoPlano="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewAparelhoPlano").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoAparelhoPlano.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoAparelhoPlano.create();
    }

    function dropdb() {
        daoAparelhoPlano.drop();
    }

    function save() {
        daoAparelhoPlano.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idCargo) {
        daoAparelhoPlano.buscaById(idCargo, popularDadosTela);
    }

    function buscarDadosTela() {
        var idAparelhoPlano = getString('idAparelhoPlano');
        var aparelho = getString("aparelho");
        var plano = getString("plano");
        var franquia = getString("franquia");
        var pontos = getString("pontos");
        var valor = getString("valor");

        var aparelhoPlano = new AparelhoPlano();
        aparelhoPlano.setId(idAparelhoPlano);
        aparelhoPlano.setAparelho(aparelho);
        aparelhoPlano.setPlano(plano);
        aparelhoPlano.setFranquia(franquia);
        aparelhoPlano.setPontos(pontos);
        aparelhoPlano.setValor(valor);

        return aparelhoPlano;
    }

    function popularDadosTela(json) {
        json = json[0];

        var idAparelhoPlano = json.id;
        var aparelho = json.aparelho;
        var plano = json.plano;
        var franquia =  json.franquia;
        var pontos = json.pontos;
        var valor = json.valor;

        setString('idAparelhoPlano', idAparelhoPlano);
        setCombo('aparelho', aparelho);
        setCombo('plano', plano);
        setCombo('franquia', franquia);
        setString('pontos', pontos);
        setString('valor', valor);
    }

    function depoisSalvar(retorno) {
        console.log('save aparelho plano '+retorno);
        $('#nmAparelhos').click();
    }

    function baixar(successCallback) {
        daoAparelhoPlano.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoAparelhoPlano.enviar(successCallback);
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadPermissoes/',
            data: {
                cargos: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadPermissoes",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }
};