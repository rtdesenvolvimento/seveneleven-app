var App = function () {

    var controllerUsuario = new ControllerUsuario();
    var controllerCargo = new ControllerCargo();
    var controllerTipoTarefa = new ControllerTipoTarefa();
    var controllerTarefa = new ControllerTarefa();
    var controllerFormulario = new ControllerFormulario();
    var controllerPerguntaFormulario = new ControllerPerguntaFormulario();
    var controllerPerguntaFormularioOpcao = new ControllerPerguntaFormularioOpcao();
    var controllerAgenda = new ControllerAgenda();
    var controllerCliente = new ControllerCliente();
    var controllerProposta = new ControllerProposta();
    var controllerRespostaProposta = new ControllerRespostaProposta();
    var controllerRespostaPropostaOpcao = new ControllerRespostaPropostaOpcao();
    var controllerConfiguracao = new ControllerConfiguracao();
    var controllerGeoLocalizacao = new ControllerGeoLocalizacao();
    var controllerDevice = new ControllerDevice();
    var controllerDigitalizacao = new ControllerDigitalizacao();
    var controllerLogin = new ControllerLogin();
    var controllerPlano = new ControllerPlano();
    var controllerMeioDivulgacao = new ControllerMeioDivulgacao();
    var controllerChecklist = new ControllerChecklist();
    var controllerCaptacao = new ControllerCaptacao();
    var controllerAtividade = new ControllerAtividade();
    var controllerTipoCheckin = new ControllerTipoCheckin();
    var controllerRegiao = new ControllerRegiao();
    var controllerFranquia = new ControllerFranquia();
    var controllerTipoSolicitacaoProposta = new ControllerTipoSolicitacaoProposta();
    var controllerCodigoAnatel = new ControllerCodigoAnatel();
    var controllerPropostaPortabilidade = new ControllerPropostaPortabilidade();
    var controllerPropostaModulo = new ControllerPropostaModulo();
    var controllerPropostaMigracao = new ControllerPropostaMigracao();
    var controllerPropostaFranquia = new ControllerPropostaFranquia();
    var controllerPropostaEvento = new ControllerPropostaEvento();
    var controllerPlugin = new ControllerPlugin();
    var controllerAparelho = new ControllerAparelho();
    var controllerSimCard = new ControllerSimCard();
    var controllerAparelhoPlano = new ControllerAparelhoPlano();
    var controllerPropostaAparelho = new ControllerPropostaAparelho();
    var controllerClassificacaoCliente = new ControllerClassificaoCliente();
    var controllerOperadora = new ControllerOperadora();
    var controllerPassaporte = new ControllerPassaporte();
    var controllerSms = new ControllerSms();
    var controllerMaterialApoio = new ControllerMaterialApoio();

    var menu = new Menu();

    this.initialize = initialize;

    function initialize() {
        cratedb();
        eventos();
    }

    function eventos() {

        controllerTipoTarefa.initialize();
        controllerTarefa.initialize();
        controllerUsuario.initialize();
        controllerFormulario.initialize();
        controllerPerguntaFormulario.initialize();
        controllerAgenda.initialize();
        controllerCliente.initialize();
        controllerPerguntaFormularioOpcao.initialize();
        controllerCargo.initialize();
        controllerProposta.initialize();
        controllerRespostaProposta.initialize();
        controllerConfiguracao.initialize();
        controllerGeoLocalizacao.initialize();
        controllerRespostaPropostaOpcao.initialize();
        controllerDigitalizacao.initialize();
        controllerLogin.initialize();
        controllerPlano.initialize();
        controllerCaptacao.initialize();
        controllerMeioDivulgacao.initialize();
        controllerTipoCheckin.initialize();
        controllerAtividade.initialize();
        controllerChecklist.initialize();
        controllerRegiao.initialize();
        controllerFranquia.initialize();
        controllerTipoSolicitacaoProposta.initialize();
        controllerCodigoAnatel.initialize();
        controllerPropostaPortabilidade.initialize();
        controllerPropostaFranquia.initialize();
        controllerPropostaModulo.initialize();
        controllerPropostaMigracao.initialize();
        controllerPlugin.initialize();
        controllerAparelho.initialize();
        controllerSimCard.initialize();
        controllerAparelhoPlano.initialize();
        controllerPropostaAparelho.initialize();
        controllerClassificacaoCliente.initialize();
        controllerOperadora.initialize();
        controllerPassaporte.initialize();
        controllerSms.initialize();
        controllerPropostaEvento.initialize();
        controllerMaterialApoio.initialize();
        menu.initialize();

        $('#fechar_aplicativo').click(function (event) {
            controllerDevice.fecharAplicativo();
        });

        $( document ).on( "pageinit", "#home", function( event ) {
            menu.initialize();
        });

        splashscreen();
        despertarGPS();
    }

    function despertarGPS() {
        controllerGeoLocalizacao.getCurrentPosition(function (p) {}, function (e) {});
    }

    function splashscreen() {
        if (navigator.splashscreen === undefined) return;

        navigator.splashscreen.show();
        setTimeout(function () {navigator.splashscreen.hide();}, 3000);
    }

    function cratedb() {
        banco.initialize();
    }
};