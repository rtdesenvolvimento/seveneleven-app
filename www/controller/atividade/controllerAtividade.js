var ControllerAtividade = function () {
    var ID_ATIVIDADE = null;

    var daoAtividade = new DaoAtividade();
    var controllerTipoCheckin = new ControllerTipoCheckin();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerGeoLocalizacao = new ControllerGeoLocalizacao();
    var controllerCamera = new ControllerCamera();
    var controllerDigitalizacao = new ControllerDigitalizacao();

    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.checkin = checkin;
    this.checkout = checkout;
    this.checkinGeoLocalizacao = checkinGeoLocalizacao;

    var tentantivasAcessoGPS = 1;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#adicionarAtividade", function( event ) {

            menu.initialize();
            preencherComboBox();
            tentantivasAcessoGPS = 1;

            $('#formAdicionarAtividade').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#digitalizacaoCamera').click(function (event) {
                adicionarCamera();
            });
        });

        $( document ).on( "pageinit", "#editarAtividade", function( event ) {

            menu.initialize();
            preencherComboBox();

            $('#formEditarAtividade').submit(function (event) {
                event.preventDefault();
                checkoutEditar();
            });

            $('#digitalizacaoCameraCheckout').click(function (event) {
                adicionarCameraCheckout();
            });

            var idAtividade = event.delegateTarget.activeElement.attributes['idatividade'].value;
            editar(idAtividade);
        });

        $( document ).on( "pageinit", "#atividadesCheckout", function( event ) {
            menu.initialize();
            listarAtividadesCheckout();
        });

        $(document).on( "pageinit", "#relatorioCheckin", function( event ) {
            menu.initialize();
            $('.ui-table-columntoggle-btn').hide();

            $('#bntFiltroAtividades').click(function (event) {
                relatorioCheckin();
            });

            controllerTipoCheckin.buscarAll(function (json) {
                preencherComboAtributos('filterTipoCheckin', json);
            });
        });
    }

    function adicionarCamera() {

        var destinationType = Camera.DestinationType.FILE_URI;
        var sourceType = Camera.PictureSourceType.CAMERA
        var mediaType = Camera.MediaType.PICTURE;
        var allowEdit = false;

        controllerCamera.getPicture(destinationType,sourceType, mediaType, allowEdit, function (imageData) {
            adicionarDigitalizacaoLisView(imageData, '');
        }, function (error) {});

    }

    function adicionarCameraCheckout() {

        var destinationType = Camera.DestinationType.FILE_URI;
        var sourceType = Camera.PictureSourceType.CAMERA
        var mediaType = Camera.MediaType.PICTURE;
        var allowEdit = false;

        controllerCamera.getPicture(destinationType,sourceType, mediaType, allowEdit, function (imageData) {
            adicionarDigitalizacaoLisViewCheckout(imageData, '');
        }, function (error) {});
    }

    function adicionarDigitalizacaoLisViewCheckout(imageData, retorno) {

        $('#digitalizacaoCameraCheckout').hide();

        var innerA = ' ' +
            '  <div style="text-align: center;">' +
            '       <img src="'+imageData+'" name="photoscheckout[]" mensagem="'+retorno+'" id="photoscheckout" style="width: 300px;height: 300px;"> ' +
            '       <h3>'+retorno+'</h3> ' +
            '  </div>';

        $("#listViewDigitalizacaoAtividadeCheckout").append(innerA);
        $('ul').listview().listview('refresh');
    }

    function adicionarDigitalizacaoLisView(imageData, retorno) {

        $('#digitalizacaoCamera').hide();

        var innerA = ' ' +
            '  <div style="text-align: center;">' +
            '       <img src="'+imageData+'" name="photos[]" mensagem="'+retorno+'" id="" style="width: 300px;height: 300px;"> ' +
            '       <h3>'+retorno+'</h3> ' +
            '  </div>';

        $("#listViewDigitalizacaoAtividade").append(innerA);
        $('ul').listview().listview('refresh');
    }

    function limparRelatorioCheckin() {
        $("#listViewAtividades").html('');
        $('ul').listview().listview('refresh');
    }

    function relatorioCheckin() {

        var filterTipoCheckin = getString('filterTipoCheckin');
        var filterDataCheckin = getString('filterDataCheckin');

        limparRelatorioCheckin();

        daoAtividade.buscarFilter(filterTipoCheckin, filterDataCheckin, function (json) {

            $("#tbAtividades tbody").empty();

            jQuery.each(json, function(i, item) {

                let idAtividade = item.id;
                var descricao = item.descricao;
                var observacao = item.observacao;
                var dataAtividade = item.dataAtividade;
                var horaInicio = item.horaInicio;
                var horaFinal = item.horaFinal;
                var tipo = item.tipo;

                dataAtividade = dataAtividade.split("-");

                var ano = dataAtividade[0];
                var mes = dataAtividade[1];
                var dia = dataAtividade[2];

                dataAtividade = dia+'/'+mes+'/'+ano;

                if (observacao !== '') observacao = '<br/>'+observacao;
                if (descricao !== '') descricao = '<br/>'+descricao;

                controllerDigitalizacao.buscarallByAtividade(idAtividade, function (json) {

                    if (Object.keys(json).length > 0) {

                        jQuery.each(json, function(i, item) {

                            let documento = item.documento;
                            let observacaoFoto = item.observacao;
                            var fotoCheckin = '';

                            var foto = ' ' +
                                '  <div style="text-align: center;border-top: 10px;">' +
                                '       <h3>Check-in</h3>'+
                                '       <img src="'+documento+'" name="photos[]" style="width: 300px;height: 300px;"> ' +
                                '       <h3>Obs:.'+observacaoFoto+'</h3> ' +
                                '  </div>';

                            controllerDigitalizacao.buscarallByAtividadeCheckout(idAtividade, function (jsonCheckout) {

                                jQuery.each(jsonCheckout, function(i, jsonCheckoutItem) {

                                    let documentoCheckout = jsonCheckoutItem.documento;
                                    let observacaoFotoCheckout = jsonCheckoutItem.observacao;

                                    fotoCheckin = ' ' +
                                        '  <div style="text-align: center;border-top: 10px;">' +
                                        '       <h3>Check-out</h3>'+
                                        '       <img src="'+documentoCheckout+'" name="photoscheckin[]" style="width: 300px;height: 300px;"> ' +
                                        '       <h3>Obs:.'+observacaoFotoCheckout+'</h3> ' +
                                        '  </div>';

                                });

                                foto = foto + fotoCheckin;

                                let innerA = '  <li style="background: #bbbbbb;">' +
                                    '               <a href="#"> ' +
                                    '                   '+tipo+
                                    '                   <br/><small>'+dataAtividade+'</small>' +
                                    '                   <br/><small>Check-in:  '+observacao+'</small>' +
                                    '                   <br/><small>Check-out: '+descricao+'</small>' +
                                    '                   <p class="ui-li-aside timer"><span>'+horaInicio+' - '+horaFinal+'</span></strong></p>' +
                                    '               </a>' +
                                    '               '+foto+
                                    '           </li>';

                                $("#listViewAtividades").append(innerA);
                                $('ul').listview().listview('refresh');
                            });
                        });
                    } else {

                        controllerDigitalizacao.buscarallByAtividadeCheckout(idAtividade, function (jsonCheckout) {

                            var fotoCheckin = '';

                            jQuery.each(jsonCheckout, function(i, jsonCheckoutItem) {

                                let documentoCheckout = jsonCheckoutItem.documento;
                                let observacaoFotoCheckout = jsonCheckoutItem.observacao;

                                fotoCheckin = ' ' +
                                    '  <div style="text-align: center;border-top: 10px;">' +
                                    '       <h3>Check-out</h3>'+
                                    '       <img src="'+documentoCheckout+'" name="photoscheckin[]" style="width: 300px;height: 300px;"> ' +
                                    '       <h3>Obs:.'+observacaoFotoCheckout+'</h3> ' +
                                    '  </div>';

                            });

                            let innerA = '  <li style="background: #bbbbbb;">' +
                                '               <a href="#"> ' +
                                '                   '+tipo+
                                '                   <br/><small>'+dataAtividade+'</small>' +
                                '                   <br/><small>Check-in:  '+observacao+'</small>' +
                                '                   <br/><small>Check-out: '+descricao+'</small>' +
                                '                   <p class="ui-li-aside timer"><span>'+horaInicio+' - '+horaFinal+'</span></strong></p>' +
                                '               </a>' +
                                '               '+fotoCheckin+
                                '           </li>';

                            $("#listViewAtividades").append(innerA);
                            $('ul').listview().listview('refresh');
                        });
                    }
                });
            });
        });
    }

    function preencherComboBox() {
        controllerTipoCheckin.buscarAll(function (json) {
            preencherComboAtributos('tipoCheckin', json);
        });
    }

    function listarAtividadesCheckout() {
        daoAtividade.buscalAllNotChecklist(listViewAtividadesCheckout);
    }

    function listViewAtividadesCheckout(json) {

        $("#listviewAtividadesCheckout").empty();

        jQuery.each(ATIVIDADES, function(i, item) {
            clearInterval(item);
        });

        ATIVIDADES = [];

        jQuery.each(json, function(i, item) {

            var idAtividade = item.id;
            var tipoCheckin = item.tipoCheckin;
            var hora = item.horaInicio;
            var dataAtividade = item.dataAtividade;
            var observacao = item.observacao;

            var ano = getYear(dataAtividade);
            var mes = getMonth(dataAtividade);
            var dia = getDay(dataAtividade);
            var sHora = getHora(hora);
            var minuto = getMinuto(hora);

            var dataM = new Date(ano, mes - 1, dia, sHora, minuto , 0);
            var dataAgora = new Date();
            var dif  = diferencaDias(dataM, dataAgora);

            hora = formatarHora(hora);

            dataAtividade = dataAtividade.split("-");

            var ano = dataAtividade[0];
            var mes = dataAtividade[1];
            var dia = dataAtividade[2];

            dataAtividade = dia+'/'+mes+'/'+ano;

            controllerDigitalizacao.buscarallByAtividade(idAtividade, function (jsonFotos) {

                if (Object.keys(jsonFotos).length > 0) {

                    jQuery.each(jsonFotos, function(i, item) {

                        let documento = item.documento;
                        let observacaoFoto = item.observacao;

                        let foto = ' ' +
                            '  <div style="text-align: center;border-top: 10px;">' +
                            '       <h3>Check-in</h3>'+
                            '       <img src="'+documento+'" name="photos[]" style="width: 300px;height: 300px;"> ' +
                            '       <h3>Obs:.'+observacaoFoto+'</h3> ' +
                            '  </div>';

                        let innerA = '  <li style="background: #bbbbbb;">' +
                            '               <a href="view/atividade/editarAtividade.html" data-ajax="true" id="atividade_'+idAtividade+'" class="checkinAtividade" idAtividade="'+idAtividade+'">'+tipoCheckin+' '+observacao+'' +
                            '                   <br/><small>'+dataAtividade+'</small>' +
                            '                   <br/><small>'+hora+'</small>' +
                            '                   <p class="ui-li-aside timer"><span id="timer_'+idAtividade+'">'+dif+'</span></strong></p>' +
                            '               </a>' +
                            '              '+foto+
                            '           </li>';

                        $("#listviewAtividadesCheckout").append(innerA);

                        let newAtividade = setInterval(function () {
                            showTimer('timer_'+idAtividade);
                        }, 1000);

                        ATIVIDADES.push(newAtividade);
                    });

                } else {

                    let innerA = '  <li>' +
                        '               <a href="view/atividade/editarAtividade.html" data-ajax="true" id="atividade_'+idAtividade+'" class="checkinAtividade" idAtividade="'+idAtividade+'">'+tipoCheckin+' '+observacao+'' +
                        '                   <br/><small>'+dataAtividade+'</small>' +
                        '                   <br/><small>'+hora+'</small>' +
                        '                   <p class="ui-li-aside timer"><span id="timer_'+idAtividade+'">'+dif+'</span></strong></p>' +
                        '               </a>' +
                        '           </li>';

                    $("#listviewAtividadesCheckout").append(innerA);

                    var newAtividade = setInterval(function () {
                        showTimer('timer_'+idAtividade);
                    }, 1000);

                    ATIVIDADES.push(newAtividade);
                }

                $('ul').listview().listview('refresh');
            });
        });
    }

    function checkoutEditar() {

        var totalCampos = document.getElementsByName('photoscheckout[]').length;

        if (totalCampos === 0) {
            alert("Foto é um campo obrigatório para Check-OUT!");
        } else  {

            ID_ATIVIDADE = getString('idAtividade');

            /*
            var mensagem = 'Confirmar o Check-OUT?';
            var title = 'Check-OUT';
            controllerNotificacao.showConfirm(mensagem, function (retorno) {
                if (retorno === 1) checkoutInner(ID_ATIVIDADE);
            }, title);
             */

            checkoutInner(ID_ATIVIDADE);
        }
    }

    function buscarAll(successCallbackJson) {
        daoAtividade.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoAtividade.create();
    }

    function dropdb() {
        daoAtividade.drop();
    }

    function save() {

        controllerTipoCheckin.buscarById(getString('tipoCheckin'), function (json) {
           if (json[0].isUsarGPS === 1) {
               if (json[0].isUsarObservacao === 1) {
                   var observacao = getString('observacao');

                   if (observacao === '') {
                       alert("Observação é obrigatorio!");
                       return;
                   } else {
                       getGeoLocalizacao();
                   }

               } else {
                   getGeoLocalizacao();
               }
           } else  if (json[0].isUsarObservacao === 1){

               var observacao = getString('observacao');

               if (observacao === '') {
                   alert("Observação é obrigatorio!");
                   return;
               } else {
                   daoAtividade.save(buscarDadosTela(), depoisSalvar);
               }
           } else {
               daoAtividade.save(buscarDadosTela(), depoisSalvar);
           }
        });
    }

    function getGeoLocalizacao() {

        $("form :input").prop("disabled", true);

        if (tentantivasAcessoGPS  === 1) showPageLoading('Acessando GPS ....');

        controllerGeoLocalizacao.getCurrentPosition(function (position) {

            hidePageLoading();

            setString('latitude', position.coords.latitude );
            setString('longitude',  position.coords.longitude);

            daoAtividade.save(buscarDadosTela(), depoisSalvar);

        }, function (error) {
            gpsDesabilitado(error);
        });
    }

    function gpsDesabilitado(error) {

        if (tentantivasAcessoGPS <= 3) {

            showPageLoading('Acessando GPS ....'+tentantivasAcessoGPS+'º tentativa');

            getGeoLocalizacao();
            tentantivasAcessoGPS = tentantivasAcessoGPS + 1;
            return;
        }

        var mensagem = 'ATENÇÃO!!! PARA REALIZAR ESTE TIPO DE CHECKIN É NECESSÁRIO ATIVAR O GPS DO SEU SMARTPHONE!';
        var title = error.message ;
        hidePageLoading();

        controllerNotificacao.showAlert(mensagem, function () {
            $("form :input").prop("disabled", false);
        }, title);
    }

    function editar(idAtividade) {
        daoAtividade.buscaById(idAtividade, popularDadosTela);
    }

    function buscarDadosTela() {

        var idAtividade = getString('idAtividade');
        var descricao = getString("descricao");
        var tipoCheckin = getString("tipoCheckin");
        var latitude = getString("latitude");
        var longitude = getString("longitude");
        var observacao = getString('observacao');

        var atividade = new Atividade();
        atividade.setId(idAtividade);
        atividade.setDescricao(descricao);
        atividade.setTipoCheckin(tipoCheckin);
        atividade.setStatus('EM ANDAMENTO');
        atividade.setResponsavel(ID_USUARIO_LOGADO);
        atividade.setLatitude(latitude);
        atividade.setLongitude(longitude);
        atividade.setDataAtividade(formatDate(new Date()));
        atividade.setHoraInicio(getTime(new Date()));
        atividade.setHoraFinal('');
        atividade.setChecklist(0);
        atividade.setAtivo(1);
        atividade.setObservacao(observacao);
        return atividade;
    }

    function checkin(idAtividade) {
        daoAtividade.checkin(idAtividade, function () {});
    }

    function checkinGeoLocalizacao(idAtividade, longitude, latitude) {
        daoAtividade.checkinGeoLocalizacao(idAtividade, latitude, longitude, function () {});
    }

    function checkout(idAtividade, descricao) {
        salvarDigitalizacaoCheckin(idAtividade);

        daoAtividade.checkout(idAtividade, descricao, function (retorno) {});
    }

    function checkoutInner(idAtividade) {

        checkout(idAtividade, getString('descricao') );

        $.confirm({
            title: 'Sucesso!',
            content: 'Salvo com sucesso!',
            buttons: {
                specialKey: {
                    text: 'OK',
                    action: function(){
                        $.mobile.changePage("#home");
                    }
                },
            }
        });
    }

    function popularDadosTela(json) {

        var atividadeJson = json[0];

        var idAtividade = atividadeJson.id;
        var descricao = atividadeJson.descricao;
        var tipoCheckin = atividadeJson.tipoCheckin;

        setString('idAtividade', idAtividade);
        setString('descricao', descricao);
        setCombo('tipoCheckin', tipoCheckin);
    }

    var naoAbrirConfirmacao = true;

    function depoisSalvar(retorno) {

        var idAtividade = getString('idAtividade');

        console.log('Salvando Atividade');

        if (naoAbrirConfirmacao) {
            naoAbrirConfirmacao = false;
            $.confirm({
                title: 'Sucesso!',
                content: 'Salvo com sucesso!',
                buttons: {
                    specialKey: {
                        text: 'OK',
                        action: function () {

                            if (isAdicionarAtividade(idAtividade)) {
                                daoAtividade.buscarMaxAtividade(function (jsonMaxAtividade) {
                                    salvarDigitalizacao(jsonMaxAtividade[0].maxAtividade);
                                    setString('idAtividade', jsonMaxAtividade[0].maxAtividade);
                                });
                            }

                            $.mobile.changePage("#home");
                            naoAbrirConfirmacao = true;
                        }
                    },

                }
            });
        }
    }

    function isAdicionarAtividade(idAtividade) {
        return idAtividade === undefined || idAtividade === '-' || idAtividade === '';
    }

    function salvarDigitalizacao(idAtividade) {

        console.log('Salvando digitalizacao '+idAtividade);

        var totalCampos = document.getElementsByName('photos[]').length;

        for (var i = 0; i < totalCampos; i++) {

            var imagem = document.getElementsByName('photos[]')[i].src;
            var mensagem = document.getElementsByName('photos[]')[i].getAttribute('mensagem');

            var digitalizacao = new Digitalizacao();
            digitalizacao.setAtividade(idAtividade);
            digitalizacao.setDocumento(imagem);
            digitalizacao.setObservacao(mensagem);

            controllerDigitalizacao.save(digitalizacao);
        }
    }

    function salvarDigitalizacaoCheckin(idAtividade) {

        var totalCampos = document.getElementsByName('photoscheckout[]').length;

        for (var i = 0; i < totalCampos; i++) {

            var imagem = document.getElementsByName('photoscheckout[]')[i].src;
            var mensagem = document.getElementsByName('photoscheckout[]')[i].getAttribute('mensagem');

            var digitalizacao = new Digitalizacao();
            digitalizacao.setCheckout(idAtividade);
            digitalizacao.setDocumento(imagem);
            digitalizacao.setObservacao(mensagem);

            controllerDigitalizacao.save(digitalizacao);
        }
    }
    
    function baixar(successCallback) {
        daoAtividade.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoAtividade.enviar(successCallback);
    }
};