var ControllerCamera = function () {

    this.initialize = initialize;
    this.getPicture = getPicture;

    function initialize() {}

    function getPicture(destinationType, sourceType, mediaType, allowEdit, onSuccess, onError) {

        if (navigator.camera === undefined) return;

        /*
        Camera.DestinationType = {
            DATA_URL : 0,                // Return image as base64 encoded string
            FILE_URI : 1                 // Return image file URI
        };
        */

        /*
        Camera.PictureSourceType = {
            PHOTOLIBRARY : 0,
            CAMERA : 1,
            SAVEDPHOTOALBUM : 2
        };
         */

        /*
        Camera.EncodingType = {
            JPEG : 0,               // Return JPEG encoded image
            PNG : 1                 // Return PNG encoded image
        };
        */

        /*
        Camera.MediaType = {
            PICTURE: 0,             // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
            VIDEO: 1,               // allow selection of video only, WILL ALWAYS RETURN FILE_URI
            ALLMEDIA : 2            // allow selection from all media types
        };
        */

        /*
        Camera.Direction = {
            BACK : 0,      // Use the back-facing camera
            FRONT : 1      // Use the front-facing camera
        };
         */

        var option = {
            quality : 100,
            destinationType : destinationType,
            sourceType : sourceType,
            allowEdit : allowEdit,
            encodingType: Camera.EncodingType.PNG,
            //targetWidth: 100,
            //targetHeight: 100,
            mediaType:  mediaType
        };
        navigator.camera.getPicture(onSuccess, onError,option);
    }

};