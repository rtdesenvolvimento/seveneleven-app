var ControllerCaptacao = function () {

    var daoCaptacao = new DaoCaptacao();
    var daoProposta = new DaoProposta();

    var controllerMeioDivulgacao = new ControllerMeioDivulgacao();
    var controllerPlano = new ControllerPlano();
    var controllerUsuario = new ControllerUsuario();
    var controllerNotificacao = new ControllerNotificacao();

    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#adicionarCaptacao", function( event ) {
            menu.initialize();
            combosBox();
            $('#formAdicionarCaptcao').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarCaptacao", function( event ) {
            menu.initialize();
            combosBox();
            $('#formEditarCaptacao').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idCaptacao = event.delegateTarget.activeElement.attributes['idcaptacao'].value;
            editar(idCaptacao);

            $('#converterProposta').click(function (event) {

                var mensagem = 'Deseja realmente converter sua captação em proposta?';
                var title = 'Converter Captação';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        converter();
                    }
                } , title );
            });

            $('#excluirCaptacao').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta Captação de sua listagem?';
                var title = 'Excluir Captação';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });
        });

        $( document ).on( "pageinit", "#captacoes", function( event ) {
            menu.initialize();
            listarCaptacoes();
        });
    }

    function combosBox() {
        controllerMeioDivulgacao.buscarAll(function (json) {
           preencherComboAtributos('meioDivulgacao', json);
        });

        controllerPlano.buscarAll(function (json) {
            preencherComboAtributos('plano', json);
        });

        controllerUsuario.buscarAll(function (json) {
            controllerUsuario.preencherCombo('responsavel', json);
            setCombo('responsavel', ID_USUARIO_LOGADO);
        });
    }

    function listarCaptacoes() {
        daoCaptacao.buscarAll(listViewCaptacao);
    }

    function listViewCaptacao(json) {
        $("#listviewCaptacoes").empty();

        jQuery.each(json, function(i, item) {

            var id = item.id;
            var nome = item.nome;
            var email = item.email;
            var telefone = item.telefone;
            var telefoneAdicional = item.telefoneAdicional;
            var cidade = item.cidade;

            var innerA = '  <li>' +
                '               <a href="view/captacao/editarCaptacao.html" data-ajax="true" idCaptacao="'+id+'">' +
                '                   <h2>'+nome+'</h2>'+
                '                   <small>'+email+'</small>' +
                '                   <br/><small>'+telefone+' / '+telefoneAdicional+'</small>' +
                '                   <br/><small>'+cidade+'</small>' +
                '               </a>' +
                '           </li>';
            $("#listviewCaptacoes").append(innerA);
        });

        $('ul').listview().listview('refresh');

        $('.pg-inicial').css('padding', '10px');
        $('.pg-inicial').css('margin-left', '10px');
    }

    function converter() {
        var idCaptacao = getString('idCaptacao');

        daoCaptacao.buscaById(idCaptacao, function (json) {
            var captacaoJson = json[0];

            var idCaptacao = captacaoJson.id;
            var nome = captacaoJson.nome;
            var email = captacaoJson.email;
            var telefone = captacaoJson.telefone;
            var descricao = captacaoJson.descricao;
            var responsavel = captacaoJson.responsavel;
            var meioDivulgacao = captacaoJson.meioDivulgacao;
            var plano =  captacaoJson.plano;
            var cidade = captacaoJson.cidade;
            var telefoneAdicional = captacaoJson.telefoneAdicional;

            var proposta = new Proposta();
            proposta.setNomeEmpresa(nome);
            proposta.setEmail(email);
            proposta.setTelefone(telefone);
            proposta.setNomeContato('');
            proposta.setDataProposta(formatDate(new Date()));
            proposta.setVisitaConfirmada(0);
            proposta.setTempoVigenciaContrato(TEMPO_VIGENCIA_CONTRATO);
            proposta.setAptoRenovar(0);
            proposta.setObservacao(descricao);
            proposta.setDataApresentacao(formatDate(new Date()));
            proposta.setValorAparelho(0);
            proposta.setValorPlano(0);
            proposta.setQtdLinhas(0);
            proposta.setIsPedido(0);
            proposta.setAptoRenovar(1);
            proposta.setBackupOnline(0);
            proposta.setIdentificacaoClienteNet('');
            //proposta.setTelefoneAdicional(telefoneAdicional);
            //proposta.setCidade(cidade);
            proposta.setDataAberta(formatDate(new Date()));
            proposta.setDescricao(descricao);
            proposta.setResponsavel(responsavel);
            proposta.setMeioDivulgacao(meioDivulgacao);
            proposta.setTipoVenda('NOVA VENDA');
            proposta.setStatus('APRESENTACAO');

            daoProposta.save(proposta, function () {});
            daoCaptacao.converter(idCaptacao, function () {});

            depoisSalvar(true);
        });
    }

    function excluir() {
        var idCaptacao = getString('idCaptacao');
        daoCaptacao.deletar(idCaptacao);
        depoisSalvar(true);
    }

    function buscarAll(successCallbackJson) {
        daoCaptacao.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoCaptacao.create();
    }

    function dropdb() {
        daoCaptacao.drop();
    }

    function save() {
        daoCaptacao.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idCargo) {
        daoCaptacao.buscaById(idCargo, popularDadosTela);
    }

    function buscarDadosTela() {

        var idCaptacao = getString('idCaptacao');
        var nome = getString('nome');
        var email = getString('email');
        var telefone = getString('telefone');
        var status = getString('status');
        var telefoneAdicional = getString('telefoneAdicional');
        var descricao = getString("descricao");
        var cidade = getString('cidade');
        var responsavel = getString('responsavel');
        var plano = getString('plano');
        var meioDivulgacao = getString('meioDivulgacao');

        var captacao = new Captacao();
        captacao.setId(idCaptacao);
        captacao.setDescricao(descricao);
        captacao.setDataCaptacao(formatDate(new Date()));
        captacao.setNome(nome);
        captacao.setEmail(email);
        captacao.setTelefone(telefone);
        captacao.setStatus(status);
        captacao.setTelefoneAdicional(telefoneAdicional);
        captacao.setCidade(cidade);
        captacao.setResponsavel(responsavel);
        captacao.setPlano(plano);
        captacao.setMeioDivulgacao(meioDivulgacao);

        return captacao;
    }

    function popularDadosTela(json) {
        var captacaoJson = json[0];

        var idCaptacao = captacaoJson.id;
        var nome = captacaoJson.nome;
        var email = captacaoJson.email;
        var telefone = captacaoJson.telefone;
        var status = captacaoJson.status;
        var telefoneAdicional = captacaoJson.telefoneAdicional;
        var descricao = captacaoJson.descricao;
        var cidade = captacaoJson.cidade;
        var responsavel = captacaoJson.responsavel;
        var plano =  captacaoJson.plano;
        var meioDivulgacao = captacaoJson.meioDivulgacao;

        setString('idCaptacao', idCaptacao);
        setString('descricao', descricao);
        setString('nome', nome);
        setString('email', email);
        setString('telefone', telefone);
        setString('status', status);
        setString('telefoneAdicional', telefoneAdicional);
        setString('cidade', cidade);
        setCombo('responsavel', responsavel);
        setCombo('plano', plano);
        setCombo('meioDivulgacao', meioDivulgacao);
    }

    function depoisSalvar(retorno) {
        console.log('save captacao '+retorno);
        $('#nmCaptacao').click();
    }

    function baixar(successCallback) {
        daoCaptacao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoCaptacao.enviar(successCallback);
    }

};