var ControllerCargo = function () {

    var daoCargo = new DaoCargo();
    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarCargo", function( event ) {
            menu.initialize();
            $('#formAdicionarCargo').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarCargo", function( event ) {
            menu.initialize();
            $('#formEditarCargo').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idCargo = event.delegateTarget.activeElement.attributes['idcargo'].value;
            editar(idCargo);
        });

        $( document ).on( "pageinit", "#cargos", function( event ) {
            menu.initialize();
            listarCargos();
        });
    }

    function listarCargos() {
        daoCargo.buscarAll(listViewCargos);
    }

    function listViewCargos(json) {
        $("#listviewCargos").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/cargo/editarCargo.html" data-ajax="true" idCargo="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewCargos").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoCargo.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoCargo.create();
    }

    function dropdb() {
        daoCargo.drop();
    }

    function save() {
        daoCargo.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idCargo) {
        daoCargo.buscaById(idCargo, popularDadosTela);
    }

    function buscarDadosTela() {
        var idCargo = getString('idCargo');
        var descricao = getString("descricao");

        debugger;
        var cargo = new Cargo();
        cargo.setId(idCargo);
        cargo.setDescricao(descricao);

        return cargo;
    }

    function popularDadosTela(json) {
        var cargoJson = json[0];

        var idCargo = cargoJson.id;
        var descricao = cargoJson.descricao;

        setString('idCargo', idCargo);
        setString('descricao', descricao);
    }

    function depoisSalvar(retorno) {
        console.log('save cargo '+retorno);
        $('#nmCargo').click();
    }
    
    function baixar(successCallback) {
        daoCargo.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoCargo.enviar(successCallback);
    }
};