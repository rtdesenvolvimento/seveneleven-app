var ControllerChecklist = function () {

    var ID_ATIVIDADE = null;

    var daoChecklist = new DaoChecklist();
    var daoTipoCheckin = new DaoTipoCheckin();
    var daoAtividade = new DaoAtividade();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerAtividade = new ControllerAtividade();
    var controllerTipoCheckin = new ControllerTipoCheckin();
    var controllerGeoLocalizacao = new ControllerGeoLocalizacao();
    var tentantivasAcessoGPS = 1;

    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarChecklist", function( event ) {
            menu.initialize();
            $('#formAdicionarChecklist').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarChecklist", function( event ) {
            menu.initialize();
            $('#formEditarChecklist').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idChecklist = event.delegateTarget.activeElement.attributes['idchecklist'].value;
            editar(idChecklist);
        });

        $( document ).on( "pageinit", "#adicionarAtividadeChecklist", function( event ) {
            menu.initialize();
            preencherComboBox();

            $('#formAdicionarAtividadeChecklist').submit(function (event) {
                event.preventDefault();
                saveChecklist(event);
            });

        });

        $( document ).on( "pageinit", "#editarAtividadeChecklist", function( event ) {
            menu.initialize();
            preencherComboBox();
            $('#formEditarAtividadeChecklist').submit(function (event) {
                event.preventDefault();
                checkoutEditar();
            });

            var idChecklist = event.delegateTarget.activeElement.attributes['idatividade'].value;
            editarAtividade(idChecklist);
        });

        $( document ).on( "pageinit", "#checklists", function( event ) {
            menu.initialize();
            listarChecklist();
        });

        $( document ).on( "pageinit", "#checklistAtividades", function( event ) {
            menu.initialize();
            CHECKLIST = event.delegateTarget.activeElement.attributes['idchecklist'].value;
            listarAllCheckis();
        });
    }

    function preencherComboBox() {
        daoTipoCheckin.buscarAll(function (json) {
            preencherComboAtributos('tipoCheckin', json);
        });
        daoChecklist.buscarAll(function (json) {
           preencherComboAtributos('checklist', json);
           setCombo('checklist', CHECKLIST);
        });
    }

    function listarAllCheckis() {
        tentantivasAcessoGPS = 1;
        daoAtividade.buscarAllAtividadesAbertasByCheckin(CHECKLIST, function (json) {
            listarChecklistAtividades('listviewChecklistAtividade', json);
        });

        daoAtividade.buscarAllAtividadesEmAndamentoByCheckin(CHECKLIST,function (json) {
            listarChecklistAtividades('listviewChecklistAtividadeEmAndamento', json);
        });
    }

    function listarChecklistAtividades(nomeLista, json) {
        $("#"+nomeLista).empty();

        jQuery.each(ATIVIDADES, function(i, item) {
            clearInterval(item);
        });
        ATIVIDADES = [];

        jQuery.each(json, function(i, item) {
            var id = item.id;
            var tipoCheckin = item.tipoCheckin;
            var hora = item.horaInicio;
            var dataAtividade = item.dataAtividade;
            var status = item.status;

            var ano = getYear(dataAtividade);
            var mes = getMonth(dataAtividade);
            var dia = getDay(dataAtividade);
            var sHora = getHora(hora);
            var minuto = getMinuto(hora);

            var dataM = new Date(ano, mes - 1, dia, sHora, minuto , 0);
            var dataAgora = new Date();

            var dif  = diferencaDias(dataM, dataAgora);

            hora = formatarHora(hora);

            var innerA = '  <li>' +
                '               <a href="view/checklist/editarAtividadeChecklist.html" data-ajax="true" id="atividade_'+id+'" class="checkinAtividade" idAtividade="'+id+'">'+tipoCheckin+'' +
                '                   <br/><small>'+formatDatePtBr(dataAtividade)+'</small>' +
                '                   <br/><small>'+hora+'</small></a>' +
                '                   <p class="ui-li-aside timer"><span id="timer_'+id+'">'+dif+'</span></strong></p>' +
                '               </a>' +
                '           </li>';
            $("#"+nomeLista).append(innerA);

            var newAtividade = setInterval(function () {
                showTimer('timer_'+id);
            }, 1000);

            ATIVIDADES.push(newAtividade);

            if (status === 'ABERTO') {
                $('#atividade_' + id).click(function (event) {
                    var mensagem = 'Confirmar o Check-IN?';
                    var title = 'Check-IN';
                    ID_ATIVIDADE = $(this).attr('idatividade');

                    controllerNotificacao.showConfirm(mensagem, function (retorno) {
                        if (retorno === 1) checkin(ID_ATIVIDADE);
                    }, title);
                });
            }

        });
        $('ul').listview().listview('refresh');
    }

    function checkoutEditar() {
        var mensagem = 'Confirmar o Check-OUT?';
        var title = 'Check-OUT';
        ID_ATIVIDADE = getString('idAtividade');

        controllerNotificacao.showConfirm(mensagem, function (retorno) {
            if (retorno === 1) checkout(ID_ATIVIDADE);
        }, title);
    }

    function checkin(id) {

        daoAtividade.buscaById(id, function (jsonAtividade) {
            controllerTipoCheckin.buscarById(jsonAtividade[0].tipoCheckin, function (json) {
                if (json[0].isUsarGPS === 1) {
                    getGeoLocalizacao(id);
                } else {
                    controllerAtividade.checkin(id);
                    listarAllCheckis();
                }
            });
        });
    }

    function getGeoLocalizacao(id) {

        $("form :input").prop("disabled", true);

        controllerGeoLocalizacao.getCurrentPosition(function (position) {

            var mensagem = 'O APLICATIVO ATRIBUIRA SUA LOCALIZAÇÃO ATUAL, COMO ORIGEM DO ATENDIMENTO DESEJA CONFIRMAR?';
            var title = 'Gravando a Origem do Atendimento!';

            controllerNotificacao.showConfirm(mensagem, function (retorno) {
                if(retorno===1) {
                    controllerAtividade.checkinGeoLocalizacao(id, position.coords.latitude,position.coords.longitude);
                    listarAllCheckis();

                } else {
                    listarAllCheckis();
                }
            } , title );
        }, function (error) {
            gpsDesabilitado(error);
        });
    }

    function gpsDesabilitado(error) {

        if (tentantivasAcessoGPS <= 3) {
            getGeoLocalizacao();
            tentantivasAcessoGPS = tentantivasAcessoGPS + 1;
            return;
        }

        var mensagem = 'ATENÇÃO!!! PARA REALIZAR ESTE TIPO DE CHECKIN É NECESSÁRIO ATIVAR O GPS DO SEU SMARTPHONE!';
        var title = error.message ;

        controllerNotificacao.showAlert(mensagem, function () {}, title);

        listarAllCheckis();
    }

    function checkout(id) {
        controllerAtividade.checkout(id, getString('descricao'));
        $('#nmChecklist').click();
    }

    function listarChecklist() {
        daoChecklist.buscarAll(listViewChecklist);
    }

    function listViewChecklist(json) {
        $("#listviewChecklist").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;
            var innerA = '  <li>' +
                '               <a href="view/checklist/checklistAtividades.html" data-ajax="true" idChecklist="'+id+'" data-role="button">'+descricao+'</a> '+
                '               <a href="view/checklist/editarChecklist.html" data-ajax="true" idChecklist="'+id+'">'+descricao+'</a>' +
            '           </li>';
            $("#listviewChecklist").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoChecklist.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoChecklist.create();
    }

    function dropdb() {
        daoChecklist.drop();
    }

    function save() {
        daoChecklist.save(buscarDadosTela(), depoisSalvar);
    }

    function saveChecklist() {
        controllerTipoCheckin.buscarById(getString('tipoCheckin'), function (json) {
            if (json[0].isUsarObservacao === 1){
                var observacao = getString('observacao');
                if (observacao === '') {
                    alert("Observação é obrigatorio!");
                    return;
                } else {
                    daoAtividade.save(buscarDadosTelaAtividade(), function (retorno) {$('#nmChecklist').click();});
                }
            } else {
                daoAtividade.save(buscarDadosTelaAtividade(), function (retorno) {$('#nmChecklist').click();});
            }
        });
    }

    function buscarDadosTelaAtividade() {

        var idAtividade = getString('idAtividade');
        var checklist = getString('checklist');
        var tipoCheckin = getString('tipoCheckin');
        var observacao = getString('observacao');

        var atividade = new Atividade();
        atividade.setId(idAtividade);
        atividade.setTipoCheckin(tipoCheckin);
        atividade.setAtivo(1);
        atividade.setDescricao('');
        atividade.setStatus('ABERTO');
        atividade.setResponsavel(ID_USUARIO_LOGADO);
        atividade.setLatitude(0);
        atividade.setLongitude(0);
        atividade.setDataAtividade(formatDate(new Date()));
        atividade.setHoraInicio(getTime(new Date()));
        atividade.setHoraFinal('');
        atividade.setChecklist(checklist);
        atividade.setObservacao(observacao);
        return atividade;
    }

    function editar(idChecklist) {
        daoChecklist.buscaById(idChecklist, popularDadosTela);
    }


    function editarAtividade(idAtividade) {
        daoAtividade.buscaById(idAtividade, popularDadosTelaAtividade);
    }

    function popularDadosTelaAtividade(json) {
        var atividadeJson = json[0];

        var idAtividade = atividadeJson.id;
        var descricao = atividadeJson.descricao;
        var tipoCheckin = atividadeJson.tipoCheckin;
        var checklist = atividadeJson.checklist;

        setString('idAtividade', idAtividade);
        setString('descricao', descricao);
        setCombo('tipoCheckin', tipoCheckin);
        setString('checklist', checklist);
    }

    function buscarDadosTela() {
        var idChecklist = getString('idChecklist');
        var descricao = getString("descricao");

        var checklist = new Checklist();
        checklist.setId(idChecklist);
        checklist.setDescricao(descricao);

        return checklist;
    }

    function popularDadosTela(json) {
        var checklistJson = json[0];

        var idChecklist = checklistJson.id;
        var descricao = checklistJson.descricao;

        setString('idChecklist', idChecklist);
        setString('descricao', descricao);
    }

    function depoisSalvar(retorno) {
        console.log('save checklist '+retorno);
        $('#nmChecklist').click();
    }
    
    function baixar(successCallback) {
        daoChecklist.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoChecklist.enviar(successCallback);
    }

};