var ControllerClassificaoCliente = function () {

    var daoClassificacaoCliente = new DaoClassificacaoCliente();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarClassificacaoCliente", function( event ) {
            menu.initialize();
            $('#formAdicionarClassificacaoCliente').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarClassificacaoCliente", function( event ) {
            menu.initialize();
            $('#formEditarClassificacaoCliente').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idClassificacaoCliente = event.delegateTarget.activeElement.attributes['idclassificacaocliente'].value;
            editar(idClassificacaoCliente);
        });

        $( document ).on( "pageinit", "#classificacoesCliente", function( event ) {
            menu.initialize();
            listarClassificaoCliente();
        });
    }

    function editar(idClassificacaoCliente) {
        daoClassificacaoCliente.buscaById(idClassificacaoCliente, popularDadosTela);
    }
    function listarClassificaoCliente() {
        daoClassificacaoCliente.buscarAll(listViewClassificaoCliente);
    }

    function listViewClassificaoCliente(json) {
        $("#listviewClassificacaoCliente").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;
            var periodoRenovacaoMes = item.periodoRenovacaoMes;

            var innerA = '  <li>' +
                '               <a href="view/classificacaocliente/editarClassificacaoCliente.html" data-ajax="true" idClassificacaoCliente="'+id+'">'+descricao+'' +
                '                   <br/><small>Período Renovação '+periodoRenovacaoMes+' mêses </small>'+
                '               </a>' +
                '           </li>';
            $("#listviewClassificacaoCliente").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoClassificacaoCliente.create();
    }

    function dropdb() {
        daoClassificacaoCliente.drop();
    }

    function save(event) {
        daoClassificacaoCliente.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idClassificacaoCliente = getString('idClassificacaoCliente');
        var descricao = getString("descricao");
        var periodoRenovacaoMes = getString('periodoRenovacaoMes');

        var classificaoCliente = new ClassificacaoCliente();
        classificaoCliente.setDescricao(descricao);
        classificaoCliente.setId(idClassificacaoCliente);
        classificaoCliente.setPeriodoRenovacaoMes(periodoRenovacaoMes);

        return classificaoCliente;
    }

    function popularDadosTela(json) {

        var jsonClassificaoCliente = json[0];
        var idClassificacaoCliente = jsonClassificaoCliente.id;
        var descricao = jsonClassificaoCliente.descricao;
        var periodoRenovacaoMes = jsonClassificaoCliente.periodoRenovacaoMes;

        setString('idClassificacaoCliente', idClassificacaoCliente);
        setString('descricao', descricao);
        setString('periodoRenovacaoMes', periodoRenovacaoMes);
    }

    function buscarAll(successCallback) {
        daoClassificacaoCliente.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save classificação cliente '+retorno);
        $('#nmClassificacao').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoClassificacaoCliente.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoClassificacaoCliente.enviar(successCallback);
    }

};