var ControllerCliente = function () {

    var controllerCamera = new ControllerCamera();
    var controllerDigitalizacao = new ControllerDigitalizacao();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerUsuario = new ControllerUsuario();

    var daoCliente = new DaoCliente();
    var daoProposta = new DaoProposta();
    var menu = new Menu();

    this.initialize = initialize;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.buscarAll = buscarAll;
    this.baixar = baixar;
    this.enviar = enviar;
    this.adicionarCliente = adicionarCliente;
    this.editarCliente = editarCliente;
    this.save = save;
    this.popularDadosTela = popularDadosTela;
    this.buscaById = buscaById;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#clientes", function( event ) {
            menu.initialize();
            listarClientesAtivos();
        });

        $( document ).on( "pageinit", "#clientesInativos", function( event ) {
            menu.initialize();
            listarClientesInativos();
        });

        $( document ).on( "pageinit", "#adicionarCliente", function( event ) {
            adicionarCliente();
        });

        $( document ).on( "pageinit", "#editarCliente", function( event ) {
            editarCliente(event);
        });
    }

    function editarCliente(event) {
        menu.initialize();
        formatarCampos();
        $('#formEditarCliente').submit(function (event) {
            event.preventDefault();
            save(event);
        });

        $('#cep').blur(function () {
            buscarCep();
        });

        $('#cnpjProposta').blur(function (event) {
            consultaPessoa();
        });

        controllerUsuario.buscarAll(preencherUsuarioCombo);

        var idCliente = event.delegateTarget.activeElement.attributes['idcliente'];

        if (idCliente !== undefined) idCliente = event.delegateTarget.activeElement.attributes['idcliente'].value;
        else idCliente = CLIENTE;

        editar(idCliente);

        editarFotos(idCliente);

        $('#digitalizacaoCamera').click(function (event) {
            adicionarCamera();
        });

        $('#digitalizacaoAlbum').click(function (event) {
            adicionarAlbum();
        });
    }

    function adicionarCliente() {

        formatarCampos();
        menu.initialize();

        $('#formAdicionarCliente').submit(function (event) {
            event.preventDefault();
            save(event);
        });

        $('#cep').blur(function () {
            buscarCep();
        });

        $('#cnpjProposta').blur(function (event) {
            setString('nome', getString('nomeEmpresa'));
            setString('telefoneCliente', getString('telefone'));
            consultaPessoa();
        });

        $('#digitalizacaoCamera').click(function (event) {
            adicionarCamera();
        });

        $('#digitalizacaoAlbum').click(function (event) {
            adicionarAlbum();
        });
        controllerUsuario.buscarAll(preencherUsuarioCombo);
    }

    function preencherUsuarioCombo(json) {
        controllerUsuario.preencherCombo('responsavel', json);
        setCombo('responsavel', ID_USUARIO_LOGADO);
    }

    function adicionarCamera() {

        var destinationType = 0;
        var sourceType = 1;
        var mediaType = 0;
        var allowEdit = false;

        controllerCamera.getPicture(destinationType,sourceType, mediaType, allowEdit, function (imageData) {
            controllerNotificacao.promptMessage('Digite uma descrição para essa digitalização','', function (retorno) {
                adicionarDigitalizacaoLisView(imageData, retorno);
            });

        }, function (error) {});
    }

    function adicionarAlbum() {
        var destinationType = 0;
        var sourceType = 0;
        var mediaType = 0;
        var allowEdit = false;

        controllerCamera.getPicture(destinationType,sourceType, mediaType, allowEdit, function (imageData) {
            controllerNotificacao.promptMessage('Digite uma descrição para essa digitalização','', function (retorno) {
                adicionarDigitalizacaoLisView(imageData, retorno);
            });
        }, function (error) {});
    }

    function adicionarDigitalizacaoLisView(imageData, retorno) {
        var foto =  "data:image/jpeg;base64," + imageData;
        var innerA = ' ' +
            '  <li>' +
            '      <a href="#popupPhoto" data-rel="popup" id="" ondblclick="abrirFotoNavegador();" data-position-to="window" onclick="" class="ui-btn ui-corner-all ui-shadow ui-btn-inline"> ' +
            '          <img src="'+foto+'" name="photos[]" mensagem="'+retorno+'" id="" style="width: 160px;height: 160px;"> ' +
            '          <h3>'+retorno+'</h3> ' +
            '      </a> ' +
            '       <div data-role="popup"  id="popupPhotoLandscape" style="display: none;" class="photopopup" data-overlay-theme="a" data-corners="false" data-tolerance="30,15">'+
            '           <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a><img src="'+foto+'" alt="">' +
            '       </div>' +
            '  </li>'

        $("#listViewDigitalizacaoCliente").append(innerA);
        $('ul').listview().listview('refresh');
    }

    function listarClientesAtivos() {
        daoCliente.buscarAll(listViewClientes);
    }

    function listarClientesInativos() {
        daoCliente.buscarlAllInativos(listViewClientes);
    }

    function buscarAll(successCallback) {
        daoCliente.buscarAll(successCallback);
    }

    function listViewClientes(json) {
        $("#sortedList").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var nome = item.nome;
            var telefone = item.telefone;
            var celular = item.celular;
            var email = item.email;
            var nomeContato = item.nomeContato;

            var innerA = '  <li>' +
                '               <a href="view/clientes/editarCliente.html" data-ajax="true" idCliente="'+id+'">'+nome+'' +
                '                   <br/><small>'+telefone+ ' - ' + celular + '</small> ' +
                '                   <br/><small>' + email + '</small> ' +
                '                   <br/><small>' + nomeContato + '</small> ' +
                '               </a>' +
                '           </li>';
            $("#sortedList").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function save() {
        if (getString('cnpjProposta') !== '' &&
            getString('idProposta') !== '') {
            daoCliente.save(buscarDadosTela(), depoisSalvar );
        }
    }

    function editar(idCliente, successCallbackJson) {
        buscaById(idCliente, successCallbackJson);
    }

    function buscaById(idCliente) {
        daoCliente.buscaById(idCliente, popularDadosTela);
    }

    function editarFotos(idCliente) {
        $("#listViewDigitalizacaoCliente").empty();
        controllerDigitalizacao.buscarAllByCliente(idCliente, function (json) {
            jQuery.each(json, function(i, item) {
                var id = item.id;
                var foto = item.documento;
                var observacao = item.observacao;

                var innerA = ' ' +
                    '  <li>' +
                    '      <a href="#popupPhoto" data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline"> ' +
                    '           <img src="'+foto+'" name="photos[]" mensagem="'+observacao+'" id="'+id+'" style="width: 160px;height: 160px;"> ' +
                    '           <h3>'+observacao+'</h3> ' +
                    '      </a> ' +
                    '       <div data-role="popup" style="display: none;" id="popupPhotoLandscape" class="photopopup" data-overlay-theme="a" data-corners="false" data-tolerance="30,15">'+
                    '           <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a><img src="'+foto+'" alt="">' +
                    '       </div>' +
                    '  </li>'

                $("#listViewDigitalizacaoCliente").append(innerA);
            });
            $('ul').listview().listview('refresh');
        });
    }

    function popularDadosTela(clienteJson) {

        if (clienteJson[0] === undefined) return;

        clienteJson = clienteJson[0];

        var idCliente = clienteJson.id;
        var tipoPessoa = clienteJson.tipoPessoa;
        var nome = clienteJson.nome;
        var cpfCnpj = clienteJson.cpfCnpj;
        var telefone = clienteJson.telefone;
        var celular = clienteJson.celular;
        var email = clienteJson.email;
        var ie = clienteJson.ie;
        var sexo = clienteJson.sexo;
        var rg = clienteJson.rg;
        var orgaoEmissor = clienteJson.orgaiEmissor;
        var estadoEmissor = clienteJson.estadoEmissor;
        var dataEmissao = clienteJson.datEmissao;
        var cep = clienteJson.cep;
        var rua = clienteJson.rua;
        var numero = clienteJson.numero;
        var complemento = clienteJson.complemento;
        var bairro = clienteJson.bairro;
        var cidade = clienteJson.cidade;
        var estado = clienteJson.estado;
        var nomeContato = clienteJson.nomeContato;
        var telefoneContato = clienteJson.telefoneContato;
        var emailContato = clienteJson.emailContato;
        var cargoContato = clienteJson.cargoContato;
        var ativo = clienteJson.ativo;
        var observacao = clienteJson.observacao;
        var responsavel = clienteJson.responsavel;

        var cpfContato = clienteJson.cpfContato;
        var nomeContatoGestor = clienteJson.nomeContatoGestor;
        var cpfContatoGestor = clienteJson.cpfContatoGestor;
        var celularAdicional = clienteJson.celularAdicional;
        var celularComplementar = clienteJson.celularComplementar;
        var numeroClaroVoz =  clienteJson.numeroClaroVoz;
        var numeroClaroBandaLarga = clienteJson.numeroClaroBandaLarga;
        var numeroClaroTelemetria = clienteJson.numeroClaroTelemetria;
        var numeroNetTelefoneFixo = clienteJson.numeroNetTelefoneFixo;
        var numeroNetInternet = clienteJson.numeroNetInternet;
        var numeroNetTv = clienteJson.numeroNetTv;
        var dataInicioContrato = clienteJson.dataInicioContrato;
        var dataTerminoContrato = clienteJson.dataTerminoContrato;
        var classificacaoCliente = clienteJson.classificacaoCliente;
        var aptoRenovar = clienteJson.aptoRenovar;
        var valorPlano = clienteJson.valorPlano;
        var loginPortalClaro = clienteJson.valorPlano;
        var senhaPortalClaro = clienteJson.senhaPortalClaro;
        var inscricaoMunicipal =  clienteJson.inscricaoMunicipal;
        var cnaeprimario =  clienteJson.cnaeprimario;
        var cnaesecundario = clienteJson.cnaesecundario;

        cpfCnpj = cpfCnpj.replace('.','');
        cpfCnpj = cpfCnpj.replace('.','');
        cpfCnpj = cpfCnpj.replace('-','');
        cpfCnpj = cpfCnpj.replace('/','');

        cep = cep.replace('.','');
        cep = cep.replace('-','');

        setString('inscricaoMunicipal', inscricaoMunicipal);
        setString('cnaeprimario', cnaeprimario);
        setString('cnaesecundario', cnaesecundario);
        setString('senhaPortalClaro', senhaPortalClaro);
        setString('loginPortalClaro', loginPortalClaro);
        setString('valorPlano', valorPlano);
        setBoolean('aptoRenovar', aptoRenovar);
        setCombo('classificacaoCliente', classificacaoCliente);
        setString('dataTerminoContrato', dataTerminoContrato);
        setString('dataInicioContrato', dataInicioContrato);
        setString('numeroNetTv', numeroNetTv);
        setString('numeroNetInternet',numeroNetInternet);
        setString('numeroNetTelefoneFixo', numeroNetTelefoneFixo);
        setString('numeroClaroTelemetria', numeroClaroTelemetria)
        setString('numeroClaroBandaLarga', numeroClaroBandaLarga);
        setString('numeroClaroVoz', numeroClaroVoz);
        setString('celularComplementar',celularComplementar);
        setString('celularAdicional', celularAdicional);
        setString('cpfContatoGestor', cpfContatoGestor);
        setString('nomeContatoGestor', nomeContatoGestor);
        setString('cpfContato',cpfContato);
        setString('idCliente', idCliente);
        setCombo('tipoPessoa', tipoPessoa);
        setString('nome', nome);
        setString('cnpjProposta', cpfCnpj);
        setString('cpfCnpj', cpfCnpj);
        setString('telefoneCliente', telefone);
        setString('celular', celular);
        setString('emailCliente', email);
        setString('ie', ie);
        setCombo('sexo',sexo);
        setString('rg', rg);
        setString('orgaoEmissor', orgaoEmissor);
        setString('estadoEmissor', estadoEmissor);
        setString('dataEmissao', dataEmissao);
        setString('cep', cep);
        setString('rua', rua);
        setString('numero', numero);
        setString('complemento', complemento);
        setString('bairro', bairro);
        setString('cidade', cidade);
        setString('estado', estado);
        setString('nomeContatoCliente', nomeContato);
        setString('telefoneContato', telefoneContato);
        setString('emailContato', emailContato);
        setString('cargoContato', cargoContato);
        setBoolean('ativo', ativo);
        setString('observacao', observacao);
        setCombo('responsavel', responsavel);

        $('#divCliente').show();
    }

    function buscarDadosTela() {

        var idCliente = getString('idCliente');
        var tipoPessoa = getString('tipoPessoa');
        var nome = getString('nome');

        var cnpjProposta = getString('cnpjProposta');
        var telefone = getString('telefoneCliente');
        var email = getString('emailCliente');
        var nomeContato = getString('nomeContatoCliente');

        var celular = getString('celular');
        var ie = getString('ie');
        var sexo = getString('sexo');
        var rg = getString('rg');
        var orgaoEmissor = getString('orgaoEmissor');
        var estadoEmissor = getString('estadoEmissor');
        var dataEmissao = getString('dataEmissao');
        var cep = getString('cep');
        var rua = getString('rua');
        var numero = getString('numero');
        var complemento = getString('complemento');
        var bairro = getString('bairro');
        var cidade = getString('cidade');
        var estado = getString('estado');
        var ativo = getBoolean('ativo');
        var observacao = getString('observacao');

        var telefoneContato = getString('telefoneContato');
        var emailContato = getString('emailContato');
        var cargoContato = getString('cargoContato')
        var cpfContato = getString('cpfContato');
        var nomeContatoGestor = getString('nomeContatoGestor');
        var cpfContatoGestor = getString('cpfContatoGestor');
        var celularAdicional = getString('celularAdicional');
        var celularComplementar = getString('celularComplementar');
        var numeroClaroVoz = getString('numeroClaroVoz');
        var numeroClaroBandaLarga = getString('numeroClaroBandaLarga');
        var numeroClaroTelemetria = getString('numeroClaroTelemetria');
        var numeroNetTelefoneFixo = getString('numeroNetTelefoneFixo');
        var numeroNetInternet = getString('numeroNetInternet');
        var numeroNetTv = getString('numeroNetTv');
        var dataInicioContrato = getString('dataInicioContrato');
        var dataTerminoContrato = getString('dataTerminoContrato');
        var classificacaoCliente = getString('classificacaoCliente');
        var aptoRenovar = getBoolean('aptoRenovar');
        var valorPlano = getString('valorPlano');
        var loginPortalClaro = getString('loginPortalClaro');
        var senhaPortalClaro = getString('senhaPortalClaro');
        var inscricaoMunicipal = getString('inscricaoMunicipal');
        var cnaeprimario = getString('cnaeprimario');
        var cnaesecundario = getString('cnaesecundario');

        var cliente = new Cliente();

        cliente.setId(idCliente);
        cliente.setTipoPessoa(tipoPessoa);
        cliente.setNome(nome);
        cliente.setCpfCnpj(cnpjProposta);
        cliente.setTelefone(telefone);
        cliente.setCelular(celular);
        cliente.setEmail(email);
        cliente.setIe(ie);
        cliente.setSexo(sexo);
        cliente.setRg(rg);
        cliente.setOrgaoEmissor(orgaoEmissor);
        cliente.setEstadoEmissor(estadoEmissor);
        cliente.setDataEmissao(dataEmissao);
        cliente.setCep(cep);
        cliente.setRua(rua);
        cliente.setNumero(numero);
        cliente.setComplemento(complemento);
        cliente.setBairro(bairro);
        cliente.setCidade(cidade);
        cliente.setEstado(estado);
        cliente.setNomeContato(nomeContato);
        cliente.setTelefoneContato(telefoneContato);
        cliente.setEmailContato(emailContato);
        cliente.setCargoContato(cargoContato);
        cliente.setAtivo(ativo);
        cliente.setObservacao(observacao);
        cliente.setCpfContato(cpfContato);
        cliente.setNomeContatoGestor(nomeContatoGestor);
        cliente.setCpfContatoGestor(cpfContatoGestor);
        cliente.setCelularAdicional(celularAdicional);
        cliente.setCelularComplementar(celularComplementar);
        cliente.setNumeroClaroVoz(numeroClaroVoz);
        cliente.setNumeroClaroBandaLarga(numeroClaroBandaLarga);
        cliente.setNumeroClaroTelemetria(numeroClaroTelemetria);
        cliente.setNumeroNetTelefoneFixo(numeroNetTelefoneFixo);
        cliente.setNumeroNetInternet(numeroNetInternet);
        cliente.setNumeroNetTv(numeroNetTv);
        cliente.setDataInicioContrato(dataInicioContrato);
        cliente.setDataTerminoContrato(dataTerminoContrato);
        cliente.setClassificacaoCliente(classificacaoCliente);
        cliente.setAptoRenovar(aptoRenovar);
        cliente.setValorPlano(valorPlano);
        cliente.setLoginPortalClaro(loginPortalClaro);
        cliente.setSenhaPortalClaro(senhaPortalClaro);
        cliente.setResponsavel(ID_USUARIO_LOGADO);
        cliente.setInscricaoMunicipal(inscricaoMunicipal);
        cliente.setCnaeprimario(cnaeprimario);
        cliente.setCnaesecundario(cnaesecundario);

        return cliente;
    }

    function depoisSalvar(retorno) {

        console.log('save cliente '+retorno);
        var idCliente = getString('idCliente');

        if (idCliente === undefined || idCliente === '-' || idCliente === '') {
            daoCliente.buscarMaxCliente(function (jsonMaxCliente) {
                salvarFotos(jsonMaxCliente[0].maxCliente);
                atualizarProposta(jsonMaxCliente[0].maxCliente);
                setString('idCliente', jsonMaxCliente[0].maxCliente);
            });
        } else {
            salvarFotos(idCliente);
            atualizarProposta(idCliente);
        }
        //$('#mnClientes').click();
    }

    function salvarFotos(idCliente) {
        var totalCampos = document.getElementsByName('photos[]').length;
        for (var i = 0; i < totalCampos; i++) {

            var imagem = document.getElementsByName('photos[]')[i].src;
            var id = document.getElementsByName('photos[]')[i].id;
            var mensagem = document.getElementsByName('photos[]')[i].getAttribute('mensagem');;

            var digitalizacao = new Digitalizacao();
            digitalizacao.setId(id);
            digitalizacao.setCliente(idCliente);
            digitalizacao.setDocumento(imagem);
            digitalizacao.setObservacao(mensagem);

            controllerDigitalizacao.save(digitalizacao);
        }
    }

    function atualizarProposta(idCliente) {
        if (getString('idProposta') !== '') {
            daoProposta.atualizarCliente(idCliente,getString('idProposta'),function () {console.log('atualizar proposta')});
            $('#divCliente').show();
        }
    }

    function createdb() {
        daoCliente.create();
    }

    function dropdb() {
        daoCliente.drop();
    }

    function formatarCampos() {
        $('#celular').mask('(99) 99999-9999');
    }

    function buscarCep() {
        if ($.trim($("#cep").val()) !== "") {
            var cep = $("#cep").val();
            cep = cep.replace('-','');
            cep = cep.replace('.','');
            var url = URL_CONSULTACEP+cep;
            $.get(url,
                function (data) {
                    if (data !== -1) {
                        $("#rua").val(data.logradouro);
                        $("#bairro").val(data.bairro);
                        $("#cidade").val(data.cidade);
                        $("#estado").val(data.estado);
                        $('#numero').focus();
                    } else {
                        alert("Endereco nao encontrado");
                    }
                });
        }
    }

    function consultaPessoa() {

        showPageLoading('Consultando dados na receita ....');

        var tipoPessoa  = $('#tipoPessoa').val();
        var cpf_cnpj    = getString('cnpjProposta');

        if (cpf_cnpj === '') return;

        if (tipoPessoa === '0') {
            if (!valida_cpf(cpf_cnpj)) {
                alert('CPF invalido!');
                setString('cnpjProposta','');
            }
            return;
        }

        if (!valida_cnpj(cpf_cnpj)) {
            alert('CNPJ invalido!');
            setString('cnpjProposta','');
            return;
        }

        cpf_cnpj = cpf_cnpj.replace(/[^0-9]/g, '');

        $.ajax({
            type: "POST",
            url: URL_CONSULTASEFAZ,
            data: {
                cnpj: cpf_cnpj
            },
            dataType: 'json',
            success: function (empresa) {

                hidePageLoading();

                if (empresa.status === undefined) {
                    alert("Nao foi possivel encontrar o CNPJ Fornecido na base da Receita Federal");
                    return;
                }

                if (empresa.status === 'ERROR') {
                    $("#divProgress").html('');
                    alert(empresa.message);
                    return;
                }
                var d = new Date(empresa.abertura);
                var date = [
                    d.getFullYear(),
                    ('0' + (d.getMonth() + 1)).slice(-2),
                    ('0' + d.getDate()).slice(-2)
                ].join('-');

                if (empresa.situacao !== 'ATIVA') {
                    if (confirm('Esta empres encontra-se na situacao '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Deseja realmente importar seus dados?')) {
                        atribuirInformacoesCliente(empresa);
                    }
                } else {
                    atribuirInformacoesCliente(empresa);
                }
            }
        });
    }

    function atribuirInformacoesCliente(empresa) {

        var cep = empresa.cep;
        cep = cep.replace('-','');
        cep = cep.replace('.','');

        $('#nome').val(empresa.nome);
        $('#nomeEmpresa').val(empresa.nome);
        $('#telefoneCliente').val(empresa.telefone);
        $('#emailCliente').val(empresa.email);
        $('#cep').val(cep);
        $('#rua').val(empresa.logradouro);
        $('#numero').val(empresa.numero);
        $('#complemento').val(empresa.complemento);
        $('#bairro').val(empresa.bairro);
        $('#cidade').val(empresa.municipio);
        $('#estado').val(empresa.uf);

        if (empresa.atividade_principal.length > 0) $('#cnaeprimario').val(empresa.atividade_principal[0].code);
        if (empresa.atividades_secundarias.length > 0)  $('#cnaesecundario').val(empresa.atividades_secundarias[0].code);

        if (empresa.qsa.length > 0) {
            $('#nomeContato').val(empresa.qsa[0].nome);
            $('#nomeContatoCliente').val(empresa.qsa[0].nome);
        }

    }

    function baixar(successCallback) {
        daoCliente.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoCliente.enviar(successCallback);
    }
};