var ControllerClienteIndicacao = function () {

    var daoPlugin = new DaoPlugin();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarTipoTarefa", function( event ) {
            menu.initialize();
            $('#formAdicionarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarTipoTarefa", function( event ) {
            menu.initialize();
            $('#formEditarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idTipoTarefa = event.delegateTarget.activeElement.attributes['idtipotarefa'].value;
            editar(idTipoTarefa);
        });

        $( document ).on( "pageinit", "#tiposTarefa", function( event ) {
            menu.initialize();
            listarTiposTarefa();
        });
    }

    function editar(idTipoTarefa) {
        daoPlugin.buscaById(idTipoTarefa, popularDadosTela);
    }
    function listarTiposTarefa() {
        daoPlugin.buscarAll(listViewTiposTarefa);
    }

    function listViewTiposTarefa(json) {
        $("#listviewTiposTarefa").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/tipotarefa/editarTipoTarefa.html" data-ajax="true" idTipoTarefa="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewTiposTarefa").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoPlugin.create();
    }

    function dropdb() {
        daoPlugin.drop();
    }

    function save(event) {
        daoPlugin.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idTipoTarefa = getString('idTipoTarefa');
        var descricao = getString("descricao");

        var tipoTarefa = new TipoTarefa();
        tipoTarefa.setDescricao(descricao);
        tipoTarefa.setId(idTipoTarefa);

        return tipoTarefa;
    }

    function popularDadosTela(json) {

        var jsonTipoTarefa = json[0];
        var idTipoTarefa = jsonTipoTarefa.id;
        var descricao = jsonTipoTarefa.descricao;

        setString('idTipoTarefa', idTipoTarefa);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoTipoTarefa.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save tipotarefa '+retorno);
        $('#nmTiposTarefa').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadTipoTarefa/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadTipoTarefa",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(tiposTarefas){
            $.each(tiposTarefas, function(i, item) {
                var tipoTarefa = new TipoTarefa();
                tipoTarefa = daoTipoTarefa.popular0bjeto(tipoTarefa, item);
                daoTipoTarefa.save(tipoTarefa, function () {console.log('tipo tarefa salvo.')});
            });
            callbacks.fire(tiposTarefas);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoTipoTarefa.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadTipoTarefa/',
            data: {
                tiposTarefa: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadTipoTarefa",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });
    }
};