var ControllerCodigoAnatel = function () {

    var daoCodigoAnatel = new DaoCodigoAnatel();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarCodigoAnatel", function( event ) {
            menu.initialize();
            $('#formAdicionarCodigoAnatel').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarCodigoAnatel", function( event ) {
            menu.initialize();
            $('#formEditarCodigoAnatel').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idCodigoAnatel = event.delegateTarget.activeElement.attributes['idcodigoanatel'].value;
            editar(idCodigoAnatel);
        });

        $( document ).on( "pageinit", "#codigosAnatel", function( event ) {
            menu.initialize();
            listarCodigosAnatel();
        });
    }

    function editar(idCodigoAnatel) {
        daoCodigoAnatel.buscaById(idCodigoAnatel, popularDadosTela);
    }
    function listarCodigosAnatel() {
        daoCodigoAnatel.buscarAll(listViewCodigoAnatel);
    }

    function listViewCodigoAnatel(json) {
        $("#listviewCodigosAnatel").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/codigoanatel/editarCodigoAnatel.html" data-ajax="true" idCodigoAnatel="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewCodigosAnatel").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoCodigoAnatel.create();
    }

    function dropdb() {
        daoCodigoAnatel.drop();
    }

    function save(event) {
        daoCodigoAnatel.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idCodigoAnatel = getString('idCodigoAnatel');
        var descricao = getString("descricao");

        var codigoAnatel = new CodigoAnatel();
        codigoAnatel.setDescricao(descricao);
        codigoAnatel.setId(idCodigoAnatel);
        return codigoAnatel;
    }

    function popularDadosTela(json) {

        var jsonCodigoAnatel = json[0];
        var idCodigoAnatel = jsonCodigoAnatel.id;
        var descricao = jsonCodigoAnatel.descricao;

        setString('idCodigoAnatel', idCodigoAnatel);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoCodigoAnatel.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save codigo anatel '+retorno);
        $('#nmCodigoAnatel').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoCodigoAnatel.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoCodigoAnatel.enviar(successCallback);
    }

};