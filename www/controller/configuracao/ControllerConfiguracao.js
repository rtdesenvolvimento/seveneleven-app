var ControllerConfiguracao = function () {

    var daoConfiguracao = new DaoConfiguracao();
    var controllerNotificacao = new ControllerNotificacao();
    var menu = new Menu();

    this.initialize = initialize;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.atualizarConfiguracaoImportada = atualizarConfiguracaoImportada;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#configuracao", function( event ) {
            menu.initialize();
            $('#formConfiguracaoGeral').submit(function (event) {
                event.preventDefault();
                save(event);
            });
            editar(1);
        });
    }

    function editar(idConfiguracao) {
        daoConfiguracao.buscaById(idConfiguracao, popularDadosTela);
    }

    function save(event) {
        daoConfiguracao.save(buscarDadosTela(), depoisSalvar);
    }

    function depoisSalvar(retorno) {
        controllerNotificacao.showAlert('Configuração geral salva com sucesso!', function () {},'Configuração Salva!');
        console.log(retorno);
    }

    function buscarDadosTela() {

        var descricao = getString('descricao');
        var urlAcessoExterno = getString('urlAcessoExterno');
        var alias = getString('alias');
        var sincronizar = getBoolean('sincronizar');
        var tempoSincronizacao = getString('tempoSincronizacao');

        var configuracao = new Configuracao();
        configuracao.setId(1);
        configuracao.setDescricao(descricao);
        configuracao.setAlias(alias);
        configuracao.setUrlAcessoExterno(urlAcessoExterno);
        configuracao.setSincronizar(sincronizar);
        configuracao.setTempoSincronizacao(tempoSincronizacao);

        return configuracao;
    }

    function popularDadosTela(json) {
        var configuracaoJson = json[0];

        var idConfiguracao = configuracaoJson.id;
        var descricao = configuracaoJson.descricao;
        var urlAcessoExterno = configuracaoJson.urlAcessoExterno;
        var alias = configuracaoJson.alias;
        var sincronizar = configuracaoJson.sincronizar;
        var tempoSincronizacao = configuracaoJson.tempoSincronizacao;

        setString('idConfiguracao', idConfiguracao);
        setString('descricao', descricao);
        setString('urlAcessoExterno', urlAcessoExterno);
        setString('alias', alias);
        setBoolean('sincronizar', sincronizar);
        setString('tempoSincronizacao', tempoSincronizacao);
    }

    function atualizarConfiguracaoImportada(idConfiguracao,successCallbackJson ) {
        daoConfiguracao.atualizarConfiguracaoImportada(idConfiguracao, successCallbackJson);
    }

    function createdb() {
        daoConfiguracao.create();
    }

    function dropdb() {
        daoConfiguracao.drop();
    }

};