var ControllerDevice = function () {

    this.initialize = initialize;
    this.fecharAplicativo = fecharAplicativo;
    this.checkConnection = checkConnection;

    function initialize() {
        document.addEventListener("deviceready", onDeviceReady, false);
    }

    function fecharAplicativo() {
        if (navigator.app !== undefined) navigator.app.exitApp();
    }

    function checkConnection() {

        if (navigator.network !== undefined) {

            var networkState = navigator.network.connection.type;

            var states = {};
            states[Connection.UNKNOWN] = 'UNKNOWN';
            states[Connection.ETHERNET] = 'ETHERNET';
            states[Connection.WIFI] = 'WIFI';
            states[Connection.CELL_2G] = 'CELL_2G';
            states[Connection.CELL_3G] = 'CELL_3G';
            states[Connection.CELL_4G] = 'CELL_4G';
            states[Connection.NONE] = 'NONE';

            return states[networkState];
        } else {
            return 'WIFI';
        }
    }

};