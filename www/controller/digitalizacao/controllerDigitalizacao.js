var ControllerDigitalizacao = function () {

    var daoDigitalizacao = new DaoDigitalizacao();

    this.createdb = createdb;
    this.dropdb = dropdb;

    this.save = save;

    this.initialize = initialize;
    this.buscarAllByCliente = buscarAllByCliente;
    this.buscarallByAtividade = buscarallByAtividade;
    this.buscarallByAtividadeCheckout = buscarallByAtividadeCheckout;

    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {}

    function buscarAllByCliente(cliente, successCallback) {
        daoDigitalizacao.buscarAllByCliente(cliente, successCallback );
    }

    function buscarallByAtividade(atividade, successCallback){
        daoDigitalizacao.buscarallByAtividade(atividade, successCallback );
    }

    function buscarallByAtividadeCheckout(atividade, successCallback) {
        daoDigitalizacao.buscarallByAtividadeCheckout(atividade, successCallback );
    }

    function createdb() {
        daoDigitalizacao.create();
    }

    function dropdb() {
        daoDigitalizacao.drop();
    }

    function save(digitalizacao) {
        daoDigitalizacao.save(digitalizacao, depoisSalvar);
    }

    function depoisSalvar(retorno) {
        console.log(retorno);
    }

    function baixar(successCallback) {
        daoDigitalizacao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoDigitalizacao.enviar(successCallback);
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url		: base_url + 'uploadDigitalizacao/' ,
            data    : {
                digitalizacao: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusEnviarDigitalizacao",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(retorno){
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });
    }

};