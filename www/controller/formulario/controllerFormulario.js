var ControllerFormulario = function () {

    var daoFormulario = new DaoFormulario();
    var menu = new Menu();

    this.initialize = initialize;
    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.buscarPerguntaAll = buscarPerguntaAll;

    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#formularioRespostas", function( event ) {
            menu.initialize();
            var idFormulario = event.delegateTarget.activeElement.attributes['idformulario'].value;
            $('#formularioPergunta').attr('idFormulario', idFormulario);
            listarPerguntas(idFormulario);
        });

        $( document ).on( "pageinit", "#formularios", function( event ) {
            menu.initialize();
            listarFormularios();
        });

        $( document ).on( "pageinit", "#adicionarFormulario", function( event ) {
            menu.initialize();
            $('#formAdicionarFormulario').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarFormulario", function( event ) {
            menu.initialize();
            $('#formEditarFormulario').submit(function (event) {
                event.preventDefault();
                save(event);
            });
            var idFormulario = event.delegateTarget.activeElement.attributes['idformulario'].value;
            editar(idFormulario);
        });
    }

    function editar(idFormulario) {
        daoFormulario.buscaById(idFormulario, popularDadosTela);
    }

    function listarPerguntas(idFormulario) {
        daoFormulario.buscarAllPerguntasByFormulario(idFormulario, listarPerguntasView);
    }

    function listarPerguntasView(json) {

        $("#listview-formulario-respostas").empty();

        jQuery.each(json, function(i, item) {

            var id = item.idResposta;
            var pergunta = item.pergunta;
            var obrigatorio = item.obrigatorio;
            var pagina = item.pagina;
            var descricaoFormulario = item.descricaoFormulario;
            var tipoCampo = item.tipoCampo;

            var innerA = '';
            var strObrigatorio = '';

            if (obrigatorio === 1) strObrigatorio = 'Obrigatório';
            if (i==='0') innerA = '<li data-role="list-divider">'+descricaoFormulario+'</li>';

            innerA  += '<li>';
            innerA  += '    <a href="view/formulario/editarPerguntaFormulario.html" data-ajax="true" idResposta="'+id+'" data-role="button">';
            innerA  += '        <h2>'+pergunta+'</h2>';
            innerA  += '        <p><strong>'+tipoCampo+'</strong></p>';
            innerA  += '        <p><strong>'+strObrigatorio+'</strong></p>';
            innerA  += '        <p><strong>Página '+pagina+'</strong></p>';
            innerA  += '    </a>';
            innerA  += '</li>';

            $("#listview-formulario-respostas").append(innerA);
        });

        $('ul').listview().listview('refresh');
    }

    function listarFormularios() {
        daoFormulario.buscarAll(listViewFormularios);
    }

    function listViewFormularios(json) {
        $("#listviewFormularios").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/formulario/editarFormulario.html" data-ajax="true" idFormulario="'+id+'">'+descricao+'' +
                '               </a>' +
                '               <a href="view/formulario/perguntas.html" data-ajax="true" idFormulario="'+id+'" data-role="button">Perguntas</a> '
                '           </li>';
            $("#listviewFormularios").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoFormulario.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoFormulario.create();
    }

    function dropdb() {
        daoFormulario.drop();
    }
    function save() {
        daoFormulario.save(buscarDadosTela(), depoisSalvar );
    }

    function buscarDadosTela () {

        var idFormulario = getString('idFormulario');
        var ativo = getBoolean('ativo')
        var descricao = getString('descricao');

        var formulario = new Formulario();
        formulario.setId(idFormulario);
        formulario.setDescricao(descricao);
        formulario.setAtivo(ativo);

        return formulario;
    }

    function popularDadosTela(json) {
        var jsonFormulario = json[0];

        var idFormulario = jsonFormulario.id;
        var descricao = jsonFormulario.descricao;

        setString('idFormulario', idFormulario);
        setString('descricao', descricao);
    }

    function depoisSalvar(retorno) {
        console.log('save  formulario '+retorno);
        $('#nmFormularios').click();
    }

    function buscarPerguntaAll(id,pagina, successCallbackJson) {
        daoFormulario.buscarPerguntaAll(id, pagina, successCallbackJson);
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadFormulario/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadFormulario",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(formularios){
            $.each(formularios, function(i, item) {
                var formulario = new Formulario();
                formulario = daoFormulario.popular0bjeto(formulario, item);
                daoFormulario.save(formulario, function () {console.log('formulario salvo.')});
            });
            callbacks.fire(formularios);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoFormulario.buscarAll(function (json) {
            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadFormulario/',
            data: {
                formularios: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadFormulario",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }
};