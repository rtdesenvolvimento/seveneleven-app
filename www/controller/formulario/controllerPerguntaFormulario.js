var ControllerPerguntaFormulario = function () {

    var daoPerguntaFormulario = new DaoPerguntaFormulario();
    var controllerFormulario = new ControllerFormulario();
    var controllerPerguntaFormularioOpcao = new ControllerPerguntaFormularioOpcao();
    var formulaFactory = new FormularioFactory();
    var controllerNotificacao = new ControllerNotificacao();

    this.initialize = initialize;
    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.totalPaginas = totalPaginas;
    this.buscaById = buscaById;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#editarPerguntaFormulario", function( event ) {
            $('#formEditarPerguntaFormulario').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPerguntaFormulario').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta Pergunta?';
                var title = 'Excluir Pergunta';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluirPergunta();
                    }
                } , title );
            });

            novo();
            var idResposta = event.delegateTarget.activeElement.attributes['idresposta'].value;
            editarResposta(idResposta);
        });

        $( document ).on( "pageinit", "#adicionarPerguntaFormulario", function( event ) {
            $('#formAdicionarPerguntaFormulario').submit(function (event) {
                event.preventDefault();
                save(event);
            });
            novo();
        });
    }

    function excluirPergunta() {
        var idPergunta = getString('idPergunta');
        daoPerguntaFormulario.deletar(idPergunta);
        controllerPerguntaFormularioOpcao.deletarAllByPergunta(idPergunta);
        $('#nmFormularios').click();
    }

    function editarResposta(idResposta) {
         daoPerguntaFormulario.buscaById(idResposta,popularDadosTela);
    }

    function novo() {

        $('#adicionarNovaOpcao').click(function (event) {
            controllerPerguntaFormularioOpcao.adicionarNovaOpcao();
        });

        $('#tipoCampo').blur(function (event) {
            if ($(this).val() === 'checkbox' || $(this).val() === 'radio' || $(this).val()=== 'select') {
                $('#li-adicionar-nova-opcao').show();
            } else {
                $('#li-adicionar-nova-opcao').hide();
                formulaFactory.createFormulario('listview-formulario-opcoes');
                formulaFactory.destroy();
            }
        });

        $('#formulario').blur(function (event) {
           atribuirOrdem();
        });

        $('#perguntaFormularioAnterior').click(function (event) {
           anterior();
        });

        controllerFormulario.buscarAll(buscarComboFormulario);
    }

    function atribuirOrdem() {
        var formulario = getString('formulario');
        daoPerguntaFormulario.buscaMaxOrdemPergunta(formulario, function (json) {
            var jsonOrdem = json[0];
            if (jsonOrdem.ordem === null) {
                setString('ordem', 1);
            } else {
                setString('ordem', jsonOrdem.ordem + 1);
            }
        });
    }

    function buscarAll(successCallbackJson) {
        daoPerguntaFormulario.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoPerguntaFormulario.create();
    }

    function dropdb() {
        daoPerguntaFormulario.drop();
    }

    function save() {
        daoPerguntaFormulario.save(buscarDadosTela(), proximaPergunta);
    }

    function buscarDadosTela () {

        var idPergunta = getString('idPergunta');
        var pergunta = getString('pergunta');
        var tipoCampo = getString('tipoCampo');
        var ativo = getBoolean('ativo')
        var descricao = getString('descricao');
        var obrigatorio = getBoolean('obrigatorio');
        var formulario = getString('formulario');
        var peso = getString('peso');
        var anterior = getString('anterior');
        var proxima = getString('proxima');
        var pagina = getString('pagina');
        var ordem = getString('ordem');
        var alias = '';

        var perguntaFormulario = new PerguntaFormulario();
        perguntaFormulario.setId(idPergunta);
        perguntaFormulario.setDescricao(descricao);
        perguntaFormulario.setAtivo(ativo);
        perguntaFormulario.setPergunta(pergunta);
        perguntaFormulario.setTipoCampo(tipoCampo);
        perguntaFormulario.setObrigatorio(obrigatorio);
        perguntaFormulario.setFormulario(formulario);
        perguntaFormulario.setPeso(peso);
        perguntaFormulario.setAlias(alias);
        perguntaFormulario.setAnterior(anterior);
        perguntaFormulario.setProxima(proxima);
        perguntaFormulario.setPagina(pagina);
        perguntaFormulario.setOrdem(ordem);

        return perguntaFormulario;
    }

    function proximaPergunta(retorno) {
        if (!retorno) {alert('Falha ao salvar a pergunta');return;}

        daoPerguntaFormulario.buscarUltimaPergunta(irNovaPergunta);
    }

    function buscaById(idResposta) {
        daoPerguntaFormulario.buscaById(idResposta,popularDadosTela);
    }

    function popularDadosTela(json) {

        var jsonPergunta = json[0];

        var idPergunta = jsonPergunta.id;
        var pergunta = jsonPergunta.pergunta;
        var tipoCampo = jsonPergunta.tipoCampo;
        var ativo = jsonPergunta.ativo;
        var descricao = jsonPergunta.descricao;
        var obrigatorio = jsonPergunta.obrigatorio;
        var formulario = jsonPergunta.formulario;
        var peso = jsonPergunta.peso;
        var anterior = jsonPergunta.anterior;
        var proxima = jsonPergunta.proxima;
        var pagina = jsonPergunta.pagina;
        var ordem = jsonPergunta.ordem;

        setString('idPergunta', idPergunta);
        setString('pergunta', pergunta);
        setCombo('tipoCampo', tipoCampo);
        setBoolean('ativo', ativo);
        setString('descricao', descricao);
        setBoolean('obrigatorio', obrigatorio);
        setCombo('formulario', formulario);
        setString('peso', peso);
        setString('anterior', anterior);
        setString('proxima', proxima);
        setString('pagina', pagina);
        setString('ordem', ordem);

        editarRespostasOpcoes(idPergunta, tipoCampo);

    }

    function editarRespostasOpcoes(idPergunta, tipoCampo) {
        if (tipoCampo === 'checkbox' ||
            tipoCampo === 'radio' ||
            tipoCampo === 'select') {

            $('#li-adicionar-nova-opcao').show();

            controllerPerguntaFormularioOpcao.buscarAllByPergunta(idPergunta, function (json) {
                formulaFactory.createFormulario('listview-formulario-opcoes');
                jQuery.each(json, function (i, item) {
                    var opcao = item.opcao;
                    formulaFactory.createInputFactory(item.id, 'opcao[]', 'Opção', 'text', true, false, opcao, '');
                });

            });
        }
    }

    function irNovaPergunta(json) {
        var jsonUltimaPergunta = json[0];

        var formulario = getString('formulario');
        var pergunta = jsonUltimaPergunta.ultimaPergunta;

        controllerPerguntaFormularioOpcao.save(formulario, pergunta);

        //controllerNotificacao.showAlert('Pergunta salva com sucesso!',function () {},'Pergunta Salva','');

        var ordem = getInteger('ordem') + 1;
        daoPerguntaFormulario.buscaByOrdem(ordem,formulario, function (json) {
            var jsonPerguntaFormulario = json[0];

            if (jsonPerguntaFormulario !== undefined) {
                limparOpcoes();
                editarResposta(jsonPerguntaFormulario.id);
            } else {
                limparFormulario();
                atribuirOrdem();
            }
        });
    }

    function anterior() {
        var ordem = getInteger('ordem') - 1;
        var formulario = getString('formulario');

        daoPerguntaFormulario.buscaByOrdem(ordem,formulario, function (json) {
            var jsonPerguntaFormulario = json[0];
            if (jsonPerguntaFormulario !== undefined) {
                limparOpcoes();
                editarResposta(jsonPerguntaFormulario.id);
            } else {
                controllerNotificacao.showAlert('Nenhuma pergunta anterior!',function () {},'Nenhuma pergunta anterior!','');
            }
        });
    }

    function limparFormulario() {
        $('#formulario').attr('disabled', true);
        setString('pergunta','');
        setString('tipoCampo','');
        setBoolean('ativo',1);
        setString('descricao','');
        setBoolean('obrigatorio',1);
        setCombo('tipoCampo','text');
        setString('peso','');
        setString('ordem', '');
        setString('idPergunta', '');
        limparOpcoes();
    }

    function limparOpcoes() {
        $('#li-adicionar-nova-opcao').hide();
        formulaFactory.createFormulario('listview-formulario-opcoes');
        formulaFactory.destroy();
    }
    function buscarComboFormulario(json) {
        preencherComboAtributos('formulario', json);
        var idFormulario = $('#formularioPergunta').attr('idFormulario');
        if (idFormulario !== undefined)  { setCombo('formulario', idFormulario); $('#formulario').attr('disabled', true); atribuirOrdem();}
    }

    function totalPaginas(formulario, successCallbackJson) {
        daoPerguntaFormulario.totalPaginas(formulario, successCallbackJson);
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadPerguntasFormulario/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadPerguntaFormulario",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(perguntasFormulario){
            $.each(perguntasFormulario, function(i, item) {
                var perguntaFormulario = new PerguntaFormulario();
                perguntaFormulario = daoPerguntaFormulario.popular0bjeto(perguntaFormulario, item);
                daoPerguntaFormulario.save(perguntaFormulario, function () {console.log('perguntas formulario salvo.')});
            });
            callbacks.fire(perguntasFormulario);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoPerguntaFormulario.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadPerguntasFormulario/',
            data: {
                perguntasFormulario: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadPerguntasFormulario",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }
};