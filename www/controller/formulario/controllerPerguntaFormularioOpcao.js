var ControllerPerguntaFormularioOpcao = function () {

    var daoPerguntaFormularioOpcao = new DaoPerguntaFormularioOpcao()
    var formulaFactory = new FormularioFactory();

    this.initialize = initialize;
    this.buscarAll = buscarAll;
    this.save = save;
    this.deletarAllByPergunta = deletarAllByPergunta;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.adicionarNovaOpcao = adicionarNovaOpcao;
    this.buscarAllByPergunta = buscarAllByPergunta;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {}

    function adicionarNovaOpcao() {
        formulaFactory.createFormulario('listview-formulario-opcoes');
        formulaFactory.createInputFactory('', 'opcao[]', 'Opção', 'text', true, false, '', '');
    }
    
    function deletarAllByPergunta(idPergunta) {
        daoPerguntaFormularioOpcao.deletarAllByPergunta(idPergunta);
    }

    function buscarAll(successCallbackJson) {
        daoPerguntaFormularioOpcao.buscarAll(successCallbackJson);
    }

    function buscarAllByPergunta(pergunta, successCallbackJson) {
        daoPerguntaFormularioOpcao.buscarAllByPergunta(pergunta, successCallbackJson);
    }

    function createdb() {
        daoPerguntaFormularioOpcao.create();
    }

    function dropdb() {
        daoPerguntaFormularioOpcao.drop();
    }
    function save(formulario, pergunta) {
        var totalCampos = document.getElementsByName('opcao[]').length;
        for(var i=0;i<totalCampos;i++) {
            var opcao = document.getElementsByName('opcao[]')[i].value;
            var id =  document.getElementsByName('opcao[]')[i].id;
            daoPerguntaFormularioOpcao.save(buscarDadosTela(formulario, pergunta,opcao, id), depoisSalvar );
        }
    }

    function buscarDadosTela (formulario, pergunta, opcao, id) {

        var tipoCampo = 'text';
        var ativo = 1;
        var descricao = opcao;
        var obrigatorio = 0;
        var peso =  0;

        var perguntaFormularioOpcao = new PerguntaFormularioOpcao();
        perguntaFormularioOpcao.setId(id);
        perguntaFormularioOpcao.setDescricao(descricao);
        perguntaFormularioOpcao.setAtivo(ativo);
        perguntaFormularioOpcao.setPergunta(pergunta);
        perguntaFormularioOpcao.setTipoCampo(tipoCampo);
        perguntaFormularioOpcao.setObrigatorio(obrigatorio);
        perguntaFormularioOpcao.setFormulario(formulario);
        perguntaFormularioOpcao.setPeso(peso);
        perguntaFormularioOpcao.setOpcao(opcao);

        return perguntaFormularioOpcao;
    }

    function depoisSalvar(retorno) {
        console.log('save formulario opçcao '+retorno);
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadPerguntasFormularioOpcao/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadPerguntaFormularioOpcao",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(perguntasFormularioOpcao){
            $.each(perguntasFormularioOpcao, function(i, item) {
                var perguntaFormularioOpcao = new PerguntaFormularioOpcao();
                perguntaFormularioOpcao = daoPerguntaFormularioOpcao.popular0bjeto(perguntaFormularioOpcao, item);
                daoPerguntaFormularioOpcao.save(perguntaFormularioOpcao, function () {console.log('perguntas formulario opcao salvo.')});
            });
            callbacks.fire(perguntasFormularioOpcao);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoPerguntaFormularioOpcao.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadPerguntasFormularioOpcao/',
            data: {
                perguntasFormularioOpcao: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadPerguntaFormulariOpcao",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }
};