var ControllerFranquia = function () {

    var daoFranquia = new DaoFranquia();
    var daoPlano = new DaoPlano();
    var controllerPassaporte = new ControllerPassaporte();
    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscarAllByPlano = buscarAllByPlano;
    this.buscarById = buscarById;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarFranquia", function( event ) {
            menu.initialize();
            montarComboBox();
            $('#formAdicionarFranquia').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarFranquia", function( event ) {
            menu.initialize();
            montarComboBox();
            $('#formEditarFranquia').submit(function (event) {
                event.preventDefault();
                save(event);
            });
            var idFranquia = event.delegateTarget.activeElement.attributes['idfranquia'].value;
            editar(idFranquia);
        });

        $( document ).on( "pageinit", "#franquias", function( event ) {
            PLANO = event.delegateTarget.activeElement.attributes['idplano'].value;
            menu.initialize();
            listarFranquias();
        });
    }

    function montarComboBox() {

        daoPlano.buscarAll(function (json) {
           preencherComboAtributos('plano', json);
           setCombo('plano', PLANO);
        });

        controllerPassaporte.buscarAll(function (json) {
           preencherComboAtributos('passaporte', json);
        });
    }

    function listarFranquias() {
        daoFranquia.buscarAllByPlano(PLANO, listViewFranquias);
    }

    function listViewFranquias(json) {
        $("#listviewFranquias").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var franquia = item.franquia;
            var valor = item.valor;
            var pontos = item.pontos;
            var bonus = item.bonus;

            var innerA = '  <li>' +
                '               <a href="view/plano/franquia/editarFranquia.html" data-ajax="true" idFranquia="'+id+'">' +
                '               <h3>'+franquia+'</h3>'+
                '               <p><strong>Valor (R$) '+valor+'</strong></p>'+
                '               <p><strong>Pontos '+pontos+'</strong></p>'+
                '               <p><strong>Bônus '+bonus+'</strong></p>'+
                '               </a>' +
                '           </li>';
            $("#listviewFranquias").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoFranquia.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoFranquia.create();
    }

    function dropdb() {
        daoFranquia.drop();
    }

    function save() {
        daoFranquia.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idCargo) {
        daoFranquia.buscaById(idCargo, popularDadosTela);
    }

    function buscarAllByPlano(idPlano, successCallback) {
        daoFranquia.buscarAllByPlano(idPlano,successCallback);
    }

    function buscarById(id, successCallback) {
        daoFranquia.buscaById(id,successCallback);
    }

    function buscarDadosTela() {

        var idFranquia = getString('idFranquia');
        var plano = getString("plano");
        var franquiaS = getString('franquia');
        var bonus = getString('bonus');
        var pontos = getString('pontos');
        var totalInternet = getString("totalInternet");
        var valor = getString("valor");
        var isPossuiMobilidade = getBoolean('isPossuiMobilidade');
        var valorMobilidade = getString('valorMobilidade');
        var isPossuiRedesSociais = getBoolean('isPossuiRedesSociais');
        var valorRedesSociais = getString('valorRedesSociais');
        var passaporte = getString('passaporte');
        var valorPassaporte = getString('valorPassaporte');
        var ativo = getBoolean('ativo');

        var franquia = new Franquia();
        franquia.setId(idFranquia);
        franquia.setPlano(plano);
        franquia.setFranquia(franquiaS);
        franquia.setBonus(bonus);
        franquia.setPontos(pontos);
        franquia.setTotalInternet(totalInternet);
        franquia.setValor(valor);
        franquia.setIsPossuiMobilidade(isPossuiMobilidade);
        franquia.setValorMobilidade(valorMobilidade);
        franquia.setIsPossuiRedesSociais(isPossuiRedesSociais);
        franquia.setValorRedesSociais(valorRedesSociais);
        franquia.setPassaporte(passaporte);
        franquia.setValorPassaporte(valorPassaporte);
        franquia.setAtivo(ativo);
        return franquia;
    }

    function popularDadosTela(json) {
        var jsonFranquia = json[0];

        var idFranquia = jsonFranquia.id;
        var plano = jsonFranquia.plano;
        var franquiaS = jsonFranquia.franquia;
        var bonus = jsonFranquia.bonus;
        var pontos = jsonFranquia.pontos;
        var totalInternet = jsonFranquia.totalInternet;
        var valor = jsonFranquia.valor;
        var isPossuiMobilidade = jsonFranquia.isPossuiMobilidade;
        var valorMobilidade = jsonFranquia.valorMobilidade;
        var isPossuiRedesSociais = jsonFranquia.isPossuiRedesSociais;
        var valorRedesSociais = jsonFranquia.valorRedesSociais;
        var passaporte = jsonFranquia.passaporte;
        var valorPassaporte = jsonFranquia.valorPassaporte;
        var ativo =  jsonFranquia.ativo;

        setString('idFranquia', idFranquia);
        setCombo('plano', plano);
        setString('franquia', franquiaS);
        setString('bonus', bonus);
        setString('pontos', pontos);
        setString('totalInternet', totalInternet);
        setString('valor', valor);
        setBoolean('isPossuiMobilidade', isPossuiMobilidade);
        setString('valorMobilidade', valorMobilidade);
        setBoolean('isPossuiRedesSociais', isPossuiRedesSociais);
        setString('valorRedesSociais', valorRedesSociais);
        setCombo('passaporte', passaporte);
        setString('valorPassaporte', valorPassaporte);
        setBoolean('ativo', ativo);
    }

    function depoisSalvar(retorno) {
        console.log('save franquia '+retorno);
        $('#nmPlanos').click();
    }
    
    function baixar(successCallback) {
        daoFranquia.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoFranquia.enviar(successCallback);
    }
};