var ControllerGeoLocalizacao = function () {

    var watchID = null;

    var daoGeoLocalizacao = new DaoGeoLocalizacao();

    this.initialize = initialize;

    this.initialize = initialize;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.getCurrentPosition = getCurrentPosition;
    this.watchPosition = watchPosition;
    this.clearWatch = clearWatch;

    function initialize() {}

    function getCurrentPosition(onSuccess, onError) {
        if (navigator.geolocation === undefined) {return false;}
        else {
            var options =  { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
            return true;
        }
    }
    
    function watchPosition(onSucess, onError, millisegundos) {
        if (navigator.geolocation === undefined) {return false;}
        else {
            var options = { frequency: millisegundos, maximumAge: 3000, timeout: 5000, enableHighAccuracy: true  };
            watchID = navigator.geolocation.watchPosition(onSucess, onError, options);
            return true;
        }
    }

    function clearWatch() {
        if (watchID != null) {
            navigator.geolocation.clearWatch(watchID);
            watchID = null;
        }
    }

    function createdb() {
        daoGeoLocalizacao.create();
    }

    function dropdb() {
        daoGeoLocalizacao.drop();
    }
};