var ControllerLogin = function () {

    var controllerUsuario = new ControllerUsuario();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerDevice = new ControllerDevice();

    var daoConfiguracao = new DaoConfiguracao();

    this.initialize = initialize;

    function initialize() {
        eventos();
    }

    function eventos() {
        $('#logar').click(function (event) {

            var id = getString('imput_login');
            var senha = getString('imput_senha');

            if (id === '') {controllerNotificacao.showAlert('Usuário obrigatório', function () {}); return;};
            if (senha === '') {controllerNotificacao.showAlert('Senha obrigatória', function () {}); return;};

            controllerUsuario.logar(id, senha, function (json) {

                if (json[0] === undefined)  {controllerNotificacao.showAlert('Senha inválida!', function () {}); return;};

                json        = json[0];
                var cargo   = json.cargo;

                if (cargo === 1) IS_ADMINISTRADOR = true;
                else if(cargo === 2) IS_ADMINISTRADOR = false;

                ID_USUARIO_LOGADO   = json.id;
                TELEFONE_USUARIO    = json.celular;
                EMAIL_USUARIO       = json.email;

                importarDados();
            });

        });

        $( document ).on( "pageinit", "#login", function( event ) {
            controllerUsuario.buscarAll(function (json) {
                controllerUsuario.preencherCombo('imput_login', json);
            });
        });

        controllerUsuario.buscarAll(function (json) {
            controllerUsuario.preencherCombo('imput_login', json);
        });

        $('#mnsInstall').click(function () {

            showPageLoading('Iniciando importação dos seus dados do servidor....');
            var controllerSincronizacao = new ControllerSincronizacao();
            controllerSincronizacao.drop();
            controllerSincronizacao.create();
            showPageLoading('Atualizando banco de dados....');

            setTimeout(function () {
                controllerSincronizacao.sincronizarUsuario();
            }, 15000);
        });

        $('#fechar_aplicativo_login').click(function (event) {
            controllerDevice.fecharAplicativo();
        });
    }

    function importarDados() {

        daoConfiguracao.buscarByCelular(function (json) {

            if (Object.keys(json).length === 0) {
                $.mobile.changePage("#home");
                return;
            }

            showPageLoading('Iniciando importação dos seus dados do servidor....');

            jQuery.each(json, function(i, item) {

                if (item.importacao === 0) {
                    var controllerSincronizacao = new ControllerSincronizacao();
                    BAIXAR_ALL = true;
                    controllerSincronizacao.sincronizar(item.id);

                }
            });
        });
    }
};