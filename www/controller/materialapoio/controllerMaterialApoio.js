var ControllerMaterialApoio = function () {

    var URL_PDF = 'http://seveneleven.resultaweb.com.br/pdf/';
    var menu = new Menu();

    this.initialize = initialize;


    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#materialapoio", function( event ) {
            menu.initialize();
        });

        $( document ).on( "pageinit", "#book", function( event ) {
            menu.initialize();
            listarBook();
        });

        $( document ).on( "pageinit", "#tabelapreco", function( event ) {
            menu.initialize();
            listarTabelapreco();
        });

        $( document ).on( "pageinit", "#apoioaparelhos", function( event ) {
            menu.initialize();
            listarApoioaparelhos();
        });

        $( document ).on( "pageinit", "#novascidades4g", function( event ) {
            menu.initialize();
            novasCidades4gMax();
        });
    }

    function novasCidades4gMax() {
        $("#listarnovascidades4g").empty();

        addLink('listarnovascidades4g', 'cidades/grandes_rios.jpeg', 'Grandes Rios/PR 43');

        addLink('listarnovascidades4g', 'cidades/garopaba.jpeg', 'Garopaba/SC 48');
        addLink('listarnovascidades4g', 'cidades/nova_trento.jpeg', 'Nova Trento/SC 48');
        addLink('listarnovascidades4g', 'cidades/passo_de_torres.jpeg', 'Passo de Torres/SC 48');
        addLink('listarnovascidades4g', 'cidades/jaguaruna.jpeg', 'Jaguaruna/SC 48');
        addLink('listarnovascidades4g', 'cidades/sideropolis.jpeg', 'Siderópolis/SC 48');

        addLink('listarnovascidades4g', 'cidades/abelardo_luz.jpeg', 'Aberlardo Luz/SC 49');
        addLink('listarnovascidades4g', 'cidades/bombinhas.jpeg', 'Bom Jesus/SC 49');
        addLink('listarnovascidades4g', 'cidades/capaoalto.jpeg', 'Capão Alto/SC 49');
        addLink('listarnovascidades4g', 'cidades/dionisio_cerqueira.jpeg', 'Dionísio Cerqueira/SC 49');
        addLink('listarnovascidades4g', 'cidades/ipira.jpeg', 'Ipira/SC 49');
        addLink('listarnovascidades4g', 'cidades/luzerna.jpeg', 'Luzerna/SC 49');
        addLink('listarnovascidades4g', 'cidades/piratuba.jpeg', 'Piratuba/SC 49');
        addLink('listarnovascidades4g', 'cidades/sao_jose_do_cerrito.jpeg', 'São Jose do Cerrito/SC 49');
        addLink('listarnovascidades4g', 'cidades/saudades.jpeg', 'Saudades Redondo/SC 49');
        addLink('listarnovascidades4g', 'cidades/joacaba.jpeg', 'Joaçaba/SC 49');
        addLink('listarnovascidades4g', 'cidades/irani.jpeg', 'Irani/SC 49');

        addLink('listarnovascidades4g', 'cidades/barra_velha.jpeg', 'Barra Velho/SC 47');
        addLink('listarnovascidades4g', 'cidades/bombinhas.jpeg', 'Bombinhas/SC 47');
        addLink('listarnovascidades4g', 'cidades/caboriu.jpeg', 'Comboriú/SC 47');
        addLink('listarnovascidades4g', 'cidades/dom_pedrito.jpeg', 'Dom Pedrito/RS 47');
        addLink('listarnovascidades4g', 'cidades/itapema.jpeg', 'Itapema/SC 47');
        addLink('listarnovascidades4g', 'cidades/indaial.jpeg', 'Indaial/SC 47');
        addLink('listarnovascidades4g', 'cidades/pouso_redondo.jpeg', 'Pouso Redondo/SC 47');
        addLink('listarnovascidades4g', 'cidades/ibirama.jpeg', 'Ibirama/SC 47');
        addLink('listarnovascidades4g', 'cidades/beneditonovo.jpeg', 'Benedito Novo/SC 47');
        addLink('listarnovascidades4g', 'cidades/presidentegetulio.jpeg', 'Presidente Getúlio/SC 47');
        addLink('listarnovascidades4g', 'cidades/apiuna.jpeg', 'Apiúna/SC 47');

        addLink('listarnovascidades4g', 'cidades/pelotas.jpeg', 'Pelotas/RS 53');
        addLink('listarnovascidades4g', 'cidades/rio_grande.jpeg', 'Rio Grande/RS 53');
        addLink('listarnovascidades4g', 'cidades/santa_vitoria_do_palmar.jpeg', 'Santa Vitória do Palmar/RS 53');

    }

    function listarApoioaparelhos() {
        $("#listarapoioaparelhos").empty();

        addLink('listarapoioaparelhos', 'aparelhos/oferta1.jpeg', 'Oferta 1');
        addLink('listarapoioaparelhos', 'aparelhos/oferta2.jpeg', 'Oferta 2');
        addLink('listarapoioaparelhos', 'aparelhos/oferta3.jpeg', 'Oferta 3');
        addLink('listarapoioaparelhos', 'aparelhos/oferta4.jpeg', 'Oferta 4');

    }

    function listarTabelapreco() {
        $("#listartabelapreco").empty();
        addLink('listartabelapreco', 'TABELA_PME_ABRIL_2019_v2.xlsx', 'Tabela de Preços');

    }

    function listarBook() {
        $("#listarbook").empty();
        addLink('listarbook', 'BOOK-DE-CONVERGENCIA-CLARO-EMPRESAS ABRIL-NET-CLARO-BAIXA.pdf', 'Book de Convergência Claro Empresas');
        addLink('listarbook', 'F09_CRO_PME_BOOK_CLAREANDOS.PDF', 'Book Claro Empresas');
    }

    function addLink(album, url, descricao) {
        url = URL_PDF + url;
        var innerA = '  <li>' +
            '               <a href="'+url+'" target="_blank" data-ajax="true">'+descricao+'' +
            '               </a>' +
            '           </li>';
        $("#"+album).append(innerA);
        $('ul').listview().listview('refresh');
    }
};