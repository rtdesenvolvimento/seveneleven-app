var ControllerMeioDivulgacao = function () {

    var daoMeioDivulgacao = new DaoMeioDivulgacao();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarMeioDivulgacao", function( event ) {
            menu.initialize();
            $('#formAdicionarMeioDivulgacao').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarMeioDivulgacao", function( event ) {
            menu.initialize();
            $('#formEditarMeioDivulgacao').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idMeioDivulgacao = event.delegateTarget.activeElement.attributes['idmeiodivulgacao'].value;
            editar(idMeioDivulgacao);
        });

        $( document ).on( "pageinit", "#meiosdivulgacao", function( event ) {
            debugger;
            menu.initialize();
            listarMeiosDivulgacao();
        });
    }

    function editar(idMeioDivulgacao) {
        daoMeioDivulgacao.buscaById(idMeioDivulgacao, popularDadosTela);
    }

    function listarMeiosDivulgacao() {
        daoMeioDivulgacao.buscarAll(listViewMeioDivulgacao);
    }

    function listViewMeioDivulgacao(json) {

        debugger;
        $("#listviewMeioDivulgacao").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/meioDivulgacao/editarMeioDivulgacao.html" data-ajax="true" idMeioDivulgacao="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewMeioDivulgacao").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoMeioDivulgacao.create();
    }

    function dropdb() {
        daoMeioDivulgacao.drop();
    }

    function save(event) {
        daoMeioDivulgacao.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idMeioDivulgacao = getString('idMeioDivulgacao');
        var descricao = getString("descricao");

        var meioDivulgacao = new MeioDivulgacao();
        meioDivulgacao.setDescricao(descricao);
        meioDivulgacao.setId(idMeioDivulgacao);

        return meioDivulgacao;
    }

    function popularDadosTela(json) {

        var jsonMeioDivulgacao = json[0];
        var idMeioDivulgacao = jsonMeioDivulgacao.id;
        var descricao = jsonMeioDivulgacao.descricao;

        setString('idMeioDivulgacao', idMeioDivulgacao);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoMeioDivulgacao.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save meio divulgacao '+retorno);
        $('#nmMeioDivugacao').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoMeioDivulgacao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoMeioDivulgacao.enviar(successCallback);
    }

};