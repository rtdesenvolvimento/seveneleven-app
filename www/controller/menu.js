var Menu = function () {

    this.initialize = initialize;

    function initialize() {
        eventos();
        mostrarMenu();
    }

    function eventos() {}

    function mostrarMenu() {

        $('.exibirMenu').load('menu.html', function (event) {
            $('.ui-page').trigger('create');
            $('.ui-collapsible').css('margin','0 0em');

            limparAutoSave();
            exibirUsuario();
            sincronizar();
        });
    }

    function limparAutoSave() {
        ///LIMPANDO CACHE DO AUTOSAVE
        jQuery.each(AUTOSAVE, function(i, item) {
            clearInterval(item);
        });
        AUTOSAVE = [];
    }

    function exibirUsuario() {
        var daoUsuario = new DaoUsuario();
        daoUsuario.buscaById(ID_USUARIO_LOGADO, function (json) {
           var jsonUsuario = json[0];

           if (jsonUsuario !== undefined) {
               var nomeCompleto = jsonUsuario.nomeCompleto;
               var cargo = jsonUsuario.cargo;
               $('.usuario').html(nomeCompleto + '<br/>');
               exibirCargo(cargo);
           }
        });
    }

    function exibirCargo(id) {
        var daoCargo = new DaoCargo();
        daoCargo.buscaById(id, function (json) {
            var jsonCargo = json[0];
            var descricao = jsonCargo.descricao;
            $('.cargo').html(descricao);
        });
    }

    function sincronizar() {
        $('.mnSincronizacao').click(function () {
            if (TELEFONE_USUARIO !== '') {
                var controllerSincronizacao = new ControllerSincronizacao();
                controllerSincronizacao.sincronizar();
            } else {
                alert('Usuário não logado!');
            }
        });
    }

};