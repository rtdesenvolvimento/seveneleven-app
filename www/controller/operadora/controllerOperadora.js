var ControllerOperadora = function () {

    var daoOperadora = new DaoOperadora();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarOperadora", function( event ) {
            menu.initialize();
            $('#formAdicionarOperadora').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarOperadora", function( event ) {
            menu.initialize();
            $('#formEditarOperadora').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idOperadora = event.delegateTarget.activeElement.attributes['idoperadora'].value;
            editar(idOperadora);
        });

        $( document ).on( "pageinit", "#operadoras", function( event ) {
            menu.initialize();
            listarOperadora();
        });
    }

    function editar(idOperadora) {
        daoOperadora.buscaById(idOperadora, popularDadosTela);
    }
    function listarOperadora() {
        daoOperadora.buscarAll(listViewOperadora);
    }

    function listViewOperadora(json) {
        $("#listviewOperadora").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/operadora/editarOperadora.html" data-ajax="true" idOperadora="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewOperadora").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoOperadora.create();
    }

    function dropdb() {
        daoOperadora.drop();
    }

    function save(event) {
        daoOperadora.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idOperadora = getString('idOperadora');
        var descricao = getString("descricao");

        var operadora = new Operadora();
        operadora.setDescricao(descricao);
        operadora.setId(idOperadora);

        return operadora;
    }

    function popularDadosTela(json) {

        var jsonOperadora = json[0];
        var idOperadora = jsonOperadora.id;
        var descricao = jsonOperadora.descricao;

        setString('idOperadora', idOperadora);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoOperadora.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save operadora '+retorno);
        $('#nmOperadora').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoOperadora.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoOperadora.enviar(successCallback);
    }

};