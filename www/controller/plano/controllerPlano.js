var ControllerPlano = function () {

    var daoPlano = new DaoPlano();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPlano", function( event ) {
            menu.initialize();
            $('#formAdicionarPlano').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPlano", function( event ) {
            menu.initialize();
            $('#formEditarPlano').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idPlano = event.delegateTarget.activeElement.attributes['idplano'].value;
            editar(idPlano);
        });

        $( document ).on( "pageinit", "#planos", function( event ) {
            menu.initialize();
            listarPlanos();
        });
    }

    function editar(idPlano) {
        daoPlano.buscaById(idPlano, popularDadosTela);
    }
    function listarPlanos() {
        daoPlano.buscarAll(listViewPlanos);
    }

    function listViewPlanos(json) {
        $("#listviewPlanos").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  ' +
                '   <li>' +
                '       <a href="view/plano/franquia/franquias.html" data-ajax="true" idPlano="'+id+'" data-role="button">'+descricao+'</a>'+
                '       <a href="view/plano/editarPlano.html" data-ajax="true" idPlano="'+id+'">'+descricao+'</a>' +
                '   </li>';
            $("#listviewPlanos").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoPlano.create();
    }

    function dropdb() {
        daoPlano.drop();
    }

    function save(event) {
        daoPlano.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idPlano = getString('idPlano');
        var descricao = getString("descricao")
        var ativo = getBoolean('ativo');
        var tipo = getString('tipo');
        var tipoPlano = getString('tipoPlano');

        var plano = new Plano();
        plano.setDescricao(descricao);
        plano.setId(idPlano);
        plano.setAtivo(ativo);
        plano.setTipo(tipo);
        plano.setTipoPlano(tipoPlano);

        return plano;
    }

    function popularDadosTela(json) {

        var jsonPlano = json[0];
        var id = jsonPlano.id;
        var descricao = jsonPlano.descricao;
        var ativo = jsonPlano.ativo;
        var tipo = jsonPlano.tipo;
        var tipoPlano = jsonPlano.tipoPlano;

        setString('idPlano', id);
        setString('descricao', descricao);
        setBoolean('ativo', ativo);
        setCombo('tipo', tipo);
        setCombo('tipoPlano', tipoPlano);
    }

    function buscarAll(successCallback) {
        daoPlano.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save plano '+retorno);
        $('#nmPlanos').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoPlano.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPlano.enviar(successCallback);
    }

};