var ControllerPlugin = function () {

    var daoPlugin = new DaoPlugin();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPlugin", function( event ) {
            menu.initialize();
            $('#formAdicionarPlugin').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPlugin", function( event ) {
            menu.initialize();
            $('#formEditarPlugin').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idPlugin = event.delegateTarget.activeElement.attributes['idplugin'].value;
            editar(idPlugin);
        });

        $( document ).on( "pageinit", "#plugins", function( event ) {
            menu.initialize();
            listarPlugins();
        });
    }

    function editar(idTipoTarefa) {
        daoPlugin.buscaById(idTipoTarefa, popularDadosTela);
    }
    function listarPlugins() {
        daoPlugin.buscarAll(listViewPlugin);
    }

    function listViewPlugin(json) {
        $("#plugins").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/plugin/editarPlugin.html" data-ajax="true" idPlugin="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#plugins").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoPlugin.create();
    }

    function dropdb() {
        daoPlugin.drop();
    }

    function save(event) {
        daoPlugin.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idPlugin = getString('idPlugin');
        var descricao = getString("descricao");
        var valor = getString('valor');
        var ativo = getBoolean('ativo');

        var plugin = new Plugin();
        plugin.setDescricao(descricao);
        plugin.setId(idPlugin);
        plugin.setValor(valor);
        plugin.setAtivo(ativo);

        return plugin;
    }

    function popularDadosTela(json) {

        var jsonPlugin = json[0];
        var idPlugin = jsonPlugin.id;
        var descricao = jsonPlugin.descricao;
        var valor = jsonPlugin.valor;
        var ativo = jsonPlugin.ativo;

        setString('idPlugin', idPlugin);
        setString('descricao', descricao);
        setBoolean('ativo', ativo);
        setString('valor', valor);
    }

    function buscarAll(successCallback) {
        daoPlugin.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save plugin '+retorno);
        $('#nmPlugin').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadTipoTarefa/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadTipoTarefa",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(tiposTarefas){
            $.each(tiposTarefas, function(i, item) {
                var tipoTarefa = new TipoTarefa();
                tipoTarefa = daoTipoTarefa.popular0bjeto(tipoTarefa, item);
                daoTipoTarefa.save(tipoTarefa, function () {console.log('tipo tarefa salvo.')});
            });
            callbacks.fire(tiposTarefas);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoTipoTarefa.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadTipoTarefa/',
            data: {
                tiposTarefa: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadTipoTarefa",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }

};