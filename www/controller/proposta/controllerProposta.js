var ControllerProposta = function () {

    var statusPropostaAbrir = '';

    var controllerCliente = new ControllerCliente();
    var controllerUsuario = new ControllerUsuario();
    var controllerFormulario = new ControllerFormulario();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerMeioDivugacao = new ControllerMeioDivulgacao();
    var controllerOperadora = new ControllerOperadora();
    var controllerClassificaoCliente = new ControllerClassificaoCliente();

    var daoRespostaPropostaOpcao = new DaoRespostaPropostaOpcao();
    var daoProposta = new DaoProposta();
    var daoRespostaProposta = new DaoRespostaProposta();
    var daoRespotasPropostaOpcao = new DaoRespostaPropostaOpcao();
    var daoPropostaFranquia = new DaoPropostaFranquia();
    var daoPropostaAparelho = new DaoPropostaAparelho();
    var daoPropostaPortabilidade = new DaoPropostaPortabilidade();
    var daoPropostaModulo = new DaoPropostaModulo();
    var daoPropostaMigracao = new DaoPropostaMigracao();
    var daoPropostaEvento = new DaoPropostaEvento();

    var menu = new Menu();

    this.initialize = initialize;
    this.dropdb = dropdb;
    this.createdb = createdb;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscarAllPropostasAbertasAguardandoFechamento = buscarAllPropostasAbertasAguardandoFechamento;
    this.atualizarProposta = atualizarProposta;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#relatorioFluxoPedido", function( event ) {
            menu.initialize();
            exibirFluxo();

            $('#bntFiltroFluxoPedido').click(function (event) {
                exibirFluxo();
            });
        });

        $( document ).on( "pageinit", "#propostasApresentacao", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasApresentacao(listarPropostasApresentacao);
        });

        $( document ).on( "pageinit", "#propostasRetornarVisita", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasRetornarVisita(listarPropostasRetornarVisita);
        });

        $( document ).on( "pageinit", "#propostasEnviarProposta", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasEnviarProposta(listarPropostasEnviarProposta);
        });

        $( document ).on( "pageinit", "#propostasAguardandoDocumentos", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasAguardandoDocumentos(listarPropostasAguardandoDocumentos);
        });

        $( document ).on( "pageinit", "#propostasAguardandoAssinatura", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasAguardandoAssinatura(listarPropostasAguardandoAssinatura);
        });

        $( document ).on( "pageinit", "#propostasArquivada", function( event ) {
            menu.initialize();
            daoProposta.buscarPropostasArquivada(listarPropostasArquivada);
        });

        $( document ).on( "pageinit", "#propostasFinalizadas", function( event ) {
            menu.initialize();
            daoProposta.buscarAllPropostasFinalizadas(listarPropostasFinalizadas);
        });

        $( document ).on( "pageinit", "#relatorioPropostas", function( event ) {
            menu.initialize();

            $("#filterStatusProposta").empty();

            addOptionToSelect('filterStatusProposta', '', 'Selecione uma opção');
            addOptionToSelect('filterStatusProposta', 'APRESENTACAO', 'E-MAIL APRESENTAÇÃO');
            addOptionToSelect('filterStatusProposta', 'ENVIAR PROPOSTA', 'ENVIAR PROPOSTA');
            addOptionToSelect('filterStatusProposta', 'RETORNAR VISITA', 'RETORNAR VISITA');
            addOptionToSelect('filterStatusProposta', 'ARQUIVADA', 'NÃO TEVE INTERESSE');

            $('#bntFiltroProposta').click(function (event) {
                filtrarEsteiraPedidos();
            });

            filtrarEsteiraPedidos();
        });

        $( document ).on( "pageinit", "#editarProposta", function( event ) {
            menu.initialize();

            $('#formEditarProposta').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#salvarProposta').click(function (event) {

                if(verificaObrigatorio()) {
                    daoProposta.save(buscarDadosTela(), depoisSalvar);

                    controllerCliente.save();

                    var mensagem = 'Dados atualizados!';
                    var title = 'Salvo!';
                    controllerNotificacao.showAlert(mensagem, function () {}, title);
                }
            });

            $('#excluirProposta').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta Proposta?';
                var title = 'Excluir Proposta';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) excluir();
                } , title );
            });

            $('#converterPedido').click(function (event) {

                var cnpjProposta = getString('cnpjProposta');

                if (cnpjProposta === '') {
                    var mensagem = 'Para gerar o pedido é obrigatório informar o CNPJ do cliente!';
                    var title = 'Obrigatório!';
                    controllerNotificacao.showAlert(mensagem, function () {}, title);
                    return;
                }

                var mensagem = 'Deseja realmente FAZER O PEDIDO  para esta Proposta?';
                var title = 'FAZER PEDIDO';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) converterPedido();
                } , title );
            });

            $('#bntAdicionarLinha').click(function (event) {
                PROPOSTA = getString('idProposta');
               $('#nmPropostaFranquia').click();
            });

            $('#bntAdicionarAparelho').click(function (event) {
                PROPOSTA = getString('idProposta');
                $('#nmPropostaAparelho').click();
            });

            $('#bntAdicionarPortabilidade').click(function (event) {
                PROPOSTA = getString('idProposta');
                $('#nmPropostaPortabilidade').click();
            });

            $('#bntAdicionarModulo').click(function (event) {
                PROPOSTA = getString('idProposta');
                $('#nmPropostaModulo').click();
            });

            $('#bntAdicionarMigracao').click(function (event) {
                PROPOSTA = getString('idProposta');
                $('#nmPropostaMigracao').click();
            });

            preencherCombos();
            blurCampos();


            var idProposta = event.delegateTarget.activeElement.attributes['idproposta'].value;

            editar(idProposta);
            exibirLinhas(idProposta);
            controllerCliente.adicionarCliente();
            $('#aBaixarProposta').attr('href',base_url+'propostaonline?idProposta='+idProposta);
        });

        $( document ).on( "pageinit", "#propostasPropostasRespostas", function( event ) {
            var idProposta = event.delegateTarget.activeElement.attributes['idproposta'].value;
            listarRespostas(idProposta);
        });

        $( document ).on( "pageinit", "#adicionarProposta", function( event ) {
            menu.initialize();
            $('#formAdicionarProposta').submit(function (event) {
                event.preventDefault();
                saveAdicionar(event);
            });
            preencherCombos();
            blurCampos();
        });
    }

    function blurCampos() {
        $('#status').blur(function (envet) {
            getExibirDadas();
        });
    }

    function converterPedido() {
        var idProposta = getString('idProposta');
        daoProposta.converterPedido(idProposta, function (retorno) {

            var mensagem = 'Pedido gerado com sucesso!';
            var title = 'Cadastrado!';
            controllerNotificacao.showAlert(mensagem, function () {}, title);

            editar(idProposta);
        });
    }

    function filtrarEsteiraPedidos() {
        var filterStatusProposta = getString('filterStatusProposta');
        var filterDataProposta = getString('filterDataProposta');
        var filterNomeEmpresa = getString('filterNomeEmpresa');
        var filterTipoPlanoProposta = getString('filterTipoPlanoProposta');
        daoProposta.relatorioPropostas(filterStatusProposta,filterDataProposta,filterNomeEmpresa, filterTipoPlanoProposta, relatorioPropostas);
    }

    function atualizarProposta(idProposta) {
        daoPropostaFranquia.buscarAllByProposta(idProposta, function (json) {
            var qtdLinhas = 0;
            var total = 0;

            jQuery.each(json, function(i, item) {
                qtdLinhas = qtdLinhas + item.qtdLinhas;
                total = total + item.total;
            });

            daoProposta.atualizarQtdLinhas(qtdLinhas, idProposta, function () {});
            daoProposta.atualizarValorPlano(total, idProposta, function () {});
        });

        daoPropostaAparelho.buscaAllByProposta(idProposta, function (json) {
            var total = 0;
            jQuery.each(json, function(i, item) {
                total = total + item.total;
            });
            daoProposta.atualizarValorAparelho(total, idProposta,function () {});
        });
    }

    function exibirFluxo() {

        var status = getString('filter_status');
        $('.ui-table-columntoggle-btn').hide();
        daoProposta.buscarFilter(status, function (json) {
            $("#tbFluxoPedido tbody").empty();

            jQuery.each(json, function(i, item) {

                var id = item.id;
                var status = item.status;
                var nomeEmpresa = item.nomeEmpresa;
                var qtdLinhas = item.qtdLinhas;
                var valorPlano = item.valorPlano;
                var valorAparelho = item.valorAparelho;

                var contador = parseInt(i)+1;
                var total = 0;

                if (!isNaN(valorAparelho)) total = parseFloat(valorAparelho);
                if (!isNaN(valorPlano)) total = total + parseFloat(valorPlano);

                var linha = nomeEmpresa+'<br/>R$ '+total+' - '+qtdLinhas+' linha(s)<br/>'+status;
                var innher = '' +
                    '<tr> ' +
                    '   <th><a href="view/proposta/editarProposta.html" data-ajax="true" idProposta="'+id+'">'+contador+'</a></th>' +
                    '   <th><a href="view/proposta/editarProposta.html" data-ajax="true" idProposta="'+id+'">'+linha+'</a></th>' +
                    '</tr>';
                $(innher).appendTo("#tbFluxoPedido tbody");
            });
        });

    }

    function exibirLinhas(idProposta) {

        daoPropostaFranquia.buscarAllByProposta(idProposta, function (json) {

            jQuery.each(json, function(i, item) {

                var id = item.id;
                var strFranquia = item.strFranquia;
                var total = item.total;
                var qtdLinhas = item.qtdLinhas;
                var descricao = '';

                if (qtdLinhas>1) descricao = qtdLinhas + ' linhas de ' + strFranquia + ' R$ ' + total;
                else descricao = qtdLinhas + ' linha de ' + strFranquia + ' R$ ' + total;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/franquia/editarPropostaFranquia.html" data-ajax="true" idPropostaFranquia="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbLinhas tbody");
            });
        });

        daoPropostaAparelho.buscaAllByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var aparelho = item.nomeAparelho;
                var qtdAparelhos = item.qtdAparelhos;
                var total = item.total;
                var descricao = qtdAparelhos+' '+aparelho+ ' R$ ' + total;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/aparelho/editarPropostaAparelho.html" data-ajax="true" idPropostaAparelho="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbEquipamento tbody");
            });
        });

        daoPropostaPortabilidade.buscaAllByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var ddd = item.ddd;
                var telefones = item.telefones;
                telefones = replaceAll(telefones,' ','<br/>');
                telefones = replaceAll(telefones,'\n','<br/>');
                var descricao = ddd+'<br/>'+telefones;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/portabilidade/editarPortabilidade.html" data-ajax="true" idPropostaPortabilidade="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbPortabilidade tbody");
            });
        });

        daoPropostaModulo.buscarAllByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var ddd = item.ddd;
                var telefones = item.telefones;
                telefones = replaceAll(telefones,' ','<br/>');
                telefones = replaceAll(telefones,'\n','<br/>');
                var descricao = ddd+'<br/>'+telefones;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/modulo/editarModulo.html" data-ajax="true" idPropostaModulo="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbModulo tbody");
            });
        });

        daoPropostaMigracao.buscarAllByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {
                var id = item.id;
                var telefones = item.telefones;
                var nomeAssinanteDoador = item.nomeAssinanteDoador;
                var cpf = item.cpf;

                telefones = replaceAll(telefones,' ','<br/>');
                telefones = replaceAll(telefones,'\n','<br/>');
                var descricao = nomeAssinanteDoador+'<br/>'+cpf+'<br/>'+telefones;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/migracao/editarMigracao.html" data-ajax="true" idPropostaMigracao="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbMigracao tbody");
            });
        });

        daoPropostaEvento.buscarAllByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var descricao = item.evento;

                var innher = '' +
                    '<tr> ' +
                    '   <td><a href="view/proposta/evento/editarEvento.html" data-ajax="true" idPropostaEvento="'+id+'">'+descricao+'</a></td>' +
                    '</tr>';
                $(innher).appendTo("#tbEventos tbody");
            });
        });

        exibirLinhasPapelada(idProposta);
    }

    function exibirLinhasPapelada(idProposta) {

        idProposta = idProposta.split('_')[0];

        /*
        var gestor = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/propostaOnline/'+idProposta+'/1/1" data-ajax="true" target="_blank" idProposta="'+idProposta+'">Baixar Proposta</a></td>' +
            '</tr>';
        $(gestor).appendTo("#tbPapelada tbody");
        */

        var gestor = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/gestorOnline/'+idProposta+'/1/1" data-ajax="true" target="_blank" idProposta="'+idProposta+'">GESTOR 3.0</a></td>' +
            '</tr>';
        $(gestor).appendTo("#tbPapelada tbody");

        var gestorLND21 = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/gestorLDN21/'+idProposta+'/1/1" data-ajax="true" target="_blank"  idProposta="'+idProposta+'">GESTOR LND21</a></td>' +
            '</tr>';
        $(gestorLND21).appendTo("#tbPapelada tbody");

        var individual = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/individual/'+idProposta+'/1/1" data-ajax="true" target="_blank"  idProposta="'+idProposta+'">TCPJ - Claro Total Individual + BL - Claro Life</a></td>' +
            '</tr>';
        $(individual).appendTo("#tbPapelada tbody");

        var compartilhado = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/compartilhado/'+idProposta+'/1/1" data-ajax="true" target="_blank"  idProposta="'+idProposta+'">TCPJ - Claro Total + GOL</a></td>' +
            '</tr>';
        $(compartilhado).appendTo("#tbPapelada tbody");

        var contratoPermanencia = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/assets/arquivos/Contrato_de_Permanência_PME_24_meses.pdf" target="_blank" data-ajax="true" idProposta="'+idProposta+'">Contrato de Permanência PME</a></td>' +
            '</tr>';
        $(contratoPermanencia).appendTo("#tbPapelada tbody");

        var AnexoContratoPermanencia = '' +
            '<tr> ' +
            '   <td><a href="'+BASE_URL_PAPELADA+'/proposta/anexocontratopermanencia/'+idProposta+'/1/1" target="_blank" data-ajax="true" idProposta="'+idProposta+'">Anexo ao Contrato de Permanência PME</a></td>' +
            '</tr>';
        $(AnexoContratoPermanencia).appendTo("#tbPapelada tbody");
    }

    function excluir() {
        var idProposta = getString('idProposta');
        daoProposta.deletar(idProposta);
        deletarAllRespostas(idProposta);
        depoisSalvar();
    }

    function deletarAllRespostas(idProposta) {
        daoRespostaProposta.buscalAllRespostaByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {
                daoRespostaProposta.deletar(item.id);
            });
        });

        daoRespotasPropostaOpcao.buscarAllRespostasOpcaoByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {
                daoRespotasPropostaOpcao.deletar(item.id);
            });
        })
    }

    function listarRespostas(idProposta) {
        daoProposta.buscarRespostasByProposta(idProposta, listarPropostasRespostaView);
    }

    function listarPropostasRespostaView(json) {

        $("#listview-propostas-respostas").empty();

        jQuery.each(json, function(i, item) {

            var pergunta = item.pergunta;
            var resposta = item.resposta;
            var descricao = item.descricao;
            var tipoCampo = item.tipoCampo;
            var proposta = item.proposta;
            var idPergunta = item.idPergunta;

            var innerA = '';

            if (i==='0') innerA = '<li data-role="list-divider">'+descricao+'</li>';
            if (tipoCampo === 'date') resposta = formatDatePtBr(resposta);

            innerA  += '<li>';
            innerA  += '    <a href="view/proposta/editarProposta.html" data-ajax="true" idProposta="'+proposta+'" data-role="button">';
            innerA  += '        <h2>'+pergunta+'</h2>';
            innerA  += '        <div id="pergunta_'+idPergunta+'"><p><strong>'+resposta+'</strong></p></div>';
            innerA  += '    </a>';
            innerA  += '</li>';

            $("#listview-propostas-respostas").append(innerA);

            daoRespostaPropostaOpcao.buscarAllRespostasOpcaoByPropostaPergunta(proposta, idPergunta, function (json) {
                var html = '';
                jQuery.each(json, function(i, item) {
                    var opcao = item.opcao;
                    var resposta = item.resposta;
                    if (resposta === '1') html = html + '[X] '+opcao+'<br/>';
                    else html = html + '[&nbsp;&nbsp;] '+opcao+'<br/>';
                });

                if(html !== '') $('#pergunta_'+idPergunta).html(html);
            });

        });

        $('ul').listview().listview('refresh');
    }

    function editar(idProposta) {
        daoProposta.buscaById(idProposta, popularDadosTela);
    }

    function getExibirDadas() {

        var status = getString('status');

        $('#divDataApresentacao').hide();
        $('#divEnvioProposta').hide();
        $('#divRetornoVisita').hide();
        $('#divAguardandoDocumento').hide();
        $('#divAguardandoAssinatura').hide();

        if (status === 'APRESENTACAO') {
            $('#divDataApresentacao').show();
        } else if (status === 'ENVIAR PROPOSTA') {
            $('#divEnvioProposta').show();
        } else if (status === 'RETORNAR VISITA') {
            $('#divRetornoVisita').show();
        } else if (status === 'AGUARDANDO DOCUMENTOS') {
            $('#divAguardandoDocumento').show();
        } else if (status === 'AGUARDANDO ASSINATURA') {
            $('#divAguardandoAssinatura').show();
        } else if (status === 'ARQUIVADA') {}
    }

    function verificaObrigatorio() {

        var status = getString('status');

        var dataApresentacao = $('#dataApresentacao').val();
        var dataEnvioProposta = $('#dataEnvioProposta').val();
        var dataRetornoVisita = $('#dataRetornoVisita').val();
        var dataAguardandoDocumento = $('#dataAguardandoDocumento').val();
        var dataAguardandoAssinatura = $('#dataAguardandoAssinatura').val();

        if (status === 'APRESENTACAO') {
            if (dataApresentacao === '') {
                var mensagem = 'Data do e-mail de apresentação é obrigatório.';
                var title = 'Obrigatório';
                controllerNotificacao.showAlert(mensagem, function () {
                }, title);
                return false;
            }
        } else if (status === 'ENVIAR PROPOSTA') {
            if (dataEnvioProposta === '') {
                var mensagem = 'Data do envio de propsta é obrigatório.';
                var title = 'Obrigatório';
                controllerNotificacao.showAlert(mensagem, function () {
                }, title);
                return false;;
            }
        } else if (status === 'RETORNAR VISITA') {
            if (dataRetornoVisita === '') {
                var mensagem = 'Data do retorno da proposta é obrigatório.';
                var title = 'Obrigatório';
                controllerNotificacao.showAlert(mensagem, function () {
                }, title);
                return false;;
            }
        } else if (status === 'AGUARDANDO DOCUMENTOS') {
            if (dataAguardandoDocumento === '') {
                var mensagem = 'Data aguardando documentos é obrigatório.';
                var title = 'Obrigatório';
                controllerNotificacao.showAlert(mensagem, function () {
                }, title);
                return false;;
            }
        } else if (status === 'AGUARDANDO ASSINATURA') {
            if (dataAguardandoAssinatura === '') {
                var mensagem = 'Data do aguardando assinatura é obrigatório.';
                var title = 'Obrigatório';
                controllerNotificacao.showAlert(mensagem, function () {
                }, title);
                return false;;
            }
        } else if (status === 'ARQUIVADA') {}

        return true;
    }

    function listarPropostasApresentacao(json) {
        listarPropostas('listview-propostas-apresentacao', json);
    }

    function listarPropostasEnviarProposta(json) {
        listarPropostas('listview-enviar-proposta', json);
    }

    function listarPropostasRetornarVisita(json) {
        listarPropostas('listview-retornar-visita', json);
    }

    function listarPropostasArquivada(json) {
        listarPropostas('listview-arquivada', json);
    }

    function listarPropostasAguardandoDocumentos(json) {
        listarPropostas('listview-propostasaguardandodocumentos', json);
    }

    function listarPropostasAguardandoAssinatura(json) {
        listarPropostas('listview-propostasaguardandoassinatura', json);
    }

    function listarPropostasFinalizadas(json) {
        listarPropostas('listview-propostas-finalizadas', json);
    }

    function relatorioPropostas(json) {
        listarPropostas('listview-relatorio-propostas', json);
    }

    function listarPropostas(listview, json) {
        var dataExtensao = '';

        $("#"+listview).empty();

        jQuery.each(json, function(i, item) {

            var id = item.idProposta;
            var nomeCliente = item.nomeCliente;
            var cliente = item.cliente;
            var observacao = item.observacaoProposta;
            var telefone = item.telefone;
            var celular = item.celular;
            var email = item.email;
            var nomeContato = item.nomeContato;
            var status = item.status;
            var qtdLinhas = item.qtdLinhas;
            var valorPlano = item.valorPlano;
            var valorAparelho = item.valorAparelho;
            var nomeEmpresa = item.nomeEmpresa;
            var tipoVenda = item.tipoVenda;
            var cnpjProposta = item.cnpjProposta;

            if (celular === undefined) celular = '';
            if (valorPlano === undefined) valorPlano = '0';
            if (valorAparelho === undefined) valorAparelho = '0';
            if (qtdLinhas === undefined) qtdLinhas = '0';
            if (cliente === undefined) nomeCliente = 'Cliente não cadastrado!';
            else nomeCliente = nomeEmpresa;

            statusPropostaAbrir = status;

            var dataExibir = item.dataProposta;

            if (status === 'APRESENTACAO') dataExibir = item.dataApresentacao;
            if (status === 'ENVIAR PROPOSTA')  dataExibir = item.dataEnvioProposta;
            if (status === 'RETORNAR VISITA') dataExibir = item.dataRetornoVisita;
            if (status === 'AGUARDANDO DOCUMENTOS') dataExibir = item.dataAguardandoDocumento;
            if (status === 'AGUARDANDO ASSINATURA') dataExibir = item.dataAguardandoAssinatura;

            var semana = getDiaSemana(dataExibir);
            var ano = getYear(dataExibir);
            var mes = getMes(dataExibir);
            var dia = getDay(dataExibir);

            var innerA = '';

           // if (dataExtensao !== dataExibir) innerA = '<li data-role="list-divider">' + semana + ', ' + mes + ' ' + dia + ', ' + ano + ' </li>';
            var dataExtenso =   semana + ', ' + dia + ' ' + mes + ', ' + ano ;

            innerA  += '<li>';
            innerA  += '    <a href="view/proposta/editarProposta.html" data-ajax="true" idProposta="'+id+'" data-role="button">';
            innerA  += '        <h3>'+dataExtenso+'</h3>';

            if (cnpjProposta !== '')innerA  += '        <p><strong>CNPJ: '+cnpjProposta+'</strong></p>';

            innerA  += '        <p><strong>'+nomeCliente+'</strong></p>';
            innerA  += '        <p><strong>'+getStatus(status)+'</strong></p>';
            innerA  += '        <p><small>Contato: '+nomeContato+'</small></p>';
            innerA  += '        <p><small>'+telefone+ ' - ' + celular+'</small></p>';
            innerA  += '        <p><small>'+email+'</small></p>';
            innerA  += '        <p><small>Tipo de Venda: '+tipoVenda+'</small></p>';
            innerA  += '        <p><small>Qtd Linhas <b>'+qtdLinhas+'</b></small></p>';
            innerA  += '        <p><small>Valor do Plano <b> R$ '+valorPlano+'</b></small></p>';
            innerA  += '        <p><small>Valor do Aparelho <b>R$ '+valorAparelho+'</b></small></p>';
            innerA  += '        <p><small>Total <b>R$ '+(valorPlano + valorAparelho)+'</b></small></p>';
            innerA  += '        <p><b>'+observacao+'</b></p>';
            innerA  += '    </a>';
            innerA  += '</li>';

            dataExtensao = dataExibir;
            $("#"+listview).append(innerA);
        });

        $('ul').listview().listview('refresh');
    }

    function getStatus(status) {
        if (status === 'APRESENTACAO') return 'E-MAIL APRESENTAÇÃO';
        if (status === 'ENVIAR PROPOSTA') return 'ENVIAR PROPOSTA';
        if (status === 'RETORNAR VISITA') return 'RETORNAR VISITA';
        if (status === 'AGUARDANDO DOCUMENTOS') return 'AGUARDANDO DOCUMENTOS';
        if (status === 'AGUARDANDO ASSINATURA') return 'AGUARDANDO ASSINATURA';
        if (status === 'ARQUIVADA') return 'NÃO TEVE INTERESSE';
        else return status;
    }

    function save(event) {
        if (verificaObrigatorio()) {
            daoProposta.save(buscarDadosTela(), depoisSalvar);
        }
    }

    function saveAdicionar(event) {
        if (verificaObrigatorio()) {
            daoProposta.save(buscarDadosTela(), function (retorno) {
                $('#liFunilVendas').click();
            });
        }
    }

    function buscarDadosTela() {

        var idProposta = getString('idProposta');
        var formulario = getString('formulario');
        var cliente = getString('cliente');
        var dataProposta = getString('dataProposta');
        var observacao = getString('observacao');
        var descricao = $("#formulario option:selected").text();
        var status = getString('status');
        var latitude = getString('latitude');
        var longitude = getString('longitude');
        var dataAberta = getString('dataAberta');
        var dataAguardando = getString('dataAguardando');
        var dataFechamento = getString('dataFechamento');
        var responsavel = getString('responsavel');
        var visitaConfirmada = $('#visitaConfirmada').is(":checked");
        var tipo = getString('tipo');
        var tipoVenda = getString('tipoVenda');
        var cnpjProposta = getString('cnpjProposta');

        var nomeEmpresa = getString('nomeEmpresa');
        var telefone = getString('telefone');
        var email = getString('email');
        var meioDivulgacao = getString('meioDivulgacao');
        var nomeContato = getString('nomeContato');
        var gastoAtual = getString('gastoAtual');
        var dataInicioContrato = getString('dataInicioContrato');
        var classificacaoCliente = getString('classificacaoCliente');
        var aptoRenovar = getBoolean('aptoRenovar');
        var valorPlano = getString('valorPlano');
        var qtdLinhas = getString('qtdLinhas');
        var dataVencimento = getString('dataVencimento');
        var possuiServicoEmbratel = getBoolean('possuiServicoEmbratel');
        var possuiServicoNet = getBoolean('possuiServicoNet');
        var operadoraAtual = getString('operadoraAtual');
        var valorAparelho= getString('valorAparelho');
        var backupOnline = getBoolean('backupOnline');
        var identificacaoClienteNet = getString('identificacaoClienteNet');
        var dataApresentacao = getString('dataApresentacao');
        var dataEnvioProposta = getString('dataEnvioProposta');
        var dataRetornoVisita = getString('dataRetornoVisita');
        var dataAguardandoDocumento = getString('dataAguardandoDocumento');
        var dataAguardandoAssinatura = getString('dataAguardandoAssinatura');
        var isPedido = getBoolean('isPedido');
        var isSmsVontade = getBoolean('isSmsVontade');
        var isAutorizoContabilidade = getBoolean('isAutorizoContabilidade');
        var isWhatsAppVontade = getBoolean('isWhatsAppVontade');
        var isMinutosIlimitados = getBoolean('isMinutosIlimitados');
        var telefoneContabilidade = getString('telefoneContabilidade');
        var observacaoAparelhos = getString('observacaoAparelhos');

        var tempoVigenciaContrato = TEMPO_VIGENCIA_CONTRATO;

        if (visitaConfirmada) visitaConfirmada = 1;
        else visitaConfirmada = 0;

        var proposta = new Proposta();
        proposta.setId(idProposta);
        proposta.setDataInicioContrato(dataInicioContrato);

        if (dataInicioContrato !== '') {
            var ano = getYear(dataInicioContrato);
            var mes = getMonth(dataInicioContrato);
            var dia = getDay(dataInicioContrato);

            if (mes <= 9 ) mes = '0'+mes;
            proposta.setDataTerminoContrato(ano + 2 + '-'+mes+'-'+dia);
        }

        proposta.setDataApresentacao(dataApresentacao);
        proposta.setDataEnvioProposta(dataEnvioProposta);
        proposta.setDataRetornoVisita(dataRetornoVisita);
        proposta.setDataAguardandoDocumento(dataAguardandoDocumento);
        proposta.setDataAguardandoAssinatura(dataAguardandoAssinatura);
        proposta.setClassificacaoCliente(classificacaoCliente);
        proposta.setAptoRenovar(aptoRenovar);
        proposta.setValorPlano(valorPlano);
        proposta.setQtdLinhas(qtdLinhas);
        proposta.setTempoVigenciaContrato(tempoVigenciaContrato);
        proposta.setDataVencimento(dataVencimento);
        proposta.setPossuiServicoEmbratel(possuiServicoEmbratel);
        proposta.setPossuiServicoNet(possuiServicoNet);
        proposta.setNomeEmpresa(nomeEmpresa);
        proposta.setTelefone(telefone);
        proposta.setEmail(email);
        proposta.setMeioDivulgacao(meioDivulgacao);
        proposta.setNomeContato(nomeContato);
        proposta.setGastoAtual(gastoAtual);
        proposta.setDescricao(descricao);
        proposta.setCliente(cliente);
        proposta.setFormulario(formulario);
        proposta.setDataProposta(dataProposta);
        proposta.setObservacao(observacao);
        proposta.setStatus(status);
        proposta.setLatitude(latitude);
        proposta.setLongitude(longitude);
        proposta.setDataAberta(dataAberta);
        proposta.setDataAguardando(dataAguardando);
        proposta.setDataFechamento(dataFechamento);
        proposta.setVisitaConfirmada(visitaConfirmada);
        proposta.setResponsavel(responsavel);
        proposta.setOperadoraAtual(operadoraAtual);
        proposta.setValorAparelho(valorAparelho);
        proposta.setTipo(tipo);
        proposta.setBackupOnline(backupOnline);
        proposta.setIdentificacaoClienteNet(identificacaoClienteNet);
        proposta.setIsPedido(isPedido);
        proposta.setTipoVenda(tipoVenda);
        proposta.setCnpjProposta(cnpjProposta);
        proposta.setIsSmsVontade(isSmsVontade);
        proposta.setIsAutorizoContabilidade(isAutorizoContabilidade);
        proposta.setIsWhatsAppVontade(isWhatsAppVontade);
        proposta.setIsMinutosIlimitados(isMinutosIlimitados);
        proposta.setTelefoneContabilidade(telefoneContabilidade);
        proposta.setObservacaoAparelhos(observacaoAparelhos);

        return proposta;
    }

    function popularDadosTela(json) {

        var jsonProposta = json[0];

        var idProposta = jsonProposta.id;
        var formulario = jsonProposta.formulario;
        var dataProposta = jsonProposta.dataProposta;
        var observacao = jsonProposta.observacao;
        var status = jsonProposta.status;
        var latitude =  jsonProposta.latitude;
        var longitude = jsonProposta.longitude;
        var dataAberta = jsonProposta.dataAberta;;
        var dataAguardando = jsonProposta.dataAguardando;
        var dataFechamento = jsonProposta.dataFechamento;
        var visitaConfirmada = jsonProposta.visitaConfirmada;
        var responsavel = jsonProposta.responsavel;
        var backupOnline = jsonProposta.backupOnline;
        var identificacaoClienteNet = jsonProposta.identificacaoClienteNet;

        var nomeEmpresa = jsonProposta.nomeEmpresa;
        var telefone = jsonProposta.telefone;
        var email = jsonProposta.email;
        var meioDivulgacao = jsonProposta.meioDivulgacao;
        var nomeContato = jsonProposta.nomeContato;
        var gastoAtual = jsonProposta.gastoAtual;
        var dataInicioContrato = jsonProposta.dataInicioContrato;
        var dataTerminoContrato = jsonProposta.dataTerminoContrato;
        var classificacaoCliente = jsonProposta.classificacaoCliente;
        var aptoRenovar = jsonProposta.aptoRenovar;
        var valorPlano = jsonProposta.valorPlano;
        var qtdLinhas =  jsonProposta.qtdLinhas;
        var tempoVigenciaContrato = jsonProposta.tempoVigenciaContrato;
        var dataVencimento = jsonProposta.dataVencimento;
        var possuiServicoEmbratel = jsonProposta.possuiServicoEmbratel;
        var possuiServicoNet = jsonProposta.possuiServicoNet;
        var operadoraAtual = jsonProposta.operadoraAtual;
        var valorAparelho = jsonProposta.valorAparelho;
        var tipo = jsonProposta.tipo;
        var dataApresentacao = jsonProposta.dataApresentacao;
        var dataEnvioProposta =  jsonProposta.dataEnvioProposta;
        var dataRetornoVisita = jsonProposta.dataRetornoVisita;
        var dataAguardandoDocumento =  jsonProposta.dataAguardandoDocumento;
        var dataAguardandoAssinatura =  jsonProposta.dataAguardandoAssinatura;
        var isPedido = jsonProposta.isPedido;
        var tipoVenda = jsonProposta.tipoVenda;
        var cnpjProposta = jsonProposta.cnpjProposta;
        var cliente = jsonProposta.cliente;
        var isSmsVontade = jsonProposta.isSmsVontade;
        var isAutorizoContabilidade = jsonProposta.isAutorizoContabilidade;
        var isWhatsAppVontade = jsonProposta.isWhatsAppVontade;
        var isMinutosIlimitados = jsonProposta.isMinutosIlimitados;
        var telefoneContabilidade = jsonProposta.telefoneContabilidade;
        var observacaoAparelhos = jsonProposta.observacaoAparelhos;

        exibirDadosPropostaPedido(isPedido);
        addComboStatus(isPedido);

        setString('observacaoAparelhos', observacaoAparelhos);
        setString('telefoneContabilidade', telefoneContabilidade);
        setBoolean('isMinutosIlimitados', isMinutosIlimitados);
        setBoolean('isWhatsAppVontade', isWhatsAppVontade);
        setBoolean('isAutorizoContabilidade', isAutorizoContabilidade);
        setBoolean('isSmsVontade', isSmsVontade);
        setString('dataEnvioProposta',dataEnvioProposta);
        setString('dataRetornoVisita',dataRetornoVisita);
        setString('dataAguardandoDocumento',dataAguardandoDocumento);
        setString('dataAguardandoAssinatura',dataAguardandoAssinatura);
        setString('dataApresentacao',dataApresentacao);
        setBoolean('possuiServicoNet', possuiServicoNet);
        setBoolean('possuiServicoEmbratel', possuiServicoEmbratel);
        setCombo('dataVencimento', dataVencimento);
        setString('tempoVigenciaContrato', tempoVigenciaContrato);
        setString('qtdLinhas', qtdLinhas);
        setString('valorPlano', valorPlano);
        setBoolean('aptoRenovar',aptoRenovar);
        setCombo('classificacaoCliente', classificacaoCliente);
        setString('dataTerminoContrato', dataTerminoContrato);
        setString('dataInicioContrato', dataInicioContrato);
        setString('gastoAtual', gastoAtual);
        setString('nomeContato', nomeContato);
        setCombo('meioDivulgacao', meioDivulgacao);
        setString('telefone', telefone);
        setString('email', email);
        setString('nomeEmpresa', nomeEmpresa);
        setString('idProposta', idProposta);
        setCombo('formulario', formulario);
        setCombo('cliente', cliente);
        setString('dataProposta', dataProposta);
        setString('observacao', observacao);
        setCombo('status', status);
        setString('latitude', latitude);
        setString('longitude', longitude);
        setString('dataAberta', dataAberta);
        setString('dataAguardando', dataAguardando);
        setString('dataFechamento', dataFechamento);
        setCombo('responsavel', responsavel);
        setCombo('operadoraAtual', operadoraAtual);
        setString('valorAparelho', valorAparelho);
        setString('totalProposta', (valorPlano + valorAparelho));
        setCombo('tipo', tipo);
        setBoolean('backupOnline', backupOnline);
        setString('identificacaoClienteNet', identificacaoClienteNet);
        setBoolean('isPedido', isPedido);
        setCombo('tipoVenda', tipoVenda);
        setString('cnpjProposta', cnpjProposta);
        setString('idCliente', cliente);

        getExibirDadas();

        if (visitaConfirmada) $('#visitaConfirmada').prop('checked', true).checkboxradio("refresh");
        else $('#visitaConfirmada').prop('checked', false).checkboxradio("refresh");

        $('#div_confirmarVisita').hide();
        $('#divBtnConfirmarVisita').hide();

        if (visitaConfirmada) {
            $('#iFrameMinhaLocalizacao').attr('src', 'view/proposta/minhaLocalizacaoEditar.html?lat=' + latitude + '&long=' + longitude);
            $('#collapsiblesetMinhaLocalizacao').show();
            $('#div_confirmarVisita').show();
        }

        if (cliente !== null && cliente !== undefined) {
            $('#nomeEmpresa').attr('disabled','true');

            controllerCliente.buscaById(cliente, function (json) {
                controllerCliente.popularDadosTela(json);
            });
        }
    }

    function addComboStatus(isPedido) {

        $("#status").empty();

        if (isPedido  === 0) {
            addOptionToSelect('status', 'APRESENTACAO', 'E-MAIL APRESENTAÇÃO');
            addOptionToSelect('status', 'ENVIAR PROPOSTA', 'ENVIAR PROPOSTA');
            addOptionToSelect('status', 'RETORNAR VISITA', 'RETORNAR VISITA');
            addOptionToSelect('status', 'ARQUIVADA', 'NÃO TEVE INTERESSE');
        } else {
            addOptionToSelect('status', 'AGUARDANDO DOCUMENTOS', 'AGUARDANDO DOCUMENTOS');
            addOptionToSelect('status', 'AGUARDANDO ASSINATURA', 'AGUARDANDO ASSINATURA');
            addOptionToSelect('status', 'FALTA DE EQUIPAMENTOS', 'FALTA DE EQUIPAMENTOS');
            addOptionToSelect('status', 'VALIDACAO PENDENTE', 'VALIDAÇÃO PENDENTE');
            addOptionToSelect('status', 'ANALISE DE CREDITO', 'ANALISE DE CRÉDITO');
            addOptionToSelect('status', 'REANALIZE DE CREDITO', 'REANALIZE DE CRÉDITO');
            addOptionToSelect('status', 'PEDIDO NEGADO', 'PEDIDO NEGADO');
            addOptionToSelect('status', 'TROCA DE CARTEIRA', 'TROCA DE CARTEIRA');
            addOptionToSelect('status', 'PRD', 'PRD');
            addOptionToSelect('status', 'ROTA DE ENTREGA', 'ROTA DE ENTREGA');
            addOptionToSelect('status', 'PORTABILIDADE AGENDADA', 'PORTABILIDADE AGENDADA');
            addOptionToSelect('status', 'SOLICITACAO DE DOCUMENTACAO COMPLEMENTAR', 'SOLICITAÇÃO DE DOCUMENTACAO COMPLEMENTAR');
            addOptionToSelect('status', 'PEDIDO ATIVO', 'PEDIDO ATIVO');
        }

    }
    function exibirDadosPropostaPedido(isPedido) {
        if (isPedido === 0) {

            $( "#status" ).prop( "disabled", false );

            $('#div_doadores_portabilidade').hide();
            $('#doadores_modulos_adicionais').hide();
            $('#termos_misgracao_doador').hide();
            $('#papelada').hide();
            $('#div_eventos').hide();

            $('#div_inicio_contrato').hide();
            $('#div_vigencia_contrato').hide();
            $('#div_termino_contrato').hide();
            $('#div_data_vencimento').hide();
            $('#div_classificacao_cliente').hide();
            $('#div_apto_renovar').hide();
            $('#div_backup_online').hide();

            $('#div_possui_servico_embratel').hide();
            $('#div_possui_servico_net').hide();

            $('#converterPedido').show();
        } else {

            $( "#status" ).prop( "disabled", true );

            //$('#div_doadores_portabilidade').show();
            //$('#doadores_modulos_adicionais').show();
            //$('#termos_misgracao_doador').show();
            //$('#papelada').show();
            //$('#div_backup_online').show();

            $('#div_doadores_portabilidade').hide();
            $('#doadores_modulos_adicionais').hide();
            $('#termos_misgracao_doador').hide();
            $('#papelada').hide();
            $('#div_backup_online').hide();

            $('#div_eventos').show();

            $('#div_inicio_contrato').show();
            $('#div_vigencia_contrato').show();
            $('#div_termino_contrato').show();
            $('#div_data_vencimento').show();
            $('#div_classificacao_cliente').show();
            $('#div_apto_renovar').show();

            $('#div_possui_servico_embratel').show();
            $('#div_possui_servico_net').show();

            $('#converterPedido').hide();
        }
    }

    function depoisSalvar(retorno) {

        if (!retorno) {alert("erro ao salvar proposta!"); return;}
        //novaResposta();
    }

    function novaResposta() {
        FORMULARIO = getString('formulario');
        PROPOSTA = getString('idProposta');
        $('#nmRespostaProposta').click();
    }

    function preencherCombos() {
        controllerCliente.buscarAll(preencherComboCliente);

        controllerFormulario.buscarAll(function (json) {
            preencherComboAtributos('formulario', json);
        });

        controllerUsuario.buscarAll(function (json) {
            controllerUsuario.preencherCombo('responsavel', json);
            setCombo('responsavel', ID_USUARIO_LOGADO);
        });

        controllerMeioDivugacao.buscarAll(function (json) {
            preencherComboAtributos('meioDivulgacao', json);
        });

        controllerOperadora.buscarAll(function (json) {
            preencherComboAtributos('operadoraAtual', json);
        });

        controllerClassificaoCliente.buscarAll(function (json) {
            preencherComboAtributos('classificacaoCliente', json);
        });

        $('#dataProposta').val(formatDate(new Date()));
    }

    function dropdb() {
        daoProposta.drop();
    }

    function createdb() {
        daoProposta.create();
    }

    function preencherComboCliente(json) {

        $("#cliente").empty();
        $('#cliente').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));

        jQuery.each(json, function(i, item) {
            $('#cliente').append($('<option>', {
                value: item.id,
                text : item.nome
            }));
        });
    }

    function buscarAllPropostasAbertasAguardandoFechamento(successCallback) {
        daoProposta.buscarAllPropostasAbertasAguardandoFechamento(successCallback);
    }

    function baixar(successCallback) {
        daoProposta.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoProposta.enviar(successCallback);
    }
};