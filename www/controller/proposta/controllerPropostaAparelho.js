var ControllerPropostaAparelho = function () {

    var valorSimCardComTelefone = 0;
    var valroSimCardSemTelefone = 0;

    var controllerNotificacao = new ControllerNotificacao();
    var controllerProposta = new ControllerProposta();

    var daoPropostaAparelho = new DaoPropostaAparelho();
    var daoPlano = new DaoPlano();
    var daoFranquia = new DaoFranquia();
    var daoProposta = new DaoProposta();
    var daoAparelho = new DaoAparelho();
    var daoSimCard = new DaoSimCard();
    var daoAparelhoPlano = new DaoAparelhoPlano();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPropostaAparelho", function( event ) {
            menu.initialize();
            preencherComboBox();
            atriburBlur();

            $('#formAdicionarPropostaAparelho').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPropostaAparelho", function( event ) {
            menu.initialize();
            preencherComboBox();
            atriburBlur();

            $('#formEditarPropostaAparelho').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPropostaAparelho').click(function (event) {
                var mensagem = 'Deseja realmente excluir este aparelho?';
                var title = 'Excluir Aparelho';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });
            var idPropostaAparelho = event.delegateTarget.activeElement.attributes['idpropostaaparelho'].value;
            editar(idPropostaAparelho);
        });
    }

    function editar(idPropostaAparelho) {
        daoPropostaAparelho.buscaById(idPropostaAparelho, popularDadosTela);
    }

    function excluir() {
        var idPropostaAparelho = getString('idPropostaAparelho');
        daoPropostaAparelho.deletar(idPropostaAparelho);
        depoisSalvar(true);
    }

    function atriburBlur() {
        $('#plano').blur(function (event) {
            depoisPreencherPlano();
        });

        $('#aparelho').blur(function (event) {
           depoisPreencherAparelho();
           depoisPreencherTipoCobranca();
        });

        $('#franquia').blur(function (event) {
            depoisPreencherAparelho();
            depoisPreencherTipoCobranca();
        });

        $('#qtdSimCard').blur(function (event) {
            depoisPreencherAparelho();
            depoisPreencherTipoCobranca();
        });

        $('#tipoCobranca').blur(function (event) {
            depoisPreencherTipoCobranca();
        });

        $('#qtdAparelhos').blur(function (event) {
            depoisPreencherTipoCobranca();
        });

        $('#pontuacao').blur(function (event) {
            depoisPreencherTipoCobranca();
        });

        $('#tipoPlanoAparelho').blur(function (event) {
            depoisPreencherTipoPlanoAparelho();
        });

        $('#isSimCardComAparelho').change(function (event) {
            var isSimCardComAparelho = getBoolean('isSimCardComAparelho');
            if (isSimCardComAparelho === 1) {
                $('#divQtdSimCard').show();
            } else {
                $('#divQtdSimCard').hide();
            }
        });

        $('#operadoraInterna').blur(function (event) {
            depoisPreencherOperadora();
        });
    }

    function depoisPreencherOperadora() {

        var operadoraInterna = getString('operadoraInterna');
        $("#tipoPlanoAparelho").empty();

        $('#divCor').hide();
        $('#divFabricante').hide();
        $('#divTipoCobranca').hide();
        $('#divPlano').hide();

        addOptionToSelect('tipoPlanoAparelho', '', 'Selecione uma opção');

        if (operadoraInterna === 'claro') {
            addOptionToSelect('tipoPlanoAparelho', 'individual', 'Claro Total Individual');
            addOptionToSelect('tipoPlanoAparelho', 'compartilhado', 'Claro Total Compartilhado');

            $('#divCor').show();
            $('#divFabricante').show();
            $('#divTipoCobranca').show();
            $('#divPlano').show();

            daoAparelho.buscarAll(function (json) {
                preencherComboAtributos('aparelho', json);
            });

            /*
            addOptionToSelect('m2m', '', 'Selecione uma opção');
            addOptionToSelect('smart', '', 'Selecione uma opção');
            addOptionToSelect('table', '', 'Selecione uma opção');
            addOptionToSelect('modem', '', 'Selecione uma opção');
            */
        } else if (operadoraInterna === 'net') {
            addOptionToSelect('tipoPlanoAparelho', 'bl', 'Net Virtua');
            addOptionToSelect('tipoPlanoAparelho', 'fixo', 'Net Fixo');
            addOptionToSelect('tipoPlanoAparelho', 'tv', 'Net TV');
        } else if (operadoraInterna === 'embratel') {}
    }

    function depoisPreencherTipoPlanoAparelho() {
        var tipoPlanoAparelho = getString('tipoPlanoAparelho');
        $('#total').val('');

        daoPlano.buscarAllByTipo(tipoPlanoAparelho, function (json) {
            preencherPlano(json);
        });

        if (tipoPlanoAparelho === 'bl') {
            daoAparelho.buscarByBandaLarga(function (json) {
                preencherComboAparelho(json);
            });
        } else if (tipoPlanoAparelho === 'fixo') {
            daoAparelho.buscarByFIXO(function (json) {
                preencherComboAparelho(json);
            });
        } else if (tipoPlanoAparelho === 'tv') {
            daoAparelho.buscarByTV(function (json) {
                preencherComboAparelho(json);
            });
        }

        $("#aparelho").val($("#aparelho option:first").val());
    }

    function preencherComboAparelho(json) {
        $("#aparelho").empty();

        $('#aparelho').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));

        jQuery.each(json, function(i, item) {
            $('#aparelho').append($('<option>', {
                value: item.id,
                text : item.descricao
            }));
        });
    }

    function depoisPreencherTipoCobranca() {

        var tipoCobranca = $('#tipoCobranca').val();
        var aparelho = $('#aparelho').val();

        $('#precoUnitarioAparelho').val(0);
        $('#total').val(0);

        $('#divFranquia').hide();
        $('#divPontuacao').hide();
        setCombo('pontuacao', 'Não');

        if ('PRECO A VISTA' === tipoCobranca) {
            daoAparelho.buscaById(aparelho,function (json) {
               var valor = json[0].precoAVista;
               setString('precoUnitarioAparelho', valor);
               toalizar();
            });
        } else if ('PRECO BASE 24x' === tipoCobranca) {
            daoAparelho.buscaById(aparelho,function (json) {
                var valor = json[0].precoBase24;
                setString('precoUnitarioAparelho', valor);
                toalizar();
            });
        } else if ('PRECO SUBSIDIADO 100 PTS (EM 24X)' === tipoCobranca) {
            daoAparelho.buscaById(aparelho,function (json) {
                var valor = json[0].subsidio100pts;
                setString('precoUnitarioAparelho', valor);
                $('#divPontuacao').show();
                setCombo('pontuacao', '100');
                toalizar();
            });
        } else if ('PRECO SUBSIDIADO 140 PTS (EM 24X)' === tipoCobranca) {
            daoAparelho.buscaById(aparelho,function (json) {
                var valor = json[0].subsidio140pts;
                setString('precoUnitarioAparelho', valor);
                $('#divPontuacao').show();
                setCombo('pontuacao', '140');
                toalizar();
            });
        } else if ('CLARO TOTAL INDIVIDUAL' === tipoCobranca) {
            $('#divFranquia').show();

            var idPlano = getString('plano');
            var idFranquia = getString('franquia');
            var idAparelho = getString('aparelho');

            daoAparelhoPlano.buscaValorByPlanoFranquiaAparelho(idPlano, idFranquia, idAparelho, function (json) {
                var valor = json[0].valor;
                setString('precoUnitarioAparelho', valor);
                toalizar();
            });

        }  else if ('PLANO AACE' === tipoCobranca) {
            daoAparelho.buscaById(aparelho,function (json) {
                var valor = json[0].claroTotalAACE;
                setString('precoUnitarioAparelho', valor);
                toalizar();
            });
        } else if ('CLARO TOTAL COMPARTILHADO' === tipoCobranca) {
            $('#divFranquia').show();
            $('#divPontuacao').show();

            var idPlano = getString('plano');
            var idFranquia = getString('franquia');
            var idAparelho = getString('aparelho');
            var pontos = getString('pontuacao');

            daoAparelhoPlano.buscaValorByPlanoFranquiaAparelhoPontos(idPlano, idFranquia, idAparelho, pontos, function (json) {
                var valor = json[0].valor;
                setString('precoUnitarioAparelho', valor);
                toalizar();
            });
        }
    }

    function toalizar() {

        var precoUnitarioAparelho = getString('precoUnitarioAparelho');
        if (precoUnitarioAparelho !== '') precoUnitarioAparelho = parseFloat(precoUnitarioAparelho);
        else precoUnitarioAparelho = 0;

        var qtdAparelhos = getString('qtdAparelhos');

        if (qtdAparelhos !== '') qtdAparelhos = parseFloat(qtdAparelhos);
        else qtdAparelhos = 0;

        var qtdSimCard = getString('qtdSimCard');
        if (qtdSimCard !== '') qtdSimCard = parseFloat(qtdSimCard);
        else qtdSimCard = 0;

        var total = precoUnitarioAparelho*qtdAparelhos;
        var isSimCardComAparelho = getBoolean('isSimCardComAparelho');

        if (isSimCardComAparelho === 1) {
            total =  total + (qtdSimCard*valorSimCardComTelefone);
        }

        $('#total').val(total);
    }

    function depoisPreencherPlano() {
        daoFranquia.buscarAllByPlano(getString('plano'), function (json) {
            preencherFranquia(json);
        });
    }

    function depoisPreencherAparelho() {
        daoAparelho.buscaById(getString('aparelho'), function (json) {
            json = json[0];
            setString('fabricante', json.fabricante);
            setCombo('isAvulso', '--');
            $('#divAvulso').hide();

            if ('SIMCARD - TRIPLE' === json.descricao) {
                setCombo('isAvulso', 'Sim');
                $('#divAvulso').show();
            }
        });
    }

    function preencherComboBox() {

        daoAparelho.buscarAll(function (json) {
           preencherComboAtributos('aparelho', json);
        });

        daoSimCard.buscarAll(function (json) {
           preencherComboAtributos('simCard', json);
        });

        daoSimCard.buscarByDescricao('SIMCAD COM APARELHO', function (json) {
            valorSimCardComTelefone = json[0].valor;
        });

        daoSimCard.buscarByDescricao('SIMCARD SEM APARELHO', function (json) {
            valroSimCardSemTelefone = json[0].valor;
        });
    }

    function preencherFranquia(json) {

        $("#franquia").empty();
        $('#franquia').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#franquia').append($('<option>', {
                value: item.id,
                text : item.franquia
            }));
        });
        setCombo('franquia','');
    }

    function preencherPlano(json) {
        $("#plano").empty();
        $('#plano').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));

        jQuery.each(json, function(i, item) {
            $('#plano').append($('<option>', {
                value: item.id,
                text : item.descricao,
                tipo: item.tipo,

            }));
        });
    }

    function createdb() {
        daoPropostaAparelho.create();
    }

    function dropdb() {
        daoPropostaAparelho.drop();
    }

    function save(event) {
        daoPropostaAparelho.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {

        var idPropostaAparelho = getString("idPropostaAparelho");
        var plano = getString('plano');
        var uf = getString('uf');
        var ddd = getString('ddd');
        var aparelho = getString('aparelho');
        var cor = getString('cor');
        var fabricante = getString('fabricante');
        var isSimCardComAparelho = getBoolean('isSimCardComAparelho');
        var simCard = getString('simCard');
        var qtdSimCard = getString('qtdSimCard');
        var precoUnitarioSimCard = getString('precoUnitarioSimCard');
        var qtdAparelhos = getString('qtdAparelhos');
        var precoUnitarioAparelho = getString('precoUnitarioAparelho');
        var pontuacao = getString('pontuacao');
        var franquia = getString('franquia');
        var isAvulso = getString('isAvulso');
        var total = getString('total');
        var formaPagamento = getString('formaPagamento');
        var tipoCobranca = getString('tipoCobranca');
        var tipoPlanoAparelho = getString('tipoPlanoAparelho');
        var operadoraInterna = getString('operadoraInterna');

        var propostaAparelho = new PropostaAparelho();

        if (PROPOSTA !== null)   propostaAparelho.setProposta(PROPOSTA);
        else   propostaAparelho.setProposta(getString('idPropostaBy'));

        propostaAparelho.setId(idPropostaAparelho);
        propostaAparelho.setPlano(plano);
        propostaAparelho.setUf(uf);
        propostaAparelho.setDdd(ddd);
        propostaAparelho.setAparelho(aparelho);
        propostaAparelho.setCor(cor);
        propostaAparelho.setFabricante(fabricante);
        propostaAparelho.setIsSimCardComAparelho(isSimCardComAparelho);
        propostaAparelho.setSimCard(simCard);
        propostaAparelho.setQtdSimCard(qtdSimCard);
        propostaAparelho.setPrecoUnitarioSimCard(precoUnitarioSimCard);
        propostaAparelho.setQtdAparelhos(qtdAparelhos);
        propostaAparelho.setPrecoUnitarioAparelho(precoUnitarioAparelho);
        propostaAparelho.setPontuacao(pontuacao);
        propostaAparelho.setFranquia(franquia);
        propostaAparelho.setIsAvulso(isAvulso);
        propostaAparelho.setTotal(total);
        propostaAparelho.setFormaPagamento(formaPagamento);
        propostaAparelho.setTipoCobranca(tipoCobranca);
        propostaAparelho.setTipoPlanoAparelho(tipoPlanoAparelho);
        propostaAparelho.setOperadoraInterna(operadoraInterna);

        return propostaAparelho;
    }

    function popularDadosTela(json) {

        json = json[0];

        var idPropostaAparelho = json.id;
        var idProposta = json.proposta;

        var plano =  json.plano;
        var uf =  json.uf;
        var ddd = json.ddd;
        var aparelho = json.aparelho;
        var cor = json.cor;
        var fabricante =  json.fabricante;
        var isSimCardComAparelho = json.isSimCardComAparelho;
        var simCard = json.simCard;
        var qtdSimCard = json.qtdSimCard;
        var precoUnitarioSimCard = json.precoUnitarioSimCard;
        var qtdAparelhos = json.qtdAparelhos;
        var precoUnitarioAparelho =  json.precoUnitarioAparelho;
        var pontuacao = json.pontuacao;
        var franquia = json.franquia;
        var isAvulso = json.isAvulso;
        var total =  json.total;
        var formaPagamento = json.formaPagamento;
        var tipoCobranca = json.tipoCobranca;
        var tipoPlanoAparelho = json.tipoPlanoAparelho;
        var operadoraInterna = json.operadoraInterna;

        daoFranquia.buscarAllByPlano(plano, function (jsonR) {
            $("#franquia").empty();
            $('#franquia').append($('<option>', {
                value: '',
                text : 'Selecione uma opção'
            }));

            jQuery.each(jsonR, function(i, item) {
                $('#franquia').append($('<option>', {
                    value: item.id,
                    text : item.franquia
                }));
            });
            setCombo('franquia', franquia);
        });


        daoPlano.buscarAllByTipo(tipoPlanoAparelho, function (json) {
            preencherPlano(json);
            setCombo('plano', plano);
        });

        setString('idPropostaBy', idProposta);
        setString('idPropostaAparelho', idPropostaAparelho);
        setString('uf', uf);
        setString('ddd',ddd);
        setString('cor', cor);
        setString('fabricante',fabricante);
        setBoolean('isSimCardComAparelho', isSimCardComAparelho);
        setCombo('simCard', simCard);
        setString('qtdSimCard', qtdSimCard);
        setString('precoUnitarioSimCard', precoUnitarioSimCard);
        setString('qtdAparelhos', qtdAparelhos);
        setString('precoUnitarioAparelho', precoUnitarioAparelho);
        setString('pontuacao', pontuacao);
        setString('isAvulso', isAvulso);
        setString('total', total);
        setCombo('formaPagamento', formaPagamento);
        setCombo('tipoCobranca', tipoCobranca);
        setCombo('operadoraInterna', operadoraInterna);

        depoisPreencherOperadora();

        setCombo('tipoPlanoAparelho', tipoPlanoAparelho);

        daoAparelho.buscarAll(function (json) {
            preencherComboAtributos('aparelho', json);
            setCombo('aparelho',aparelho);
        });

        if ('PRECO SUBSIDIADO 100 PTS (EM 24X)' === tipoCobranca) {
            $('#divPontuacao').show();
            setCombo('pontuacao', '100');
        } else if ('PRECO SUBSIDIADO 140 PTS (EM 24X)' === tipoCobranca) {
            $('#divPontuacao').show();
            setCombo('pontuacao', '140');
        } else if ('CLARO TOTAL INDIVIDUAL' === tipoCobranca) {
            $('#divFranquia').show();
        } else  if ('CLARO TOTAL COMPARTILHADO' === tipoCobranca) {
            $('#divFranquia').show();
            $('#divPontuacao').show();
        }
    }

    function buscarAll(successCallback) {
        daoPropostaAparelho.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save proposta aparelho '+retorno);
        atualizarProposta();
    }

    function atualizarProposta() {
        var proposta = null;
        if (PROPOSTA !== null) {
            proposta = PROPOSTA;
        } else {
            proposta = getString('idPropostaBy');
            PROPOSTA  = proposta;
        }
        controllerProposta.atualizarProposta(proposta);
        //$('#mnsEditarProposta').click();


        $('#liFunilVendas').click();
    }

    function baixar(successCallback) {
        daoPropostaAparelho.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaAparelho.enviar(successCallback);
    }
};