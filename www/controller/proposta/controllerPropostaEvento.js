var ControllerPropostaEvento = function () {

    var controllerNotificacao = new ControllerNotificacao();

    var daoPropostaEvento = new DaoPropostaEvento();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.baixar = baixar;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#eventos", function( event ) {
            menu.initialize();
            daoPropostaEvento.buscarAll(listarEventos);
        });

        $( document ).on( "pageinit", "#adicionarPropostaEvento", function( event ) {
            menu.initialize();

            $('#formAdicionarPropostaEvento').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPropostaEvento", function( event ) {
            menu.initialize();

            $('#formEditarPropostaEvento').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idPropostaEvento = event.delegateTarget.activeElement.attributes['idpropostaevento'].value;
            editar(idPropostaEvento);
            daoPropostaEvento.lido(idPropostaEvento, function (retorno) {console.log('atualizado como lido')});
        });
    }

    function listarEventos(json) {

        $("#listview-eventos").empty();

        jQuery.each(json, function(i, item) {

            var id = item.id;
            var evento = item.evento;
            var innerA = '';

            innerA  += '<li>';
            innerA  += '    <a href="view/proposta/evento/editarEvento.html" data-ajax="true" idPropostaEvento="'+id+'" data-role="button">';
            innerA  += '        <h2>'+evento+'</h2>';
            innerA  += '    </a>';
            innerA  += '</li>';

            $("#listview-eventos").append(innerA);
        });

        $('ul').listview().listview('refresh');
    }

    function editar(idPropostaEvento) {
        daoPropostaEvento.buscaById(idPropostaEvento, popularDadosTela);
    }

    function createdb() {
        daoPropostaEvento.create();
    }

    function dropdb() {
        daoPropostaEvento.drop();
    }

    function save(event) {
        daoPropostaEvento.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarAll(successCallback) {
        daoPropostaEvento.buscarAll(successCallback)
    }

    function buscarDadosTela() {

        var idPropostaMigracao = getString('idPropostaEvento');
        var data = getString('data');
        var hora = getString('hora');
        var evento = getString('evento');
        var lido = getBoolean('lido');
        var resposta = getString('resposta');

        var propostaEvento = new PropostaEvento();

        if (PROPOSTA !== null)   propostaEvento.setProposta(PROPOSTA);
        else   propostaEvento.setProposta(getString('idPropostaBy'));

        propostaEvento.setId(idPropostaMigracao);
        propostaEvento.setData(data);
        propostaEvento.setHora(hora);
        propostaEvento.setLido(lido);
        propostaEvento.setEvento(evento);

        propostaEvento.setDataLido(formatDate(new Date()));
        propostaEvento.setHoraLido(getTime(new Date()));

        propostaEvento.setResposta(resposta);
        return propostaEvento;
    }

    function popularDadosTela(json) {

        json = json[0];

        var idPropostaEvento = json.id;
        var idProposta = json.proposta;

        var data = json.data;
        var hora = json.hora;
        var evento = json.evento;
        var lido = json.lido;
        var dataLido = json.dataLido;
        var horaLido = json.horaLido;
        var resposta = json.resposta;

        setString('idPropostaBy', idProposta);
        setString('idPropostaEvento', idPropostaEvento);

        setString('data', data);
        setString('hora', hora);
        setString('evento', evento);
        setString('dataLido', dataLido);
        setString('horaLido', horaLido);
        setString('resposta', resposta);
        setBoolean('lido', lido);
    }

    function depoisSalvar(retorno) {
        console.log('save porposta evento '+retorno);
        $('#mnPropostasAbertas').click();
    }

    function baixar(successCallback) {
        daoPropostaEvento.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaEvento.enviar(successCallback);
    }
};