var ControllerPropostaFranquia = function () {

    var daoPropostaFranquia = new DaoPropostaFranquia();
    var daoPlano = new DaoPlano();
    var daoProposta = new DaoProposta();
    var daoSms = new DaoSms();

    var controllerCodigoAnatel = new ControllerCodigoAnatel();
    var controllerRegiao = new ControllerRegiao();
    var controllerTipoSolicitacaoCliente = new ControllerTipoSolicitacaoProposta();
    var controllerFranquia = new ControllerFranquia();
    var controllerPassaporte = new ControllerPassaporte();
    var controllerNotificacao = new ControllerNotificacao();
    var controllerProposta = new ControllerProposta();

    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#adicionarPropostaFranquia", function( event ) {

            menu.initialize();
            preencherComboBox();
            blurCampos();
            formatarFomulario('');

            $('#formAdicionarFranquia').submit(function (event) {
                event.preventDefault();
                save(event);
            });

        });

        $( document ).on( "pageinit", "#editarPropostaFranquia", function( event ) {

            menu.initialize();
            preencherComboBox();
            blurCampos();

            $('#formEditarFranquia').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPropostaFranquia').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta linha?';
                var title = 'Excluir Linha';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });

            var idPropostaFranquia = event.delegateTarget.activeElement.attributes['idpropostafranquia'].value;
            editar(idPropostaFranquia);
        });
    }

    function blurCampos() {
        $('#plano').blur(function (event) {
            depoisPreencherPlano();
        });

        $('#franquia').blur(function (event) {
            preencherInformacoesFranquia();
        });

        $('.qtdLinhas').blur(function (event) {
            totalizador();
        });

        $('#valorSms').blur(function (event) {
            totalizador();
        });

        $('#qtdSms').blur(function (event) {
            totalizador();
        });

        $('#qtdLinhasBaseCliente').blur(function (event) {
           totalizador();
        });

        $('#valorPassaporteAvulso').blur(function (event) {
            totalizador();
        });

        $('#qtdSmartphones').blur(function (event) {
            totalizador();
        });

        $('#qtdTabletModem').blur(function (event) {
            totalizador();
        });

        $('#qtdCelulares').blur(function (event) {
            totalizador();
        });

        $('#qtdVozCelular').blur(function (event) {
            totalizador();
        });

        $('#passaporteAvulso').blur(function (event) {
            preencherValorPassaporteAvulso();
        });

        $("#sms").blur(function (event) {
            depoisPreencherSms();
        });

        $('#tipoPlanoFranquia').blur(function (event) {
            depoisPreencherTipoPlanoFranquia();
        });
    }

    function depoisPreencherTipoPlanoFranquia() {
        daoPlano.buscarAllByTipo(getString('tipoPlanoFranquia'), function (json) {
            preencherPlano(json);
        })
    }

    function depoisPreencherPlano() {
        controllerFranquia.buscarAllByPlano(getString('plano'), function (json) {
            preencherFranquia(json);
        });

        formatarFomulario( $('#plano').find('option:selected').attr("tipo"));
    }

    function depoisPreencherSms() {
        daoSms.buscaById(getString('sms'), function (json) {
            var valor = json[0].valor;
            setString('valorSms', valor);
            totalizador();
        });
    }

    function preencherValorPassaporteAvulso() {
        totalizador();
    }

    function preencherInformacoesFranquia() {
        controllerFranquia.buscarById(getString('franquia'), function (json) {

            var franquiaJson = json[0];

            setString('strFranquia', franquiaJson.franquia);
            setString('valorFranquia', franquiaJson.valor);

            var isPossuiMobilidade = franquiaJson.isPossuiMobilidade;
            var valorMobilidade = franquiaJson.valorMobilidade;
            var isPossuiRedesSociais = franquiaJson.isPossuiRedesSociais;
            var valorRedesSociais = franquiaJson.valorRedesSociais;
            var passaporte = franquiaJson.passaporte;
            var valorPassaporte = franquiaJson.valorPassaporte;

            if (isPossuiMobilidade === 1) {
                $('#divMobilidade').show();
                if (valorMobilidade > 0)  $('#divValorMobildade').show();

                setBoolean('isPossuiMobilidade', 1);
                setString('valorMobilidade', valorMobilidade);

            } else {
                $('#divMobilidade').hide();
                $('#divValorMobildade').hide();
                setBoolean('isPossuiMobilidade', 0);
                setString('valorMobilidade', '0,00');
            }

            if (isPossuiRedesSociais) {
                $('#divRedesSociais').show();
                if (valorRedesSociais > 0) $('#divValoreRedesSociais').show();

                setBoolean('isPossuiRedesSociais', 1);
                setString('valorRedesSociais', valorRedesSociais);
            } else {
                $('#divRedesSociais').hide();
                $('#divValoreRedesSociais').hide();
                setBoolean('isPossuiRedesSociais', 0);
                setString('valorRedesSociais', '0,00');
            }

            if (passaporte !== null) {
                $('#divPassaporteIncluso').show();
                if (valorPassaporte > 0) $('#divValorPassporte').show();

                setCombo('passaporte', passaporte);
                setString('valorPassaporte', valorPassaporte);
            } else {
                $('#divPassaporteIncluso').hide();
                $('#divValorPassporte').hide();
                setCombo('passaporte', '');
                setString('valorPassaporte', '0,00');
            }


            var tipo = $('#plano').find('option:selected').attr("tipo");

            if (tipo === 'Total Share   GOL - Claro Total PME (Anatel N 072)') {
                setString('limiteLinhasPlano', franquiaJson.maximoLinhaComGol);
            }

            if (tipo === 'Total Share - Claro Total PME (Anatel N 072)') {
                setString('limiteLinhasPlano', franquiaJson.maximoLinhaSemGol);
            }

            setString('bonus', franquiaJson.bonus);
            totalizador();
        });
    }

    function formatarFomulario(tipo) {

        var idProposta = '';
        if (PROPOSTA !== null)  idProposta = PROPOSTA;
        else idProposta  = getString('idPropostaBy');

        daoProposta.buscaById(idProposta, function (json) {

            $('#divCodigoAnatel').hide();
            $('#divFranquia').hide();
            $('#divMobilidade').hide();
            $('#divValorMobildade').hide();
            $('#divRedesSociais').hide();
            $('#divValoreRedesSociais').hide();
            $('#divPassaporteIncluso').hide();
            $('#divValorPassporte').hide();
            $('#divPassaporteAvulso').hide();
            $('#divValorPassaporteAvulso').hide();
            $('#divLimiteLinhasPorPlano').hide();
            $('#divQtdLinhasBaseCliente').hide();
            $('#divQtdLinhasUteis').hide();
            $('#divQtdSmartphones').hide();
            $('#divQtdTabletModem').hide();
            $('#divQtdCelulares').hide();
            $('#divQtdVozCelular').hide();
            $('#divTipoSMS').hide();
            $('#divValorSms').hide();
            $('#divQtdSms').hide();
            $('#divTradeIn').hide();
            $('#divDobroBonus').hide();
            $('#divDDD').hide();
            $('#divRegiao').hide();
            $('#divTipoSolicitacaoProposta').hide();
            $('#divLabelTotal').html('Total (R$):');
            $('#divQtdLinhas').show();

            if (json[0] !== undefined) {
                if (json[0].isPedido === 1) {
                    if (tipo === 'Plano Claro Total Individual - Claro Life (Anatel N 190)') {
                        $('#divLabelValorFranquia').html('Valor (R$):');
                        $('#divLabelTotal').html('Total (R$) <br/>(Franquia + Plug-ins + pacotes):');

                        $('#divMobilidade').show();
                        $('#divValorMobildade').show();
                        $('#divRedesSociais').show();
                        $('#divValoreRedesSociais').show();
                        $('#divPassaporteIncluso').show();
                        $('#divValorPassporte').show();
                        $('#divPassaporteAvulso').show();
                        $('#divValorPassaporteAvulso').show();
                        $('#divDobroBonus').show();
                        $('#divQtdLinhas').show();
                        $('#divDDD').show();
                        $('#divRegiao').show();
                        $('#divTipoSolicitacaoProposta').show();
                    }

                    if (tipo === 'Plano Internet Movel Individual (Anatel N 29 / 41 / 45 / 47 )') {
                        $('#divLabelValorFranquia').html('Valor Unitário (R$):');
                        $('#divLabelTotal').html('Total (R$):');

                        $('#divCodigoAnatel').show();
                        $('#divFranquia').show();
                        $('#divDobroBonus').show();
                        $('#divQtdLinhas').show();
                        $('#divDDD').show();
                        $('#divRegiao').show();
                        $('#divTipoSolicitacaoProposta').show();
                    }

                    if (tipo === 'Total Share - Claro Total PME (Anatel N 072)') {
                        $('#divLabelTotal').html('Total (R$) <br/>(Franquia + Plug-ins + pacotes):');

                        $('#divLimiteLinhasPorPlano').show();
                        $('#divQtdLinhasBaseCliente').show();
                        $('#divQtdLinhasUteis').show();
                        $('#divQtdSmartphones').show();
                        $('#divQtdTabletModem').show();
                        $('#divQtdCelulares').show();
                        $('#divQtdVozCelular').show();
                        $('#divTipoSMS').show();
                        $('#divValorSms').show();
                        $('#divQtdSms').show();
                        $('#divTradeIn').show();
                        $('#divPassaporteAvulso').show();
                        $('#divValorPassaporteAvulso').show();
                        $('#divDDD').show();
                        $('#divRegiao').show();
                        $('#divTipoSolicitacaoProposta').show();

                        setString('qtdLinhas', 1);
                    }

                    if (tipo === 'Total Share   GOL - Claro Total PME (Anatel N 072)') {
                        $('#divLabelTotal').html('Total (R$) <br/>(Franquia + Plug-ins + SMS):');

                        $('#divLimiteLinhasPorPlano').show();
                        $('#divQtdLinhasBaseCliente').show();
                        $('#divQtdLinhasUteis').show();
                        $('#divQtdSmartphones').show();
                        $('#divQtdTabletModem').show();
                        $('#divQtdCelulares').show();
                        $('#divQtdVozCelular').show();
                        $('#divTipoSMS').show();
                        $('#divValorSms').show();
                        $('#divQtdSms').show();
                        $('#divTradeIn').show();
                        $('#divPassaporteAvulso').show();
                        $('#divValorPassaporteAvulso').show();
                        $('#divDDD').show();
                        $('#divRegiao').show();
                        $('#divTipoSolicitacaoProposta').show();

                        setString('qtdLinhas', 1);
                    }
                }
            }

        });
    }

    function totalizador() {

        var valorFranquia = parseDouble(getString('valorFranquia'));
        var qtdLinhas = parseDouble(getString('qtdLinhas'));

        var valorMobilidade = parseDouble(getString('valorMobilidade'));
        var valorRedesSociais = parseDouble(getString('valorRedesSociais'));
        var valorPassaporte = parseDouble(getString('valorPassaporte'));
        var valorPassaporteAvulso = parseDouble(getString('valorPassaporteAvulso'));

        var valorSms =  parseDouble(getString('valorSms'));
        var qtdSms = parseDouble(getString('qtdSms'));

        var limiteLinhasPlano = parseDouble(getString('limiteLinhasPlano'));
        var qtdLinhasBaseCliente = parseDouble(getString('qtdLinhasBaseCliente'));
        var qtdLinhasUteis = limiteLinhasPlano - qtdLinhasBaseCliente;

        setString('qtdLinhasUteis', qtdLinhasUteis);

        var qtdSmartphones = parseDouble(getString('qtdSmartphones'))*SMARTPHONE;
        var qtdTabletModem = parseDouble(getString('qtdTabletModem'))*TABLET_MODEM;
        var qtdCelulares = parseDouble(getString('qtdCelulares'))*CELULAR;
        var qtdVozCelular = parseDouble(getString('qtdVozCelular'))*VOZ_CELULAR;

        var plugins = (qtdSmartphones + qtdTabletModem + qtdCelulares + qtdVozCelular);

        var total = (valorFranquia*qtdLinhas) + valorMobilidade + valorRedesSociais + valorPassaporte + valorPassaporteAvulso + (valorSms*qtdSms) + plugins;

        setString('total', total.toFixed(2));
    }

    function zerarTotalizador() {
        setString('valorFranquia', 0);
        setString('valorMobilidade', 0);
        setString('valorRedesSociais', 0);
        setString('valorPassaporte', 0);
        setString('valorPassaporteAvulso', 0);
        setString('total', 0);
        setString('strFranquia','');
        setString('bonus','');
    }

    function preencherComboBox() {

        controllerCodigoAnatel.buscarAll(function (json) {
           preencherComboAtributos('codAnatel', json);
        });

        controllerRegiao.buscarAll(function (json) {
           preencherComboAtributos('regiao', json);
        });

        controllerTipoSolicitacaoCliente.buscarAll(function (json) {
           preencherComboAtributos('tipoSolicitacaoProposta', json);
        });

        controllerPassaporte.buscarAll(function (json) {
           preencherComboAtributos('passaporte', json);
           preencherComboAtributos('passaporteAvulso', json);
        });

        daoSms.buscarAll(function (json) {
            preencherComboAtributos('sms', json);
        });
    }

    function preencherPlano(json) {
        $("#plano").empty();
        $('#plano').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#plano').append($('<option>', {
                value: item.id,
                text : item.descricao,
                tipo: item.tipo,
            }));
        });
    }

    function preencherFranquia(json) {
        $("#franquia").empty();
        $('#franquia').append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#franquia').append($('<option>', {
                value: item.id,
                text : item.franquia
            }));
        });

        setCombo('franquia','');
        zerarTotalizador();
    }

    function createdb() {
        daoPropostaFranquia.create();
    }

    function dropdb() {
        daoPropostaFranquia.drop();
    }

    function save(event) {
        daoPropostaFranquia.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {

        var idPropostaFranquia = getString('idPropostaFranquia');

        var strFranquia = getString('strFranquia');
        var codAnatel = getString('codAnatel');
        var plano = getString('plano');
        var regiao = getString('regiao');
        var operadoraPortabilidade = getString('operadoraPortabilidade');
        var isPortabilidade = getBoolean('isPortabilidade');
        var ddd = getString('ddd');
        var telefonePortabilidade = getString('telefonePortabilidade');
        var tipoSolicitacaoProposta = getString('tipoSolicitacaoProposta');
        var franquia = getString('franquia');
        var valorFranquia = getString('valorFranquia');
        var bonus = getString('bonus');
        var qtdLinhas = getString('qtdLinhas');
        var isPossuiMobilidade = getBoolean('isPossuiMobilidade');
        var valorMobilidade = getString('valorMobilidade');
        var isPossuiRedesSociais = getBoolean('isPossuiRedesSociais');
        var valorRedesSociais = getString('valorRedesSociais');
        var passaporte = getString('passaporte');
        var valorPassaporte = getString('valorPassaporte');
        var passaporteAvulso = getString('passaporteAvulso');
        var valorPassaporteAvulso = getString('valorPassaporteAvulso');
        var responsavel = getString('responsavel');
        var total = getString('total');
        var observacao = getString('observacao');
        var limiteLinhasPlano = getString('limiteLinhasPlano');
        var qtdLinhasBaseCliente = getString('qtdLinhasBaseCliente');
        var qtdLinhasUteis = getString('qtdLinhasUteis');
        var qtdSmartphones = getString('qtdSmartphones');
        var qtdTabletModem = getString('qtdTabletModem');
        var qtdCelulares = getString('qtdCelulares');
        var qtdVozCelular = getString('qtdVozCelular');
        var tradeIn = getString('tradeIn');
        var sms = getString('sms');
        var valorSms = getString('valorSms');
        var qtdSms = getString('qtdSms');
        var tipoPlanoFranquia = getString('tipoPlanoFranquia');

        var propostaFranquia = new PropostaFranquia();

        if (PROPOSTA !== null)   propostaFranquia.setProposta(PROPOSTA);
        else   propostaFranquia.setProposta(getString('idPropostaBy'));

        propostaFranquia.setId(idPropostaFranquia);
        propostaFranquia.setStrFranquia(strFranquia);
        propostaFranquia.setCodAnatel(codAnatel);
        propostaFranquia.setPlano(plano);
        propostaFranquia.setRegiao(regiao);
        propostaFranquia.setOperadoraPortabilidade(operadoraPortabilidade);
        propostaFranquia.setIsPortabilidade(isPortabilidade);
        propostaFranquia.setDdd(ddd);
        propostaFranquia.setTelefonePortabilidade(telefonePortabilidade);
        propostaFranquia.setTipoSolicitacaoProposta(tipoSolicitacaoProposta);
        propostaFranquia.setFranquia(franquia);
        propostaFranquia.setValorFranquia(valorFranquia);
        propostaFranquia.setBonus(bonus);
        propostaFranquia.setQtdLinhas(qtdLinhas);
        propostaFranquia.setIsPossuiMobilidade(isPossuiMobilidade);
        propostaFranquia.setValorMobilidade(valorMobilidade);
        propostaFranquia.setIsPossuiRedesSociais(isPossuiRedesSociais);
        propostaFranquia.setValorRedesSociais(valorRedesSociais);
        propostaFranquia.setPassaporte(passaporte);
        propostaFranquia.setValorPassaporte(valorPassaporte);
        propostaFranquia.setPassaporteAvulso(passaporteAvulso);
        propostaFranquia.setValorPassaporteAvulso(valorPassaporteAvulso);
        propostaFranquia.setResponsavel(responsavel);
        propostaFranquia.setTotal(total);
        propostaFranquia.setObservacao(observacao);
        propostaFranquia.setLimiteLinhasPlano(limiteLinhasPlano);
        propostaFranquia.setQtdLinhasBaseCliente(qtdLinhasBaseCliente);
        propostaFranquia.setQtdLinhasUteis(qtdLinhasUteis);
        propostaFranquia.setQtdSmartphones(qtdSmartphones);
        propostaFranquia.setQtdTabletModem(qtdTabletModem);
        propostaFranquia.setQtdCelulares(qtdCelulares);
        propostaFranquia.setQtdVozCelular(qtdVozCelular);
        propostaFranquia.setTradeIn(tradeIn);
        propostaFranquia.setSms(sms);
        propostaFranquia.setValorSms(valorSms);
        propostaFranquia.setQtdSms(qtdSms);
        propostaFranquia.setTipoPlanoFranquia(tipoPlanoFranquia);

        return propostaFranquia;
    }


    function editar(idPropostaFranquia) {
        daoPropostaFranquia.buscaById(idPropostaFranquia, popularDadosTela);
    }

    function popularDadosTela(json) {

        var json = json[0];
        var idPropostaFranquia = json.id;
        var idProposta = json.proposta;

        var tipoPlanoFranquia = json.tipoPlanoFranquia;
        var strFranquia = json.strFranquia;
        var codAnatel = json.codAnatel;
        var plano = json.plano;
        var regiao = json.regiao;
        var operadoraPortabilidade = json.operadoraPortabilidade;
        var isPortabilidade =  json.isPortabilidade;
        var ddd = json.ddd;
        var telefonePortabilidade =  json.telefonePortabilidade;
        var tipoSolicitacaoProposta = json.tipoSolicitacaoProposta;
        var franquia = json.franquia;
        var valorFranquia = json.valorFranquia;
        var bonus = json.bonus;
        var qtdLinhas =  json.qtdLinhas;
        var isPossuiMobilidade =  json.isPossuiMobilidade;
        var valorMobilidade = json.valorMobilidade;
        var isPossuiRedesSociais = json.isPossuiRedesSociais;
        var valorRedesSociais = json.valorRedesSociais;
        var passaporte = json.passaporte;
        var valorPassaporte = json.valorPassaporte;
        var passaporteAvulso = json.passaporteAvulso;
        var valorPassaporteAvulso =  json.valorPassaporteAvulso;
        var responsavel =  json.responsavel;
        var total =  json.total;
        var observacao = json.observacao;
        var limiteLinhasPlano = json.limiteLinhasPlano;
        var qtdLinhasBaseCliente = json.qtdLinhasBaseCliente;
        var qtdLinhasUteis = json.qtdLinhasUteis;
        var qtdSmartphones = json.qtdSmartphones;
        var qtdTabletModem = json.qtdTabletModem;
        var qtdCelulares = json.qtdCelulares;
        var qtdVozCelular = json.qtdVozCelular;
        var tradeIn = json.tradeIn;
        var sms = json.sms;
        var valorSms = json.valorSms;
        var qtdSms = json.qtdSms;


        controllerFranquia.buscarAllByPlano(plano, function (json) {
            $("#franquia").empty();
            $('#franquia').append($('<option>', {
                value: '',
                text : 'Selecione uma opção'
            }));
            jQuery.each(json, function(i, item) {
                $('#franquia').append($('<option>', {
                    value: item.id,
                    text : item.franquia
                }));
            });
            setCombo('franquia', franquia);
            preencherInformacoesFranquia();
        });

        daoPlano.buscarAllByTipo(tipoPlanoFranquia, function (json) {
            preencherPlano(json);
            setCombo('plano', plano);
            formatarFomulario( $('#plano').find('option:selected').attr("tipo"));
        });

        setString('idPropostaBy', idProposta);
        setString('idPropostaFranquia', idPropostaFranquia);
        setString('strFranquia', strFranquia);
        setCombo('codAnatel', codAnatel);
        setCombo('regiao', regiao);
        setCombo('operadoraPortabilidade', operadoraPortabilidade);
        setBoolean('isPortabilidade', isPortabilidade);
        setString('ddd', ddd);
        setString('telefonePortabilidade', telefonePortabilidade);
        setCombo('tipoSolicitacaoProposta', tipoSolicitacaoProposta);
        setString('valorFranquia', valorFranquia);
        setString('bonus', bonus);
        setBoolean('isPossuiMobilidade', isPossuiMobilidade);
        setString('valorMobilidade', valorMobilidade);
        setBoolean('isPossuiRedesSociais', isPossuiRedesSociais);
        setString('valorRedesSociais', valorRedesSociais);
        setCombo('passaporte', passaporte);
        setString('valorPassaporte', valorPassaporte);
        setCombo('passaporteAvulso', passaporteAvulso);
        setString('valorPassaporteAvulso', valorPassaporteAvulso);
        setCombo('responsavel', responsavel);
        setString('observacao', observacao);
        setString("total", total);
        setString('limiteLinhasPlano', limiteLinhasPlano);
        setString('qtdLinhasBaseCliente', qtdLinhasBaseCliente);
        setString('qtdLinhasUteis', qtdLinhasUteis);
        setString('qtdSmartphones', qtdSmartphones);
        setString('qtdTabletModem', qtdTabletModem);
        setString('qtdCelulares', qtdCelulares);
        setString('qtdVozCelular', qtdVozCelular);
        setCombo('tradeIn', tradeIn);
        setCombo('sms', sms);
        setString('valorSms', valorSms);
        setString('qtdSms', qtdSms);
        setCombo('tipoPlanoFranquia', tipoPlanoFranquia);

        $('.qtdLinhas').val(qtdLinhas)
    }

    function buscarAll(successCallback) {
        daoPropostaFranquia.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save proposta franquia '+retorno);
        atualizarProposta();
    }

    function atualizarProposta() {
        var proposta = null;
        if (PROPOSTA !== null)  {
            proposta = PROPOSTA;
        } else {
            proposta = getString('idPropostaBy');
            PROPOSTA = proposta;
        }

        controllerProposta.atualizarProposta(proposta);
        //$('#mnsEditarProposta').click();

        $('#liFunilVendas').click();

    }

    function excluir() {
        var idPropostaFranquia = getString('idPropostaFranquia');
        daoPropostaFranquia.deletar(idPropostaFranquia);
        depoisSalvar(true);
    }

    function baixar(successCallback) {
        daoPropostaFranquia.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaFranquia.enviar(successCallback);
    }

};