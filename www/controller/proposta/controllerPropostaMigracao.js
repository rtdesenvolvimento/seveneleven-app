var ControllerPropostaMigracao = function () {

    var controllerNotificacao = new ControllerNotificacao();

    var daoPropostaMigracao = new DaoPropostaMigracao();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.baixar = baixar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPropostaMigracao", function( event ) {
            menu.initialize();

            $('#formAdicionarPropostaMigracao').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPropostaMigracao", function( event ) {
            menu.initialize();

            $('#formEditarPropostaMigracao').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPropostaMigracao').click(function (event) {
                var mensagem = 'Deseja realmente excluir estes número da migração?';
                var title = 'Excluir Migração';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });
            var idPropostaMigracao = event.delegateTarget.activeElement.attributes['idpropostamigracao'].value;
            editar(idPropostaMigracao);
        });
    }

    function excluir() {
        var idPropostaMigracao = getString('idPropostaMigracao');
        daoPropostaMigracao.deletar(idPropostaMigracao);
        depoisSalvar(true);
    }

    function editar(idPropostaMigracao) {
        daoPropostaMigracao.buscaById(idPropostaMigracao, popularDadosTela);
    }

    function createdb() {
        daoPropostaMigracao.create();
    }

    function dropdb() {
        daoPropostaMigracao.drop();
    }

    function save(event) {
        daoPropostaMigracao.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarAll(successCallback) {
        daoPropostaMigracao.buscarAll(successCallback)
    }

    function buscarDadosTela() {

        var idPropostaMigracao = getString('idPropostaMigracao');
        var telefones = getString('telefones');
        var nomeAssinanteDoador = getString('nomeAssinanteDoador');
        var cpf = getString('cpf');
        var telefoneContato = getString('telefoneContato');
        var endereco = getString('endereco');
        var cep = getString('cep');
        var cidade = getString('cidade');
        var estado = getString('estado');

        var propostaMigracao = new PropostaMigracao();

        if (PROPOSTA !== null)   propostaMigracao.setProposta(PROPOSTA);
        else   propostaMigracao.setProposta(getString('idPropostaBy'));

        propostaMigracao.setId(idPropostaMigracao);
        propostaMigracao.setTelefones(telefones);
        propostaMigracao.setNomeAssinanteDoador(nomeAssinanteDoador);
        propostaMigracao.setCpf(cpf);
        propostaMigracao.setEndereco(endereco);
        propostaMigracao.setTelefoneContato(telefoneContato);
        propostaMigracao.setCep(cep);
        propostaMigracao.setCidade(cidade);
        propostaMigracao.setEstado(estado);

        return propostaMigracao;
    }

    function popularDadosTela(json) {

        json = json[0];

        var idPropostaMigracao= json.id;
        var idProposta = json.proposta;
        var telefones = json.telefones;
        var nomeAssinanteDoador = json.nomeAssinanteDoador;
        var cpf = json.cpf;
        var telefoneContato = json.telefoneContato;
        var endereco = json.endereco;
        var cep = json.cep;
        var cidade = json.cidade;
        var estado = json.estado;

        setString('idPropostaBy', idProposta);
        setString('idPropostaMigracao', idPropostaMigracao);
        setString('telefones', telefones);
        setString('nomeAssinanteDoador', nomeAssinanteDoador);
        setString('cpf', cpf);
        setString('telefoneContato', telefoneContato);
        setString('endereco', endereco);
        setString('cep', cep);
        setString('cidade', cidade);
        setString('estado', estado);
    }

    function depoisSalvar(retorno) {
        console.log('save porposta portabilidade '+retorno);
        $('#mnPropostasAbertas').click();
    }

    function baixar(successCallback) {
        daoPropostaMigracao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaMigracao.enviar(successCallback);
    }

}