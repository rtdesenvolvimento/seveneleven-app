var ControllerPropostaModulo = function () {

    var controllerNotificacao = new ControllerNotificacao();

    var daoPropostaModulo = new DaoPropostaModulo();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.baixar = baixar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPropostaModulo", function( event ) {
            menu.initialize();

            $('#formAdicionarPropostaModulo').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPropostaModulo", function( event ) {
            menu.initialize();

            $('#formEditarPropostaModulo').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPropostaModulo').click(function (event) {
                var mensagem = 'Deseja realmente excluir estes número do modulo?';
                var title = 'Excluir Modulo';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });
            var idPropostaModulo = event.delegateTarget.activeElement.attributes['idpropostamodulo'].value;
            editar(idPropostaModulo);
        });
    }

    function excluir() {
        var idPropostaModulo = getString('idPropostaModulo');
        daoPropostaModulo.deletar(idPropostaModulo);
        depoisSalvar(true);
    }

    function editar(idPropostaModulo) {
        daoPropostaModulo.buscaById(idPropostaModulo, popularDadosTela);
    }

    function createdb() {
        daoPropostaModulo.create();
    }

    function dropdb() {
        daoPropostaModulo.drop();
    }

    function save(event) {
        daoPropostaModulo.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarAll(successCallback) {
        daoPropostaModulo.buscarAll(successCallback)
    }

    function buscarDadosTela() {

        var idPropostaPortabilidade = getString('idPropostaPortabilidade');
        var plano = getString('plano');
        var telefones = getString('telefones');
        var ddd = getString('ddd');
        var ban = getString('ban');
        var passaporte = getString('passaporte');
        var sms = getString('sms');

        var propostaModulo = new PropostaModulo();

        if (PROPOSTA !== null)   propostaModulo.setProposta(PROPOSTA);
        else   propostaModulo.setProposta(getString('idPropostaBy'));

        propostaModulo.setId(idPropostaPortabilidade);
        propostaModulo.setPlano(plano);
        propostaModulo.setTelefones(telefones);
        propostaModulo.setDdd(ddd);
        propostaModulo.setBan(ban);
        propostaModulo.setPassaporte(passaporte);
        propostaModulo.setSms(sms);

        return propostaModulo;
    }

    function popularDadosTela(json) {

        json = json[0];

        var idPropostaModulo = json.id;
        var idProposta = json.proposta;
        var plano = json.plano;
        var telefones = json.telefones;
        var ddd = json.ddd;
        var ban = json.ban;
        var passaporte = json.passaporte;
        var sms = json.sms;

        setString('idPropostaBy', idProposta);
        setString('idPropostaModulo', idPropostaModulo);
        setString('plano', plano);
        setString('telefones', telefones);
        setString('ddd', ddd);
        setString('ban', ban);
        setString('passaporte', passaporte);
        setString('sms', sms);
    }

    function depoisSalvar(retorno) {
        console.log('save porposta portabilidade '+retorno);
        $('#mnPropostasAbertas').click();
    }

    function baixar(successCallback) {
        daoPropostaModulo.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaModulo.enviar(successCallback);
    }

}