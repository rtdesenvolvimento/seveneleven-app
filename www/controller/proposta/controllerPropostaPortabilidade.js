var ControllerPropostaPortabilidade = function () {

    var controllerNotificacao = new ControllerNotificacao();

    var daoPropostaPortabilidade = new DaoPropostaPortabilidade();
    var daoOperadora = new DaoOperadora();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.baixar = baixar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarPropostaPortabilidade", function( event ) {
            menu.initialize();
            preencherComboBox();

            $('#formAdicionarPropostaPortabilidade').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPropostaPortabilidade", function( event ) {
            menu.initialize();
            preencherComboBox();

            $('#formEditarPropostaPortabilidade').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#excluirPropostaPortabilidade').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta portabilidade?';
                var title = 'Excluir Portabilidade';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });
            var idPropostaPortabilidade = event.delegateTarget.activeElement.attributes['idpropostaportabilidade'].value;
            editar(idPropostaPortabilidade);
        });
    }

    function excluir() {
        var idPropostaPortabilidade = getString('idPropostaPortabilidade');
        daoPropostaPortabilidade.deletar(idPropostaPortabilidade);
        depoisSalvar(true);
    }

    function editar(idPropostaPortabilidade) {
        daoPropostaPortabilidade.buscaById(idPropostaPortabilidade, popularDadosTela);
    }

    function preencherComboBox() {
        daoOperadora.buscarAll(function (json) {
           preencherComboAtributos('operadora', json);
        });
    }

    function createdb() {
        daoPropostaPortabilidade.create();
    }

    function dropdb() {
        daoPropostaPortabilidade.drop();
    }

    function save(event) {
        daoPropostaPortabilidade.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarAll(successCallback) {
        daoPropostaPortabilidade.buscarAll(successCallback)
    }

    function buscarDadosTela() {
        var idPropostaPortabilidade = getString('idPropostaPortabilidade');
        var plano = getString('plano');
        var operadora = getString('operadora');
        var simcard = getString('simcard');
        var telefones = getString('telefones');
        var ddd = getString('ddd');

        var propostaPortabilidade = new PropostaPortabilidade();

        if (PROPOSTA !== null)   propostaPortabilidade.setProposta(PROPOSTA);
        else   propostaPortabilidade.setProposta(getString('idPropostaBy'));

        propostaPortabilidade.setId(idPropostaPortabilidade);
        propostaPortabilidade.setPlano(plano);
        propostaPortabilidade.setOperadora(operadora);
        propostaPortabilidade.setSimcard(simcard);
        propostaPortabilidade.setTelefones(telefones);
        propostaPortabilidade.setDdd(ddd);

        return propostaPortabilidade;
    }

    function popularDadosTela(json) {

        json = json[0];

        var idPropostaPortabilidade = json.id;
        var idProposta = json.proposta;
        var plano = json.plano;
        var operadora = json.operadora;
        var simcard = json.simcard;
        var telefones = json.telefones;
        var ddd = json.ddd;

        setString('idPropostaBy', idProposta);
        setString('idPropostaPortabilidade', idPropostaPortabilidade);
        setString('plano', plano);
        setCombo('operadora', operadora);
        setString('simcard', simcard);
        setString('telefones', telefones);
        setString('ddd', ddd);
    }

    function depoisSalvar(retorno) {
        console.log('save porposta portabilidade '+retorno);
        $('#mnPropostasAbertas').click();
    }

    function baixar(successCallback) {
        daoPropostaPortabilidade.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoPropostaPortabilidade.enviar(successCallback);
    }
};