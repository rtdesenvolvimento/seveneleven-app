var ControllerRespostaProposta = function () {

    var daoProposta = new DaoProposta();
    var daoRespostaProposta = new DaoRespostaProposta();
    var daoRespostaPropostaOpcao = new DaoRespostaPropostaOpcao();
    
    var controllerFormulario = new ControllerFormulario();
    var controllerPerguntaFormulario = new ControllerPerguntaFormulario();
    var controllerPerguntaFormularioOpcao = new ControllerPerguntaFormularioOpcao();
    var controllerRespostaPropostaOpcao = new ControllerRespostaPropostaOpcao();
    var menu = new Menu();

    var formulaFactory = new FormularioFactory();

    this.initialize = initialize;
    this.dropdb = dropdb;
    this.createdb = createdb;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function createdb() {
        daoRespostaProposta.create();
    }

    function dropdb() {
        daoRespostaProposta.drop();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarRespostaProposta", function( event ) {
            menu.initialize();

            $('#formAdicionarRespostaProposta').submit(function (event) {
                event.preventDefault();
                saveAll(event);
            });

            chamarFormulario(FORMULARIO, $('#pagina').val() );
            totalPaginas();
            buscarProposta();

            $('#perguntaFormularioAnterior').click(function (event) {
               anterior();
            });
        });
    }

    function buscarProposta() {

        if (PROPOSTA !== undefined) {
            setString('idProposta', PROPOSTA);
            setString('idPropostaPai', PROPOSTA);
        } else {
            daoProposta.buscarUltimaProposta(function (json) {
                setString('idProposta', json[0].maxId);
                setString('idPropostaPai', json[0].maxId);
            });
        }
    }
    function totalPaginas() {
        controllerPerguntaFormulario.totalPaginas(FORMULARIO, function (json) {
            var totalPaginas = json[0].totalPaginas;
            $('#totalPaginas').val(totalPaginas);
            $('#paginacao').html('Página 1 de ' +totalPaginas);

            if (totalPaginas > 1) {
                $('#btnProxima').html('PROXIMA');
            } else {
                $('#btnProxima').html('ENVIAR');
            }
        })
    }

    function saveAll(event) {
        buscarDadosTela();
    }

    function buscarDadosTela() {

        jQuery.each(JSON_FORMULARIO, function(i, item) {

            var respostaProposta = new RespostaProposta();
            var proposta = getString('idPropostaPai');

            var id = item.id;
            var formulario = item.formulario;
            var descricao = item.descricao;
            var peso = item.peso;
            var obrigatorio = item.obrigatorio;
            var alias = item.alias;
            var tipoCampo = item.tipoCampo;
            var resposta = getString(id);
            var objeto = getObjeto(id);

            var idResposta = objeto.attr('idresposta');

            respostaProposta.setId(idResposta);
            respostaProposta.setProposta(proposta);
            respostaProposta.setAlias(alias);
            respostaProposta.setFormulario(formulario);
            respostaProposta.setPergunta(id);
            respostaProposta.setDescricao(descricao);
            respostaProposta.setPeso(peso);
            respostaProposta.setObrigatorio(obrigatorio);
            respostaProposta.setResposta(resposta);
            respostaProposta.setTipoCampo(tipoCampo);

            daoRespostaProposta.save(respostaProposta, despoisSalvar);

            if (tipoCampo === 'radio' || tipoCampo === 'checkbox') {

                var contador = document.getElementsByName('pergunta_'+id).length;

                for (var h=0;h<contador;h++) {

                    var obj = document.getElementsByName('pergunta_'+id)[h];
                    var opcao = obj.parentElement.innerText;
                    var valor = obj.checked
                    var formularioOpcao = obj.id.split('_')[1];
                    var idRespostaOpcao = obj.getAttribute('idrespostaopcao');

                    if (valor) valor = '1'
                    else valor = '0';

                    var respostaPropostaOpcao = new RespostaPropostaOpcao();

                    respostaPropostaOpcao.setId(idRespostaOpcao);
                    respostaPropostaOpcao.setFormulario(formulario);
                    respostaPropostaOpcao.setAlias(alias);
                    respostaPropostaOpcao.setDescricao(descricao);
                    respostaPropostaOpcao.setOpcao(opcao);
                    respostaPropostaOpcao.setPergunta(id);
                    respostaPropostaOpcao.setPeso(peso);
                    respostaPropostaOpcao.setObrigatorio(obrigatorio);
                    respostaPropostaOpcao.setTipoCampo(tipoCampo);
                    respostaPropostaOpcao.setResposta(valor);
                    respostaPropostaOpcao.setProposta(proposta);
                    respostaPropostaOpcao.setFormularioOpcao(formularioOpcao);

                    controllerRespostaPropostaOpcao.save(respostaPropostaOpcao, function () {
                       console.log('save');
                    });
                }
            }
        });

        proxima();
    }

    function chamarFormulario(idFormulario, pagina) {
        controllerFormulario.buscarPerguntaAll(idFormulario,pagina,montarFormulario);
    }

    function proxima() {
        controle(1);
    }

    function controle(soma) {

        var pagina = parseInt($('#pagina').val());
        var totalPaginas = parseInt($('#totalPaginas').val());
        pagina = pagina + soma;

        if (pagina === 0) {alert('Nenhuma informação anterior!');return;}

        $('#pagina').val(pagina);
        $('#paginacao').html('Página '+pagina+' de ' +totalPaginas);

        if (totalPaginas > pagina) {
            $('#btnProxima').html('PROXIMA');
            chamarFormulario(FORMULARIO, pagina);
        } else if (totalPaginas < pagina){
            $('#mnPropostasAbertas').click();
        } else {
            $('#btnProxima').html('ENVIAR');
            chamarFormulario(FORMULARIO, pagina);
        }
    }
    
    function anterior() {
        controle(-1);
    }

    function despoisSalvar(json) {
        console.log(' salve resposta ' +json);
    }

    function montarFormulario(json) {

        formulaFactory.createFormulario('listview-respostaProposta');
        formulaFactory.destroy();
        JSON_FORMULARIO = json;

        jQuery.each(json, function(i, item) {

            var id = item.id;
            var pergunta = item.pergunta;
            var tipoCampo = item.tipoCampo;
            var obrigatorio = item.obrigatorio;
            var valor = '';
            var classe = '';

            if (obrigatorio === 1) obrigatorio = true;
            else obrigatorio = false;

            if (tipoCampo === 'textarea') {
                formulaFactory.createTextAreaFactory(id, id, pergunta, obrigatorio, false, valor, classe);
            } else if(tipoCampo === 'select') {
                chamarFormularioPerguntaOpcaoSelect(id, pergunta);
            } else if (tipoCampo === 'radio'){
                chamarFormularioPerguntaOpcaoRadio(id, pergunta);
            } else if (tipoCampo === 'checkbox'){
                chamarFormularioPerguntaOpcaoCheckbox(id, pergunta);
            } else {
                formulaFactory.createInputFactory(id, id, pergunta, tipoCampo, obrigatorio, false, valor, classe);
            }
        });
        
        preencherCamposTela();
    }
    
    function preencherCamposTela() {

        var idProposta = getString('idPropostaPai');

        if (idProposta === undefined) { return ;}

        daoRespostaProposta.buscalAllRespostaByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var pergunta = item.pergunta;
                var resposta = item.resposta;
                var tipoCampo = item.tipoCampo;

                if (tipoCampo === 'select') setCombo(pergunta, resposta);
                else setString(pergunta, resposta);

                addAtributte(pergunta, 'idResposta',id);
            });
        });

        daoRespostaPropostaOpcao.buscarAllRespostasOpcaoByProposta(idProposta, function (json) {
            jQuery.each(json, function(i, item) {

                var id = item.id;
                var pergunta = item.pergunta;
                var formularioOpcao = item.formularioOpcao;
                var resposta = item.resposta;

                var idRespostaOpcao = pergunta+'_'+formularioOpcao;

                addAtributte(idRespostaOpcao, 'idRespostaOpcao', id);

                setCheckBox(idRespostaOpcao, resposta);
            });
        });

    }
    
    function chamarFormularioPerguntaOpcaoSelect(pergunta_id,pergunta) {
        controllerPerguntaFormularioOpcao.buscarAllByPergunta(pergunta_id, function (json) {

            formulaFactory.createFormulario('listview-respostaProposta');

            var select = formulaFactory.select('pergunta_'+pergunta_id, 'pergunta_'+pergunta_id, pergunta , true, false, '', '');

            jQuery.each(json, function(i, item) {
                select.append($('<option>', {
                    value: item.opcao,
                    text : item.opcao
                }));
            });

        });
    }

    function chamarFormularioPerguntaOpcaoRadio(pergunta_id,pergunta) {

        controllerPerguntaFormularioOpcao.buscarAllByPergunta(pergunta_id, function (json) {

            formulaFactory.createFormulario('listview-respostaProposta');
            formulaFactory.label(pergunta, pergunta);
            var li = formulaFactory.createLiUiFieldContain(pergunta_id);

            jQuery.each(json, function(i, item) {
                formulaFactory.createInputFactory(item.pergunta+'_'+item.id, 'pergunta_'+item.pergunta , item.opcao, 'radio', true, false, '', '', li);
            });

        });
    }

    function chamarFormularioPerguntaOpcaoCheckbox(pergunta_id,pergunta) {

        controllerPerguntaFormularioOpcao.buscarAllByPergunta(pergunta_id, function (json) {

            formulaFactory.createFormulario('listview-respostaProposta');
            formulaFactory.label(pergunta, pergunta);
            var li = formulaFactory.createLiUiFieldContain(pergunta_id);

            jQuery.each(json, function(i, item) {
                formulaFactory.createInputFactory(item.pergunta+'_'+item.id, 'pergunta_'+item.pergunta, item.opcao, 'checkbox', false, false, '', '', li );
            });
        });
    }

    function donwload(successCallback) {
        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadRespostasProposta/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonloadResponstasProposta",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(respostasProposta){
            $.each(respostasProposta, function(i, item) {
                var respostaProposta = new RespostaProposta();
                respostaProposta = daoRespostaProposta.popular0bjeto(respostaProposta, item);
                daoRespostaProposta.save(respostaProposta, function () {console.log('proposta resposta salvo.')});
            });
            callbacks.fire(respostasProposta);
        });
    }

    function upload(successCallback) {
        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoRespostaProposta.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

         $.ajax({
            url: base_url + 'uploadRespostasProposta/',
            data: {
                respostasPropostas: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadResponstaPropostas",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
             console.log(retorno);
             var contador = i+1;

             if (contador === total) callbacksSucess.fire(true);
             else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }
};