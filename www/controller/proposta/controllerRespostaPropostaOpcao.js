var ControllerRespostaPropostaOpcao = function () {

    var daoRespostaPropostaOpcao = new DaoRespostaPropostaOpcao();

    this.initialize = initialize;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.save = save;
    this.donwload = donwload;
    this.upload = upload;

    function initialize() {
        eventos();
    }

    function eventos() {}

    function save(perguntaformularioopcao, sucessCallBack) {
        daoRespostaPropostaOpcao.save(perguntaformularioopcao, sucessCallBack);
    }

    function createdb() {
        daoRespostaPropostaOpcao.create();
    }

    function dropdb() {
        daoRespostaPropostaOpcao.drop();
    }

    function donwload(successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        $.ajax({
            url		: base_url + 'donwloadRespostasPropostaOpcao/' ,
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusDonwloadResponstasPropostasOpcao",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(respostasPropostaOpcao){
            $.each(respostasPropostaOpcao, function(i, item) {
                var respostasOpcao = new RespostaPropostaOpcao();
                respostasOpcao = daoRespostaPropostaOpcao.popular0bjeto(respostasOpcao, item);
                daoRespostaPropostaOpcao.save(respostasOpcao, function () {console.log('proposta respostas opcao salvo.')});
            });
            callbacks.fire(daoRespostaPropostaOpcao);
        });
    }

    function upload(successCallback) {

        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        daoRespostaPropostaOpcao.buscarAll(function (json) {

            var total = Object.keys(json).length;

            if (total === 0) callbacksSucess.fire(true);
            else _upload(0, json, total, callbacksSucess);
        });
    }

    function _upload(i, json, total, callbacksSucess) {

        var callbacks = $.Callbacks();
        callbacks.add(_upload);

        var item = json[i];

        $.ajax({
            url: base_url + 'uploadRespostasPropostaOpcao/',
            data: {
                respostasPropostasOpcao: item,
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusUploadRespostasPropostasOpcao",
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function (retorno) {
            console.log(retorno);
            var contador = i+1;

            if (contador === total) callbacksSucess.fire(true);
            else callbacks.fire(contador, json, total, callbacksSucess);
        });

    }

};