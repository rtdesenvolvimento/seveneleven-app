var ControllerRegiao = function () {

    var daoRegiao = new DaoRegiao();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarRegiao", function( event ) {
            menu.initialize();
            $('#formAdicionarRegiao').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarRegiao", function( event ) {
            menu.initialize();
            $('#formEditarRegiao').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idRegiao = event.delegateTarget.activeElement.attributes['idregiao'].value;
            editar(idRegiao);
        });

        $( document ).on( "pageinit", "#regioes", function( event ) {
            menu.initialize();
            listarRegioes();
        });
    }

    function editar(idRegiao) {
        daoRegiao.buscaById(idRegiao, popularDadosTela);
    }
    function listarRegioes() {
        daoRegiao.buscarAll(listViewRegioes);
    }

    function listViewRegioes(json) {
        $("#listviewRegiao").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/regiao/editarRegiao.html" data-ajax="true" idRegiao="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewRegiao").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoRegiao.create();
    }

    function dropdb() {
        daoRegiao.drop();
    }

    function save(event) {
        daoRegiao.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idRegiao = getString('idRegiao');
        var descricao = getString("descricao");

        var regiao = new Regiao();
        regiao.setDescricao(descricao);
        regiao.setId(idRegiao);

        return regiao;
    }

    function popularDadosTela(json) {

        var jsonRegiao = json[0];
        var idRegiao = jsonRegiao.id;
        var descricao = jsonRegiao.descricao;

        setString('idRegiao', idRegiao);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoRegiao.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save regiao '+retorno);
        $('#nmRegiao').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoRegiao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoRegiao.enviar(successCallback);
    }
};