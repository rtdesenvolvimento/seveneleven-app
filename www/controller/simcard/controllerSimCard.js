var ControllerSimCard = function () {

    var daoSimCard = new DaoSimCard();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarSimCard", function( event ) {
            menu.initialize();
            $('#formAdicionarSimCard').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarSimCard", function( event ) {
            menu.initialize();
            $('#formEditarSimCard').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idSimCard = event.delegateTarget.activeElement.attributes['idsimcard'].value;
            editar(idSimCard);
        });

        $( document ).on( "pageinit", "#simcard", function( event ) {
            menu.initialize();
            listarSimCard();
        });
    }

    function editar(idSimCard) {
        daoSimCard.buscaById(idSimCard, popularDadosTela);
    }
    function listarSimCard() {
        daoSimCard.buscarAll(listViewSimCard);
    }

    function listViewSimCard(json) {
        $("#listviewSimCard").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;
            var valor = item.valor;

            var innerA = '  <li>' +
                '               <a href="view/simcard/editarSimCard.html" data-ajax="true" idSimCard="'+id+'">'+descricao+'' +
                '                   <br/><small> Valor R$ '+valor+' </small>'+
                '               </a>' +
                '           </li>';
            $("#listviewSimCard").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoSimCard.create();
    }

    function dropdb() {
        daoSimCard.drop();
    }

    function save(event) {
        daoSimCard.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idSimCard = getString('idSimCard');
        var descricao = getString("descricao");
        var valor = getString('valor');

        var simCard = new SimCard();
        simCard.setDescricao(descricao);
        simCard.setId(idSimCard);
        simCard.setValor(valor);

        return simCard;
    }

    function popularDadosTela(json) {

        var jsonSimCard = json[0];
        var idSimCard = jsonSimCard.id;
        var descricao = jsonSimCard.descricao;
        var valor = jsonSimCard.valor;

        setString('idSimCard', idSimCard);
        setString('descricao', descricao);
        setString('valor', valor);
    }

    function buscarAll(successCallback) {
        daoSimCard.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save simcard '+retorno);
        $('#nmSimCard').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoSimCard.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoSimCard.enviar(successCallback);
    }
};