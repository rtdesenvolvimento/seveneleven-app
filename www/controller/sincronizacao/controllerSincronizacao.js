var ControllerSincronizacao = function () {

    var controllerNotificacao = new ControllerNotificacao();

    var controllerUsuario = new ControllerUsuario();
    var controllerCargo = new ControllerCargo();
    var controllerTipoTarefa = new ControllerTipoTarefa();
    var controllerTarefa = new ControllerTarefa();
    var controllerFormulario = new ControllerFormulario();
    var controllerPerguntaFormulario = new ControllerPerguntaFormulario();
    var controllerPerguntaFormularioOpcao = new ControllerPerguntaFormularioOpcao();
    var controllerCliente = new ControllerCliente();
    var controllerProposta = new ControllerProposta();
    var controllerRespostaProposta = new ControllerRespostaProposta();
    var controllerRespostaPropostaOpcao = new ControllerRespostaPropostaOpcao();
    var controllerConfiguracao = new ControllerConfiguracao();
    var controllerGeoLocalizacao = new ControllerGeoLocalizacao();
    var controllerDigitalizacao = new ControllerDigitalizacao();
    var controllerPlano = new ControllerPlano();
    var controllerMeioDivulgacao = new ControllerMeioDivulgacao();
    var controllerChecklist = new ControllerChecklist();
    var controllerCaptacao = new ControllerCaptacao();
    var controllerAtividade = new ControllerAtividade();
    var controllerTipoCheckin = new ControllerTipoCheckin();
    var controllerRegiao = new ControllerRegiao();
    var controllerTipoSolicitacaoProposta = new ControllerTipoSolicitacaoProposta();
    var controllerCodigoAnatel = new ControllerCodigoAnatel();
    var controllerPropostaPortabilidade = new ControllerPropostaPortabilidade();
    var controllerPlugin = new ControllerPlugin();
    var controllerAparelho = new ControllerAparelho();
    var controllerSimCard = new ControllerSimCard();
    var controllerAparelhoPlano = new ControllerAparelhoPlano();
    var controllerPropostaAparelho = new ControllerPropostaAparelho();
    var controllerOperadora = new ControllerOperadora();
    var controllerClassificacaoCliente = new ControllerClassificaoCliente();
    var controllerPassaporte = new ControllerPassaporte();
    var controllerPropostaFranquia = new ControllerPropostaFranquia();
    var controllerPropostaModulo = new ControllerPropostaModulo();
    var controllerPropostaMigracao = new ControllerPropostaMigracao();
    var controllerFranquia = new ControllerFranquia();
    var controllerSms = new ControllerSms();
    var controllerPropostaEvento = new ControllerPropostaEvento();

    var controllerDevice = new ControllerDevice();

    this.initialize = initialize;
    this.drop = drop;
    this.create = create;
    this.sincronizar = sincronizar;
    this.initialize = initialize;
    this.sincronizarUsuario = sincronizarUsuario;

    var permiteDonwloadDadosUsuario = true;
    var permiteDonwloadDadosAdministrador = true;

    function initialize() {
        enviar();
    }

    function drop() {
        controllerUsuario.dropdb();
        controllerCargo.dropdb();
        controllerTipoTarefa.dropdb();
        controllerTarefa.dropdb();
        controllerFormulario.dropdb();
        controllerPerguntaFormulario.dropdb();
        controllerPerguntaFormularioOpcao.dropdb();
        controllerCliente.dropdb();
        controllerProposta.dropdb();
        controllerRespostaProposta.dropdb();
        controllerConfiguracao.dropdb();
        controllerGeoLocalizacao.dropdb();
        controllerRespostaPropostaOpcao.dropdb();
        controllerDigitalizacao.dropdb();
        controllerPlano.dropdb();
        controllerMeioDivulgacao.dropdb();
        controllerCaptacao.dropdb();
        controllerTipoCheckin.dropdb();
        controllerAtividade.dropdb();
        controllerChecklist.dropdb();
        controllerRegiao.dropdb();
        controllerTipoSolicitacaoProposta.dropdb();
        controllerCodigoAnatel.dropdb();
        controllerPropostaPortabilidade.dropdb();
        controllerPropostaModulo.dropdb();
        controllerPropostaMigracao.dropdb();
        controllerPlugin.dropdb();
        controllerAparelho.dropdb();
        controllerSimCard.dropdb();
        controllerAparelhoPlano.dropdb();
        controllerPropostaAparelho.dropdb();
        controllerOperadora.dropdb();
        controllerClassificacaoCliente.dropdb();
        controllerPassaporte.dropdb();
        controllerPropostaFranquia.dropdb();
        controllerFranquia.dropdb();
        controllerSms.dropdb();
        controllerPropostaEvento.dropdb();
    }

    function create() {
        controllerCargo.createdb();
        controllerUsuario.createdb();
        controllerTipoTarefa.createdb();
        controllerTarefa.createdb();
        controllerFormulario.createdb();
        controllerPerguntaFormulario.createdb();
        controllerPerguntaFormularioOpcao.createdb();
        controllerCliente.createdb();
        controllerProposta.createdb();
        controllerRespostaProposta.createdb();
        controllerConfiguracao.createdb();
        controllerGeoLocalizacao.createdb();
        controllerRespostaPropostaOpcao.createdb();
        controllerDigitalizacao.createdb();
        controllerPlano.createdb();
        controllerMeioDivulgacao.createdb();
        controllerCaptacao.createdb();
        controllerTipoCheckin.createdb();
        controllerAtividade.createdb();
        controllerChecklist.createdb();
        controllerRegiao.createdb();
        controllerTipoSolicitacaoProposta.createdb();
        controllerCodigoAnatel.createdb();
        controllerPropostaPortabilidade.createdb();
        controllerPropostaModulo.createdb();
        controllerPropostaMigracao.createdb();
        controllerPlugin.createdb();
        controllerAparelho.createdb();
        controllerSimCard.createdb();
        controllerAparelhoPlano.createdb();
        controllerPropostaAparelho.createdb();
        controllerOperadora.createdb();
        controllerClassificacaoCliente.createdb();
        controllerPassaporte.createdb();
        controllerPropostaFranquia.createdb();
        controllerFranquia.createdb();
        controllerSms.createdb();
        controllerPropostaEvento.createdb();
    }

    function enviar() {
        if (controllerDevice.checkConnection()  === 'NONE') enviarCheckConnection(TEMPO_ENVIAR_WIFFI);
        else if (controllerDevice.checkConnection() === 'WIFI')  enviarCheckConnection(TEMPO_ENVIAR_WIFFI);
        else if (controllerDevice.checkConnection() === 'ETHERNET') enviarCheckConnection(TEMPO_ENVIAR_WIFFI);
        else if (controllerDevice.checkConnection() === 'CELL_4G') enviarCheckConnection(TEMPO_ENVIAR_4G);
        else if (controllerDevice.checkConnection() === 'CELL_3G') enviarCheckConnection(TEMPO_ENVIAR_4G*5);
    }

    function enviarCheckConnection(tempo) {
        setInterval(function () {
            if (controllerDevice.checkConnection() === 'NONE') return;
            enviarDadosUsuario();
            baixarDadosUsuario(false);

        }, tempo);
    }

    function baixarDadosUsuario(exibir, idConfiguracao) {

        if (permiteDonwloadDadosUsuario) {
            permiteDonwloadDadosUsuario = false;

            if (exibir) showPageLoading('Importando suas Captações ....');

            controllerCaptacao.baixar(function (captacoes) {

                if (exibir) showPageLoading('Importando suas Tarefas ....');

                controllerTarefa.baixar(function (tarefas) {

                   // controllerChecklist.baixar(function (checklist) {

                        if (exibir) showPageLoading('Importando seus Checkins ....');

                        controllerAtividade.baixar(function (atividades) {

                            //if (exibir) showPageLoading('Importando seus Clientes ....');

                            //controllerCliente.baixar(function (clientes) {

                                //if (exibir) showPageLoading('Importando suas Propostas ....');

                               // controllerProposta.baixar(function (propostas) {

                                    //if (exibir) showPageLoading('Importando suas Propostas  Franquias ....');

                                   // controllerPropostaFranquia.baixar(function (franquias) {

                                        //if (exibir) showPageLoading('Importando suas Propostas e aparelhos ....');

                                        //controllerPropostaAparelho.baixar(function (propostasAparelho) {

                                           // if (exibir) showPageLoading('Importando suas Propostas e portabilidades ....');

                                           // controllerPropostaPortabilidade.baixar(function (propostaPortabilidade) {

                                               // if (exibir) showPageLoading('Importando suas Propostas e Modulos adicionais ....');

                                                //controllerPropostaModulo.baixar(function (propsotaModulos) {

                                                 //   if (exibir) showPageLoading('Importando suas Propostas e Migração ....');

                                                    //controllerPropostaMigracao.baixar(function (propostaMigracao) {

                                                      //  if (exibir) showPageLoading('Importando suas Propostas e eventos ....');

                                                       // controllerPropostaEvento.baixar(function (propostaEventos) {

                                                            controllerDigitalizacao.baixar(function (digitalizacoes) {

                                                                permiteDonwloadDadosUsuario = true;
                                                                hidePageLoading();

                                                                if (idConfiguracao !== null && idConfiguracao !== undefined) {
                                                                    controllerConfiguracao.atualizarConfiguracaoImportada(idConfiguracao, function (retonro) {
                                                                        hidePageLoading();
                                                                        $.mobile.changePage("#home");
                                                                        BAIXAR_ALL = false;
                                                                    });
                                                                }

                                                            });
                                                        //})
                                                    //})
                                                //});
                                            //});
                                        //});
                                    //})
                               // });
                            //});
                        });
                    //});
                });
            });
        }
    }

    function baixarDadosServidor(baixarDadosUsuarioPrimeiro, idConfiguracao) {
        if (permiteDonwloadDadosAdministrador) {

            permiteDonwloadDadosAdministrador = false;
            $('#show-page-loading-msg').click();

            showPageLoading('Importando Cargo ....');

            controllerCargo.baixar(function (cargos) {

                showPageLoading('Importando Usuario ....');

                controllerUsuario.baixar(function (usuarios) {

                    showPageLoading('Importando Tipo Tarefa ....');

                    controllerTipoTarefa.baixar(function (tiposTarefas) {

                        showPageLoading('Importando Tipo checkin ....');

                        controllerTipoCheckin.baixar(function (tiposCheckin) {

                            showPageLoading('Importando Tipo de Prospecção ....');

                            controllerMeioDivulgacao.baixar(function (meiosDivulgacao) {

                                permiteDonwloadDadosAdministrador = true;

                                if (baixarDadosUsuarioPrimeiro) {
                                    baixarDadosUsuario(true, idConfiguracao);
                                } else {
                                    //TODO ENVIAR DADOS DO USUARIO;
                                    enviarDadosUsuario();
                                }

                            });
                        });
                    });
                });
            });
        }
    }

    //posso enviar como funcionario ou administrador
    function enviarDadosUsuario() {
      //  controllerCliente.enviar(function (retorno) {
           // controllerProposta.enviar(function (retorno) {
              //  controllerPropostaFranquia.enviar(function (retorno) {
                  //  controllerPropostaAparelho.enviar(function (retorno) {
                      //  controllerPropostaPortabilidade.enviar(function (retorno) {
                          //  controllerPropostaModulo.enviar(function (retorno) {
                               // controllerPropostaMigracao.enviar(function (retorno) {
                                    controllerTarefa.enviar(function (retorno) {
                                        //controllerChecklist.enviar(function (retorno) {
                                            controllerCaptacao.enviar(function (retorno) {
                                                controllerAtividade.enviar(function (retorno) {
                                                    controllerDigitalizacao.enviar(function (retorno) {
                                                       // controllerPropostaEvento.enviar(function (retorno) {

                                                            //TODO BAIXAR OS DADOS ATUALIZADOS DO SERVIDOR;
                                                            baixarDadosUsuario();

                                                      //  });
                                                    });
                                                });
                                            });
                                      //  });
                                    });
                               // });
                         //   });
                       // });
                //    });
             //   });
          //  });
      //  });
    }

    function sincronizar(idConfiguracao) {
        if (controllerDevice.checkConnection() === 'NONE') {
            var mensagem = 'ATENÇÃO!!! VOCÊ NÃO ESTÁ CONECTADO A UMA REDE WIFFI OU MOVEL!';
            var title = 'DESCONTECTADO!!';
            controllerNotificacao.showAlert(mensagem, function () {
            }, title);
        } else {
            baixarDadosServidor(true, idConfiguracao);
        }
    }

    function sincronizarUsuario() {
        showPageLoading('Importando Usuários ....');
        controllerCargo.baixar(function (cargos) {
            controllerUsuario.baixar(function (usuarios) {

                hidePageLoading();

                var controllerUsuario = new ControllerUsuario();

                controllerUsuario.buscarAll(function (json) {
                    controllerUsuario.preencherCombo('imput_login', json);
                    var mensagem = 'ATENÇÃO!!! SINCRONIZAÇÃO EFETUADA COM SUCESSO! VERIFIQUE SEU NOME NA LISTA DE USUÁRIOS';
                    var title = 'ATUALIZADO COM SUCESSO!!';
                    controllerNotificacao.showAlert(mensagem, function () {}, title);
                    controllerConfiguracao.createdb();
                });
            });
        });
    }

};