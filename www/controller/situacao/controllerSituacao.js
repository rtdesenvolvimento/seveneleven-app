var ControllerSituacao = function () {

    var daoSituacao = new DaoSituacao();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {}

    function createdb() {
        daoSituacao.create();
    }

    function dropdb() {
        daoSituacao.drop();
    }

    function buscarAll(successCallback) {
        daoSituacao.buscarAll(successCallback)
    }

    function buscarById(idTipoCheckin, successCallback) {
        daoSituacao.buscaById(idTipoCheckin, successCallback);
    }

    function baixar(successCallback) {
        daoSituacao.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoSituacao.enviar(successCallback);
    }
};