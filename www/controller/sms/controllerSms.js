var ControllerSms = function () {

    var daoSms = new DaoSms();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarSms", function( event ) {
            menu.initialize();
            $('#formAdicionarPassaporte').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarPassaporte", function( event ) {
            menu.initialize();
            $('#formEditarPassaporte').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idPassaporte = event.delegateTarget.activeElement.attributes['idpassaporte'].value;
            editar(idPassaporte);
        });

        $( document ).on( "pageinit", "#passaportes", function( event ) {
            menu.initialize();
            listarPassaporte();
        });
    }

    function editar(idPassaporte) {
        daoSms.buscaById(idPassaporte, popularDadosTela);
    }
    function listarPassaporte() {
        daoSms.buscarAll(listViewPassaporte);
    }

    function listViewPassaporte(json) {
        $("#listviewPassaporte").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;
            var valor = item.valor;

            var innerA = '  <li>' +
                '               <a href="view/passaporte/editarPassaporte.html" data-ajax="true" idPassaporte="'+id+'">'+descricao+'<br/>R$ ' + valor +
                '               </a>' +
                '           </li>';
            $("#listviewPassaporte").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoSms.create();
    }

    function dropdb() {
        daoSms.drop();
    }

    function save(event) {
        daoSms.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idPassaporte = getString('idPassaporte');
        var descricao = getString("descricao");
        var valor = getString('valor');

        var passaporte = new Passaporte();
        passaporte.setDescricao(descricao);
        passaporte.setId(idPassaporte);
        passaporte.setValor(valor);

        return passaporte;
    }

    function popularDadosTela(json) {

        var jsonPasssaporte = json[0];
        var idPassaporte = jsonPasssaporte.id;
        var descricao = jsonPasssaporte.descricao;
        var valor = jsonPasssaporte.valor;

        setString('idPassaporte', idPassaporte);
        setString('descricao', descricao);
        setString('valor', valor);
    }

    function buscarAll(successCallback) {
        daoPassaporte.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save passaporte '+retorno);
        $('#nmPassaporte').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoSms.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoSms.enviar(successCallback);
    }
};