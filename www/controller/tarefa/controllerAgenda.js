var ControllerAgenda = function () {

    var allEventos = [];

    var controllerTarefa = new ControllerTarefa();
    var daoAtividade = new DaoAtividade();

    var menu = new Menu();

    this.initialize = initialize;
    
    function initialize() {
        eventos();
    }

    function eventos() {

        $( document ).on( "pageinit", "#agenda", function( event ) {

            menu.initialize();
            allEventos = [];

            daoAtividade.buscarFilter('', '', carregarAgendaCheckin);
            //controllerTarefa.buscaAgendadaConfirmadaAll(ID_USUARIO_LOGADO, carregarAgenda);
            //controllerPropostas.buscarAllPropostasAbertasAguardandoFechamento(carregarPropostas);
        });

        $( document ).on( "pageinit", "#agendaGoogle", function( event ) {
            menu.initialize();
            var email = EMAIL_USUARIO;
            $('#agendaGoogleCalendar').attr('src','https://calendar.google.com/calendar/embed?src='+email);
        });
    }

    function carregarPropostas(propostasJson) {

        jQuery.each(propostasJson, function(i, item) {

            var dataAberta = item.dataAberta;
            var dataAguardando = item.dataAguardando;
            var dataFechamento = item.dataFechamento;

            var dataApresentacao = item.dataApresentacao;
            var dataEnvioProposta =  item.dataEnvioProposta;
            var dataRetornoVisita = item.dataRetornoVisita;
            var dataAguardandoDocumento =  item.dataAguardandoDocumento;
            var dataAguardandoAssinatura =  item.dataAguardandoAssinatura;

            var nome = item.nomeContato;
            var telefone = item.telefone;
            var telefoneContato = item.telefoneContato;
            var email = item.email;

            if (telefoneContato === undefined) telefoneContato = '';

            if (dataAguardandoAssinatura !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Ag. assinatura - '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataAguardandoAssinatura,
                    end: dataAguardandoAssinatura
                });
            }

            if (dataAguardandoDocumento !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Ag. documentos - '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataAguardandoDocumento,
                    end: dataAguardandoDocumento
                });
            }

            if (dataRetornoVisita !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Retorno - '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataRetornoVisita,
                    end: dataRetornoVisita
                });
            }

            if (dataEnvioProposta !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Envio - '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataEnvioProposta,
                    end: dataEnvioProposta
                });
            }

            if (dataApresentacao !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Apresentação :: '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataApresentacao,
                    end: dataApresentacao
                });
            }


            if (dataAberta !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'E-mail Apresentação :: '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataAberta,
                    end: dataAberta
                });
            }

            if (dataAguardando !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Envio de Proposta :: '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataAguardando,
                    end: dataAguardando
                });
            }

            if (dataFechamento !== '') {
                allEventos.push({
                    id: item.id,
                    title: 'Retorno Visita :: '+nome+' / '+telefone+' - '+telefoneContato + ' - '+email,
                    start: dataFechamento,
                    end: dataAguardando
                });
            }
        });

    }

    function carregarEventosNaAgenda() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            defaultDate: formatDate(new Date()),
            locale: 'pt-br',
            buttonIcons: false, // show the prev/next text
            weekNumbers: true,
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: allEventos,
        });

        $('.fc-agendaDay-button').hide()
        $('.fc-agendaWeek-button').hide()
        $('.fc-month-button').hide();
        $('.fc-listMonth-button').click();
    }

    function carregarAgendaCheckin(atividadesJson) {

        jQuery.each(atividadesJson, function(i, item) {

            let descricao = item.descricao;
            let observacao = item.observacao;
            let start = item.dataAtividade;
            let end = item.dataAtividade;
            let horaInicio = item.horaInicio;
            let horaFinal = item.horaFinal;
            let tipo = item.tipo;

            if (horaFinal === '') horaFinal = horaInicio;

            horaInicio = formatarHora(horaInicio);
            horaFinal = formatarHora(horaFinal);

            console.log('horaInicio = '+item.horaInicio);
            console.log('horaFinal' + item.horaFinal);

            if (horaInicio !== '' && horaFinal !== '') {
                start = start+'T'+horaInicio;
                end = end+'T'+horaFinal;
            } else if (horaInicio !== '') {
                start = start+'T'+horaInicio;
                end = end+'T'+horaInicio;
            } else if (horaFinal !== '') {
                start = start+'T'+horaFinal;
                end = end+'T'+horaFinal;
            }

            allEventos.push({
                id : item.id,
                title: tipo+' | Observação Check-in: '+descricao+' | Check-out: '+observacao,
                start: start,
                end: end
            });
        });

        controllerTarefa.buscaAgendadaConfirmadaAll(ID_USUARIO_LOGADO, carregarAgenda);
    }

    function carregarAgenda(tarefasJson) {

        jQuery.each(tarefasJson, function(i, item) {

            var horaInicio = item.horaInicio;
            var horaFinal = item.horaFinal;
            var start = item.data;
            var end = item.data;

            if (horaInicio !== '' && horaFinal !== '') {
                start = start+'T'+horaInicio;
                end = end+'T'+horaFinal;
            } else if (horaInicio !== '') {
                start = start+'T'+horaInicio;
                end = end+'T'+horaInicio;
            } else if (horaFinal !== '') {
                start = start+'T'+horaFinal;
                end = end+'T'+horaFinal;
            }

            allEventos.push({
                id : item.id,
                title: item.tipoTarefa+' - '+item.titulo,
                start: start,
                end: end
            });
        });

        carregarEventosNaAgenda();
    }
};