var ControllerTarefa = function () {

    var controllerTipoTarefa = new ControllerTipoTarefa();
    var controllerUsuario = new ControllerUsuario();
    var controllerNotificacao = new ControllerNotificacao();
    var menu = new Menu();

    var daoTarefa = new DaoTarefa();

    this.initialize = initialize;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.buscarAll = buscarAll;
    this.buscaAgendadaConfirmadaAll = buscaAgendadaConfirmadaAll;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarTarefa", function( event ) {

            menu.initialize();

            $('#formAdicionarTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });
            novo();
        });

        $( document ).on( "pageinit", "#editarTarefa", function( event ) {
            menu.initialize();

            $('#excluirTarefa').click(function (event) {
                var mensagem = 'Deseja realmente excluir esta Tarefa de sua agenda?';
                var title = 'Excluir Tarefa';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        excluir();
                    }
                } , title );
            });

            $('#confirmarTarefa').click(function (event) {
                var mensagem = 'Deseja realmente confirmar esta Tarefa em sua agenda?';
                var title = 'Confirmar Tarefa';

                controllerNotificacao.showConfirm(mensagem, function (retorno) {
                    if(retorno===1) {
                        confirmar();
                    }
                } , title );
            });

            $('#formEditarTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idTarefa = event.delegateTarget.activeElement.attributes['idtarefa'].value;
            controllerTipoTarefa.buscarAll(preencherTipoTarefaCombo);
            controllerUsuario.buscarAll(preencherUsuarioCombo);
            editar(idTarefa);
        });

        $( document ).on( "pageinit", "#tarefasAgendada", function( event ) {
            menu.initialize();
            daoTarefa.buscaAgendadaAll(ID_USUARIO_LOGADO, listarTarefasAgendadas);
        });

        $( document ).on( "pageinit", "#tarefasConfirmadas", function( event ) {
            menu.initialize();
            daoTarefa.buscaConfirmadaAll(ID_USUARIO_LOGADO, listarTarefasConfirmadas);
        });

        $( document ).on( "pageinit", "#tarefasExecutadas", function( event ) {
            menu.initialize();
            daoTarefa.buscaExecutadaAll(ID_USUARIO_LOGADO, listarTarefasExecutadas);
        });

        $( document ).on( "pageinit", "#tarefasVisitas", function( event ) {
            menu.initialize();
            daoTarefa.visitas(ID_USUARIO_LOGADO, listarTarefasVisitas);
        });
    }

    function novo() {
        controllerTipoTarefa.buscarAll(preencherTipoTarefaCombo);
        controllerUsuario.buscarAll(preencherUsuarioCombo);
    }

    function confirmar() {
        var idTarefa = getString('idTarefa');
        daoTarefa.confirmar(idTarefa, depoisSalvar);
    }

    function excluir() {
        var idTarefa = getString('idTarefa');
        daoTarefa.deletar(idTarefa);
        depoisSalvar(true);
    }

    function editar(idTarefa) {
        daoTarefa.buscaById(idTarefa, popularDadosTela);
    }

    function createdb() {
        daoTarefa.create();
    }

    function dropdb() {
        daoTarefa.drop();
    }

    function save(event) {
        daoTarefa.save(buscarDadosTela(), depoisSalvar );
    }


    function listarTarefasVisitas(json) {
        listarTarefas('listview-visitas', json);
    }

    function listarTarefasAgendadas(json) {
        listarTarefas('listview-tarefasagendada', json);
    }

    function listarTarefasExecutadas(json) {
        listarTarefas('listview-tarefasexecutadas', json);
    }

    function listarTarefasConfirmadas(json) {
        listarTarefas('listview-tarefasconfirmadas', json);
    }

    function listarTarefas(listview, json) {
        var dataExtensao = '';

        $("#"+listview).empty();

        jQuery.each(json, function(i, item) {

            var id = item.id;
            var titulo = item.titulo;
            var descriao = item.descricao;
            var tipoTarefa = item.tipoTarefa;
            var horaInicio = item.horaInicio;
            var horaFinal = item.horaFinal;
            var data = item.data;
            var nomeCompleto = item.nomeCompleto;

            var hora = '';
            if (horaInicio !== '' && horaFinal !== '') {
                hora = horaInicio + ' até ' + horaFinal;
            } else if (horaInicio !== '') {
                hora = horaInicio;
            } else if (horaFinal !== '') {
                hora = horaFinal;
            }

            var semana = getDiaSemana(data);
            var ano = getYear(data);
            var mes = getMes(data);
            var dia = getDay(data);

            var dataExibir = semana+ano+mes+dia;
            var innerA = '';

            if (dataExtensao !== dataExibir) {
                innerA = '<li data-role="list-divider">' + semana + ', ' + mes + ' ' + dia + ', ' + ano + ' </li>';
            }

            innerA  += '<li>';
            innerA  += '    <a href="view/tarefas/editarTarefa.html" data-ajax="true" idTarefa="'+id+'" data-role="button">';
            innerA  += '        <h2>'+titulo+'</h2>';
            innerA  += '        <small>'+nomeCompleto+'</small>';
            innerA  += '        <p><strong>'+tipoTarefa+'</strong></p>';
            innerA  += '        <p>'+descriao+'.</p>';
            innerA  += '        <p class="ui-li-aside"><strong>'+hora+'</strong></p>';
            innerA  += '    </a>';
            innerA  += '</li>';

            dataExtensao = dataExibir;
            $("#"+listview).append(innerA);
        });

        $('ul').listview().listview('refresh');
    }

    function popularDadosTela(tarefaJson) {
        tarefaJson = tarefaJson[0];

        var idTarefa = tarefaJson.id;
        var titulo = tarefaJson.titulo;
        var tipoTarefa = tarefaJson.tipoTarefa;
        var data = tarefaJson.data;
        var descricao = tarefaJson.descricao;
        var atribuir = tarefaJson.atribuir;
        var responsavel = tarefaJson.responsavel;
        var horaInicio = tarefaJson.horaInicio;
        var horaFinal = tarefaJson.horaFinal;
        var status = tarefaJson.status;

        setString('idTarefa', idTarefa);
        setString('titulo', titulo);
        setCombo('tipoTarefa', tipoTarefa);
        setString('data', data);
        setString('descricao', descricao);
        setBoolean('atribuir', atribuir);
        setCombo('responsavel', responsavel);
        setString('horaInicio', horaInicio);
        setString('horaFinal', horaFinal);
        setString('status', status);
    }

    function buscarDadosTela () {

        var idTarefa = getString('idTarefa');
        var status = getString('status');
        var titulo = getString('titulo');
        var tipoTarefa = getString('tipoTarefa');
        var data = getString('data');
        var descricao = getString('descricao');
        var atribuir = getBoolean('atribuir');
        var responsavel = getString('responsavel');
        var horaInicio = getString('horaInicio');
        var horaFinal = getString('horaFinal');

        var tarefa = new Tarefa();
        tarefa.setId(idTarefa);
        tarefa.setTitulo(titulo);
        tarefa.setTipoTarefa(tipoTarefa);
        tarefa.setData(data);
        tarefa.setHoraInicio(horaInicio)
        tarefa.setHoraFinal(horaFinal)
        tarefa.setDescricao(descricao);
        tarefa.setAtribuir(atribuir);
        tarefa.setResponsavel(responsavel);
        tarefa.setStatus(status);
        return tarefa;
    }

    function buscarAll(successCallbackJson) {
        daoTarefa.buscarAll(successCallbackJson);
    }

    function depoisSalvar(retorno) {
        console.log('save tarefa '+retorno);
        $('#nmTarefasAgendadas').click();
    }

    function preencherTipoTarefaCombo(json) {
        controllerTipoTarefa.preencherCombo('tipoTarefa', json);
    }

    function preencherUsuarioCombo(json) {
        controllerUsuario.preencherCombo('responsavel', json);
        setCombo('responsavel', ID_USUARIO_LOGADO);
    }

    function buscaAgendadaConfirmadaAll(idUsaurio, successCallbackJson) {
        daoTarefa.buscaAgendadaConfirmadaAll(idUsaurio, successCallbackJson)
    }

    function baixar(successCallback) {
        daoTarefa.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoTarefa.enviar(successCallback);
    }
};