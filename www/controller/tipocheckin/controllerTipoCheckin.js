var ControllerTipoCheckin = function () {

    var daoTipoCheckin = new DaoTipoCheckin();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscarById = buscarById;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarTipoCheckin", function( event ) {
            menu.initialize();
            $('#formAdicionarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarTipoCheckin", function( event ) {
            menu.initialize();
            $('#formEditarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idTipoCheckin = event.delegateTarget.activeElement.attributes['idtipoCheckin'].value;
            editar(idTipoCheckin);
        });

        $( document ).on( "pageinit", "#tiposCheckin", function( event ) {
            menu.initialize();
            listarTiposCheckin();
        });
    }

    function editar(idTipoCheckin) {
        daoTipoCheckin.buscaById(idTipoCheckin, popularDadosTela);
    }
    function listarTiposCheckin() {
        daoTipoCheckin.buscarAll(listViewTiposCheckin);
    }

    function listViewTiposCheckin(json) {
        $("#listviewTiposCheckin").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;
            var tempoMedio = item.tempoMedio;

            var innerA = '  <li>' +
                '               <a href="view/tipocheckin/editarTipoCheckin.html" data-ajax="true" idTipoCheckin="'+id+'">'+descricao+'' +
                '                   <br/><small>Tempo médio '+tempoMedio+' horas </small>'+
                '               </a>' +
                '           </li>';
            $("#listviewTiposCheckin").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoTipoCheckin.create();
    }

    function dropdb() {
        daoTipoCheckin.drop();
    }

    function save(event) {
        daoTipoCheckin.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idTipoCheckin = getString('idTipoCheckin');
        var descricao = getString("descricao");
        var tempoMedio = getString('tempoMedio');
        var isUsarObservacao = getBoolean("isUsarObservacao");
        var isUsarGPS = getBoolean("isUsarGPS");

        var tipoCheckin = new TipoCheckin();
        tipoCheckin.setDescricao(descricao);
        tipoCheckin.setId(idTipoCheckin);
        tipoCheckin.setTempoMedio(tempoMedio)
        tipoCheckin.setIsUsarGPS(isUsarGPS);
        tipoCheckin.setIsUsarObservacao(isUsarObservacao);
        return tipoCheckin;
    }

    function popularDadosTela(json) {

        var jsonTipoCheckin = json[0];

        var idTipoCheckin = jsonTipoCheckin.id;
        var descricao = jsonTipoCheckin.descricao;
        var tempoMedio = jsonTipoCheckin.tempoMedio;
        var isUsarObservacao = jsonTipoCheckin.isUsarObservacao;
        var isUsarGPS = jsonTipoCheckin.isUsarGPS;

        setString('idTipoCheckin', idTipoCheckin);
        setString('descricao', descricao);
        setString('tempoMedio', tempoMedio);
        setBoolean('isUsarObservacao', isUsarObservacao);
        setBoolean('isUsarGPS', isUsarGPS);

    }

    function buscarAll(successCallback) {
        daoTipoCheckin.buscarAll(successCallback)
    }

    function buscarById(idTipoCheckin, successCallback) {
        daoTipoCheckin.buscaById(idTipoCheckin, successCallback);
    }

    function depoisSalvar(retorno) {
        console.log('save tipo checkin '+retorno);
        $('#nmTipoChekin').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoTipoCheckin.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoTipoCheckin.enviar(successCallback);
    }
};