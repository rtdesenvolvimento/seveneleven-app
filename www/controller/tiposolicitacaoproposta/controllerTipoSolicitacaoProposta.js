var ControllerTipoSolicitacaoProposta = function () {

    var daoTipoSolicitacaoProposta = new DaoTipoSolicitacaoProposta();
    var menu = new Menu();

    this.buscarAll = buscarAll;
    this.save = save;
    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarTipoSolicitacaoProposta", function( event ) {
            menu.initialize();
            $('#formAdicionarTipoSolicitacaoProposta').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarTipoSolicitacaoProposta", function( event ) {
            menu.initialize();
            $('#formEditarTipoSolicitacaoProposta').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idTipoSolicitacaoProposta = event.delegateTarget.activeElement.attributes['idtiposolicitacaoproposta'].value;
            editar(idTipoSolicitacaoProposta);
        });

        $( document ).on( "pageinit", "#tiposSolicitacaoProposta", function( event ) {
            menu.initialize();
            listarTiposSolicitacaoProposta();
        });
    }

    function listarTiposSolicitacaoProposta() {
        daoTipoSolicitacaoProposta.buscarAll(listViewTiposSolicitacaoProposta);
    }

    function listViewTiposSolicitacaoProposta(json) {
        $("#listviewTiposSolicitacaoProposta").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/tiposolicitacaoproposta/editarTipoSolicitacaoProposta.html" data-ajax="true" idTipoSolicitacaoProposta="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewTiposSolicitacaoProposta").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function buscarAll(successCallbackJson) {
        daoTipoSolicitacaoProposta.buscarAll(successCallbackJson);
    }

    function createdb() {
        daoTipoSolicitacaoProposta.create();
    }

    function dropdb() {
        daoTipoSolicitacaoProposta.drop();
    }

    function save() {
        daoTipoSolicitacaoProposta.save(buscarDadosTela(), depoisSalvar);
    }

    function editar(idTipoSolicitacaoProposta) {
        daoTipoSolicitacaoProposta.buscaById(idTipoSolicitacaoProposta, popularDadosTela);
    }

    function buscarDadosTela() {
        var idTipoSolicitacaoProposta = getString('idTipoSolicitacaoProposta');
        var descricao = getString("descricao");

        var tipoSolicitacaoProposta = new TipoSolicitacaoProposta();
        tipoSolicitacaoProposta.setId(idTipoSolicitacaoProposta);
        tipoSolicitacaoProposta.setDescricao(descricao);

        return tipoSolicitacaoProposta;
    }

    function popularDadosTela(json) {
        var tipoSolicitacaoJson = json[0];

        var idTipoSolicitacaoProposta = tipoSolicitacaoJson.id;
        var descricao = tipoSolicitacaoJson.descricao;

        setString('idTipoSolicitacaoProposta', idTipoSolicitacaoProposta);
        setString('descricao', descricao);
    }

    function depoisSalvar(retorno) {
        console.log('save tipo solicitação proposta '+retorno);
        $('#nmTipoSolicitacaoProposta').click();
    }
    
    function baixar(successCallback) {
        daoTipoSolicitacaoProposta.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoTipoSolicitacaoProposta.enviar(successCallback);
    }
};