var ControllerTipoTarefa = function () {

    var daoTipoTarefa = new DaoTipoTarefa();
    var menu = new Menu();

    this.dropdb = dropdb;
    this.createdb = createdb;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.save = save;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    function initialize() {
        eventos();
    }

    function eventos() {
        $( document ).on( "pageinit", "#adicionarTipoTarefa", function( event ) {
            menu.initialize();
            $('#formAdicionarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });
        });

        $( document ).on( "pageinit", "#editarTipoTarefa", function( event ) {
            menu.initialize();
            $('#formEditarTipoTarefa').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            var idTipoTarefa = event.delegateTarget.activeElement.attributes['idtipotarefa'].value;
            editar(idTipoTarefa);
        });

        $( document ).on( "pageinit", "#tiposTarefa", function( event ) {
            menu.initialize();
            listarTiposTarefa();
        });
    }

    function editar(idTipoTarefa) {
        daoTipoTarefa.buscaById(idTipoTarefa, popularDadosTela);
    }
    function listarTiposTarefa() {
        daoTipoTarefa.buscarAll(listViewTiposTarefa);
    }

    function listViewTiposTarefa(json) {
        $("#listviewTiposTarefa").empty();
        jQuery.each(json, function(i, item) {
            var id = item.id;
            var descricao = item.descricao;

            var innerA = '  <li>' +
                '               <a href="view/tipotarefa/editarTipoTarefa.html" data-ajax="true" idTipoTarefa="'+id+'">'+descricao+'' +
                '               </a>' +
                '           </li>';
            $("#listviewTiposTarefa").append(innerA);
        });
        $('ul').listview().listview('refresh');
    }

    function createdb() {
        daoTipoTarefa.create();
    }

    function dropdb() {
        daoTipoTarefa.drop();
    }

    function save(event) {
        daoTipoTarefa.save(buscarDadosTela(), depoisSalvar);
    }

    function buscarDadosTela() {
        var idTipoTarefa = getString('idTipoTarefa');
        var descricao = getString("descricao");

        var tipoTarefa = new TipoTarefa();
        tipoTarefa.setDescricao(descricao);
        tipoTarefa.setId(idTipoTarefa);

        return tipoTarefa;
    }

    function popularDadosTela(json) {

        var jsonTipoTarefa = json[0];
        var idTipoTarefa = jsonTipoTarefa.id;
        var descricao = jsonTipoTarefa.descricao;

        setString('idTipoTarefa', idTipoTarefa);
        setString('descricao', descricao);
    }

    function buscarAll(successCallback) {
        daoTipoTarefa.buscarAll(successCallback)
    }

    function depoisSalvar(retorno) {
        console.log('save tipotarefa '+retorno);
        $('#nmTiposTarefa').click();
    }

    function preencherCombo(atributo, json) {
        preencherComboAtributos(atributo, json);
    }

    function baixar(successCallback) {
        daoTipoTarefa.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoTipoTarefa.enviar(successCallback);
    }
};