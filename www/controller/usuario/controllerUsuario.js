var ControllerUsuario = function () {

    var daoUsuario = new DaoUsuario();
    var controllerCargo = new ControllerCargo();
    var controllerCamera = new ControllerCamera();
    var menu = new Menu();

    var usuario = new Usuario();

    this.createdb = createdb;
    this.dropdb = dropdb;
    this.initialize = initialize;
    this.baixar = baixar;
    this.enviar = enviar;

    this.editarUsuario = editar;

    this.listUsuarios = listUsuarios;
    this.buscarAll = buscarAll;
    this.preencherCombo = preencherCombo;
    this.logar = logar;

    function initialize() {
        eventos();
    }

    function createdb() {
        daoUsuario.create();
    }

    function dropdb() {
        daoUsuario.drop();
    }

    function novo() {
        controllerCargo.buscarAll(preencherComboCargo);
    }

    function editar(idUsuario) {
        daoUsuario.buscaById(idUsuario, popularDadosTela);
    }

    function buscarAll(successCallback) {
        daoUsuario.buscarAll(successCallback)
    }

    function eventos() {

        $( document ).on( "pageinit", "#usuarios", function( event ) {
            menu.initialize();
            listUsuarios();
        });

        $( document ).on( "pageinit", "#adicionarUsuario", function( event ) {
            menu.initialize();
            $('#formAdicionarUsuario').submit(function (event) {
                event.preventDefault();
                save(event);
            });

            $('#foto').click(function (event) {
                adicionarFoto();
            });
            novo();
        });

        $( document ).on( "pageinit", "#editarUsuario", function( event ) {
            menu.initialize();
            $('#formEditarUsuario').submit(function (event) {
                event.preventDefault();
                update(event);
            });

            $('#foto').click(function (event) {
                adicionarFoto();
            });

            var idUsuario = event.delegateTarget.activeElement.attributes['idusuario'].value;
            controllerCargo.buscarAll(preencherComboCargo);
            editar(idUsuario);
        });

        $( document ).on( "pageinit", "#dadosUsuarioLogado", function( event ) {
            menu.initialize();
            $('#formEditarUsuarioLogado').submit(function (event) {
                event.preventDefault();
                update(event);
            });

            controllerCargo.buscarAll(preencherComboCargo);
            editar(ID_USUARIO_LOGADO);
        });
    }


    function adicionarFoto() {

        var destinationType = 0;
        var sourceType = 0;
        var mediaType = 0;
        var allowEdit = false;

        controllerCamera.getPicture(destinationType,sourceType, mediaType, allowEdit, function (imageData) {
            var image = document.getElementById('foto');
            image.src = "data:image/jpeg;base64," + imageData;
        }, function (error) {});
    }

    function save(event) {
        daoUsuario.save( buscarDadosTela(), voltarListaUsuarios);
    }

    function update() {
        daoUsuario.save( buscarDadosTela(), voltarListaUsuarios);
    }

    function listUsuarios() {
        daoUsuario.buscarAll(listViewUsuarios);
    }

    function listViewUsuarios(json) {
        $("#listviewUsuarios").empty();
        jQuery.each(json, function(i, item) {

            var id = item.id;
            var nomeCompleto = item.nomeCompleto;
            var email = item.email;
            var celular = item.celular;
            var foto = item.foto;

            if (foto === '' || foto === undefined) foto = 'img/sem_foto.jpg';

            var innerA = ' ' +
                 '  <li>' +
                 '      <a href="view/usuarios/editarUsuario.html" class="classIdUsuario" data-ajax="true" idUsuario="'+id+'"> ' +
                 '          <img src="'+foto+'" style="width: 160px;height: 160px;"> ' +
                 '          <h3>'+nomeCompleto+'</h3> ' +
                 '          <small>'+celular+ ' - ' + email + '</small> ' +
                 '      </a> ' +
                 '  </li>'

                $("#listviewUsuarios").append(innerA);
        });

        $('ul').listview().listview('refresh');
    }

    function buscarDadosTela() {

        var idUsuario = getString('idUsuario');
        var nomeCompleto = getString('nomeCompleto');
        var cargo = getString('cargo');
        var email = getString('email');
        var celular = getString('celular');
        var login = getString('login');
        var password = getString('senha');
        var ativo = getBoolean('ativo');
        var foto = getImagem('foto');

        usuario.setId(idUsuario);
        usuario.setNomeCompleto(nomeCompleto);
        usuario.setCargo(cargo);
        usuario.setEmail(email);
        usuario.setCelular(celular);
        usuario.setLogin(login);
        usuario.setPassword(password);
        usuario.setAtivo(ativo);
        usuario.setFoto(foto);
        return usuario;
    }

    function popularDadosTela(usuarioJson) {

        usuarioJson = usuarioJson[0];

        var idUsuario = usuarioJson.id;
        var nomeCompleto = usuarioJson.nomeCompleto;
        var cargo = usuarioJson.cargo;
        var email = usuarioJson.email;
        var celular = usuarioJson.celular;
        var login = usuarioJson.login;
        var password = usuarioJson.password;
        var ativo = usuarioJson.ativo;
        var foto = usuarioJson.foto;

        usuario = new Usuario();
        usuario.setId(idUsuario);
        usuario.setNomeCompleto(nomeCompleto);
        usuario.setCargo(cargo);
        usuario.setEmail(email);
        usuario.setCelular(celular);
        usuario.setLogin(login);
        usuario.setPassword(password);
        usuario.setAtivo(ativo);
        usuario.setFoto(foto);

        setString('idUsuario', usuario.getId());
        setString('nomeCompleto', usuario.getNomeCompleto());
        setCombo('cargo', usuario.getCargo());
        setString('email', usuario.getEmail());
        setString('celular', usuario.getCelular());
        setString('login', usuario.getLogin());
        setString('senha', usuario.getPassword());
        setBoolean('ativo', usuario.getAtivo());
        setImagem('foto', foto);
    }

    function voltarListaUsuarios(retorno) {
        $('#mnUsuarios').click();
    }

    function preencherCombo(atributo, json) {
        $("#"+atributo).empty();
        $('#'+atributo).append($('<option>', {
            value: '',
            text : 'Selecione uma opção'
        }));
        jQuery.each(json, function(i, item) {
            $('#'+atributo).append($('<option>', {
                value: item.id,
                text : item.nomeCompleto
            }));
        });
    }

    function preencherComboCargo(json) {
        preencherComboAtributos('cargo', json);
    }

    function logar(id, senha, successCallback) {
        var senha = MD5(senha);
        daoUsuario.logar(id, senha,successCallback);
    }

    function baixar(successCallback) {
        daoUsuario.baixar(successCallback);
    }

    function enviar(successCallback) {
        daoUsuario.enviar(successCallback);
    }
};
