var DaoAparelho = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscarByTV = buscarByTV;
    this.buscarByFIXO = buscarByFIXO;
    this.buscarByBandaLarga = buscarByBandaLarga;

    function tabela() {
        return 'aparelho';
    }

    function colunas() {
        return 'idapp integer,id integer, tipo text, fabricante text, codigo text, descricao text, gama text, tecnologia text, tipoSimCard text, precoAVista real, descontoClaroFacilNascional real, subsidio100pts real, subsidio140pts real, claroTotalAACE real, precoBase24 real, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(aparelho,successCallback) {
        banco.save(tabela(),colunas(),aparelho, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarByTV(successCallbackJson) {
        var sql = 'select * from '+tabela()+' where descricao = "TV"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarByFIXO(successCallbackJson) {
        var sql = 'select * from '+tabela()+' where descricao = "FIXO"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarByBandaLarga(successCallbackJson) {
        var sql = 'select * from '+tabela()+' where descricao = "BANDA LARGA"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(aparelho, item) {
        return banco.popular0bjeto(aparelho, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(),new Aparelho(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Aparelho(), successCallbackJson);
    }

};