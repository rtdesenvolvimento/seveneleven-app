var DaoAparelhoPlano = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscaValorByPlanoFranquiaAparelho = buscaValorByPlanoFranquiaAparelho;
    this.buscaValorByPlanoFranquiaAparelhoPontos = buscaValorByPlanoFranquiaAparelhoPontos;

    function tabela() {
        return 'aparelhoplano';
    }

    function colunas() {
        return 'idapp integer, id integer, aparelho integer, plano integer, franquia integer, pontos text, valor real, telefoneUsuario text, sincronizacao integer';
    }

    function buscaValorByPlanoFranquiaAparelho(plano, franquia, aparelho, successCallback) {
      var sql = 'select * from aparelhoplano where plano = "'+plano+'" and franquia="'+franquia+'" and aparelho="'+aparelho+'"';
      console.log(sql);
      banco.buscarCustomerQuery(sql, successCallback);
    }


    function buscaValorByPlanoFranquiaAparelhoPontos(plano, franquia, aparelho, pontos, successCallback) {
        var sql = 'select * from aparelhoplano where pontos="'+pontos+'" and plano = "'+plano+'" and franquia="'+franquia+'" and aparelho="'+aparelho+'"';
        console.log(sql);
        banco.buscarCustomerQuery(sql, successCallback);
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(aparelhoPlano,successCallback) {
        banco.save(tabela(),colunas(),aparelhoPlano, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(aparelhoPlano, item) {
        return banco.popular0bjeto(aparelhoPlano, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new AparelhoPlano(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new AparelhoPlano(), successCallbackJson);
    }
};