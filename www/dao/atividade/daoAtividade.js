var DaoAtividade = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarFilter = buscarFilter;
    this.popular0bjeto = popular0bjeto;
    this.buscarAllAtividadesAbertasByCheckin = buscarAllAtividadesAbertasByCheckin;
    this.buscarAllAtividadesEmAndamentoByCheckin = buscarAllAtividadesEmAndamentoByCheckin;
    this.checkin = checkin;
    this.checkinGeoLocalizacao = checkinGeoLocalizacao;
    this.checkout = checkout;
    this.buscalAllNotChecklist = buscalAllNotChecklist;
    this.baixar = baixar;
    this.enviar = enviar;
    this.buscarMaxAtividade = buscarMaxAtividade;

    function tabela() {
        return 'atividade';
    }
    
    function colunas() {
        return 'idapp integer, id integer, observacao  text, descricao text, dataAtividade text, status text, horaInicio text, horaFinal text, latitude text, longitude text, ativo integer, checklist integer,  tipoCheckin integer,responsavel integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(), colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(cargo,successCallback) {
        banco.save(tabela(),colunas(), cargo , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscalAllNotChecklist(successCallbackJson) {
        var sql = 'select a.*, tc.descricao as tipoCheckin from atividade a , ' +
            'tipoCheckin tc where a.tipoCheckin = tc.id and (checklist = 0 or checklist = "_") and status = "EM ANDAMENTO"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }
    function buscarAllAtividadesAbertasByCheckin(checklist, successCallbackJson) {
        var sql = '' +
            '   select a.*, tc.descricao as tipoCheckin ' +
            '   from atividade a, tipoCheckin tc ' +
            '   where a.tipoCheckin = tc.id  and a.status = "ABERTO" and a.checklist = "'+checklist+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarAllAtividadesEmAndamentoByCheckin(checklist, successCallbackJson) {
        var sql = '' +
            '   select a.*, tc.descricao as tipoCheckin ' +
            '   from atividade a, tipoCheckin tc ' +
            '   where a.tipoCheckin = tc.id  and a.status = "EM ANDAMENTO" and a.checklist = "'+checklist+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function checkin(idAtividade, successCallbackJson) {
        var atividade = new Atividade();
        atividade.setStatus('EM ANDAMENTO');
        banco.update(tabela(), 'status', idAtividade, atividade, successCallbackJson);
    }

    function buscarFilter(filterTipoCheckin, filterDataCheckin, successCallbackJson) {
        var sql = '' +
            '   select a.id, ' +
            '   a.descricao , ' +
            '   a.observacao, ' +
            '   a.dataAtividade, ' +
            '   a.horaInicio, ' +
            '   a.horaFinal, ' +
            '   a.status, ' +
            '   tc.descricao as tipo ' +
            '   from atividade a, tipoCheckin tc ' +
            '   where a.tipoCheckin = tc.id and ' +
            '         a.telefoneUsuario = "'+TELEFONE_USUARIO+'"';

        if (filterTipoCheckin !== '') sql = sql + ' and tc.id = "'+filterTipoCheckin+'"';
        if (filterDataCheckin !== '') sql = sql + ' and a.dataAtividade = "'+filterDataCheckin+'"';

        banco.buscarCustomerQuery(sql+' order by a.dataAtividade desc', successCallbackJson);
    }

    function buscarMaxAtividade(successCallbackJson) {
        var query = 'select max(idapp) as maxAtividade from '+tabela();
        banco.buscarCustomerQuery(query,function (json) {
            var sql = 'select id as maxAtividade from atividade where idapp="'+json[0].maxAtividade+'"';
            banco.buscarCustomerQuery(sql, successCallbackJson);
        });
    }

    function checkinGeoLocalizacao(idAtividade,latitude, longitude, successCallbackJson) {
        var atividade = new Atividade();
        atividade.setStatus('EM ANDAMENTO');
        atividade.setLatitude(latitude);
        atividade.setLongitude(longitude);
        banco.update(tabela(), 'status,latitude,longitude', idAtividade, atividade, successCallbackJson);
    }

    function checkout(idAtividade, descricao , successCallbackJson) {
        var atividade = new Atividade();
        atividade.setStatus('FECHADO');
        atividade.setDescricao(descricao);
        atividade.setHoraFinal(getTime(new Date()));
        banco.update(tabela(), 'status, descricao, horaFinal', idAtividade, atividade, successCallbackJson);
    }

    function popular0bjeto(atividade, item) {
        return banco.popular0bjeto(atividade, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Atividade(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Atividade(),successCallbackJson);
    }

};