var DaoCaptacao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.deletar = deletar;
    this.converter = converter;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'captacao';
    }

    function colunas() {
        return 'idapp integer, id integer, nome text, email text, telefone text, status text, telefoneAdicional text, dataCaptacao text, descricao text, cidade text, cliente integer,  responsavel integer, plano integer , meioDivulgacao integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(tipoTarefa,successCallback) {
        banco.save(tabela(),colunas(),tipoTarefa, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        var sql = '' +
            '   select * ' +
            '   from '+tabela()+' ' +
            '   where status = "ABERTO" and ' +
            '         telefoneUsuario="'+TELEFONE_USUARIO+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(captacao, item) {
        return banco.popular0bjeto(captacao, item, colunas());
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function converter(idCaptacao, successCallbackJson) {
        var captacao = new Captacao();
        captacao.setStatus('CONVERTIDA');
        banco.update(tabela(), 'status', idCaptacao, captacao, successCallbackJson);
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Captacao(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Captacao(), successCallbackJson);
    }

};