var DaoCargo = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'cargo';
    }
    
    function colunas() {
        return 'idapp integer,id integer, descricao text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(cargo,successCallback) {
        banco.save(tabela(),colunas(), cargo , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(objeto, item) {
        return banco.popular0bjeto(objeto, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Cargo(), colunas(), successCallbackJson);
    }
    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Cargo(), successCallbackJson);
    }

};