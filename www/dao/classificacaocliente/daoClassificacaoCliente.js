var DaoClassificacaoCliente = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'classificacaocliente';
    }

    function colunas() {
        return 'idapp integer,id integer, descricao text, periodoRenovacaoMes integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(classificacaomes,successCallback) {
        banco.save(tabela(),colunas(),classificacaomes, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(classificacaocliente, item) {
        return banco.popular0bjeto(classificacaocliente, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new ClassificacaoCliente(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new ClassificacaoCliente(), successCallbackJson);
    }

};