var DaoCliente = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarlAllInativos = buscarlAllInativos;
    this.buscarMaxCliente = buscarMaxCliente;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'cliente';
    }

    function colunas() {
        return '' +
            '   idapp integer,'+
            '   id integer , ' +
            '   tipoPessoa text, ' +
            '   nome text, ' +
            '   cpfCnpj text, ' +
            '   telefone text, ' +
            '   celular text, ' +
            '   email text, ' +
            '   ie text, ' +
            '   inscricaoMunicipal text, ' +
            '   cnaeprimario text, ' +
            '   cnaesecundario text, ' +
            '   sexo text, ' +
            '   rg text, ' +
            '   orgaoEmissor text, ' +
            '   estadoEmissor text, ' +
            '   dataEmissao text, ' +
            '   cep text, ' +
            '   rua text, ' +
            '   numero text, ' +
            '   complemento text, ' +
            '   bairro text, ' +
            '   cidade text, ' +
            '   estado text, ' +
            '   nomeContato text, ' +
            '   cpfContato text, ' +
            '   nomeContatoGestor text, '+
            '   cpfContatoGestor text, '+
            '   celularAdicional text, ' +
            '   celularComplementar text, '+
            '   numeroClaroVoz integer, '+
            '   numeroClaroBandaLarga integer, '+
            '   numeroClaroTelemetria integer, '+
            '   numeroNetTelefoneFixo integer, '+
            '   numeroNetInternet integer, '+
            '   numeroNetTv integer, '+
            '   dataInicioContrato text, '+
            '   dataTerminoContrato text, '+
            '   classificacaoCliente integer, '+
            '   aptoRenovar integer, '+
            '   valorPlano real, '+
            '   loginPortalClaro text, '+
            '   senhaPortalClaro text, '+
            '   telefoneContato text, ' +
            '   emailContato text, ' +
            '   cargoContato text, ' +
            '   observacao text , ' +
            '   ativo integer,' +
            '   telefoneUsuario text,' +
            '   responsavel integer,' +
            '   sincronizacao integer ';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(cliente,successCallback) {
        banco.save(tabela(),colunas(), cliente , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        var sql = '' +
            '   select * ' +
            '   from cliente' +
            '   where telefoneUsuario="'+TELEFONE_USUARIO+'"';
        banco.buscarCustomerQuery(sql,successCallbackJson);
    }

    function buscarlAllInativos(successCallbackJson) {
        var sql = '' +
            '   select c.*, u.nomeCompleto ' +
            '   from cliente c, usuario u ' +
            '   where   c.responsavel = u.id and ' +
            '           c.ativo = 0 and ' +
            '           c.telefoneUsuario = "'+TELEFONE_USUARIO+'"';
        banco.buscarCustomerQuery(sql,successCallbackJson);
    }

    function buscarMaxCliente(successCallbackJson) {
        var query = 'select max(idapp) as maxCliente from '+tabela();
        banco.buscarCustomerQuery(query,function (json) {
            var sql = 'select id as maxCliente from cliente where idapp="'+json[0].maxCliente+'"';
            banco.buscarCustomerQuery(sql, successCallbackJson);
        });
    }

    function popular0bjeto(cliente, item) {
        return banco.popular0bjeto(cliente, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Cliente(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Cliente(), successCallbackJson);
    }
};