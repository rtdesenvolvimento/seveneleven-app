var DaoCodigoAnatel = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'codigoanatel';
    }

    function colunas() {
        return 'idapp integer,id integer, descricao text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(tipoTarefa,successCallback) {
        banco.save(tabela(),colunas(),tipoTarefa, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(codigoAnatel, item) {
        return banco.popular0bjeto(codigoAnatel, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(),new CodigoAnatel(), colunas(),successCallbackJson);
    }

    function enviar(successCallbackJson) {
     banco.enviar(tabela(), new CodigoAnatel(), successCallbackJson);
    }
};