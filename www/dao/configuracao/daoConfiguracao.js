var DaoConfiguracao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.enviar = enviar;
    this.baixar = baixar;
    this.buscarByCelular = buscarByCelular;
    this.atualizarConfiguracaoImportada = atualizarConfiguracaoImportada;

    initialize();

    function initialize() {}

    function tabela() {
        return 'configuracao';
    }

    function colunas() {
        return 'idapp integer,id integer, usuario text,  importacao integer, descricao text, urlAcessoExterno text, sincronizar integer, tempoSincronizacao integer, alias text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(), colunas(), createInner);
    }

    function createInner(retorno) {
        if (retorno) crateNewConfiguracaoUsuario();
    }

    function getUsuarios(successCallback) {
        var sql = "select * from usuario ";
        banco.buscarCustomerQuery(sql, successCallback)
    }

    function crateNewConfiguracaoUsuario() {

        getUsuarios(function (json) {
            jQuery.each(json, function(i, item) {
                var configuracao = new Configuracao();
                configuracao.setDescricao('CONFIGURAÇÃO GERAL ' + item.celular);
                configuracao.setTempoSincronizacao(3000);
                configuracao.setUrlAcessoExterno(base_url);
                configuracao.setImportacao(0);
                configuracao.setSincronizar(1);
                configuracao.setAlias(gerarAlias());
                configuracao.setUsuario(item.celular);
                save(configuracao, depoisSalvar);
            });
        });
    }

    function buscarByCelular(successCallback) {
        var sql = 'select * from configuracao where usuario="'+TELEFONE_USUARIO+'" and importacao = 0';
        banco.buscarCustomerQuery(sql, successCallback)
    }

    function atualizarConfiguracaoImportada(idConfiguracao, successCallbackJson) {
        var configuracao = new Configuracao();
        configuracao.setImportacao(1);
        banco.update(tabela(), 'importacao', idConfiguracao, configuracao, successCallbackJson);
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(configuracao,successCallback) {
        banco.save(tabela(),colunas(), configuracao , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Configuracao(), successCallbackJson);
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Configuracao(), colunas(), successCallbackJson);
    }

    function depoisSalvar(retorno) {
        console.log('save configuracao padrao '+retorno);
    }
};