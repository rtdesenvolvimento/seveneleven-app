var db;

var banco = {

    initialize: function() {
        db = window.openDatabase('siga', '3' ,'siga', 9000000, function (database) {});
    },
    getBancoDados: function () {
        return db;
    },
    create: function (nomeTabela,colunas,successCallback) {
        var callbacks = $.Callbacks();
        db.transaction(function(tx) {
            var sql = 'CREATE TABLE IF NOT EXISTS '+nomeTabela+' ('+colunas+')';
            console.log(sql+'...');
            tx.executeSql(sql);
            callbacks.add(successCallback);
            callbacks.fire(true);
        }, function (error) {console.log(error);}, function () {});
    },
    save: function (nomeTabela,select, objeto, successCallback) {
        var id = eval('objeto.getId()');

        if (id !== undefined) banco.update(nomeTabela, select, id, objeto, successCallback);
        else banco.saveInner(nomeTabela, select, objeto, successCallback);
    },
    replaceAllDo: function(select){
        select = replaceAll(select, 'text', '');
        select = replaceAll(select, 'integer', '');
        select = replaceAll(select, 'real', '');
        return select;
    },
    saveInner: function (nomeTabela,select, objeto,successCallback, totalObjetos, indice) {
        select = this.replaceAllDo(select);

        var callbacks   = $.Callbacks();
        var tamanho     = select.split(',');
        var campos      = banco.camposInner(tamanho);

        db.transaction(function(tx) {
            if(objeto.getIdapp() !== '' && objeto.getIdapp() !== undefined) {
                var dados = banco.convertObjetToArray(tamanho, objeto);
                tx.executeSql("INSERT INTO "+nomeTabela+" ("+select+") VALUES ("+campos+")", dados);
                callbacks.add(successCallback);
            } else {
                var sql = "select max(idapp) maxId from " + nomeTabela + " where telefoneUsuario='" + TELEFONE_USUARIO + "'";
                tx.executeSql(sql, [], function (tx, res) {

                    var idapp = res.rows[0].maxId;
                    if (idapp == null) idapp = 1;
                    else idapp = idapp + 1;

                    objeto.setIdapp(idapp);
                    objeto.setId(idapp+'_'+TELEFONE_USUARIO);

                    var dados = banco.convertObjetToArray(tamanho, objeto);

                    var newObjetoDao = eval('new Dao'+objeto.constructor.name+'()');
                    tx.executeSql("INSERT INTO "+nomeTabela+" ("+select+") VALUES ("+campos+")", dados);

                    newObjetoDao.enviar(successCallback);

                });
            }

        },function (error) {console.log(error);callbacks.fire(false, totalObjetos, indice);}, function () {callbacks.fire(true, totalObjetos, indice);});
    },
    camposInner: function(tamanho) {
        var campos = '?';
        for(var i=1;i<tamanho.length;i++) {
            campos += ',?';
        }
        return campos;
    },
    convertObjetToArray: function(tamanho, objeto) {
        var dados = [];

        for(var i=0;i<tamanho.length;i++) {
            var atributo = tamanho[i];
            atributo = replaceAll(atributo,' ','');

            atributo = atributo.charAt(0).toUpperCase() + atributo.slice(1);
            var comando = 'objeto.get'+atributo+'()';
            var valor = eval(comando);

            if (atributo === 'TelefoneUsuario' && valor === undefined) valor = TELEFONE_USUARIO;
            if (atributo === 'Sincronizacao' && valor === undefined) valor = 1;

            dados.push(valor);
        }
        return dados;
    },
    convertObjetToArrayUpdate: function(tamanho, objeto) {
        var dados = [];
        for(var i=0;i<tamanho.length;i++) {
            var atributo = tamanho[i];
            atributo = replaceAll(atributo,' ','');

            if (atributo === 'id') continue;
            if (atributo === 'idapp') continue;
            if (atributo === 'telefoneUsuario') continue;
            if (atributo === 'Sincronizacao') continue;

            atributo = atributo.charAt(0).toUpperCase() + atributo.slice(1);
            var comando = 'objeto.get'+atributo+'()';
            var valor = eval(comando);

            dados.push(valor);
        }
        return dados;
    },
    popular0bjeto: function(objeto, item, select) {

        select = this.replaceAllDo(select);

        var tamanho = select.split(',');

        for(var i=0;i<tamanho.length;i++) {
            var atributo = tamanho[i];
            atributo = replaceAll(atributo,' ','');

            if (atributo === 'telefoneUsuario') continue;
            if (atributo === 'idapp') continue;
            if (atributo === 'sincronizacao') continue;

            var strValor = 'item.'+atributo;
            var valor = eval(strValor);

            if (valor === null) valor = '';
            if (valor !== undefined) valor = valor.replace('\n','');
            else valor = '';

            atributo = atributo.charAt(0).toUpperCase() + atributo.slice(1);

            var comando = 'objeto.set'+atributo+'("'+valor+'")';
            eval(comando);
        }

        return objeto;
    },
    popular0bjetoBaixar: function(objeto, item, select) {

        select = this.replaceAllDo(select);

        var tamanho = select.split(',');

        for(var i=0;i<tamanho.length;i++) {
            var atributo = tamanho[i];
            atributo = replaceAll(atributo,' ','');
            var strValor;

            if (atributo === 'id') strValor = 'item.idsistema';
            else strValor = 'item.'+atributo;

            var valor = eval(strValor);

            if (valor === null) valor = '';
            if (valor !== undefined) valor = valor.replace('\n','');
            else valor = '';

            atributo = atributo.charAt(0).toUpperCase() + atributo.slice(1);

            valor = replaceAll(valor,'\n','');

            var comando = 'objeto.set'+atributo+'("'+valor+'")';

            eval(comando);
        }
        return objeto;
    },
    update: function(nomeTabela,select, id, objeto, successCallback, isApenasBaixar,totalObjetos, indice) {

        select = this.replaceAllDo(select);

        var callbacks = $.Callbacks();
        var campos = select.split(',');
        var dados = banco.convertObjetToArrayUpdate(campos, objeto);
        var strCampos = '';

        for(var i=0;i<campos.length;i++) {

            var campo = campos[i];
            campo = replaceAll(campo, ' ', '');

            if (campo === 'id') continue;
            if (campo === 'idapp') continue;
            if (campo === 'telefoneUsuario') continue;

            strCampos += campo + ' =  ? ,';
        }

        strCampos = strCampos.substr(0, strCampos.length-1);

        if (isApenasBaixar !== undefined && isApenasBaixar===true) dados.push(0);
        else dados.push(1);

        db.transaction(function(tx) {
            tx.executeSql("UPDATE "+nomeTabela+" SET "+strCampos+", sincronizacao = ? WHERE id = '"+id+"'", dados, function (tx, res) {
                if (res.rowsAffected === 0) {
                    banco.saveInner(nomeTabela, select, objeto, successCallback);
                } else {
                    var newObjetoDao = eval('new Dao'+objeto.constructor.name+'()');
                    newObjetoDao.enviar(successCallback);
                }
            }, function (erro) {});
            callbacks.add(successCallback);
        },function (error) {console.log(error);callbacks.fire(false, totalObjetos, indice);}, function () {callbacks.fire(true, totalObjetos, indice);});
    },
    drop: function (nomeTabela) {
        db.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+nomeTabela);
            console.log('drop table '+nomeTabela);
        });
    },
    deleteById: function (nomeTabela, id) {
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+nomeTabela+" WHERE id ='"+id+"'");
        });
    },
    deleteAll: function (nomeTabela) {
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+nomeTabela);
        });
    },
    buscaById: function (nomeTabela, id, successCallbackJson) {
        var callbacks = $.Callbacks();
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+nomeTabela+" WHERE id = '"+id+"';", [], function(tx, res) {
                var strJson = JSON.stringify(res.rows);
                var json = JSON.parse(strJson);
                callbacks.add(successCallbackJson);
                callbacks.fire(json);
            });
        }, function (error) {console.log(error);}, function () {});
    },
    buscarAll: function (nomeTabela, successCallbackJson) {
        var callbacks = $.Callbacks();
        callbacks.add(function (nomeTabela) {
            db.transaction(function (tx) {
                if ( typeof nomeTabela === "string") {
                    tx.executeSql("SELECT * FROM " + nomeTabela, [], function (tx, res) {
                        var strJson = JSON.stringify(res.rows);
                        var json = JSON.parse(strJson);
                        callbacks.add(successCallbackJson);
                        callbacks.fire(json);
                    });
                }
            }, function (error) {
                console.log(error.message);
            }, function () {});
        });
        callbacks.fire(nomeTabela);
    },
    buscarAllOnlySincronizado: function (nomeTabela, successCallbackJson) {
        var callbacks = $.Callbacks();
        callbacks.add(function (nomeTabela) {
            db.transaction(function (tx) {
                if ( typeof nomeTabela === "string") {
                    tx.executeSql("SELECT * FROM " + nomeTabela+' WHERE sincronizacao = 1', [], function (tx, res) {
                        var strJson = JSON.stringify(res.rows);
                        var json = JSON.parse(strJson);
                        callbacks.add(successCallbackJson);
                        callbacks.fire(json);
                    });
                }
            }, function (error) {
                console.log(error.message);
            }, function () {});
        });
        callbacks.fire(nomeTabela);
    },
    buscarCustomerQuery: function (query, successCallbackJson) {
        var callbacks = $.Callbacks();
        callbacks.add(function (query) {
            db.transaction(function (tx) {
                if ( typeof query === "string") {
                    tx.executeSql(query, [], function (tx, res) {
                        var strJson = JSON.stringify(res.rows);
                        var json = JSON.parse(strJson);
                        callbacks.add(successCallbackJson);
                        callbacks.fire(json);
                    });
                }
            }, function (error) {
                console.log(error);
            }, function () {});
        });
        callbacks.fire(query);
    },
    saveBaixar: function (nomeTabela,select, objeto, successCallback, totalObjetos, indice) {
        var id = eval('objeto.getId()');
        banco.verificaId(nomeTabela, id, function (json) {
            if (json[0] !== undefined) banco.update(nomeTabela, select, id, objeto, successCallback, true, totalObjetos, indice);
            else banco.saveInner(nomeTabela, select, objeto, successCallback, totalObjetos, indice);
        });
    },
    verificaId: function(nomeTabela, id, successCallback){
        banco.buscarCustomerQuery("select * from "+nomeTabela+" where id = '"+id+"'", successCallback);
    },
    baixar: function (tabela,objeto,colunas,successCallback) {

        var callbacks = $.Callbacks();
        callbacks.add(successCallback);

        var horaProcessamento = getStrTime(new Date());

        $.ajax({
            url		: base_url + '/app/jspAppSincronizacao.jsp',
            data: {
                metodo: 'baixar',
                baixar_tudo: BAIXAR_ALL,
                nomeObjeto: objeto.constructor.name,
                telefoneUsuario: TELEFONE_USUARIO,
                horaProcessamento: horaProcessamento,
                CNPJ: CNPJ_CLIENTE
            },
            dataType: "jsonp",
            type: "POST",
            cache: true,
            jsonp: false,
            jsonpCallback: "apiStatusBaixar"+objeto.constructor.name+'_'+horaProcessamento,
            contentType: "application/json; charset=utf-8;",
            crossDomain: true,
        }).done(function(objetos){

            if(Object.keys(objetos).length === 0) {
                callbacks.fire(objetos);
                return;
            }

            $.each(objetos, function (i, item) {
                var newObjeto = eval('new ' + objeto.constructor.name + '()');

                newObjeto = banco.popular0bjetoBaixar(newObjeto, item, colunas);

                banco.saveBaixar(tabela, colunas, newObjeto, function (retorno, total, i) {
                    if ( (total- 1) === i) {
                        console.log('save '+i+' '+tabela);
                        callbacks.fire(objetos);
                    }
                }, Object.keys(objetos).length , i);

            });
        });
    },
    enviar: function (tabela, objeto, successCallback) {
        var callbacksSucess = $.Callbacks();
        callbacksSucess.add(successCallback);

        banco.buscarAllOnlySincronizado(tabela, function (json) {
            var total = Object.keys(json).length;
            if (total === 0) callbacksSucess.fire(true);
            else banco._upload(tabela, 0, json, total, objeto, callbacksSucess);
        });
    },
    _upload: function (tabela, i, json, total, objeto, callbacksSucess) {

        if (objeto.constructor.name === 'Digitalizacao') {
            this._save_to_base_64_into_server(tabela, i, json, total, objeto, callbacksSucess);
        } else {

            var callbacks = $.Callbacks();
            var item = json[i];
            callbacks.add(banco._upload);

            var horaProcessamento = getStrTime(new Date());

            $.ajax({
                url		: base_url + '/app/jspAppSincronizacao.jsp',
                data    : {
                    metodo: 'enviar',
                    itens: item,
                    nomeObjeto: objeto.constructor.name,
                    telefoneUsuario: TELEFONE_USUARIO,
                    horaProcessamento: horaProcessamento,
                    CNPJ: CNPJ_CLIENTE
                },
                dataType: "jsonp",
                type: "POST",
                cache: true,
                jsonp: false,
                jsonpCallback: "apiStatusEnviar"+objeto.constructor.name+'_'+horaProcessamento,
                contentType: "application/json; charset=utf-8;",
                crossDomain: true,
            }).done(function(retorno){

                banco.updateToNotSincronizacao(tabela, item.id);

                var contador = i+1;

                if (contador === total) callbacksSucess.fire(true);
                else callbacks.fire(tabela, contador, json, total, objeto, callbacksSucess);

            });
        }
    },
    _save_to_base_64_into_server(tabela, i, json, total, objeto, callbacksSucess) {

        let callbacks = $.Callbacks();
        let item = json[i];

        callbacks.add(banco._upload);

        let horaProcessamento = getStrTime(new Date());

        let imageUrl = item.documento;

        let transfer = new ControllerFileTransfer();
        let parametros = '';

        parametros += "?metodo=enviar";

        parametros += '&itens[idapp]='+item.idapp;
        parametros += '&itens[id]='+item.id;

        if (item.documento !== '') parametros += '&itens[documento]='+TELEFONE_USUARIO+'_'+(Math.random(1000)+'').split('.')[1]+'.png';
        if (item.documento !== '') parametros += '&itens[documento]='+TELEFONE_USUARIO+'_'+(Math.random(1000)+'').split('.')[1]+'.png';

        parametros += '&itens[observacao]='+item.observacao;
        parametros += '&itens[cliente]='+item.cliente;
        parametros += '&itens[atividade]='+item.atividade;
        parametros += '&itens[checkout]='+item.checkout;
        parametros += '&itens[telefoneUsuario]='+item.telefoneUsuario;
        parametros += '&itens[sincronizacao]='+item.sincronizacao;

        parametros += '&nomeObjeto='+objeto.constructor.name;
        parametros += '&telefoneUsuario='+TELEFONE_USUARIO;
        parametros += '&horaProcessamento'+horaProcessamento;
        parametros += '&CNPJ='+CNPJ_CLIENTE;

        transfer.uploadPhoto(imageUrl, parametros,
            function (r){
                /*
                console.log("Code = " + r.responseCode);
                console.log("Response = " + r.response);
                console.log("Sent = " + r.bytesSent);
                */
                banco.updateToNotSincronizacao(tabela, item.id);

                var contador = i+1;

                if (contador === total) callbacksSucess.fire(true);
                else callbacks.fire(tabela, contador, json, total, objeto, callbacksSucess);

            } ,
            function (error){
                console.log("An error has occurred: Code = " + error.code);
        });

    },
    updateToNotSincronizacao: function (nomeTabela, id) {
        db.transaction(function(tx) {
            tx.executeSql("UPDATE "+nomeTabela+" SET sincronizacao = 0 WHERE id = '"+id+"'", [], function (tx, res) {
            }, function (erro) {});
        },function (error) {console.log(error+' '+ nomeTabela+' not sincronizado');}, function () {console.log(nomeTabela+' sincronizado')});
    }
};