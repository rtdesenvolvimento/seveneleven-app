var DaoDigitalizacao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarAllByCliente = buscarAllByCliente;
    this.buscarallByAtividade = buscarallByAtividade;
    this.buscarallByAtividadeCheckout = buscarallByAtividadeCheckout;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'digitalizacao';
    }

    function colunas() {
        return 'idapp integer, id integer, documento text, observacao text, cliente integer, atividade integer, checkout integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(formulario,successCallback) {
        banco.save(tabela(),colunas(),formulario, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByCliente(cliente, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where cliente="'+cliente+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarallByAtividade(atividade, successCallbackJson){
        var sql = 'select * from '+tabela()+' where atividade="'+atividade+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarallByAtividadeCheckout(atividade, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where checkout="'+atividade+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarAllByAtividade(atividade, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where atividade="'+atividade+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(digitalizacao, item) {
        return banco.popular0bjeto(digitalizacao, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Digitalizacao(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Digitalizacao(), successCallbackJson);
    }
};