var DaoFormulario = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarPerguntaAll = buscarPerguntaAll;
    this.buscarAllPerguntasByFormulario = buscarAllPerguntasByFormulario;
    this.popular0bjeto = popular0bjeto;

    function initialize() {}

    function tabela() {
        return 'formulario';
    }

    function colunas() {
        return 'id integer primary key, descricao text,  ativo integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(formulario,successCallback) {
        banco.save(tabela(),colunas(),formulario, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarPerguntaAll(formulario, pagina, successCallbackJson) {
        var sql = 'select * from perguntaformulario WHERE formulario = '+formulario+' and pagina='+pagina;
        banco.buscarCustomerQuery(sql,successCallbackJson);
    }


    function buscarAllPerguntasByFormulario(formulario, successCallbackJson) {
        var sql = 'select *,pf.id as idResposta, f.descricao descricaoFormulario' +
            '    from perguntaformulario pf, formulario f' +
            ' WHERE pf.formulario = f.id and pf.formulario = '+formulario+' order by pf.pagina, pf.ordem desc';
        banco.buscarCustomerQuery(sql,successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(formulario, item) {
        return banco.popular0bjeto(formulario, item, colunas());
    }
};