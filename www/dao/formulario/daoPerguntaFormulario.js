var DaoPerguntaFormulario = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarUltimaPergunta = buscarUltimaPergunta;
    this.totalPaginas = totalPaginas;
    this.atualizarProximaPegunta = atualizarProximaPegunta;
    this.buscaMaxOrdemPergunta = buscaMaxOrdemPergunta;
    this.buscaByOrdem = buscaByOrdem;
    this.deletar = deletar;
    this.popular0bjeto = popular0bjeto;

    function initialize() {}

    function tabela() {
        return 'perguntaformulario';
    }

    function colunas() {
        return 'id integer primary key, formulario integer, pergunta text, tipoCampo text,  descricao text, peso real, obrigatorio integer,  ativo integer, anterior integer, proxima integer, alias text, pagina integer, ordem integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(perguntaformulario,successCallback) {
        banco.save(tabela(),colunas(),perguntaformulario, successCallback);
    }

    function deletar(idPergunta) {
        banco.deleteById(tabela(), idPergunta);
    }

    function atualizarProximaPegunta(idPergunta, idProxima, successCallback) {
        var perguntaFormulario = new PerguntaFormulario();
        perguntaFormulario.setProxima(idProxima);
        banco.update(tabela(), 'proxima', idPergunta, perguntaFormulario, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscaByOrdem(ordem,formulario, successCallbackJson) {
        var query = 'select * from '+tabela()+' where ordem = '+ordem+' and formulario = '+formulario;
        banco.buscarCustomerQuery(query, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarUltimaPergunta(successCallbackJson) {
        var query = 'select max(id) as ultimaPergunta from '+tabela();
        banco.buscarCustomerQuery(query,successCallbackJson);
    }

    function buscaMaxOrdemPergunta(formulario, successCallbackJson) {
        var query = 'select max(ordem) as ordem from '+tabela()+" where formulario ="+formulario;
        banco.buscarCustomerQuery(query,successCallbackJson);
    }

    function totalPaginas(formulario, successCallbackJson) {
        var query = 'select max(pagina) totalPaginas from '+tabela()+' where formulario='+formulario;
        banco.buscarCustomerQuery(query,successCallbackJson);
    }

    function popular0bjeto(perguntaFormulario, item) {
        return banco.popular0bjeto(perguntaFormulario, item, colunas());
    }

};