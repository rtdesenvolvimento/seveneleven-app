var DaoPerguntaFormularioOpcao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletarAllByPergunta = deletarAllByPergunta;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarAllByPergunta = buscarAllByPergunta;
    this.popular0bjeto = popular0bjeto;

    initialize();

    function initialize() {}

    function tabela() {
        return 'perguntaformularioopcao';
    }

    function colunas() {
        return 'id integer primary key, pergunta integer, formulario integer,  opcao text, peso real, anterior integer, proxima integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(perguntaformularioopcao,successCallback) {
        banco.save(tabela(),colunas(),perguntaformularioopcao, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function deletarAllByPergunta(idPergunta) {
        var sql = 'delete from '+tabela()+' where pergunta='+idPergunta;
        banco.buscarCustomerQuery(sql);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByPergunta(pergunta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where pergunta ='+pergunta;
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(perguntaFormularioOpcao, item) {
        return banco.popular0bjeto(perguntaFormularioOpcao, item, colunas());
    }
};