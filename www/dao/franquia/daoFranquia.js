var DaoFranquia = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscarAllByPlano = buscarAllByPlano;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'franquia';
    }
    
    function colunas() {
        return 'idapp integer,id integer,plano integer, franquia text, bonus text,  pontos  text, totalInternet integer , valor real,  isPossuiMobilidade integer, valorMobilidade real, isPossuiRedesSociais integer, valorRedesSociais real, passaporte integer, valorPassaporte real, maximoLinhaSemGol real, maximoLinhaComGol real, minimoLinha real, ativo integer,  telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(franquia,successCallback) {
        banco.save(tabela(),colunas(), franquia , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByPlano(idPlano, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where plano = "'+idPlano+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }
    function popular0bjeto(objeto, item) {
        return banco.popular0bjeto(objeto, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Franquia(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Franquia(), successCallbackJson);
    }
};