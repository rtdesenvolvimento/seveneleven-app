var DaoGeoLocalizacao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;

    initialize();

    function initialize() {}

    function tabela() {
        return 'geolocalizacao';
    }

    function colunas() {
        return 'id integer primary key, proposta integer, latitude text, longitude text, altitude text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(cargo,successCallback) {
        banco.save(tabela(),colunas(), cargo , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

};