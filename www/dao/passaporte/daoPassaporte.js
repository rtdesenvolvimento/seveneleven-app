var DaoPassaporte = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'passaporte';
    }

    function colunas() {
        return 'idapp integer,id integer, descricao text, valor real, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(passaporte,successCallback) {
        banco.save(tabela(),colunas(),passaporte, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(passaporte, item) {
        return banco.popular0bjeto(passaporte, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Passaporte(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Passaporte(), successCallbackJson);
    }

};