var DaoPlano = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarAllByTipo = buscarAllByTipo;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'plano';
    }

    function colunas() {
        return 'idapp integer,id integer, tipoPlano text, descricao text, tipo integer, ativo integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(plano,successCallback) {
        banco.save(tabela(),colunas(),plano, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByTipo(tipo,successCallbackJson ) {
        var sql = 'select * from plano where tipoPlano="'+tipo+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Plano(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Plano(), successCallbackJson);
    }

};