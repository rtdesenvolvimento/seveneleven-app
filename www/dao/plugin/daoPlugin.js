var DaoPlugin = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;

    function initialize() {}

    function tabela() {
        return 'plugin';
    }

    function colunas() {
        return 'id integer primary key, descricao text, valor real, ativo integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(plugin,successCallback) {
        banco.save(tabela(),colunas(),plugin, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(plugin, item) {
        return banco.popular0bjeto(plugin, item, colunas());
    }

};