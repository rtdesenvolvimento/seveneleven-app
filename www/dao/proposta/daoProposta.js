var DaoProposta = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarPropostasApresentacao = buscarPropostasApresentacao;
    this.buscarPropostasEnviarProposta = buscarPropostasEnviarProposta;
    this.buscarPropostasRetornarVisita = buscarPropostasRetornarVisita;
    this.buscarPropostasArquivada = buscarPropostasArquivada;
    this.buscarRespostasByProposta = buscarRespostasByProposta;
    this.buscarPropostasAguardandoDocumentos = buscarPropostasAguardandoDocumentos;
    this.buscarPropostasAguardandoAssinatura = buscarPropostasAguardandoAssinatura;
    this.buscarUltimaProposta = buscarUltimaProposta;
    this.buscarAllPropostasAbertasAguardandoFechamento = buscarAllPropostasAbertasAguardandoFechamento;
    this.buscarAllPropostasFinalizadas = buscarAllPropostasFinalizadas;
    this.popular0bjeto = popular0bjeto;
    this.atualizarQtdLinhas = atualizarQtdLinhas;
    this.atualizarValorPlano = atualizarValorPlano;
    this.atualizarValorAparelho = atualizarValorAparelho;
    this.atualizarCliente = atualizarCliente;
    this.baixar = baixar;
    this.enviar = enviar;
    this.relatorioPropostas = relatorioPropostas;
    this.buscarFilter = buscarFilter;
    this.converterPedido = converterPedido;

    function tabela() {
        return 'proposta';
    }

    function colunas() {
        return 'idapp integer, id integer, isPedido integer, observacaoAparelhos text,  isSmsVontade integer, isAutorizoContabilidade integer , telefoneContabilidade text,  isWhatsAppVontade integer, isMinutosIlimitados integer, cnpjProposta text, tipoVenda text, dataApresentacao text, dataEnvioProposta text, dataRetornoVisita text, dataAguardandoDocumento text, dataAguardandoAssinatura text, descricao text, tipo text, nomeEmpresa text, valorPlano real, valorAparelho real, possuiServicoEmbratel integer,operadoraAtual integer, possuiServicoNet integer,  qtdLinhas integer, tempoVigenciaContrato  integer, dataVencimento text,  classificacaoCliente integer, aptoRenovar integer,  telefone text, nomeContato text, dataTerminoContrato text,  gastoAtual real, dataInicioContrato text, email text, meioDivulgacao  integer, status text, formulario integer, cliente integer, observacao text, dataProposta text, dataAberta text, dataAguardando text, dataFechamento text,latitude text, longitude text, altitude text, visitaConfirmada integer, telefoneUsuario text, responsavel integer, identificacaoClienteNet text, backupOnline integer, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(proposta,successCallback) {
        banco.save(tabela(),colunas(),proposta, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        var sql = '' +
            '   select * ' +
            '   from proposta ' +
            '   where isPedido = 0 and telefoneUsuario="' + TELEFONE_USUARIO + '"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarFilter(status, successCallbackJson) {
        if (status === 'TODOS') {
            buscarAllByFilter(successCallbackJson);
        } else {
            var sql = '' +
                '   select * ' +
                '   from proposta ' +
                '   where isPedido = 1 and status = "' + status + '" and telefoneUsuario="' + TELEFONE_USUARIO + '"';
            banco.buscarCustomerQuery(sql, successCallbackJson);
        }
    }

    function buscarAllByFilter(successCallbackJson) {
        var sql = '' +
            '   select * ' +
            '   from proposta ' +
            '   where isPedido = 1 and telefoneUsuario="' + TELEFONE_USUARIO + '"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function sqlListar(status) {
        var sql = '' +
            '   select *, p.id idProposta, p.observacao as observacaoProposta, p.valorPlano as valorPlano   ' +
            '   from proposta p ' +
            '   where  p.isPedido = 0 and p.status = "'+status+'" and p.telefoneUsuario="'+TELEFONE_USUARIO+'"';
        return sql;
    }

    function buscarAllPropostasFinalizadas(successCallbackJson) {
        var sql = '' +
            '   select *, p.id idProposta, p.observacao as observacaoProposta, p.valorPlano as valorPlano   ' +
            '   from proposta p' +
            '   where p.isPedido = 0 and p.telefoneUsuario="'+TELEFONE_USUARIO+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function relatorioPropostas(filterStatusProposta, filterDataProposta, filterNomeEmpresa, filterTipoPlanoProposta, successCallbackJson) {
        var sql = '' +
            '   select *, p.id idProposta, p.observacao as observacaoProposta, p.valorPlano as valorPlano   ' +
            '   from proposta p' +
            '   where   p.isPedido = 0 and ' +
            '           p.telefoneUsuario="'+TELEFONE_USUARIO+'"';

        if (filterStatusProposta !== '') sql = sql + ' and p.status = "'+filterStatusProposta+'"';
        if (filterTipoPlanoProposta !== '') sql = sql + ' and p.tipo = "'+filterTipoPlanoProposta+'"';
        if (filterNomeEmpresa !== '') sql = sql + ' and p.nomeEmpresa like "%'+filterNomeEmpresa+'%"';

        if (filterDataProposta !== '') {
            if (filterStatusProposta === 'APRESENTACAO') sql = sql + ' and p.dataApresentacao = "'+filterDataProposta+'"';
            else if (filterStatusProposta === 'ENVIAR PROPOSTA') sql = sql + ' and p.dataEnvioProposta = "'+filterDataProposta+'"';
            else if (filterStatusProposta === 'RETORNAR VISITA') sql = sql + ' and p.dataRetornoVisita = "'+filterDataProposta+'"';
            else if (filterStatusProposta === 'ARQUIVADA') sql = sql + ' and p.dataProposta = "'+filterDataProposta+'"';
            else if (filterStatusProposta === '') sql = sql + ' and p.dataProposta = "'+filterDataProposta+'"';
        }

        console.log(sql);
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarPropostasApresentacao(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('APRESENTACAO'), successCallbackJson);
    }

    function buscarPropostasEnviarProposta(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('ENVIAR PROPOSTA'), successCallbackJson);
    }

    function buscarPropostasRetornarVisita(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('RETORNAR VISITA'), successCallbackJson);
    }

    function buscarPropostasArquivada(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('ARQUIVADA'), successCallbackJson);
    }

    function buscarPropostasAguardandoDocumentos(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('AGUARDANDO DOCUMENTOS'), successCallbackJson);
    }


    function buscarPropostasAguardandoAssinatura(successCallbackJson) {
        banco.buscarCustomerQuery(sqlListar('AGUARDANDO ASSINATURA'), successCallbackJson);
    }

    function buscarRespostasByProposta(idProposta, successCallbackJson) {
        var sql = 'select *, rp.pergunta idPergunta ' +
            '   from respostaproposta rp ,' +
            '    perguntaformulario pf, proposta p ' +
            '   where rp.proposta = p.id and  rp.pergunta = pf.id and rp.proposta = "'+idProposta+"'";
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarUltimaProposta(successCallbackJson) {
        var sql = 'select max(id) as maxId from '+tabela();
        banco.buscarCustomerQuery(sql,successCallbackJson);
    }

    function buscarAllPropostasAbertasAguardandoFechamento(successCallbackJson) {
        var sql = ' select * ' +
            '       from proposta p ' +
            '       where p.telefoneUsuario = "'+TELEFONE_USUARIO+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(proposta, item) {
        return banco.popular0bjeto(proposta, item, colunas());
    }

    function atualizarQtdLinhas(qtdLinhas, idProposta, successCallbackJson) {
        var proposta = new Proposta();
        proposta.setQtdLinhas(qtdLinhas);
        banco.update(tabela(), 'qtdLinhas', idProposta, proposta, successCallbackJson);
    }

    function atualizarValorPlano(valorPlano, idProposta, successCallbackJson) {
        var proposta = new Proposta();
        proposta.setValorPlano(valorPlano);
        banco.update(tabela(), 'valorPlano', idProposta, proposta, successCallbackJson);
    }

    function atualizarValorAparelho(valorAparelho, idProposta, successCallbackJson) {
        var proposta = new Proposta();
        proposta.setValorAparelho(valorAparelho)
        banco.update(tabela(), 'valorAparelho', idProposta, proposta, successCallbackJson);
    }

    function atualizarCliente(cliente, idProposta, successCallbackJson) {
        var proposta = new Proposta();
        proposta.setCliente(cliente);
        banco.update(tabela(), 'cliente', idProposta, proposta, successCallbackJson);
    }

    function converterPedido(idProposta, successCallbackJson) {
        var proposta = new Proposta();
        proposta.setIsPedido(1);
        proposta.setStatus('AGUARDANDO DOCUMENTOS');
        banco.update(tabela(), 'isPedido, status', idProposta, proposta, successCallbackJson);
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new Proposta(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Proposta(), successCallbackJson);
    }
};