var DaoPropostaAparelho = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscaAllByProposta = buscaAllByProposta;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'propostaaparelho';
    }

    function colunas() {
        return 'idapp integer, id integer, tipoPlanoAparelho text, operadoraInterna text, tipoCobranca text, plano integer, proposta integer, uf text, ddd text, aparelho integer, cor text,  fabricante text, simCard integer, isSimCardComAparelho integer,  qtdSimCard, precoUnitarioSimCard real, qtdAparelhos integer,  precoUnitarioAparelho real, pontuacao text, franquia integer, isAvulso text, formaPagamento text, total real , telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(propostaaparelho,successCallback) {
        banco.save(tabela(),colunas(),propostaaparelho, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscaAllByProposta(proposta, successCallbackJson) {
        var sql = ' select pa.*, a.descricao as nomeAparelho, a.codigo as codigoAparelho' +
            ' from  propostaaparelho pa, aparelho a where pa.aparelho = a.id and  proposta = "'+proposta+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(propostaaparelho, item) {
        return banco.popular0bjeto(propostaaparelho, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new PropostaAparelho(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new PropostaAparelho(),successCallbackJson);
    }
};