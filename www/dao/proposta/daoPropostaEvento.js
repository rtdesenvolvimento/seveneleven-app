var DaoPropostaEvento = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscarAllByProposta = buscarAllByProposta;
    this.baixar = baixar;
    this.enviar = enviar;
    this.lido = lido;

    function tabela() {
        return 'propostaevento';
    }

    function colunas() {
        return 'idapp integer, id integer, proposta integer, data text, hora text, evento text, lido integer, dataLido text, horaLido text, resposta text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(proposta,successCallback) {
        banco.save(tabela(),colunas(),proposta, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByProposta(idProposta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where proposta = "'+idProposta+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(propostaevento, item) {
        return banco.popular0bjeto(propostaevento, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new PropostaEvento(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new PropostaEvento(), successCallbackJson);
    }

    function lido(idPropostaEvento, successCallbackJson) {
        var propostaEvento = new PropostaEvento();
        propostaEvento.setLido(1);
        banco.update(tabela(), 'lido', idPropostaEvento, propostaEvento, successCallbackJson);
    }
};