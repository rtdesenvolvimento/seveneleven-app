var DaoPropostaFranquia = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscarAllByProposta = buscarAllByProposta;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'propostafranquia';
    }

    function colunas() {
        return 'idapp integer, id integer, proposta integer, tipoPlanoFranquia text, strFranquia text, observacao text, codAnatel integer, plano integer, regiao integer, operadoraPortabilidade integer, isPortabilidade integer, sms integer,  valorSms real, qtdSms integer, ddd text, telefonePortabilidade text, tipoSolicitacaoProposta integer, franquia integer, valorFranquia real, bonus text, qtdLinhas integer, isPossuiMobilidade integer, valorMobilidade real, isPossuiRedesSociais integer, valorRedesSociais real, passaporte integer, valorPassaporte real, passaporteAvulso integer, valorPassaporteAvulso real, responsavel integer, total real, limiteLinhasPlano real, qtdLinhasBaseCliente real, qtdLinhasUteis real, qtdSmartphones real, qtdCelulares real, qtdTabletModem real, qtdVozCelular real, tradeIn text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(proposta,successCallback) {
        banco.save(tabela(),colunas(),proposta, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByProposta(idProposta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where proposta = "'+idProposta+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(propostafranquia, item) {
        return banco.popular0bjeto(propostafranquia, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new PropostaFranquia(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new PropostaFranquia(), successCallbackJson);
    }
};