var DaoPropostaModulo = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscarAllByProposta = buscarAllByProposta;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'propostamodulo';
    }

    function colunas() {
        return 'idapp integer, id integer, proposta integer, ddd text, plano text, ban text, passaporte text, sms text, telefones text ,telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(proposta,successCallback) {
        banco.save(tabela(),colunas(),proposta, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByProposta(idProposta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where proposta = "'+idProposta+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(propostamodulo, item) {
        return banco.popular0bjeto(propostamodulo, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new PropostaModulo(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new PropostaModulo(), successCallbackJson);
    }
};