var DaoPropostaPortabilidade = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.buscaAllByProposta = buscaAllByProposta;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'propostaportabilidade';
    }

    function colunas() {
        return 'idapp integer, id integer,  proposta integer, ddd text, plano text, operadora integer, simcard text, telefones text ,telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(proposta,successCallback) {
        banco.save(tabela(),colunas(),proposta, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(propostafranquia, item) {
        return banco.popular0bjeto(propostafranquia, item, colunas());
    }

    function buscaAllByProposta(proposta, successCallbackJson) {
        var sql =  'select * from propostaportabilidade where proposta = "'+proposta+'"';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function baixar(successCallback) {
        banco.baixar(tabela(), new PropostaPortabilidade(), colunas(), successCallback);
    }

    function enviar(successCallback) {
        banco.enviar(tabela(), new PropostaPortabilidade(), successCallback);
    }
};