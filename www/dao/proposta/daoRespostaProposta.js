var DaoRespostaProposta = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscalAllRespostaByProposta = buscalAllRespostaByProposta;
    this.popular0bjeto = popular0bjeto;

    function tabela() {
        return 'respostaproposta';
    }

    function colunas() {
        return 'idapp integer, id integer, proposta integer, formulario integer, pergunta integer, resposta text, tipoCampo text,  descricao text, peso real, obrigatorio integer, anterior integer, proxima integer, alias text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(formulario,successCallback) {
        banco.save(tabela(),colunas(),formulario, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscalAllRespostaByProposta(proposta, successCallback) {
        var sql = 'select * from '+tabela()+' where proposta = '+proposta;
        banco.buscarCustomerQuery(sql, successCallback);
    }

    function popular0bjeto(respostaProposta, item) {
        return banco.popular0bjeto(respostaProposta, item, colunas());
    }
};