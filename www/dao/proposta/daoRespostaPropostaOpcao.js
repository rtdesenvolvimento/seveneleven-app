var DaoRespostaPropostaOpcao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.deletar = deletar;
    this.deletarAllByPergunta = deletarAllByPergunta;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscarAllByPergunta = buscarAllByPergunta;
    this.buscarAllRespostasOpcaoByProposta = buscarAllRespostasOpcaoByProposta;
    this.buscarAllRespostasOpcaoByPropostaPergunta = buscarAllRespostasOpcaoByPropostaPergunta;
    this.popular0bjeto = popular0bjeto;

    function initialize() {}

    function tabela() {
        return 'respostapropostaopcao';
    }

    function colunas() {
        return 'id integer primary key, pergunta integer, formulario integer, proposta integer,  formularioOpcao  integer, opcao text, resposta text, peso real, anterior integer, proxima integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(perguntaformularioopcao,successCallback) {
        banco.save(tabela(),colunas(),perguntaformularioopcao, successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function deletarAllByPergunta(idPergunta) {
        var sql = 'delete from '+tabela()+' where pergunta='+idPergunta;
        banco.buscarCustomerQuery(sql);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscarAllByPergunta(pergunta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where pergunta ='+pergunta;
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarAllRespostasOpcaoByProposta(idProposta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where proposta = '+idProposta;
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function buscarAllRespostasOpcaoByPropostaPergunta(idProposta, idPergunta, successCallbackJson) {
        var sql = 'select * from '+tabela()+' where proposta = '+idProposta+' and pergunta='+idPergunta;
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(respostaPropostaOpcao, item) {
        return banco.popular0bjeto(respostaPropostaOpcao, item, colunas());
    }
};