var DaoSituacao = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'situacao';
    }

    function colunas() {
        return 'id integer, idapp integer, descricao text, sincronizacao integer, telefoneUsuario text';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(situacao,successCallback) {
        banco.save(tabela(),colunas(),situacao, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(situacao, item) {
        return banco.popular0bjeto(situacao, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixarServer(tabela(), new Situacao(),colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Situacao(), successCallbackJson);
    }

};