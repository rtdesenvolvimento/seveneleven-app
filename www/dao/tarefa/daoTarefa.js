var DaoTarefa = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.buscaAgendadaAll = buscaAgendadaAll;
    this.buscaConfirmadaAll = buscaConfirmadaAll;
    this.buscaExecutadaAll = buscaExecutadaAll;
    this.buscaAgendadaConfirmadaAll = buscaAgendadaConfirmadaAll;
    this.deletar = deletar;
    this.visitas = visitas;
    this.confirmar = confirmar;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'tarefa';
    }

    function colunas() {
        return "" +
            "   idapp integer, "+
            "   id integer ," +
            "   responsavel integer,  " +
            "   tipoTarefa integer,  " +
            "   atribuir integer, "+
            "   status text, " +
            "   descricao text, " +
            "   titulo text , " +
            "   data text, " +
            "   horaInicio text, " +
            "   horaFinal  text, " +
            "   telefoneUsuario text," +
            "   sincronizacao integer";
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(tarefa,successCallback) {
        banco.save(tabela(),colunas(),tarefa,successCallback);
    }

    function deletar(id) {
        banco.deleteById(tabela(), id);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function buscaAgendadaAll(responsavel, successCallbackJson) {
        var query = '' +
            '   select t.*, tp.descricao tipoTarefa, u.nomeCompleto  ' +
            '   from tarefa t , tipoTarefa tp, usuario u ' +
            '   where  t.tipoTarefa = tp.id and' +
            '          t.responsavel = u.id AND ' +
            '          responsavel = "'+responsavel+'" and status = "ABERTA"';
        banco.buscarCustomerQuery(query, successCallbackJson);
    }

    function buscaConfirmadaAll(responsavel, successCallbackJson) {
        var query = '' +
            '   select t.*, tp.descricao tipoTarefa, u.nomeCompleto ' +
            '   from tarefa t , tipoTarefa tp, usuario u ' +
            '   where  t.tipoTarefa = tp.id and' +
            '          t.responsavel = u.id AND ' +
            '          responsavel = "'+responsavel+ '" and status = "CONFIRMADA"';
        banco.buscarCustomerQuery(query, successCallbackJson);
    }

    function buscaExecutadaAll(responsavel, successCallbackJson) {
        var query = '' +
            '   select t.*, tp.descricao tipoTarefa, u.nomeCompleto ' +
            '   from tarefa t , tipoTarefa tp, usuario u ' +
            '   where  t.tipoTarefa = tp.id and' +
            '          t.responsavel = u.id AND ' +
            '          responsavel = "'+responsavel+ '" and status = "EXECUTADA"';
        banco.buscarCustomerQuery(query, successCallbackJson);
    }

    function buscaAgendadaConfirmadaAll(responsavel, successCallbackJson) {
        var query = '' +
            '   select t.*, tp.descricao tipoTarefa, u.nomeCompleto ' +
            '   from tarefa t , tipoTarefa tp, usuario u' +
            '   where  t.tipoTarefa = tp.id and' +
            '          t.responsavel = u.id AND ' +
            '          responsavel = "'+responsavel+'" AND ( status = "ABERTA" OR status = "CONFIRMADA") ';
        banco.buscarCustomerQuery(query, successCallbackJson);
    }

    function visitas(responsavel, successCallbackJson) {
        var query = '' +
            '   select t.*, tp.descricao tipoTarefa, u.nomeCompleto' +
            '   from tarefa t , tipoTarefa tp , usuario u ' +
            '   where  t.tipoTarefa = tp.id AND ' +
            '          t.tipoTarefa = 1 AND ' +
            '          t.responsavel = u.id AND ' +
            '          responsavel = "'+responsavel+ '" AND ( status = "ABERTA" OR status = "CONFIRMADA") ';
        banco.buscarCustomerQuery(query, successCallbackJson);
    }


    function confirmar(idTarefa, successCallbackJson) {
        var tarefa = new Tarefa();
        tarefa.setStatus('CONFIRMADA');
        banco.update(tabela(), 'status', idTarefa, tarefa, successCallbackJson);
    }

    function popular0bjeto(tarefa, item) {
        return banco.popular0bjeto(tarefa, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(),new Tarefa(), colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new Tarefa(), successCallbackJson);
    }

};