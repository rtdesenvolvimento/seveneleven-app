var DaoTipoCheckin = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'tipocheckin';
    }

    function colunas() {
        return 'idapp integer, id integer, descricao text, tempoMedio integer, inativo integer, isUsarObservacao integer, isUsarGPS integer, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(tipoTarefa,successCallback) {
        banco.save(tabela(),colunas(),tipoTarefa, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        var sql = 'select * from tipocheckin where inativo = 0 ';
        banco.buscarCustomerQuery(sql, successCallbackJson);
    }

    function popular0bjeto(tipocheckin, item) {
        return banco.popular0bjeto(tipocheckin, item, colunas());
    }

    function baixar(successCallbackJson) {
        banco.baixar(tabela(), new TipoCheckin(),colunas(), successCallbackJson);
    }

    function enviar(successCallbackJson) {
        banco.enviar(tabela(), new TipoCheckin(), successCallbackJson);
    }

};