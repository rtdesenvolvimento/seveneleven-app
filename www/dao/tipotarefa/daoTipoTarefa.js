var DaoTipoTarefa = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscaById = buscaById;
    this.buscarAll = buscarAll;
    this.popular0bjeto = popular0bjeto;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'tipotarefa';
    }

    function colunas() {
        return 'idapp integer, id integer, descricao text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(),colunas() );
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(tipoTarefa,successCallback) {
        banco.save(tabela(),colunas(),tipoTarefa, successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(),id, successCallbackJson);
    }

    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(tipoTarefa, item) {
        return banco.popular0bjeto(tipoTarefa, item, colunas());
    }

    function baixar(successCallback) {
        banco.baixar(tabela(), new TipoTarefa(), colunas(), successCallback);
    }

    function enviar(successCallback) {
        banco.enviar(tabela(), new TipoTarefa(), successCallback);
    }

};