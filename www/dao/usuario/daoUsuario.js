var DaoUsuario = function () {

    this.create = create;
    this.drop = drop;
    this.save = save;
    this.buscarAll = buscarAll;
    this.buscaById = buscaById;
    this.popular0bjeto = popular0bjeto;
    this.logar = logar;
    this.baixar = baixar;
    this.enviar = enviar;

    function tabela() {
        return 'usuario';
    }

    function colunas() {
        return 'idapp integer, id text, nomeCompleto text, cargo integer, email text, celular text, login text, password text, ativo integer, foto text, telefoneUsuario text, sincronizacao integer';
    }

    function create() {
        banco.create(tabela(), colunas());
    }

    function drop() {
        banco.drop(tabela());
    }

    function save(usuario, successCallback) {
        banco.save(tabela(),colunas(), usuario , successCallback);
    }

    function buscaById(id, successCallbackJson) {
        banco.buscaById(tabela(), id , successCallbackJson );
    }
    
    function buscarAll(successCallbackJson) {
        banco.buscarAll(tabela(), successCallbackJson);
    }

    function popular0bjeto(usuario, item) {
        return banco.popular0bjeto(usuario, item, colunas() );
    }

    function logar(id, senha, successCallback) {
        var sql = 'select * from usuario where id="'+id+'" and password="'+senha+'"';
        banco.buscarCustomerQuery(sql, successCallback);
    }

    function baixar(successCallback) {
        banco.baixar(tabela(), new Usuario(), colunas(), successCallback);
    }

    function enviar(successCallback) {
        banco.enviar(tabela(), new Usuario(), successCallback);
    }
};