/*Dados do usuario logado*/
var ID_USUARIO_LOGADO   = '';
var TELEFONE_USUARIO    = '';
var IS_ADMINISTRADOR    = true;
var EMAIL_USUARIO       = '';

/*Dados da atualizacao*/
var TEMPO_ENVIAR_WIFFI  = 60000*5;//5 minutos;
var TEMPO_ENVIAR_4G     = 60000*5;//5 minutos;

//var TEMPO_ENVIAR_WIFFI  = 60000*1;//1 minutos;
//var TEMPO_ENVIAR_4G     = 60000*1;//1 minutos;

/*controles dos formularios*/
var FORMULARIO          = null;
var PROPOSTA            = null;
var PLANO               = null;
var APARELHO            = null;
var JSON_FORMULARIO     = null;
var CLIENTE             = null;

/*CNPJ DO CLIENTE */
var CNPJ_CLIENTE        = '30831687000178';

//var base_url            = 'http://localhost:9070';
//var BASE_URL_PAPELADA   = 'http://192.168.0.103/rt-php-sos.net-1.0';

var base_url            = 'http://162.214.147.108:18247/seveneleven';
var BASE_URL_PAPELADA   = 'https://rtoficina.resultaweb.com.br';

var URL_CONSULTACEP     = 'https://api.postmon.com.br/v1/cep/';
var URL_CONSULTASEFAZ   = base_url+'/clientesefaz';

var ATIVIDADES          = [];
var AUTOSAVE            = [];

/*Tempo em vigencia do contrato para renovação*/
var TEMPO_VIGENCIA_CONTRATO = 24;

/*valores dos plugins*/
var SMARTPHONE          = 35.90;
var TABLET_MODEM        = 14.90;
var CELULAR             = 39.90;
var VOZ_CELULAR         = 39.90;

var BAIXAR_ALL          = false;