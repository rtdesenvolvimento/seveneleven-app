var FormularioFactory = function () {

    var formulario = null;

    this.createSelectFactory = createSelectFactory;
    this.createTextAreaFactory = createTextAreaFactory;
    this.createInputFactory = createInputFactory;
    this.createFormulario = createFormulario;
    this.destroy = destroy;
    this.refresh = refresh;

    this.select = select;
    this.text = text;
    this.textArea = textArea;
    this.ckebox = ckebox;
    this.radioBox = radioBox;
    this.tel = tel;
    this.date = date;
    this.time = time;
    this.email = email;
    this.number = number;
    this.confirmar = confirmar;
    this.label = label;
    this.createLiUiFieldContain = createLiUiFieldContain;

    function createFormulario(idFormulario) {
        formulario = $('#'+idFormulario);
    }

    function createLiUiFieldContain(id) {
        if (formulario=== null){alert('Formulario não construido1!'); return;}
        var html = '';

        html = html + ' <li class="ui-field-contain" id="'+id+'">';
        html = html + '</li>';
        formulario.append(html);
        refresh();

        return $('#'+id);
    }

    function destroy() {
        if (formulario=== null){alert('Formulario não construido1!'); return;}
        formulario.empty();
    }

    function createInputFactory(id, nome, nomeUsual, type, isObrigatorio, isDisabled, valor, classe, li) {

        if (formulario=== null){alert('Formulario não construido1!'); return;}

        if (valor === undefined) valor = '';
        if (classe === undefined) classe = '';

        var html = '';
        var obrigatorio = '';
        var disabled = '';
        var doisPontos = ':';

        if (type === 'radio' || type === 'checkbox') doisPontos = '';
        if (isObrigatorio) obrigatorio = 'required="required"';
        if (isDisabled) disabled = 'disabled';

        if (li !== undefined) {
            html = html + '<label for="' + id + '">' + nomeUsual + doisPontos + '</label>';
            html = html + '<input type="' + type + '" ' + obrigatorio + ' name="' + nome + '" ' + disabled + ' id="' + id + '" value="' + valor + '" data-clear-btn="true">';
            li.append(html);
        } else {
            html = html + ' <li class="ui-field-contain">';
            html = html + '    <label for="' + id + '">' + nomeUsual + ':</label>';
            html = html + '    <input type="' + type + '" ' + obrigatorio + ' name="' + nome + '" ' + disabled + ' id="' + id + '" value="' + valor + '" data-clear-btn="true">';
            html = html + '</li>';
            formulario.append(html);
        }
        refresh();
    }

    function createTextAreaFactory(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {

        if (formulario === null){alert('Formulario não construido1!'); return;}
        if (valor === undefined) valor = '';
        if (classe === undefined) classe = '';

        var html = '';
        var obrigatorio = '';
        var disabled = '';

        if (isObrigatorio) obrigatorio = 'required="required"';
        if (isDisabled) disabled = 'disabled';

        html = html + ' <li class="ui-field-contain">';
        html = html + '    <label for="'+id+'">'+nomeUsual+':</label>';
        html = html + '    <textarea class="'+classe+'" '+obrigatorio+' name="'+nome+'" '+disabled+' id="'+id+'"data-clear-btn="true">'+valor+'</textarea>';
        html = html + '</li>';

        formulario.append(html);
        refresh();
    }

    function createSelectFactory(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {

        if (formulario === null){alert('Formulario não construido1!'); return;}
        if (valor === undefined) valor = '';
        if (classe === undefined) classe = '';

        var html = '';
        var obrigatorio = '';
        var disabled = '';

        if (isObrigatorio) obrigatorio = 'required="required"';
        if (isDisabled) disabled = 'disabled';

        html = html + ' <li class="ui-field-contain">';
        html = html + '    <label for="'+id+'">'+nomeUsual+':</label>';
        html = html + '    <select class="'+classe+'" '+obrigatorio+' name="'+nome+'" '+disabled+' id="'+id+'">'+valor+'</select>';
        html = html + '</li>';

        formulario.append(html);
        refresh();

        return $('#'+id);
    }

    function refresh() {
        $('ul').listview().listview('refresh');
        $('.ui-page').trigger('create');
    }

    function select(id, nome, nomeUsual, isObrigatorio,isDisabled, valor, classe) {
        return createSelectFactory(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe);
    }

    function text(id, nome, nomeUsual, isObrigatorio,isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'text', isObrigatorio, valor, classe);
    }

    function textArea(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {
        createTextAreaFactory(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe);
    }

    function ckebox(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe, li) {
        createInputFactory(id, nome, nomeUsual, 'checkbox', isObrigatorio, isDisabled, valor, classe, li);
    }

    function radioBox(id, nome, nomeUsual, isObrigatorio,isDisabled,  valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'radio', isObrigatorio, isDisabled, valor, classe, li);
    }

    function tel(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'tel', isObrigatorio,isDisabled, valor, classe);
    }

    function date(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'date', isObrigatorio, isDisabled, valor, classe);
    }

    function time(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'time', isObrigatorio,isDisabled, valor, classe);
    }

    function email(id, nome, nomeUsual, isObrigatorio,isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'email', isObrigatorio, isDisabled, valor, classe);
    }

    function number(id, nome, nomeUsual, isObrigatorio, isDisabled, valor, classe) {
        createInputFactory(id, nome, nomeUsual, 'number', isObrigatorio, isDisabled, valor, classe);
    }

    function label(nome, nomeUsual) {

        if (formulario=== null){alert('Formulario não construido1!'); return;}

        var html = '';

        html = html + ' <li class="ui-field-contain">';
        html = html + '    <label for="'+nome+'">'+nomeUsual+':</label>';
        html = html + '</li>';

        formulario.append(html);
        refresh();
    }

    function confirmar(pergunta) {
        if(confirm(pergunta)){
            return true;
        }else{
            return false;
        }
    }
};