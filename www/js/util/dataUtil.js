
function getDiaSemana(data) {
    var d = new Date(data);
    d.setDate(d.getDate() + 1);
    var dia = d.getDay();
    var semana = new Array(6);

    semana[0]='Domingo';
    semana[1]='Segunda-Feira';
    semana[2]='Terça-Feira';
    semana[3]='Quarta-Feira';
    semana[4]='Quinta-Feira';
    semana[5]='Sexta-Feira';
    semana[6]='Sábado';
    return (semana[dia]);
}

function getMes(data) {
    var m = getMonth(data);
    var mes = new Array(12);
    mes[1]='Janeiro';
    mes[2]='Fevereiro';
    mes[3]='Março';
    mes[4]='Abril';
    mes[5]='Maio';
    mes[6]='Junho';
    mes[7]='Julho';
    mes[8]='Agosto';
    mes[9]='Setembro';
    mes[10]='Outubro';
    mes[11]='Novembro';
    mes[12]='Dezembro';
    return (mes[m]);
}

function getYear(data) {
    var d = new Date(data);
    return d.getFullYear()
}

function getDay(data) {
    var d = new Date(data);
    d.setDate(d.getDate() + 1);
    return  d.getDate();
}

function getMonth(data) {
    var d = new Date(data);
    return  (d.getMonth() + 1);
}

function formatDate(data) {
    var d = new Date(data),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
}

function formatDatePtBr(data){

    console.log(data);

    var data = new Date(data),
        dia  = data.getDate().toString(),
        diaF = (dia.length === 1) ? '0'+dia : dia,
        mes  = (data.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length === 1) ? '0'+mes : mes,
        anoF = data.getFullYear();
    return diaF+"/"+mesF+"/"+anoF;
}

function getTime(d) {
    var data = new Date(d);
    var hora    = data.getHours();          // 0-23
    var min     = data.getMinutes();        // 0-59
    return hora+':'+min;
}

function getStrTime(d) {
    var data = new Date(d);
    var hora    = data.getHours();          // 0-23
    var min     = data.getMinutes();        // 0-59
    var segundo = data.getSeconds();
    var milissegundos = data.getMilliseconds();

    return hora+'_'+min+'_'+segundo+'_'+milissegundos;
}

function getHora(hora) {
    if (hora === undefined) return 0;
    if (hora === 'undefined') return 0;

    var sHora = hora.split(":");
    return sHora[0];
}

function getMinuto(hora) {

    if (hora === undefined) return 0;
    if (hora === 'undefined') return 0;

    var sHora = hora.split(":");
    return sHora[1];
}

function diferencaDias(data1, data2){
    var dif =
        Date.UTC(data1.getYear(),data1.getMonth(),data1.getDate(),0,0,0)
        - Date.UTC(data2.getYear(),data2.getMonth(),data2.getDate(),0,0,0);

    dif = Math.abs((dif / 1000 / 60 / 60));

    var difH = Math.abs(data2.getHours()-data1.getHours());
    var difM = Math.abs(data2.getMinutes()-data1.getMinutes());
    var difS = Math.abs(data2.getSeconds()-data1.getSeconds());
    var d = (dif+difH);

    if (d <= 9)  d = '0'+d;
    if (difM<=9) difM = '0'+difM;
    if (difS<=9) difS = '0'+difS;

    return (d+":"+difM+":"+difS);
}

function formatarHora(hora) {
    var strData = hora.split(":");

    var hour = parseInt(strData[0]);
    var minute = parseInt(strData[1]);

    if (hour < 10) hour = "0" + hour;
    if (minute < 10) minute = "0" + minute;
    return  hour + ":" + minute;
}

function showTimer(id) {

    if (document.getElementById(id) === null) return;

    var data = new Date();
    var strData =  document.getElementById(id).innerHTML;
    strData = strData.split(":");

    var sHora = strData[0];
    var minuto = strData[1];
    var segundo = strData[2];

    segundo = parseFloat(segundo) + 1;

    var time = new Date(data.getYear(), data.getMonth() - 1, data.getDay(), sHora, minuto , segundo);

    var hour = time.getHours();
    var minute = time.getMinutes();
    var second = time.getSeconds();

    if (hour < 10) hour = "0" + hour;
    if (minute < 10) minute = "0" + minute;
    if (second < 10) second = "0" + second;

    var st = hour + ":" + minute + ":" + second;
    document.getElementById(id).innerHTML = st;
}
