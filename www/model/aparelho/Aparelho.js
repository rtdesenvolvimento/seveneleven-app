function Aparelho() {

    var tipo;

    this.setTipo = setTipo;
    this.getTipo = getTipo;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    function setTipo(_tipo) {
        this.tipo = _tipo;
    }

    function getTipo() {
        return this.tipo;
    }

    var fabricante;

    this.setFabricante = setFabricante;
    this.getFabricante = getFabricante;

    function setFabricante(_fabricante) {
        this.fabricante = _fabricante;
    }

    function getFabricante() {
        return this.fabricante;
    }

    var codigo;

    this.setCodigo = setCodigo;
    this.getCodigo = getCodigo;

    function setCodigo(_codigo) {
        this.codigo = _codigo;
    }

    function getCodigo() {
        return this.codigo;
    }

    var descricao;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function setDescricao(_descricao) {
        this.descricao = _descricao;
    }

    function getDescricao() {
        return this.descricao;
    }

    var gama;

    this.setGama = setGama;
    this.getGama = getGama;

    function setGama(_gama) {
        this.gama = _gama;
    }

    function getGama() {
        return this.gama;
    }

    var tecnologia;

    this.setTecnologia = setTecnologia;
    this.getTecnologia = getTecnologia;

    function setTecnologia(_tecnologia) {
        this.tecnologia = _tecnologia;
    }

    function getTecnologia() {
        return this.tecnologia;
    }

    var tipoSimCard;

    this.setTipoSimCard = setTipoSimCard;
    this.getTipoSimCard = getTipoSimCard;

    function setTipoSimCard(_tipoSimCard) {
        this.tipoSimCard = _tipoSimCard;
    }

    function getTipoSimCard() {
        return this.tipoSimCard;
    }

    var precoAVista;

    this.setPrecoAVista = setPrecoAVista;
    this.getPrecoAVista = getPrecoAVista;

    function setPrecoAVista(_precoAVista) {
        this.precoAVista = _precoAVista;
    }

    function getPrecoAVista() {
        return this.precoAVista;
    }

    var descontoClaroFacilNascional;

    this.setDescontoClaroFacilNascional = setDescontoClaroFacilNascional;
    this.getDescontoClaroFacilNascional = getDescontoClaroFacilnascional;

    function setDescontoClaroFacilNascional(_descontoClaroFacilNascional) {
        this.descontoClaroFacilNascional = _descontoClaroFacilNascional;
    }

    function getDescontoClaroFacilnascional() {
        return this.descontoClaroFacilNascional;
    }

    var subsidio100pts;

    this.setSubsidio100pts = setSubsidio100pts;
    this.getSubsidio100pts = getSubsidio100pts;

    function setSubsidio100pts(_subsidio100pts) {
        this.subsidio100pts = _subsidio100pts;
    }

    function getSubsidio100pts() {
        return this.subsidio100pts;
    }

    var subsidio140pts;

    this.setSubsidio140pts = setSubsidio140pts;
    this.getSubsidio140pts = getSubsidio140pts;

    function setSubsidio140pts(_subsidio140pts) {
        this.subsidio140pts = _subsidio140pts;
    }

    function getSubsidio140pts() {
        return this.subsidio140pts;
    }

    var claroTotalAACE;

    this.setClaroTotalAACE = setClaroTotalAACE;
    this.getClaroTotalAACE = getClaroTotalAACE;

    function setClaroTotalAACE(_claroTotalAACE) {
        this.claroTotalAACE = _claroTotalAACE;
    }

    function getClaroTotalAACE() {
        return this.claroTotalAACE;
    }

    var precoBase24;

    this.setPrecoBase24 = setPrecoBase24;
    this.getPrecoBase24 = getPrecoBase24;

    function setPrecoBase24(_precoBase24) {
        this.precoBase24 = _precoBase24;
    }

    function getPrecoBase24() {
        return this.precoBase24;
    }

    var id;

    this.getId = getId;
    this.setId = setId;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

}