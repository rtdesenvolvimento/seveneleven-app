function AparelhoPlano() {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var aparelho;

    this.setAparelho = setAparelho;
    this.getAparelho = getAparelho;

    function setAparelho(_aparelho) {
        this.aparelho = _aparelho;
    }

    function getAparelho() {
        return this.aparelho;
    }

    var plano;

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }

    var franquia;

    this.setFranquia = setFranquia;
    this.getFranquia = getFranquia;

    function setFranquia(_franquia) {
        this.franquia = _franquia;
    }

    function getFranquia() {
        return this.franquia;
    }

    var pontos;

    this.setPontos = setPontos;
    this.getPontos = getPontos;

    function setPontos(_pontos) {
        this.pontos = _pontos;
    }

    function getPontos() {
        return this.pontos;
    }

    var valor;

    this.setValor = setValor;
    this.getValor = getValor;

    function setValor(_valor) {
        this.valor = _valor;
    }

    function getValor() {
        return this.valor;
    }

    var id;

    this.getId = getId;
    this.setId = setId;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}