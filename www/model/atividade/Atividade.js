function Atividade() {

    var id;
    var descricao;
    var dataAtividade;
    var status;
    var horaInicio;
    var horaFinal;
    var latitude;
    var longitude;
    var ativo;
    var tipoCheckin;
    var responsavel;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var checklist;

    this.setChecklist = setChecklist;
    this.getChecklist = getChecklist;

    function setChecklist(_checklist) {
        this.checklist = _checklist;
    }

    function getChecklist() {
        return this.checklist;
    }

    this.setDataAtividade = setDataAtividade;
    this.getDataAtividade = getDataAtividade;

    function setDataAtividade(_dataAtividade) {
        this.dataAtividade = _dataAtividade;
    }

    function getDataAtividade() {
        return this.dataAtividade;
    }

    this.setStatus = setStatus;
    this.getStatus = getStatus;

    function setStatus(_status) {
        this.status = _status;
    }

    function getStatus() {
        return this.status;
    }


    this.setHoraInicio = setHoraInicio;
    this.getHoraInicio = getHoraInicio;

    function setHoraInicio(_horaInicio) {
        this.horaInicio = _horaInicio;
    }

    function getHoraInicio() {
        return this.horaInicio;
    }

    this.setHoraFinal = setHoraFinal;
    this.getHoraFinal = getHoraFinal;

    function setHoraFinal(_horaFinal) {
        this.horaFinal = _horaFinal;
    }

    function getHoraFinal() {
        return this.horaFinal;
    }

    this.setLatitude = setLatitude;
    this.getLatitude = getLatitude;

    function setLatitude(_latitude) {
        this.latitude = _latitude;
    }

    function getLatitude() {
        return this.latitude;
    }

    this.setLongitude = setLongitude;
    this.getLongitude = getLongitude;

    function setLongitude(_longitude) {
        this.longitude = _longitude;
    }

    function getLongitude() {
        return this.longitude;
    }

    this.setAtivo = setAtivo;
    this.getAtivo = getAtivo;

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    this.setTipoCheckin = setTipoCheckin;
    this.getTipoCheckin = getTipoCheckin;

    function setTipoCheckin(_tipoCheckin) {
        this.tipoCheckin = _tipoCheckin;
    }

    function getTipoCheckin() {
        return this.tipoCheckin;
    }

    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }
    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    var observacao;

    this.setObservacao = setObservacao;
    this.getObservacao = getObservacao;

    function setObservacao(_observacao) {
        this.observacao = _observacao;
    }

    function getObservacao() {
        return this.observacao
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}