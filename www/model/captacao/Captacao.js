function Captacao() {

    var id;
    var nome;
    var email;
    var telefone;
    var status;
    var telefoneAdicional;
    var dataCaptacao;
    var descricao;
    var cidade;
    var cliente;
    var responsavel;
    var plano;
    var meioDivulgacao;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.getId = getId;
    this.setId = setId;

    this.setNome = setNome;
    this.getNome = getNome;

    function setNome(_nome) {
        this.nome = _nome;
    }

    function getNome() {
        return this.nome;
    }

    this.setEmail = setEmail;
    this.getEmail = getEmail;

    function setEmail(_email) {
        this.email = _email;
    }

    function getEmail() {
        return this.email;
    }

    this.setTelefone = setTelefone;
    this.getTelefone = getTelefone;

    function setTelefone(_telefone) {
        this.telefone = _telefone;
    }

    function getTelefone() {
        return this.telefone;
    }

    this.setStatus = setStatus;
    this.getStatus = getStatus;

    function setStatus(_status) {
        this.status = _status;
    }

    function getStatus() {
        return this.status;
    }

    this.setTelefoneAdicional = setTelefoneAdicional;
    this.getTelefoneAdicional = getTelefoneAdicional;

    function setTelefoneAdicional(_telefoneAdicional) {
        this.telefoneAdicional = _telefoneAdicional;
    }

    function getTelefoneAdicional() {
        return this.telefoneAdicional;
    }

    this.setDataCaptacao = setDataCaptacao;
    this.getDataCaptacao = getDataCaptacao;

    function setDataCaptacao(_dataCaptacao) {
        this.dataCaptacao = _dataCaptacao;
    }

    function getDataCaptacao() {
        return this.dataCaptacao;
    }

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function setDescricao(_descricao) {
        this.descricao = _descricao;
    }

    function getDescricao() {
        return this.descricao;
    }
    this.setCidade = setCidade;
    this.getCidade = getCidade;

    function setCidade(_cidade) {
        this.cidade = _cidade;
    }

    function getCidade() {
        return this.cidade;
    }

    this.setCliente = setCliente;
    this.getCliente = getCliente;

    function setCliente(_cliente) {
        this.cliente = _cliente;
    }

    function getCliente() {
        return this.cliente;
    }

    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }

    this.setMeioDivulgacao = setMeioDivulgacao;
    this.getMeioDivulgacao = getMeioDivulgacao;

    function setMeioDivulgacao(_meioDivulgacao) {
        this.meioDivulgacao = _meioDivulgacao;
    }

    function getMeioDivulgacao() {
        return this.meioDivulgacao;
    }

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}