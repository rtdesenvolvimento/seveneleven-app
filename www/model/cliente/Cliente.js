function Cliente() {

    var id;
    var tipoPessoa;
    var nome;
    var cpfCnpj;
    var telefone;
    var celular;
    var email;
    var ie;
    var sexo;
    var rg;
    var orgaoEmissor;
    var estadoEmissor;
    var dataEmissao;
    var cep;
    var rua;
    var numero;
    var complemento;
    var bairro;
    var cidade;
    var estado;
    var nomeContato;
    var telefoneContato;
    var emailContato;
    var cargoContato;
    var ativo;
    var observacao;
    var responsavel;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var cpfContato;

    this.setCpfContato = setCpfContato;
    this.getCpfContato = getCpfContato;

    function setCpfContato(_cpfContato) {
        this.cpfContato = _cpfContato;
    }

    function getCpfContato() {
        return this.cpfContato;
    }


    var nomeContatoGestor;

    this.setNomeContatoGestor = setNomeContatoGestor;
    this.getNomeContatoGestor = getNomeContatoGestor;

    function setNomeContatoGestor(_nomeContatoGestor) {
        this.nomeContatoGestor = _nomeContatoGestor;
    }

    function getNomeContatoGestor() {
        return this.nomeContatoGestor;
    }


    var cpfContatoGestor;

    this.setCpfContatoGestor = setCpfContatoGestor;
    this.getCpfContatoGestor = getCpfContatoGestor;

    function setCpfContatoGestor(_cpfContatoGestor) {
        this.cpfContatoGestor = _cpfContatoGestor;
    }

    function getCpfContatoGestor() {
        return this.cpfContatoGestor;
    }

    var celularAdicional;

    this.setCelularAdicional = setCelularAdicional;
    this.getCelularAdicional = getCelularAdicional;

    function setCelularAdicional(_celularAdicional) {
        this.celularAdicional = _celularAdicional;
    }

    function getCelularAdicional() {
        return this.celularAdicional;
    }

    var celularComplementar;

    this.setCelularComplementar = setCelularComplementar;
    this.getCelularComplementar = getCelularComplementar;

    function setCelularComplementar(_celularComplementar) {
        this.celularComplementar = _celularComplementar;
    }

    function getCelularComplementar() {
        return this.celularComplementar;
    }

    var numeroClaroVoz;

    this.setNumeroClaroVoz = setNumeroClaroVoz;
    this.getNumeroClaroVoz = getNumeroClaroVoz;

    function setNumeroClaroVoz(_numeroClaroVoz) {
        this.numeroClaroVoz = _numeroClaroVoz;
    }

    function getNumeroClaroVoz() {
        return this.numeroClaroVoz;
    }

    var numeroClaroBandaLarga;

    this.setNumeroClaroBandaLarga = setNumeroClaroBandaLarga;
    this.getNumeroClaroBandaLarga = getNumeroClaroBandaLarga;

    function setNumeroClaroBandaLarga(_numeroClaroBandaLarga) {
        this.numeroClaroBandaLarga = _numeroClaroBandaLarga;
    }

    function getNumeroClaroBandaLarga() {
        return this.numeroClaroBandaLarga;
    }

    var numeroClaroTelemetria;

    this.setNumeroClaroTelemetria = setNumeroClaroTelemetria;
    this.getNumeroClaroTelemetria = getNumeroClaroTelemetria;

    function setNumeroClaroTelemetria(_numeroClaroTelemetria) {
        this.numeroClaroTelemetria = _numeroClaroTelemetria;
    }

    function getNumeroClaroTelemetria() {
        return this.numeroClaroTelemetria;
    }

    var numeroNetTelefoneFixo;

    this.setNumeroNetTelefoneFixo = setNumeroNetTelefoneFixo;
    this.getNumeroNetTelefoneFixo = getNumeroNetTelefoneFixo;

    function setNumeroNetTelefoneFixo(_numeroNetTelefoneFixo) {
        this.numeroNetTelefoneFixo = _numeroNetTelefoneFixo;
    }

    function getNumeroNetTelefoneFixo() {
        return this.numeroNetTelefoneFixo;
    }

    var numeroNetInternet;

    this.setNumeroNetInternet = setNumeroNetInternet;
    this.getNumeroNetInternet = getNumeroNetInternet;

    function setNumeroNetInternet(_numeroNetInternet) {
        this.numeroNetInternet = _numeroNetInternet;
    }

    function getNumeroNetInternet() {
        return this.numeroNetInternet;
    }

    var numeroNetTv;

    this.setNumeroNetTv = setNumeroNetTv;
    this.getNumeroNetTv = getNumeroNetTv;

    function setNumeroNetTv(_numeroNetTv) {
        this.numeroNetTv = _numeroNetTv;
    }

    function getNumeroNetTv() {
        return this.numeroNetTv;
    }

    var dataInicioContrato;

    this.setDataInicioContrato = setDataInicioContrato;
    this.getDataInicioContrato = getDataInicioContrato;

    function setDataInicioContrato(_dataInicioContrato) {
        this.dataInicioContrato = _dataInicioContrato;
    }

    function getDataInicioContrato() {
        return this.dataInicioContrato;
    }

    var dataTerminoContrato;

    this.setDataTerminoContrato = setDataTerminoContrato;
    this.getDataTerminoContrato = getDataTerminoContrato;

    function setDataTerminoContrato(_dataTerminoContrato) {
        this.dataTerminoContrato = _dataTerminoContrato;
    }

    function getDataTerminoContrato() {
        return this.dataTerminoContrato;
    }

    var classificacaoCliente;

    this.setClassificacaoCliente = setClassificacaoCliente;
    this.getClassificacaoCliente = getClassificacaoCliente;

    function setClassificacaoCliente(_classificacaoCliente) {
        this.classificacaoCliente = _classificacaoCliente;
    }

    function getClassificacaoCliente() {
        return this.classificacaoCliente;
    }

    var aptoRenovar;

    this.setAptoRenovar = setAptoRenovar;
    this.getAptoRenovar = getAptoRenovar;

    function setAptoRenovar(_aptoRenovar) {
        this.aptoRenovar = _aptoRenovar;
    }

    function getAptoRenovar() {
        return this.aptoRenovar;
    }

    var valorPlano;

    this.setValorPlano = setValorPlano;
    this.getValorPlano = getValorPlano;

    function setValorPlano(_valorPlano) {
        this.valorPlano = _valorPlano;
    }

    function getValorPlano() {
        return this.valorPlano;
    }

    var loginPortalClaro;

    this.setLoginPortalClaro = setLoginPortalClaro;
    this.getLoginPortalClaro = getLoginPortalClaro;

    function setLoginPortalClaro(_loginPortalClaro) {
        this.loginPortalClaro = _loginPortalClaro;
    }

    function getLoginPortalClaro() {
        return this.loginPortalClaro;
    }

    var senhaPortalClaro;

    this.setSenhaPortalClaro = setSenhaPortalClaro;
    this.getSenhaPortalClaro = getSenhaPortalClaro;

    function setSenhaPortalClaro(_senhaPortalClaro) {
        this.senhaPortalClaro = _senhaPortalClaro;
    }

    function getSenhaPortalClaro() {
        return this.senhaPortalClaro;
    }

    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }

    this.setId = setId;
    this.getId = getId;
    this.setTipoPessoa = setTipoPessoa;
    this.getTipoPessoa = getTipoPessoa;
    this.setNome = setNome;
    this.getNome = getNome;
    this.setCpfCnpj = setCpfCnpj;
    this.getCpfCnpj = getCpfCnpj;
    this.setTelefone = setTelefone;
    this.getTelefone = getTelefone;
    this.setCelular = setCelular;
    this.getCelular = getCelular;
    this.setEmail = setEmail;
    this.getEmail = getEmail;
    this.setIe = setIe;
    this.getIe = getIe;
    this.setSexo = setSexo;
    this.getSexo = getSexo;
    this.setRg = setRg;
    this.getRg = getRg;
    this.setOrgaoEmissor = setOrgaoEmissor;
    this.getOrgaoEmissor = getOrgaoEmissor;
    this.setEstadoEmissor = setEstadoEmissor;
    this.getEstadoEmissor = getEstadoEmissor;
    this.setCep = setCep;
    this.getCep = getCep;
    this.setDataEmissao =  setDataEmissao;
    this.getDataEmissao = getDataEmissao;
    this.setNumero = setNumero;
    this.getNumero = getNumero;
    this.setRua = setRua;
    this.getRua = getRua;
    this.setComplemento = setComplemento;
    this.getComplemento = getComplemento;
    this.setBairro = setBairro;
    this.getBairro = getBairro;
    this.setNomeContato = setNomeContato;
    this.getNomeContato = getNomeContato;
    this.setCidade = setCidade;
    this.getCidade = getCidade;
    this.setEstado = setEstado;
    this.getEstado = getEstado;
    this.setEmailContato = setEmailContato;
    this.getEmailContato = getEmailContato;
    this.setTelefoneContato = setTelefoneContato;
    this.getTelefoneContato = getTelefoneContato;
    this.setCargoContato = setCargoContato;
    this.getCargoContato = getCargoContato;
    this.setCargoContato = setCargoContato;
    this.getCargoContato = getCargoContato;
    this.setAtivo = setAtivo;
    this.getAtivo = getAtivo;

    this.setObservacao = setObservacao;
    this.getObservacao = getObservacao;

    function setId(_id) {
        this.id = _id;
    }

    function getId() {
        return this.id;
    }

    function setTipoPessoa(_tipoPessoa) {
        this.tipoPessoa = _tipoPessoa
    }

    function getTipoPessoa() {
        return this.tipoPessoa;
    }

    function setNome(_nome) {
        this.nome = _nome;
    }

    function getNome() {
        return this.nome;
    }

    function setCpfCnpj(_cpfCnpj) {
        this.cpfCnpj = _cpfCnpj;
    }

    function getCpfCnpj() {
        return this.cpfCnpj;
    }

    function setTelefone(_telefone) {
        this.telefone = _telefone
    }

    function getTelefone() {
        return this.telefone;
    }

    function setCelular(_celular) {
        this.celular = _celular;
    }

    function getCelular() {
        return this.celular;
    }

    function setEmail(_email) {
        this.email = _email;
    }

    function getEmail() {
        return this.email;
    }

    function setIe(_ie) {
        this.ie = _ie;
    }

    function getIe() {
        return this.ie;
    }

    function setSexo(_sexo) {
        this.sexo = _sexo;
    }

    function getSexo() {
        return this.sexo;
    }

    function setRg(_rg) {
        this.rg = _rg;
    }

    function getRg() {
        return this.rg;
    }

    function setOrgaoEmissor(_orgaoEmissor) {
        this.orgaoEmissor = _orgaoEmissor;
    }

    function getOrgaoEmissor() {
        return this.orgaoEmissor;
    }

    function setEstadoEmissor(_estadoEmissor) {
        this.estadoemissor = _estadoEmissor;
    }

    function getEstadoEmissor() {
        return this.estadoemissor;
    }

    function setDataEmissao(_dataEmissao) {
        this.dataemissao = _dataEmissao;
    }

    function getDataEmissao() {
        return this.dataEmissao;
    }

    function setCep(_cep) {
        this.cep = _cep;
    }

    function getCep() {
        return this.cep;
    }

    function setRua(_rua) {
        this.rua = _rua;
    }

    function getRua() {
        return this.rua;
    }

    function setNumero(_numero) {
        this.numero = _numero;
    }

    function getNumero() {
        return this.numero;
    }

    function setComplemento(_complemento) {
        this.complemento = _complemento;
    }

    function getComplemento() {
        return this.complemento;
    }

    function setBairro(_bairro) {
        this.bairro = _bairro;
    }

    function getBairro() {
        return this.bairro;
    }

    function setCidade(_cidade) {
        this.cidade = _cidade;
    }

    function getCidade() {
        return this.cidade;
    }

    function setEstado(_estado) {
        this.estado = _estado;
    }

    function getEstado() {
        return this.estado;
    }

    function setNomeContato(_nomeContato) {
        this.nomeContato = _nomeContato;
    }

    function getNomeContato() {
        return this.nomeContato;
    }

    function setTelefoneContato(_telefoneContato) {
        this.telefoneContato = _telefoneContato;
    }

    function getTelefoneContato() {
        return this.telefoneContato;
    }

    function setEmailContato(_emailContato) {
        this.emailContato = _emailContato;
    }

    function getEmailContato() {
        return this.emailContato;
    }

    function setCargoContato(_cargoContato) {
        this.cargoContato = _cargoContato;
    }

    function getCargoContato() {
        return this.cargoContato;
    }

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    function setObservacao(_observacao) {
        this.observacao = _observacao;
    }

    function getObservacao() {
        return this.observacao;
    }

    var inscricaoMunicipal;

    this.setInscricaoMunicipal = setInscricaoMunicipal;
    this.getInscricaoMunicipal = getInscricaoMunicipal;

    function setInscricaoMunicipal(_inscricaoMunicipal) {
        this.inscricaoMunicipal = _inscricaoMunicipal;
    }

    function getInscricaoMunicipal() {
        return this.inscricaoMunicipal;
    }

    var cnaeprimario;

    this.setCnaeprimario = setCnaeprimario;
    this.getCnaeprimario = getCnaeprimario;

    function setCnaeprimario(_cnaeprimario) {
        this.cnaeprimario = _cnaeprimario;
    }

    function getCnaeprimario() {
        return this.cnaeprimario;
    }

    var cnaesecundario;

    this.setCnaesecundario = setCnaesecundario;
    this.getCnaesecundario = getCnaesecundario;

    function setCnaesecundario(_cnaesecundario) {
        this.cnaesecundario = _cnaesecundario;
    }

    function getCnaesecundario() {
        return this.cnaesecundario;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}