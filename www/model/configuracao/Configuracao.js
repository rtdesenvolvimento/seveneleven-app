function Configuracao() {

    var id;
    var descricao;
    var urlAcessoExterno;
    var alias;
    var sincronizar;
    var tempoSincronizacao;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    this.setUrlAcessoExterno = setUrlAcessoExterno;
    this.getUrlAcessoExterno = getUrlAcessoExterno;

    this.setAlias = setAlias;
    this.getAlias = getAlias;

    this.setSincronizar = setSincronizar;
    this.getSincronizar = getSincronizar;

    this.setTempoSincronizacao = setTempoSincronizacao;
    this.getTempoSincronizacao = getTempoSincronizacao;

    function setUrlAcessoExterno(_urlAcessoExterno) {
        this.urlAcessoExterno = _urlAcessoExterno;
    }

    function getUrlAcessoExterno() {
        return this.urlAcessoExterno;
    }

    function setAlias(_alias) {
        this.alias = _alias;
    }

    function getAlias() {
        return this.alias;
    }

    function setSincronizar(_sincronizar) {
        this.sincronizar = _sincronizar;
    }

    function setTempoSincronizacao(_tempoSincronizacao) {
        this.tempoSincronizacao = _tempoSincronizacao;
    }

    function getTempoSincronizacao() {
        return this.tempoSincronizacao;
    }

    function getSincronizar() {
        return this.sincronizar;
    }

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    var importacao;

    this.setImportacao = setImportacao;
    this.getImportacao = getImportacao;

    function setImportacao(_importacao) {
        this.importacao = _importacao;
    }

    function getImportacao() {
        return this.importacao;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

    var usuario;

    this.setUsuario = setUsuario;
    this.getUsuario = getUsuario;

    function setUsuario(_usuario) {
        this.usuario = _usuario;
    }

    function getUsuario() {
        return this.usuario;
    }
}