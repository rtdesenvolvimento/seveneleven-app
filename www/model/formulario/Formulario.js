function Formulario() {

    var id;
    var descricao;
    var ativo;

    this.getId = getId;
    this.setId = setId;
    this.setAtivo = setAtivo;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;
    this.getAtivo = getAtivo;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}