function PerguntaFormulario() {

    var id;
    var pergunta;
    var formulario;
    var obrigatorio;
    var peso;
    var descricao;
    var tipoCampo;
    var ativo;
    var anterior;
    var proxima;
    var alias;
    var pagina;
    var ordem;

    this.getId = getId;
    this.setId = setId;
    this.setAnterior = setAnterior;
    this.getAnterior = getAnterior;
    this.setProxima = setProxima;
    this.getProxima = getProxima;
    this.setTipoCampo = setTipoCampo;
    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;
    this.getAtivo = getAtivo;
    this.setAtivo = setAtivo;
    this.getPergunta = getPergunta;
    this.getTipoCampo = getTipoCampo;
    this.setPergunta = setPergunta;
    this.getPergunta = getPergunta;
    this.setFormulario = setFormulario;
    this.getFormulario = getFormulario;
    this.setObrigatorio = setObrigatorio;
    this.getObrigatorio = getObrigatorio;
    this.setPeso = setPeso;
    this.getPeso = getPeso;

    this.setAlias = setAlias;
    this.getAlias = getAlias;

    this.setPagina = setPagina;
    this.getPagina = getPagina;

    this.setOrdem = setOrdem;
    this.getOrdem = getOrdem;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function setAnterior(_anterior) {
        this.anterior = _anterior;
    }

    function getAnterior() {
        return this.anterior;
    }

    function setProxima(_proxima) {
        this.proxima = _proxima;
    }

    function getProxima() {
        return this.proxima;
    }

    function setObrigatorio(_obrigatorio) {
        this.obrigatorio = _obrigatorio;
    }

    function getObrigatorio() {
        return this.obrigatorio;
    }

    function setPeso(_peso) {
        this.peso = _peso;
    }

    function getPeso() {
        return this.peso;
    }

    function setFormulario(_formulario) {
        this.formulario = _formulario;
    }

    function getFormulario() {
        return this.formulario;
    }

    function setPergunta(_pergunta) {
        this.pergunta = _pergunta;
    }

    function getPergunta() {
        return this.pergunta;
    }

    function setTipoCampo(_tipoCampo) {
        this.tipoCampo = _tipoCampo;
    }

    function getTipoCampo() {
        return this.tipoCampo;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    function setAlias(_alias) {
        this.alias = _alias;
    }

    function getAlias() {
        return this.alias;
    }

    function setPagina(_pagina) {
        this.pagina = _pagina;
    }

    function getPagina() {
        return this.pagina;
    }

    function setOrdem(_ordem) {
        this.ordem = _ordem;
    }

    function getOrdem() {
        return this.ordem;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}