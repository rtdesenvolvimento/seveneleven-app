function Franquia() {

    var id;

    var plano;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }

    var franquia;

    this.setFranquia = setFranquia;
    this.getFranquia = getFranquia;

    function setFranquia(_franquia) {
        this.franquia = _franquia;
    }

    function getFranquia() {
        return this.franquia;
    }

    var bonus;

    this.setBonus = setBonus;
    this.getBonus = getBonus;

    function setBonus(_bonus) {
        this.bonus = _bonus;
    }

    function getBonus() {
        return this.bonus;
    }

    var pontos;

    this.setPontos = setPontos;
    this.getPontos = getPontos;

    function setPontos(_pontos) {
        this.pontos = _pontos;
    }

    function getPontos() {
        return this.pontos;
    }


    var totalInternet;

    this.setTotalInternet = setTotalInternet;
    this.getTotalInternet = getTotalInternet;

    function setTotalInternet(_totalInternet) {
        this.totalInternet = _totalInternet;
    }

    function getTotalInternet() {
        return this.totalInternet;
    }

    var valor;

    this.setValor = setValor;
    this.getValor = getValor;

    function setValor(_valor) {
        this.valor = _valor;
    }

    function getValor() {
        return this.valor;
    }

    var isPossuiMobilidade;

    this.setIsPossuiMobilidade = setIsPossuiMobilidade;
    this.getIsPossuiMobilidade = getIsPossuiMobilidade;

    function setIsPossuiMobilidade(_isPossuiMobilidade) {
        this.isPossuiMobilidade = _isPossuiMobilidade;
    }

    function getIsPossuiMobilidade() {
        return this.isPossuiMobilidade;
    }

    var valorMobilidade;

    this.setValorMobilidade = setValorMobilidade;
    this.getValorMobilidade = getValorMobilidade;

    function setValorMobilidade(_valorMobilidade) {
        this.valorMobilidade = _valorMobilidade;
    }

    function getValorMobilidade() {
        return this.valorMobilidade;
    }

    var isPossuiRedesSociais;

    this.setIsPossuiRedesSociais = setIsPossuiRedesSociais;
    this.getIsPossuiRedesSociais = getIsPossuiRedesSociais;

    function setIsPossuiRedesSociais(_isPossuiRedesSociais) {
        this.isPossuiRedesSociais = _isPossuiRedesSociais;
    }

    function getIsPossuiRedesSociais() {
        return this.isPossuiRedesSociais;
    }

    var valorRedesSociais;

    this.setValorRedesSociais = setValorRedesSociais;
    this.getValorRedesSociais = getValorRedesSociais;

    function setValorRedesSociais(_valorRedesSociais) {
        this.valorRedesSociais = _valorRedesSociais;
    }

    function getValorRedesSociais() {
        return this.valorRedesSociais;
    }

    var passaporte;

    this.setPassaporte = setPassaporte;
    this.getPassaporte = getPassaporte;

    function setPassaporte(_passaporte) {
        this.passaporte = _passaporte;
    }

    function getPassaporte() {
        return this.passaporte;
    }

    var valorPassaporte;

    this.setValorPassaporte = setValorPassaporte;
    this.getValorPassaporte = getValorPassaporte;

    function setValorPassaporte(_valorPassaporte) {
        this.valorPassaporte = _valorPassaporte;
    }

    function getValorPassaporte() {
        return this.valorPassaporte;
    }

    var ativo;

    this.setAtivo = setAtivo;
    this.getAtivo = getAtivo;

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    this.getId = getId;
    this.setId = setId;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var maximoLinhaSemGol;

    this.setMaximoLinhaSemGol = setMaximoLinhaSemGol;
    this.getMaximoLinhaSemGol = getMaximoLinhaSemGol;

    function setMaximoLinhaSemGol(_maximoLinhaSemGol) {
        this.maximoLinhaSemGol = _maximoLinhaSemGol;
    }

    function getMaximoLinhaSemGol() {
        return this.maximoLinhaSemGol;
    }

    var maximoLinhaComGol;

    this.setMaximoLinhaComGol = setMaximoLinhaComGol;
    this.getMaximoLinhaComGol = getMaximoLinhaComGol;

    function setMaximoLinhaComGol(_maximoLinhaComGol) {
        this.maximoLinhaComGol = _maximoLinhaComGol;
    }

    function getMaximoLinhaComGol() {
        return this.maximoLinhaComGol;
    }

    var minimoLinha;

    this.setMinimoLinha = setMinimoLinha;
    this.getMinimoLinha = getMinimoLinha;

    function setMinimoLinha(_maximoLinha) {
        this.minimoLinha = _maximoLinha;
    }

    function getMinimoLinha() {
        return this.minimoLinha;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

}