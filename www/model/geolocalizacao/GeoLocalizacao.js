function GeoLocalizacao() {

    var id;
    var proposta;
    var latitude;
    var longitude;
    var altitude;

    this.setId = setId;
    this.getId = getId;

    this.setProposta = setProposta;
    this.getProposta = getProposta;

    this.setLatitude = setLatitude;
    this.getLatitude = getLatitude;

    this.setLongitude = setLongitude;
    this.getLongitude = getLongitude;

    this.setAltitude = setAltitude;
    this.getAltitude = getAltitude;

    function setId(_id) {
        this.id = _id;
    }

    function getId() {
        return this.id;
    }

    function setProposta(_propopsta) {
        this.proposta = _propopsta;
    }

    function getProposta() {
        return this.proposta;
    }

    function setLatitude(_latitude) {
        this.latitude = _latitude;
    }

    function getLatitude() {
        return this.latitude;
    }

    function setLongitude(_longitude) {
        this.longitude = _longitude;
    }

    function getLongitude() {
        return this.longitude;
    }

    function setAltitude(_altitude) {
        this.altitude = _altitude;
    }

    function getAltitude() {
        return this.altitude;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
};