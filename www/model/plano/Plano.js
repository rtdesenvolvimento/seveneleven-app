function Plano() {

    var id;
    var descricao;
    var ativo;
    var tipo;
    var tipoPlano;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.setTipoPlano = setTipoPlano;
    this.getTipoPlano = getTipoPlano;

    function setTipoPlano(_tipoPlano) {
        this.tipoPlano = _tipoPlano;
    }

    function getTipoPlano() {
        return this.tipoPlano;
    }

    this.setTipo = setTipo;
    this.getTipo = getTipo;

    function setTipo(_tipo) {
        this.tipo = _tipo;
    }

    function getTipo() {
        return this.tipo;
    }

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    this.setAtivo = setAtivo;
    this.getAtivo = getAtivo;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }
    function getAtivo() {
        return this.ativo;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}