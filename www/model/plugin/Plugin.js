function Plugin() {

    var id;
    var descricao;
    var valor;

    this.setValor = setValor;
    this.getValor = getValor;

    function setValor(_valor) {
        this.valor = _valor;
    }

    function getValor() {
        return this.valor;
    }
    var ativo;
    this.setAtivo = setAtivo;
    this.getAtivo = getAtivo;

    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    function getAtivo() {
        return this.ativo;
    }

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}