function Proposta() {

    var id;
    var descricao;
    var status;
    var formulario;
    var cliente;
    var observacao;
    var dataProposta;
    var latitude;
    var longitude;
    var altitude;

    var dataAberta;
    var dataAguardando;
    var dataFechamento;
    var visitaConfirmada;
    var responsavel;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var valorAparelho;

    this.setValorAparelho = setValorAparelho;
    this.getValorAparelho = getValorAparelho;


    var tipo;

    this.setTipo = setTipo;
    this.getTipo = getTipo;

    function setTipo(_tipo) {
        this.tipo = _tipo;
    }

    function getTipo() {
        return this.tipo;
    }

    function setValorAparelho(_valorAparelho) {
        this.valorAparelho = _valorAparelho;
    }

    function getValorAparelho() {
        return this.valorAparelho;
    }

    var operadoraAtual;

    this.setOperadoraAtual = setOperadoraAtual;
    this.getOperadoraAtual = getOperadoraAtual;

    function setOperadoraAtual(_operadoraAtual) {
        this.operadoraAtual = _operadoraAtual;
    }

    function getOperadoraAtual() {
        return this.operadoraAtual;
    }

    var nomeEmpresa;


    this.setNomeEmpresa = setNomeEmpresa;
    this.getNomeEmpresa = getNomeEmpresa;

    function setNomeEmpresa(_nomeEmpresa) {
        this.nomeEmpresa = _nomeEmpresa;
    }

    function getNomeEmpresa() {
        return this.nomeEmpresa;
    }

    var telefone;

    this.setTelefone = setTelefone;
    this.getTelefone = getTelefone;

    function setTelefone(_telefone) {
        this.telefone = _telefone;
    }

    function getTelefone() {
        return this.telefone;
    }

    var email;

    this.setEmail = setEmail;
    this.getEmail = getEmail;

    function setEmail(_email) {
        this.email = _email;
    }

    function getEmail() {
        return this.email;
    }

    var meioDivulgacao;

    this.setMeioDivulgacao = setMeioDivulgacao;
    this.getMeioDivulgacao = getMeioDivulgacao;

    function setMeioDivulgacao(_meioDivulgacao) {
        this.meioDivulgacao = _meioDivulgacao;
    }

    function getMeioDivulgacao() {
        return this.meioDivulgacao;
    }

    var nomeContato;

    this.setNomeContato = setNomeContato;
    this.getNomeContato = getNomeContato;

    function setNomeContato(_nomeContato) {
        this.nomeContato = _nomeContato;
    }

    function getNomeContato() {
        return this.nomeContato;
    }

    var gastoAtual;

    this.setGastoAtual = setGastoAtual;
    this.getGastoAtual = getGastoAtual;

    function setGastoAtual(_gastoAtual) {
        this.gastoAtual = _gastoAtual;
    }

    function getGastoAtual() {
        return this.gastoAtual;
    }

    var dataInicioContrato;

    this.setDataInicioContrato = setDataInicioContrato;
    this.getDataInicioContrato = getDataInicioContrato;

    function setDataInicioContrato(_dataInicioContrato) {
        this.dataInicioContrato = _dataInicioContrato;
    }

    function getDataInicioContrato() {
        return this.dataInicioContrato;
    }

    var dataTerminoContrato;

    this.setDataTerminoContrato = setDataTerminoContrato;
    this.getDataTerminoContrato = getDataTerminoContrato;

    function setDataTerminoContrato(_dataTerminoContrato) {
        this.dataTerminoContrato = _dataTerminoContrato;
    }

    function getDataTerminoContrato() {
        return this.dataTerminoContrato;
    }


    var classificacaoCliente;


    this.setClassificacaoCliente = setClassificacaoCliente;
    this.getClassificacaoCliente = getClassificacaoCliente;

    function setClassificacaoCliente(_classificacaoCliente) {
        this.classificacaoCliente = _classificacaoCliente;
    }

    function getClassificacaoCliente() {
        return this.classificacaoCliente;
    }


    var aptoRenovar;

    this.setAptoRenovar = setAptoRenovar;
    this.getAptoRenovar = getAptoRenovar;

    function setAptoRenovar(_aptoRenovar) {
        this.aptoRenovar = _aptoRenovar;
    }

    function getAptoRenovar() {
        return this.aptoRenovar;
    }

    var valorPlano;

    this.setValorPlano = setValorPlano;
    this.getValorPlano = getValorPlano;

    function setValorPlano(_valorPlano) {
        this.valorPlano = _valorPlano;
    }

    function getValorPlano() {
        return this.valorPlano;
    }

    var qtdLinhas;

    this.setQtdLinhas = setQtdLinhas;
    this.getQtdLinhas = getQtdLinhas;

    function setQtdLinhas(_qtdLinhas) {
        this.qtdLinhas = _qtdLinhas;
    }

    function getQtdLinhas() {
        return this.qtdLinhas;
    }

    var tempoVigenciaContrato;

    this.setTempoVigenciaContrato = setTempoVigenciaContrato;
    this.getTempoVigenciaContrato = getTempoVigenciaContrato;

    function setTempoVigenciaContrato(_tempoVigenciaContrato) {
        this.tempoVigenciaContrato = _tempoVigenciaContrato;
    }

    function getTempoVigenciaContrato() {
        return this.tempoVigenciaContrato;
    }

    var dataVencimento;

    this.setDataVencimento = setDataVencimento;
    this.getDataVencimento = getDataVencimento;

    function setDataVencimento(_dataVencimento) {
        this.dataVencimento = _dataVencimento;
    }

    function getDataVencimento() {
        return this.dataVencimento;
    }

    var possuiServicoEmbratel;

    this.setPossuiServicoEmbratel = setPossuiServicoEmbratel;
    this.getPossuiServicoEmbratel = getPossuiServicoEmbratel;

    function setPossuiServicoEmbratel(_possuiServicoEmbratel) {
        this.possuiServicoEmbratel = _possuiServicoEmbratel;
    }

    function getPossuiServicoEmbratel() {
        return this.possuiServicoEmbratel;
    }


    var possuiServicoNet;

    this.setPossuiServicoNet = setPossuiServicoNet;
    this.getPossuiServicoNet = getPossuiServicoNet;

    function setPossuiServicoNet(_possuiServicoNet) {
        this.possuiServicoNet = _possuiServicoNet;
    }

    function getPossuiServicoNet() {
        return this.possuiServicoNet;
    }

    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }

    this.setVisitaConfirmada = setVisitaConfirmada;
    this.getVisitaConfirmada = getVisitaConfirmada;

    function setVisitaConfirmada(_visitaConfirmada) {
        this.visitaConfirmada = _visitaConfirmada;
    }

    function getVisitaConfirmada() {
        return this.visitaConfirmada;
    }

    this.setDataAberta = setDataAberta;
    this.getDataAberta = getDataAberta;

    this.setDataAguardando = setDataAguardando;
    this.getDataAguardando = getDataAguardando;

    this.setDataFechamento = setDataFechamento;
    this.getDataFechamento = getDataFechamento;

    function setDataAberta(_dataAberta) {
        this.dataAberta = _dataAberta;
    }

    function getDataAberta() {
        return this.dataAberta;
    }

    function setDataAguardando(_dataAguardando) {
        this.dataAguardando = _dataAguardando;
    }

    function getDataAguardando() {
        return this.dataAguardando;
    }

    function setDataFechamento(_dataFechamento) {
        this.dataFechamento = _dataFechamento;
    }

    function getDataFechamento() {
        return this.dataFechamento;
    }

    this.setId = setId;
    this.getId = getId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    this.setStatus = setStatus;
    this.getStatus = getStatus;

    this.setFormulario = setFormulario;
    this.getFormulario = getFormulario;

    this.setCliente = setCliente;
    this.getCliente = getCliente;

    this.setObservacao = setObservacao;
    this.getObservacao = getObservacao;

    this.setDataProposta = setDataProposta;
    this.getDataProposta = getDataProposta;

    this.setLatitude = setLatitude;
    this.getLatitude = getLatitude;

    this.setLongitude = setLongitude;
    this.getLongitude = getLongitude;

    this.setAltitude = setAltitude;
    this.getAltitude = getAltitude;

    function setId(_id) {
        this.id = _id;
    }

    function getId() {
        return this.id;
    }

    function setDescricao(_descricao) {
        this.descricao = _descricao;
    }

    function getDescricao() {
        return this.descricao;
    }

    function setStatus(_status) {
        this.status = _status;
    }

    function getStatus() {
        return this.status;
    }

    function setFormulario(_formulario) {
        this.formulario = _formulario;
    }

    function getFormulario() {
        return this.formulario;
    }

    function setCliente(_cliente) {
        this.cliente = _cliente;
    }

    function getCliente() {
        return this.cliente;
    }

    function setObservacao(_observacao) {
        this.observacao = _observacao;
    }

    function getObservacao() {
        return this.observacao;
    }

    function setDataProposta(_dataProposta) {
        this.dataProposta = _dataProposta;
    }

    function getDataProposta() {
        return this.dataProposta;
    }

    function setLatitude(_latitude) {
        this.latitude = _latitude;
    }

    function getLatitude() {
        return this.latitude;
    }

    function setLongitude(_longitude) {
        this.longitude = _longitude;
    }

    function getLongitude() {
        return this.longitude;
    }

    function setAltitude(_altitude) {
        this.altitude = _altitude;
    }

    function getAltitude() {
        return this.altitude;
    }

    var backupOnline;

    this.setBackupOnline = setBackupOnline;
    this.getBackupOnline = getBackupOnline;

    function setBackupOnline(_backupOnline) {
        this.backupOnline = _backupOnline;
    }

    function getBackupOnline() {
        return this.backupOnline;
    }

    var identificacaoClienteNet;

    this.setIdentificacaoClienteNet = setIdentificacaoClienteNet;
    this.getIdentificacaoClienteNet = getIdentificacaoClienteNet;

    function setIdentificacaoClienteNet(_identificacaoClienteNet) {
        this.identificacaoClienteNet = _identificacaoClienteNet;
    }

    function getIdentificacaoClienteNet() {
        return this.identificacaoClienteNet;
    }


    var dataApresentacao;

    this.setDataApresentacao = setDataApresentacao;
    this.getDataApresentacao = getDataApresentacao;

    function setDataApresentacao(_dataApresentacao) {
        this.dataApresentacao = _dataApresentacao;
    }

    function getDataApresentacao() {
        return this.dataApresentacao;
    }

    var dataEnvioProposta;

    this.setDataEnvioProposta = setDataEnvioProposta;
    this.getDataEnvioProposta = getDataEnvioProposta;

    function setDataEnvioProposta(_dataEnvioProposta) {
        this.dataEnvioProposta = _dataEnvioProposta;
    }

    function getDataEnvioProposta() {
        return this.dataEnvioProposta;
    }

    var dataRetornoVisita;

    this.setDataRetornoVisita = setDataRetornoVisita;
    this.getDataRetornoVisita = getDataRetornoVisita;

    function setDataRetornoVisita(_dataRetornoVistia) {
        this.dataRetornoVisita = _dataRetornoVistia;
    }

    function getDataRetornoVisita() {
        return this.dataRetornoVisita;
    }

    var dataAguardandoDocumento;

    this.setDataAguardandoDocumento = setDataAguardandoDocumento;
    this.getDataAguardandoDocumento = getDataAguardandoDocumento;

    function setDataAguardandoDocumento(_dataAguardandoDocumento) {
        this.dataAguardandoDocumento = _dataAguardandoDocumento;
    }

    function getDataAguardandoDocumento() {
        return this.dataAguardandoDocumento;
    }

    var dataAguardandoAssinatura;

    this.setDataAguardandoAssinatura = setDataAguardandoAssinatura;
    this.getDataAguardandoAssinatura = getDataAguardandoAssinatura;

    function setDataAguardandoAssinatura(_dataAguardandoAssinatura) {
        this.dataAguardandoAssinatura = _dataAguardandoAssinatura;
    }

    function getDataAguardandoAssinatura() {
        return this.dataAguardandoAssinatura;
    }

    var isPedido;

    this.setIsPedido = setIsPedido;
    this.getIsPedido = getIsPedido;

    function setIsPedido(_isPedido) {
        this.isPedido = _isPedido;
    }

    function getIsPedido() {
        return this.isPedido;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

    var tipoVenda;

    this.setTipoVenda = setTipoVenda;
    this.getTipoVenda = getTipoVenda;

    function setTipoVenda(_tipoVenda) {
        this.tipoVenda = _tipoVenda;
    }

    function getTipoVenda() {
        return this.tipoVenda;
    }

    var cnpjProposta;

    this.setCnpjProposta = setCnpjProposta;
    this.getCnpjProposta = getCnpjProposta;

    function setCnpjProposta(_cnpjProposta) {
        this.cnpjProposta = _cnpjProposta;
    }

    function getCnpjProposta() {
        return this.cnpjProposta;
    }

    var isSmsVontade;

    this.setIsSmsVontade = setIsSmsVontade;
    this.getIsSmsVontade = getIsSmsVontade;

    function setIsSmsVontade(_setIsSmsVontade) {
        this.setIsSmsVontade = _setIsSmsVontade;
    }

    function getIsSmsVontade() {
        return this.setIsSmsVontade;
    }

    var isAutorizoContabilidade;

    this.setIsAutorizoContabilidade = setIsAutorizoContabilidade;
    this.getIsAutorizoContabilidade = getIsAutorizoContabilidade;

    function setIsAutorizoContabilidade(_isAutorizoContabilidade) {
        this.isAutorizoContabilidade = _isAutorizoContabilidade;
    }

    function getIsAutorizoContabilidade() {
        return this.isAutorizoContabilidade;
    }

    var telefoneContabilidade;

    this.setTelefoneContabilidade = setTelefoneContabilidade;
    this.getTelefoneContabilidade = getTelefoneContabilidade;

    function setTelefoneContabilidade(_telefoneContabilidade) {
        this.telefoneContabilidade = _telefoneContabilidade;
    }

    function getTelefoneContabilidade() {
        return this.telefoneContabilidade;
    }

    var isWhatsAppVontade;

    this.setIsWhatsAppVontade = setIsWhatsAppVontade;
    this.getIsWhatsAppVontade = getIsWhatsAppVontade;

    function setIsWhatsAppVontade(_isWhatsAppVontade) {
        this.isWhatsAppVontade = _isWhatsAppVontade;
    }

    function getIsWhatsAppVontade() {
        return this.isWhatsAppVontade;
    }

    var isMinutosIlimitados;

    this.setIsMinutosIlimitados = setIsMinutosIlimitados;
    this.getIsMinutosIlimitados = getIsMinutosIlimitados;

    function setIsMinutosIlimitados(_isMinutosIlimitados) {
        this.isMinutosIlimitados = _isMinutosIlimitados;
    }

    function getIsMinutosIlimitados() {
        return this.isMinutosIlimitados;
    }

    var observacaoAparelhos;

    this.setObservacaoAparelhos = setObservacaoAparelhos;
    this.getObservacaoAparelhos = getObservacaoAparelhos;

    function setObservacaoAparelhos(_observacaoAparelhos) {
        this.observacaoAparelhos = _observacaoAparelhos;
    }

    function getObservacaoAparelhos() {
        return this.observacaoAparelhos;
    }
}