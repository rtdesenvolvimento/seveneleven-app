
function PropostaAparelho () {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var tipoCobranca;

    this.setTipoCobranca = setTipoCobranca;
    this.getTipoCobranca = getTipoCobranca;

    function setTipoCobranca(_tipoCobranca) {
        this.tipoCobranca = _tipoCobranca;
    }

    function getTipoCobranca() {
        return this.tipoCobranca;
    }

    var plano;

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }


    var proposta;

    this.setProposta = setProposta;
    this.getProposta = getProposta;

    function setProposta(_proposta) {
        this.proposta = _proposta;
    }

    function getProposta() {
        return this.proposta;
    }

    var uf;

    this.setUf = setUf;
    this.getUf = getUf;

    function setUf(_uf) {
        this.uf = _uf;
    }

    function getUf() {
        return this.uf;
    }

    var ddd;

    this.setDdd = setDdd;
    this.getDdd = getDdd;

    function setDdd(_ddd) {
        this.ddd = _ddd;
    }

    function getDdd() {
        return this.ddd;
    }

    var aparelho;

    this.setAparelho = setAparelho;
    this.getAparelho = getAparelho;

    function setAparelho(_aparelho) {
        this.aparelho =  _aparelho;
    }

    function getAparelho() {
        return this.aparelho;
    }

    var cor;

    this.setCor = setCor;
    this.getCor = getCor;

    function setCor(_cor) {
        this.cor = _cor;
    }

    function getCor() {
        return this.cor;
    }

    var fabricante;

    this.setFabricante = setFabricante;
    this.getFabricante = getFabricante;

    function setFabricante(_fabricante) {
        this.fabricante = _fabricante;
    }

    function getFabricante() {
        return this.fabricante;
    }

    var simCard;

    this.setSimCard = setSimCard;
    this.getSimCard = getSimCard;

    function setSimCard(_simCard) {
        this.simCard = _simCard;
    }

    function getSimCard() {
        return this.SimCard;
    }

    var isSimCardComAparelho;

    this.setIsSimCardComAparelho = setIsSimCardComAparelho;
    this.getIsSimCardComAparelho = getIsSimCardComAparelho;

    function setIsSimCardComAparelho(_isSimCardComAparelho) {
        return this.isSimCardComAparelho = _isSimCardComAparelho;
    }

    function getIsSimCardComAparelho() {
        return this.isSimCardComAparelho;
    }

    var qtdSimCard;

    this.setQtdSimCard = setQtdSimCard;
    this.getQtdSimCard = getQtdSimCard;

    function setQtdSimCard(_qtdSimCard) {
        this.qtdSimCard = _qtdSimCard;
    }

    function getQtdSimCard() {
        return this.qtdSimCard;
    }

    var precoUnitarioSimCard;

    this.setPrecoUnitarioSimCard = setPrecoUnitarioSimCard;
    this.getPrecoUnitarioSimCard = getPrecoUnitarioSimCard;

    function setPrecoUnitarioSimCard(_precoUnitarioSimCard) {
        this.precoUnitarioSimCard = _precoUnitarioSimCard;
    }

    function getPrecoUnitarioSimCard() {
        return this.precoUnitarioSimCard;
    }


    var qtdAparelhos;

    this.setQtdAparelhos = setQtdAparelhos;
    this.getQtdAparelhos = getQtdAparelhos;

    function setQtdAparelhos(_qtdAparelhos) {
        this.qtdAparelhos = _qtdAparelhos;
    }

    function getQtdAparelhos() {
        return this.qtdAparelhos;
    }

    var precoUnitarioAparelho;

    this.setPrecoUnitarioAparelho = setPrecoUnitarioAparelho;
    this.getPrecoUnitarioAparelho = getPrecoUnitarioAparelho;

    function setPrecoUnitarioAparelho(_precoUnitarioAparelho) {
        this.precoUnitarioAparelho =_precoUnitarioAparelho;
    }

    function getPrecoUnitarioAparelho() {
        return this.precoUnitarioAparelho;
    }

    var pontuacao;

    this.setPontuacao = setPontuacao;
    this.getPontuacao = getPontuacao;

    function setPontuacao(_pontuacao) {
        this.pontuacao = _pontuacao;
    }

    function getPontuacao() {
        return this.pontuacao;
    }

    var franquia;

    this.setFranquia = setFranquia;
    this.getFranquia = getFranquia;

    function setFranquia(_franquia) {
        this.franquia = _franquia;
    }

    function getFranquia() {
        return this.franquia;
    }

    var isAvulso;

    this.setIsAvulso = setIsAvulso;
    this.getIsAvulso = getIsAvulso;

    function setIsAvulso(_isAvulso) {
        this.isAvulso = _isAvulso;
    }

    function getIsAvulso() {
        return this.isAvulso;
    }

    var formaPagamento;

    this.setFormaPagamento = setFormaPagamento;
    this.getFormaPagamento = getFormaPagamento;

    function setFormaPagamento(_formaPagamento) {
        this.formaPagamento = _formaPagamento;
    }

    function getFormaPagamento() {
        return this.formaPagamento;
    }

    var total;

    this.setTotal = setTotal;
    this.getTotal = getTotal;

    function setTotal(_total) {
        this.total = _total;
    }

    function getTotal() {
        return this.total;
    }

    var id;

    this.getId = getId;
    this.setId = setId;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

    var tipoPlanoAparelho;

    this.setTipoPlanoAparelho = setTipoPlanoAparelho;
    this.getTipoPlanoAparelho = getTipoPlanoAparelho;

    function setTipoPlanoAparelho(_tipoPlanoAparelho) {
        this.tipoPlanoAparelho = _tipoPlanoAparelho;
    }

    function getTipoPlanoAparelho() {
        return this.tipoPlanoAparelho;
    }

    var operadoraInterna;

    this.setOperadoraInterna = setOperadoraInterna;
    this.getOperadoraInterna = getOperadoraInterna;

    function setOperadoraInterna(_operadoraInterna) {
        this.operadoraInterna = _operadoraInterna;
    }

    function getOperadoraInterna() {
        return this.operadoraInterna;
    }

}