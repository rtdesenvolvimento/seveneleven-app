function PropostaEvento() {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var id;
    this.getId = getId;
    this.setId = setId;
    function getId(){return id;}
    function setId(_id) {id = _id;}

    var proposta;
    this.setProposta = setProposta;
    this.getProposta = getProposta;
    function setProposta(_proposta) {this.proposta = _proposta;}
    function getProposta() {return this.proposta;}


    var data;

    this.setData = setData;
    this.getData = getData;

    function setData(_data) {
        this.data = _data;
    }

    function getData() {
        return this.data;
    }


    var hora;

    this.setHora = setHora;
    this.getHora = getHora;

    function setHora(_hora) {
        this.hora = _hora;
    }

    function getHora() {
        return this.hora;
    }

    var evento;

    this.setEvento = setEvento;
    this.getEvento = getEvento;

    function setEvento(_evento) {
        this.evento = _evento;
    }

    function getEvento() {
        return this.evento;
    }


    var lido;

    this.setLido = setLido;
    this.getLido = getLido;

    function setLido(_lido) {
        this.lido = _lido;
    }

    function getLido() {
        return this.lido;
    }

    var dataLido;

    this.setDataLido = setDataLido;
    this.getDataLido = getDataLido;

    function setDataLido(_dataLido) {
        this.dataLido = _dataLido;
    }

    function getDataLido() {
        return this.dataLido;
    }

    var horaLido;

    this.setHoraLido = setHoraLido;
    this.getHoraLido = getHoraLido;

    function setHoraLido(_horaLido) {
        this.horaLido = _horaLido;
    }

    function getHoraLido() {
        return this.horaLido;
    }


    var resposta;

    this.setResposta = setResposta;
    this.getResposta = getResposta;

    function setResposta(_resposta) {
        this.resposta = _resposta;
    }

    function getResposta() {
        return this.resposta;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}