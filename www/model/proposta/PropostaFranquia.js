function PropostaFranquia() {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var proposta;

    this.setProposta = setProposta;
    this.getProposta = getProposta;

    function setProposta(_proposta) {
        this.proposta = _proposta;
    }

    function getProposta() {
        return this.proposta;
    }

    var strFranquia;

    this.setStrFranquia = setStrFranquia;
    this.getStrFranquia = getStrFranquia;

    function setStrFranquia(_strFranquia) {
        this.strFranquia = _strFranquia;
    }

    function getStrFranquia() {
        return this.strFranquia;
    }

    var codAnatel;

    this.setCodAnatel = setCodAnatel;
    this.getCodAnatel = getCodAnatel;

    function setCodAnatel(_codAnatel) {
        this.codAnatel = _codAnatel;
    }

    function getCodAnatel() {
        return this.codAnatel;
    }

    var plano;

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }

    var regiao;

    this.setRegiao = setRegiao;
    this.getRegiao = getRegiao;

    function setRegiao(_regiao) {
        this.regiao = _regiao;
    }

    function getRegiao() {
        return this.regiao;
    }

    var operadoraPortabilidade;

    this.setOperadoraPortabilidade = setOperadoraPortabilidade;
    this.getOperadoraPortabilidade = getOperadoraPortabilidade;

    function setOperadoraPortabilidade(_operadoraPortabilidade) {
        this.operadoraPortabilidade = _operadoraPortabilidade;
    }

    function getOperadoraPortabilidade() {
        return this.operadoraPortabilidade;
    }

    var isPortabilidade;

    this.setIsPortabilidade = setIsPortabilidade;
    this.getIsPortabilidade = getIsPortabilidade;

    function setIsPortabilidade(_isPortabilidade) {
        this.isPortabilidade = _isPortabilidade;
    }

    function getIsPortabilidade() {
        return this.isPortabilidade;
    }

    var ddd;

    this.setDdd = setDdd;
    this.getDdd = getDdd;

    function setDdd(_ddd) {
        this.ddd  = _ddd;
    }

    function getDdd() {
        return this.ddd;
    }

    var telefonePortabilidade;

    this.setTelefonePortabilidade = setTelefonePortabilidade;
    this.getTelefonePortabilidade = getTelefonePortabilidade;


    function setTelefonePortabilidade(_telefonePortabilidade) {
        this.telefonePortabilidade = _telefonePortabilidade;
    }

    function getTelefonePortabilidade() {
        return this.telefonePortabilidade;
    }

    var tipoSolicitacaoProposta;


    this.setTipoSolicitacaoProposta = setTipoSolicitacaoProposta;
    this.getTipoSolicitacaoProposta = getTipoSolicitacaoProposta;

    function setTipoSolicitacaoProposta(_tipoSolicitacaoProposta) {
        this.tipoSolicitacaoProposta = _tipoSolicitacaoProposta;
    }

    function getTipoSolicitacaoProposta() {
        return this.tipoSolicitacaoProposta;
    }

    var franquia;

    this.setFranquia = setFranquia;
    this.getFranquia = getFranquia;

    function setFranquia(_franquia) {
        this.franquia = _franquia;
    }

    function getFranquia() {
        return this.franquia;
    }

    var valorFranquia;

    this.setValorFranquia = setValorFranquia;
    this.getValorFranquia = getValorFranquia;

    function setValorFranquia(_valorFranquia) {
        this.valorFranquia = _valorFranquia;
    }

    function getValorFranquia() {
        return this.valorFranquia;
    }

    var bonus;

    this.setBonus = setBonus;
    this.getBonus = getBonus;

    function setBonus(_bonus) {
        this.bonus = _bonus;
    }

    function getBonus() {
        return this.bonus;
    }

    var qtdLinhas;

    this.setQtdLinhas = setQtdLinhas;
    this.getQtdLinhas = getQtdLinhas;

    function setQtdLinhas(_qtdLinhas) {
        this.qtdLinhas = _qtdLinhas;
    }

    function getQtdLinhas() {
        return this.qtdLinhas;
    }

    var isPossuiMobilidade;

    this.setIsPossuiMobilidade = setIsPossuiMobilidade;
    this.getIsPossuiMobilidade = getIsPossuiMobilidade;

    function setIsPossuiMobilidade(_isPossuiMobilidade) {
        this.isPossuiMobilidade = _isPossuiMobilidade;
    }

    function getIsPossuiMobilidade() {
        return this.isPossuiMobilidade;
    }

    var valorMobilidade;

    this.setValorMobilidade = setValorMobilidade;
    this.getValorMobilidade = getValorMobilidade;

    function setValorMobilidade(_valorMobilidade) {
        this.valorMobilidade = _valorMobilidade;
    }

    function getValorMobilidade() {
        return this.valorMobilidade;
    }

    var isPossuiRedesSociais;

    this.setIsPossuiRedesSociais = setIsPossuiRedesSociais;
    this.getIsPossuiRedesSociais = getIsPossuiRedesSociais;

    function setIsPossuiRedesSociais(_possuiRedesSociais) {
        this.isPossuiRedesSociais = _possuiRedesSociais;
    }

    function getIsPossuiRedesSociais() {
        return this.isPossuiRedesSociais;
    }

    var valorRedesSociais;

    this.setValorRedesSociais = setValorRedesSociais;
    this.getValorRedesSociais = getValorRedesSociais;

    function setValorRedesSociais(_valorRedesSociais) {
        this.valorRedesSociais = _valorRedesSociais;
    }

    function getValorRedesSociais() {
        return this.valorRedesSociais;
    }

    var passaporte;

    this.setPassaporte = setPassaporte;
    this.getPassaporte = getPassaporte;

    function setPassaporte(_passaporte) {
        this.passaporte = _passaporte;
    }

    function getPassaporte() {
        return this.passaporte;
    }

    var valorPassaporte;


    this.setValorPassaporte = setValorPassaporte;
    this.getValorPassaporte = getValorPassaporte;

    function setValorPassaporte(_valorPasssaporte) {
        this.valorPassaporte = _valorPasssaporte;
    }

    function getValorPassaporte() {
        return this.valorPassaporte;
    }

    var passaporteAvulso;

    this.setPassaporteAvulso = setPassaporteAvulso;
    this.getPassaporteAvulso = getPassaporteAvulso;

    function setPassaporteAvulso(_passaporteAvulso) {
        this.passaporteAvulso = _passaporteAvulso;
    }

    function getPassaporteAvulso() {
        return this.passaporteAvulso;
    }

    var valorPassaporteAvulso;

    this.setValorPassaporteAvulso = setValorPassaporteAvulso;
    this.getValorPassaporteAvulso = getValorPassaporteAvulso;

    function setValorPassaporteAvulso(_valorPassaporteAvulso) {
        this.valorPassaporteAvulso = _valorPassaporteAvulso;
    }

    function getValorPassaporteAvulso() {
        return this.valorPassaporteAvulso;
    }

    var responsavel;

    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }

    var total;

    this.setTotal = setTotal;
    this.getTotal = getTotal;

    function setTotal(_total) {
        this.total = _total;
    }
    function getTotal() {
        return this.total;
    }

    var observacao;

    this.setObservacao = setObservacao;
    this.getObservacao = getObservacao;

    function setObservacao(_observacao) {
        this.observacao = _observacao;
    }

    function getObservacao() {
        return this.observacao;
    }
    var id;

    this.getId = getId;
    this.setId = setId;


    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var limiteLinhasPlano;

    this.setLimiteLinhasPlano = setLimiteLinhasPlano;
    this.getLimiteLinhasPlano = getLimiteLinhasPlano;

    function setLimiteLinhasPlano(_limiteLinhasPlano) {
        this.limiteLinhasPlano = _limiteLinhasPlano;
    }

    function getLimiteLinhasPlano() {
        return this.limiteLinhasPlano;
    }

    var qtdLinhasBaseCliente;

    this.setQtdLinhasBaseCliente = setQtdLinhasBaseCliente;
    this.getQtdLinhasBaseCliente = getQtdLinhasBaseCliente;

    function setQtdLinhasBaseCliente(_qtdLinhasBaseCliente) {
        this.qtdLinhasBaseCliente = _qtdLinhasBaseCliente;
    }

    function getQtdLinhasBaseCliente() {
        return this.qtdLinhasBaseCliente;
    }

    var qtdLinhasUteis;

    this.setQtdLinhasUteis = setQtdLinhasUteis;
    this.getQtdLinhasUteis = getQtdLinhasUteis;

    function setQtdLinhasUteis(_qtdLinhasUteis) {
        this.qtdLinhasUteis = _qtdLinhasUteis;
    }

    function getQtdLinhasUteis() {
        return this.qtdLinhasUteis;
    }

    var qtdSmartphones;

    this.setQtdSmartphones = setQtdSmartphones;
    this.getQtdSmartphones = getQtdSmartphones;

    function setQtdSmartphones(_qtdSmartphones) {
        this.qtdSmartphones = _qtdSmartphones;
    }

    function getQtdSmartphones() {
        return this.qtdSmartphones;
    }


    var qtdTabletModem;

    this.setQtdTabletModem = setQtdTabletModem;
    this.getQtdTabletModem = getQtdTabletModem;

    function setQtdTabletModem(_qtdTabletModem) {
        this.qtdTabletModem = _qtdTabletModem;
    }

    function getQtdTabletModem() {
        return this.qtdTabletModem;
    }

    var qtdCelulares;

    this.setQtdCelulares = setQtdCelulares;
    this.getQtdCelulares = getQtdCelulares;

    function setQtdCelulares(_qtdCelulares) {
        this.qtdCelulares = _qtdCelulares;
    }

    function getQtdCelulares() {
        return this.qtdCelulares;
    }

    var qtdVozCelular;

    this.setQtdVozCelular = setQtdVozCelular;
    this.getQtdVozCelular = getQtdVozCelular;

    function setQtdVozCelular(_qtdVozCelular) {
        this.qtdVozCelular = _qtdVozCelular;
    }

    function getQtdVozCelular() {
        return this.qtdVozCelular;
    }

    var tradeIn;

    this.setTradeIn = setTradeIn;
    this.getTradeIn = getTradeIn;

    function setTradeIn(_tradeIn) {
        this.tradeIn = _tradeIn;
    }

    function getTradeIn() {
        return this.tradeIn;
    }

    var sms;

    this.setSms = setSms;
    this.getSms = getSms;

    function setSms(_sms) {
        this.sms = _sms;
    }

    function getSms() {
        return this.sms;
    }

    var valorSms;

    this.setValorSms = setValorSms;
    this.getValorSms = getValorSms;

    function setValorSms(_valorSms) {
        this.valorSms = _valorSms;
    }

    function getValorSms() {
        return this.valorSms;
    }

    var qtdSms;

    this.setQtdSms = setQtdSms;
    this.getQtdSms = getQtdSms;

    function setQtdSms(_qtdSms) {
        this.qtdSms = _qtdSms;
    }

    function getQtdSms() {
        return this.qtdSms;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

    var tipoPlanoFranquia;

    this.setTipoPlanoFranquia = setTipoPlanoFranquia;
    this.getTipoPlanoFranquia = getTipoPlanoFranquia;

    function setTipoPlanoFranquia(_tipoPlanoFranquia) {
        this.tipoPlanoFranquia = _tipoPlanoFranquia;
    }

    function getTipoPlanoFranquia() {
        return this.tipoPlanoFranquia;
    }
}