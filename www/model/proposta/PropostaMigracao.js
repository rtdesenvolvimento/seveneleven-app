function PropostaMigracao() {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var id;
    this.getId = getId;
    this.setId = setId;
    function getId(){return id;}
    function setId(_id) {id = _id;}

    var proposta;
    this.setProposta = setProposta;
    this.getProposta = getProposta;
    function setProposta(_proposta) {this.proposta = _proposta;}
    function getProposta() {return this.proposta;}

    var telefones;
    this.setTelefones = setTelefones;
    this.getTelefones = getTelefones;
    function setTelefones(_telefones) {this.telefones = _telefones;}
    function getTelefones() {return this.telefones;}

    var nomeAssinanteDoador;

    this.setNomeAssinanteDoador = setNomeAssinanteDoador;
    this.getNomeAssinanteDoador = getNomeAssinanteDoador;

    function setNomeAssinanteDoador(_nomeAssinanteDoador) {
        this.nomeAssinanteDoador = _nomeAssinanteDoador;
    }

    function getNomeAssinanteDoador() {
        return this.nomeAssinanteDoador;
    }

    var cpf;

    this.setCpf = setCpf;
    this.getCpf = getCpf;

    function setCpf(_cpf) {
        this.cpf = _cpf;
    }

    function getCpf() {
        return this.cpf;
    }

    var telefoneContato;

    this.setTelefoneContato = setTelefoneContato;
    this.getTelefoneContato = getTelefoneContato;

    function setTelefoneContato(_telefoneContato) {
        this.telefoneContato = _telefoneContato;
    }

    function getTelefoneContato() {
        return this.telefoneContato;
    }


    var endereco;

    this.setEndereco = setEndereco;
    this.getEndereco = getEndereco;

    function setEndereco(_endereco) {
        this.endereco = _endereco;
    }

    function getEndereco() {
        return this.endereco;
    }

    var cep;

    this.setCep = setCep;
    this.getCep = getCep;

    function setCep(_cep) {
        this.cep = _cep;
    }

    function getCep() {
        return this.cep;
    }

    var cidade;

    this.setCidade = setCidade;
    this.getCidade = getCidade;

    function setCidade(_cidade) {
        this.cidade = _cidade;
    }

    function getCidade() {
        return this.cidade;
    }

    var estado;

    this.setEstado = setEstado;
    this.getEstado = getEstado;

    function setEstado(_estado) {
        this.estado = _estado;
    }

    function getEstado() {
        return this.estado;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

}