function PropostaPortabilidade() {

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    var proposta;

    this.setProposta = setProposta;
    this.getProposta = getProposta;

    function setProposta(_proposta) {
        this.proposta = _proposta;
    }

    function getProposta() {
        return this.proposta;
    }

    var ddd;

    this.setDdd = setDdd;
    this.getDdd = getDdd;

    function setDdd(_ddd) {
        this.ddd = _ddd;
    }

    function getDdd() {
        return this.ddd;
    }

    var plano;

    this.setPlano = setPlano;
    this.getPlano = getPlano;

    function setPlano(_plano) {
        this.plano = _plano;
    }

    function getPlano() {
        return this.plano;
    }

    var operadora;

    this.setOperadora = setOperadora;
    this.getOperadora = getOperadora;

    function setOperadora(_operadora) {
        this.operadora = _operadora;
    }

    function getOperadora() {
        return this.operadora;
    }

    var simcard;

    this.setSimcard = setSimcard;
    this.getSimcard = getSimcard;

    function setSimcard(_simcard) {
        this.simcard = _simcard;
    }

    function getSimcard() {
        return this.simcard;
    }

    var telefones;

    this.setTelefones = setTelefones;
    this.getTelefones = getTelefones;

    function setTelefones(_telefones) {
        this.telefones = _telefones;
    }

    function getTelefones() {
        return this.telefones;
    }

    var id;

    this.getId = getId;
    this.setId = setId;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}