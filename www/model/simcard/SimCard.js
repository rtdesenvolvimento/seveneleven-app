function SimCard() {

    var id;
    var descricao;
    var valor;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.setValor = setValor;
    this.getValor = getValor;

    function setValor(_valor) {
        this.valor = _valor;
    }

    function getValor() {
        return this.valor;
    }

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}