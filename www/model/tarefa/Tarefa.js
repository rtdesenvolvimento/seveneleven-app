function Tarefa() {

    var id;
    var descricao;
    var responsavel;
    var tipoTarefa;
    var atribuir;
    var status;
    var titulo;
    var data;
    var horaInicio;
    var horaFinal;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;
    this.setResponsavel = setResponsavel;
    this.getResponsavel = getResponsavel;
    this.setTipoTarefa = setTipoTarefa;
    this.getTipoTarefa = getTipoTarefa;
    this.setAtribuir = setAtribuir;
    this.getAtribuir = getAtribuir
    this.setStatus = setStatus;
    this.getStatus = getStatus;
    this.setTitulo = setTitulo;
    this.getTitulo = getTitulo;
    this.setData = setData;
    this.getData = getData;

    this.setHoraInicio = setHoraInicio;
    this.getHoraInicio = getHoraInicio;
    this.setHoraFinal = setHoraFinal;
    this.getHoraFinal = getHoraFinal;

    function getId() {
        return this.id;
    }

    function setId(_id) {
        this.id = _id;
    }

    function setDescricao(_descricao) {
        this.descricao = _descricao;
    }

    function getDescricao() {
        return this.descricao;
    }

    function setResponsavel(_responsavel) {
        this.responsavel = _responsavel;
    }

    function getResponsavel() {
        return this.responsavel;
    }

    function setTipoTarefa(_tipoTarefa) {
        this.tipoTarefa = _tipoTarefa
    }

    function getTipoTarefa() {
        return this.tipoTarefa;
    }

    function setAtribuir(_setAtribuir) {
        this.atribuir = _setAtribuir
    }

    function getAtribuir() {
        return this.atribuir;
    }

    function setStatus(_status) {
        this.status = _status;
    }

    function getStatus() {
        return this.status;
    }

    function setTitulo(_titulo) {
        this.titulo = _titulo;
    }

    function getTitulo() {
        return this.titulo;
    }

    function setData(_data) {
        this.data = _data;
    }

    function getData() {
        return this.data;
    }

    function setHoraInicio(_horaInicio) {
        this.horaInicio = _horaInicio;
    }

    function getHoraInicio() {
        return this.horaInicio;
    }

    function setHoraFinal(_horaFinal) {
        this.horaFinal = _horaFinal;
    }

    function getHoraFinal() {
        return this.horaFinal;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}

}