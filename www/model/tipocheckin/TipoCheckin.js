function TipoCheckin() {

    var id;
    var descricao;
    var tempoMedio;
    var isUsarObservacao;
    var isUsarGPS;
    var inativo;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}

    this.setTempoMedio = setTempoMedio;
    this.getTempoMedio = getTempoMedio;

    function setTempoMedio(_tempomedio) {
        this.tempoMedio = _tempomedio;
    }

    function getTempoMedio() {
        return this.tempoMedio;
    }

    this.setIsUsarObservacao = setIsUsarObservacao;
    this.getIsUsarObservacao = getIsUsarObservacao;

    function setIsUsarObservacao(_isUsarObservacao) {
        this.isUsarObservacao = _isUsarObservacao;
    }

    function getIsUsarObservacao() {
        return this.isUsarObservacao;
    }

    this.setIsUsarGPS = setIsUsarGPS;
    this.getIsUsarGPS = getIsUsarGPS;

    function setIsUsarGPS(_isUsarGPS) {
        this.isUsarGPS = _isUsarGPS;
    }

    function getIsUsarGPS() {
        return this.isUsarGPS;
    }

    this.setInativo = setInativo;
    this.getInativo = getInativo;

    function setInativo(_inativo) {
        this.inativo = _inativo;
    }

    function getInativo() {
        return this.inativo;
    }

    this.getId = getId;
    this.setId = setId;

    this.setDescricao = setDescricao;
    this.getDescricao = getDescricao;

    function getId(){
        return id;
    }

    function setId(_id) {
        id = _id;
    }

    function getDescricao() {
        return descricao;
    }

    function setDescricao(_descricao) {
        descricao = _descricao;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}