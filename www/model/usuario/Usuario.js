function Usuario() {

    var id;
    var nomeCompleto;
    var cargo;
    var email;
    var celular;
    var login;
    var password;
    var ativo;
    var foto;

    this.setId = setId;
    this.setNomeCompleto = setNomeCompleto;
    this.setCargo = setCargo;
    this.setEmail = setEmail;
    this.setCelular = setCelular;
    this.setLogin = setLogin;
    this.setPassword = setPassword;
    this.setAtivo = setAtivo;

    this.getId = getId;
    this.getNomeCompleto = getNomeCompleto;
    this.getCargo = getCargo;
    this.getEmail = getEmail;
    this.getCelular = getCelular;
    this.getLogin = getLogin;
    this.getPassword = getPassword;
    this.getAtivo = getAtivo;
    this.setFoto = setFoto;
    this.getFoto = getFoto;

    var idapp;
    this.setIdapp = setIdapp;
    this.getIdapp = getIdapp;
    function setIdapp(_idapp) {this.idapp = _idapp;}
    function getIdapp() {return this.idapp;}

    var telefoneUsuario;
    this.setTelefoneUsuario = setTelefoneUsuario;
    this.getTelefoneUsuario = getTelefoneUsuario;
    function setTelefoneUsuario(_telefoneUsuario) {this.telefoneUsuario = _telefoneUsuario;}
    function getTelefoneUsuario() { return this.telefoneUsuario;}


    function setId(_id) {
        this.id = _id;
    }
    function setNomeCompleto(_nomeCompleto) {
        this.nomeCompleto = _nomeCompleto;
    }
    function setCargo(_cargo) {
        this.cargo = _cargo;
    }
    function setEmail(_email) {
        this.email = _email;
    }
    function setCelular(_celular) {
        _celular = _celular.replace('(','');
        _celular = _celular.replace(')','');
        _celular = _celular.replace('-','');
        _celular = _celular.replace(' ','');
        this.celular = _celular;
    }
    function setLogin(_login) {
        this.login = _login;
    }
    function setPassword(_password) {
        this.password = _password;
    }
    function setAtivo(_ativo) {
        this.ativo = _ativo;
    }

    /*GET*/
    function getId() {
        return this.id;
    }
    function getNomeCompleto() {
        return this.nomeCompleto;
    }
    function getCargo() {
        return this.cargo;
    }
    function getEmail() {
        return this.email;
    }
    function getCelular() {
        return this.celular;
    }
    function getLogin() {
        return this.login;
    }
    function getPassword() {
        return this.password;
    }
    function getAtivo() {
        return this.ativo;
    }

    function setFoto(_foto) {
        this.foto = _foto;
    }

    function getFoto() {
        return this.foto;
    }

    var sincronizacao;

    this.setSincronizacao = setSincronizacao;
    this.getSincronizacao = getSincronizacao;

    function setSincronizacao(_sincronizacao) {this.sincronizacao = _sincronizacao;}
    function getSincronizacao() {return this.sincronizacao;}
}