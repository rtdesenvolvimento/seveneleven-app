<!DOCTYPE html>

<div data-role="page" data-theme="a" data-url="relatorioFluxoPedido" id="relatorioFluxoPedido">

    <!---------------->
    <!------MENU------>
    <!---------------->
    <div data-role="panel" id="menu" class="exibirMenu"></div>

    <div data-role="header" data-position="fixed">
        <a href="#menu" data-icon="bars" data-display="overlay" data-iconpos="notext">Menu</a>
        <h2 style="font-weight: bold;">STATUS<br/><span style="font-size: 10px;">PEDIDOS</span></h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>

    <div role="main" class="ui-content">
        <li class="ui-field-contain">
            <div data-role="collapsibleset" data-theme="a"  data-collapsed-icon="search" data-content-theme="a">
                <div data-role="collapsible">
                    <h3>FILTROS</h3>
                    <ul data-role="listview" data-inset="true">
                        <li class="ui-field-contain">
                            <label for="status" class="select">Status</label>
                            <select name="filter_status" id="filter_status" required="required">
                                <option value="TODOS">TODOS</option>
                                <option value="AGUARDANDO DOCUMENTOS">AGUARDANDO DOCUMENTOS</option>
                                <option value="AGUARDANDO ASSINATURA">AGUARDANDO ASSINATURA</option>
                                <option value="FALTA DE EQUIPAMENTOS">FALTA DE EQUIPAMENTOS</option>
                                <option value="VALIDACAO PENDENTE">VALIDAÇÃO PENDENTE</option>
                                <option value="ANALISE DE CREDITO">ANALISE DE CRÉDITO</option>
                                <option value="REANALIZE DE CREDITO">REANALIZE DE CRÉDITO</option>
                                <option value="PEDIDO NEGADO">PEDIDO NEGADO</option>
                                <option value="TROCA DE CARTEIRA">TROCA DE CARTEIRA</option>
                                <option value="PRD">PRD</option>
                                <option value="ROTA DE ENTREGA">ROTA DE ENTREGA</option>
                                <option value="PORTABILIDADE AGENDADA">PORTABILIDADE AGENDADA</option>
                                <option value="SOLICITACAO DE DOCUMENTACAO COMPLEMENTAR">SOLICITAÇÃO DE DOCUMENTACAO COMPLEMENTAR</option>
                                <option value="PEDIDO ATIVO">PEDIDO ATIVO</option>
                            </select>
                        </li>
                        <li class="ui-body ui-body-a">
                            <fieldset class="ui-grid-a">
                                <button type="button" id="bntFiltroFluxoPedido" class="ui-btn ui-corner-all ui-btn-a">CONSULTAR</button>
                            </fieldset>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <table data-role="table" id="tbFluxoPedido" data-mode="columntoggle" class="ui-body-d ui-shadow table-stripe ui-responsive" data-column-btn-theme="b">
            <thead>
            <tr class="ui-bar-d">
                <th>Nº</th>
                <th>Dados</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

</div>